REM use this to export beginning/ending sequence
REM param 1 = input png; param 2 = total card duration (seconds); param 3 = fade in duration (seconds); param 4 = fade out duration (seconds) param 5 = output file name

set /a numFrames = %2*30
set /a fadeInDuration = %3 * 30
set /a fadeOutDuration = %4 * 30
set /a startFade = %numFrames% - %fadeOutDuration%

REM first create a video from the source png with a fade in/ fade out
ffmpeg -y  -loop 1 -i  %1 -frames:v %numFrames% -vf "fade=in:0:%fadeInDuration%,fade=out:%startFade%:%fadeOutDuration%" -shortest -pix_fmt yuv420p soundlessOverlay.mp4

REM then add an empty audio channel so we can concat with the actual video later
ffmpeg -y -f lavfi -i anullsrc=channel_layout=stereo:sample_rate=44100 -i soundlessOverlay.mp4 -shortest -c:v copy -c:a aac %5



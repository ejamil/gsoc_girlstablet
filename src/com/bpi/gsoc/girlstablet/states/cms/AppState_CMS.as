package com.bpi.gsoc.girlstablet.states.cms
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.citrusas.citrus.framework.layers.LayerIDs;
	import com.bpi.citrusas.input.singleinput.Input;
	import com.bpi.citrusas.states.AppState;
	import com.bpi.citrusas.states.StateControllerManager;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_IDs;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_Main;
	
	import flash.ui.Keyboard;

	public class AppState_CMS extends AppState
	{
		private var m_App:App_CMS;
		
		public function AppState_CMS()
		{
			super(StateController_StateIDs_Main.STATE_CMS);
			
			m_App = new App_CMS();
		}
		
		public override function Initialize():void
		{
			m_App.InitializeEventListeners();
			m_App.Initialize();
			
			super.Initialize();
		}
		
		public override function CleanUp():void
		{
			m_App.CleanUp();
			
			super.CleanUp();
		}
		
		public override function Update():void
		{
			checkMoveToConnectionField();
			
			m_App.Update();
			
			super.Update();
		}
		
		protected override function Init():void
		{
			m_App.Init();
			
			CitrusFramework.layerController.GetLayerByID(LayerIDs.CONTENT).addChild(m_App);
			
			super.Init();
		}
		
		protected override function DeInit():void
		{
			m_App.DeInit();
			
			CitrusFramework.layerController.GetLayerByID(LayerIDs.CONTENT).removeChild(m_App);
			
			super.DeInit();
		}
		
		public override function QueuedUpdate():void
		{
			
		}
		
		private function checkMoveToConnectionField():void
		{
			if(Input.IsKeyPressed(Keyboard.N) && Input.IsKeyDown(Keyboard.CONTROL))
			{
				StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_MAIN, StateController_StateIDs_Main.STATE_NETWORKCONNECTION, true);
			}
		}
	}
}
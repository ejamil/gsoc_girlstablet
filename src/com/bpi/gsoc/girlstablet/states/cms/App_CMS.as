package com.bpi.gsoc.girlstablet.states.cms
{
	import com.bpi.citrusas.citrus.controllers.cms.events.Event_CMS;
	import com.bpi.citrusas.citrus.controllers.user.UserDataMethods;
	import com.bpi.citrusas.citrus.controllers.user.events.Event_UserDataController;
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.citrusas.images.AnimationClip;
	import com.bpi.citrusas.states.StateControllerManager;
	import com.bpi.gsoc.components.girlstablet.general.CMS_Background;
	import com.bpi.gsoc.framework.constants.GSOC_BPINet_Constants;
	import com.bpi.gsoc.framework.states.general.servercommunication.ServerCommunication_PopUp;
	import com.bpi.gsoc.framework.states.general.servercommunication.events.Event_ServerCommunication_PopUp;
	import com.bpi.gsoc.framework.states.general.servercommunication.states.ServerCommunication_PopUp_State_IDs;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_IDs;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_Main;
	
	import flash.display.Sprite;
	import flash.geom.Point;

	public class App_CMS extends Sprite
	{
		private var m_Background:AnimationClip;
		
		private var m_CMS_PopUp:ServerCommunication_PopUp;
		private var m_CMS_PopUp_Position:Point = new Point(1071, 615);
		
		public function App_CMS()
		{
			m_Background = new AnimationClip(CMS_Background);
			
			m_CMS_PopUp = new ServerCommunication_PopUp();
		}
		
		public function InitializeEventListeners():void
		{
			m_CMS_PopUp.InitializeEventListeners();
			
			ServerCommunication_PopUp.E_EventDispatcher.addEventListener(Event_ServerCommunication_PopUp.EVENT_RETRY_CMS, eh_Retry);
		}
		
		public function Initialize():void
		{	
			m_CMS_PopUp.Initialize(ServerCommunication_PopUp_State_IDs.STATE_UPDATE_CMS);
			m_CMS_PopUp.SetPosition(m_CMS_PopUp_Position);
			
			this.addChild(m_Background);
			this.addChild(m_CMS_PopUp);
		}
		
		public function CleanUp():void
		{
		}
		
		public function Init():void
		{
			m_CMS_PopUp.Init();
			
			addEventListeners();
			login();
		}
		
		public function DeInit():void
		{
			m_CMS_PopUp.DeInit();
			
			removeEventListeners();
		}
		
		public function Update():void
		{
			m_CMS_PopUp.Update();
		}
		
		public function QueuedUpdate():void
		{
			
		}
		
		public function Resume():void
		{
			
		}
		
		public function Pause():void
		{
			
		}
		
		private function eh_Retry(evt:Event_ServerCommunication_PopUp):void
		{
			login();
		}
		
		private function eh_CMSLoginComplete(event:Event_UserDataController):void
		{
			var username:String = event.eventData["username"];
			var authorizationToken:String = event.eventData["returnedData"]["token"];
			
			CitrusFramework.cmsController.authorizationToken = authorizationToken;
			CitrusFramework.cmsController.exhibitUsernames = new <String> [username];
			
			CitrusFramework.cmsController.RunCMS();
			CitrusFramework.cmsController.addEventListener(Event_CMS.CMS_DATA_RECEIVED, eh_CMSComplete);
			CitrusFramework.cmsController.addEventListener(Event_CMS.CMS_DATA_RECEIVED_FAILED, eh_CMSFailed);
		}
		
		private function eh_CMSComplete(event:Event_CMS):void
		{
			if(isCMSActive())
			{
				moveToAttract()
			}
		}
		
		private function eh_CMSLoginFailed(event:Event_UserDataController):void
		{
			trace("CMS couldn't log in!");
			m_CMS_PopUp.MoveToState(ServerCommunication_PopUp_State_IDs.STATE_FAILED_CMS);
		}
		
		private function eh_CMSFailed(event:Event_CMS):void
		{
			trace("CMS failed");
			m_CMS_PopUp.MoveToState(ServerCommunication_PopUp_State_IDs.STATE_FAILED_CMS);
		}
		
		private function addEventListeners():void
		{
			UserDataMethods.userDataMethodEventDispatcher.addEventListener(Event_UserDataController.USER_TOKEN_RETRIEVED_COMPLETE, eh_CMSLoginComplete);
			UserDataMethods.userDataMethodEventDispatcher.addEventListener(Event_UserDataController.USER_TOKEN_RETRIEVED_ERROR, eh_CMSLoginFailed);
		}
		
		private function removeEventListeners():void
		{ 
			if(UserDataMethods.userDataMethodEventDispatcher.hasEventListener(Event_UserDataController.USER_TOKEN_RETRIEVED_COMPLETE))
			{
				UserDataMethods.userDataMethodEventDispatcher.removeEventListener(Event_UserDataController.USER_TOKEN_RETRIEVED_COMPLETE, eh_CMSLoginComplete);
			}
			
			if(UserDataMethods.userDataMethodEventDispatcher.hasEventListener(Event_UserDataController.USER_TOKEN_RETRIEVED_ERROR))
			{
				UserDataMethods.userDataMethodEventDispatcher.removeEventListener(Event_UserDataController.USER_TOKEN_RETRIEVED_ERROR, eh_CMSLoginFailed);
			}
			
			if(CitrusFramework.cmsController.hasEventListener(Event_CMS.CMS_DATA_RECEIVED))
			{
				CitrusFramework.cmsController.removeEventListener(Event_CMS.CMS_DATA_RECEIVED, eh_CMSComplete);
				CitrusFramework.cmsController.removeEventListener(Event_CMS.CMS_DATA_RECEIVED_FAILED, eh_CMSFailed);
			}
		}
		
		private function login():void
		{
			m_CMS_PopUp.MoveToState(ServerCommunication_PopUp_State_IDs.STATE_UPDATE_CMS);
			UserDataMethods.GetUserAuthorizationToken(CitrusFramework.serverURL, GSOC_BPINet_Constants.USERNAME_SCOUT_TABLET, GSOC_BPINet_Constants.PASSWORD);
		}
		
		private function isCMSActive():Boolean
		{
			var isStateControllerMainActive:Boolean = StateControllerManager.IsStateControllerActive(StateController_IDs.STATECONTROLLER_MAIN);
			var activeAppStateID:int = StateControllerManager.GetActiveAppStateID(StateController_IDs.STATECONTROLLER_MAIN);
			var isStateCMSActive:Boolean = activeAppStateID == StateController_StateIDs_Main.STATE_CMS;
			return isStateControllerMainActive && isStateCMSActive;
		}
		
		private function moveToAttract():void
		{
			StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_MAIN, StateController_StateIDs_Main.STATE_ATTRACT);
		}
	}
}
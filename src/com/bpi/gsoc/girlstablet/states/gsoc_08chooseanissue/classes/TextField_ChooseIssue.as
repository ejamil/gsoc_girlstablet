package com.bpi.gsoc.girlstablet.states.gsoc_08chooseanissue.classes
{
	import com.bpi.gsoc.girlstablet.states.gsoc_01pictureyourself.name_selection.classes.TextField_NameEntry;
	
	import flash.display.DisplayObjectContainer;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;

	public class TextField_ChooseIssue extends Sprite
	{
		public var asset:MovieClip;
		private var originalPosition:Point;
		private var theX:int;
		private var theY:int;
		public var theFont:String;
		private var saveText:String = "";
		public var isHash:Boolean = false;
		
		public static const TEXT_ENTERED:String = "TEXT_ENTERED";
		public static var TextEntryDispatcher:EventDispatcher = new EventDispatcher();
		public static const ABBREVIATE_TEXT:String = "Abbr. your issue as a hashtag";
		
		public function TextField_ChooseIssue(skin:MovieClip, context:DisplayObjectContainer, font:String, tx:int, ty:int, h:Boolean = false)
		{			
			this.addChild(skin);
			context.addChild(this);
			theFont = font;
			theX = tx;
			theY = ty;
			isHash = h;
			
			this.asset = skin;
			if(isHash)
			{
				this.asset.txt.text = ABBREVIATE_TEXT;
			}
			saveText = this.asset.txt.text;
			
			this.originalPosition = new Point();
			this.originalPosition.y = this.asset.txt.y;
			this.originalPosition.x = this.asset.txt.x; 
			var tf:TextFormat = new TextFormat(font, (this.asset.txt as TextField).defaultTextFormat.size, (this.asset.txt as TextField).defaultTextFormat.color, false);
			
			(this.asset.txt as TextField).defaultTextFormat = tf;
			(this.asset.txt as TextField).embedFonts = true;
			(this.asset.txt as TextField).setTextFormat(tf);
			(this.asset.txt as TextField).autoSize = TextFieldAutoSize.CENTER;
			
			this.asset.x = theX;
			this.asset.y = theY;
			
			(this.asset.txt as TextField).addEventListener(Event.CHANGE, eh_ToUpper, false, 0, true);
		}
		
		public function changeColor(i:int):void
		{
			var tf:TextFormat;
			
			switch (i)
			{
				case 0:
					tf = new TextFormat(theFont, (this.asset.txt as TextField).defaultTextFormat.size, (this.asset.txt as TextField).defaultTextFormat.color, false);
					break;
				case 1:
					tf = new TextFormat(theFont, (this.asset.txt as TextField).defaultTextFormat.size, (this.asset.txt as TextField).defaultTextFormat.color, false);
					break;
			}
			
			(this.asset.txt as TextField).defaultTextFormat = tf;
			(this.asset.txt as TextField).embedFonts = true;
			(this.asset.txt as TextField).setTextFormat(tf);
			(this.asset.txt as TextField).autoSize = TextFieldAutoSize.CENTER;
		}
		
		public function setText(s:String):void
		{
			this.asset.txt.text = s;
		}
		
		public function resetText():void
		{
			if (this.asset.txt.text == "")
			{
				this.asset.txt.text = saveText;
			}
			if (isHash == true)
			{
				if (this.asset.txt.text == "#")
				{
					this.asset.txt.text = ABBREVIATE_TEXT;
				}
			}
		}
		
		public function clearField():void
		{
			if (this.asset.txt.text == saveText)
			{
				this.asset.txt.text = "";
				if (isHash == true)
				{
					this.asset.txt.text = "#";
					this.asset.txt.setSelection(1, 1);
				}
				else
				{
					
					this.asset.txt.setSelection(0, 1);
				}
				this.asset.txt.scaleX = this.asset.txt.scaleY = 1;
			}
		}
		
		public function receiveChar(e:KeyboardEvent):void
		{
			this.asset.txt.text = (this.asset.txt.text as String).toLocaleUpperCase();
			if(this.isHash == true)
			{
				if (this.asset.txt.text.charAt(0) != "#")
				{
					this.asset.txt.text = "#" + this.asset.txt.text;
				}
			}
		}
		
		public function caps():void
		{
			this.asset.txt.text = (this.asset.txt.text as String).toLocaleUpperCase();
		}
		
		public function backspace():void
		{
			var txt:String = this.asset.txt.text;
			txt = txt.substr(0, txt.length - 1);
			if (isHash == true && txt.length < 1)
			{
				txt = "#";
			}
			
			this.asset.txt.text = txt;
			this.asset.txt.scaleX = 1;
			this.asset.txt.scaleY = 1;
			this.scaleText();
		}
		
		private function scaleText():void
		{
			while((this.asset.txt as TextField).textWidth * this.asset.txt.scaleX > 720)
			{
				this.asset.txt.scaleX -= 0.01;
				this.asset.txt.scaleY = this.asset.txt.scaleX;
			}
			
			if(this.asset.txt.scaleX < 1)
			{
				this.asset.txt.x = this.originalPosition.x + ((1 - this.asset.txt.scaleX)/0.03);
				this.asset.txt.y = this.originalPosition.y + ((1 - this.asset.txt.scaleX)/0.03);
			}	
		}
		
		public function getTextFieldLength():Number
		{
			return this.asset.txt.text.length;
		}
		
		public function getText():String
		{
			return this.asset.txt.text;
		}
		
		public function isShown():Boolean
		{
			return this.asset.visible;
		}
		
		public function hide():void
		{
			this.asset.txt.text = "";
			this.asset.visible = false;
		}
		
		public function show():void
		{
			this.asset.visible = true;
		}
		
		public function activate(e:Event = null):void
		{
			this.asset.gotoAndStop(2);
		}
		
		public function deactivate():void
		{
			this.asset.gotoAndStop(1);
		}
		
		public function enableMouseClick():void
		{
			this.addEventListener(MouseEvent.CLICK, this.activate);
		}
		
		public function disableMouseClick():void
		{
			this.removeEventListener(MouseEvent.CLICK, this.activate);
		}
		
		private function eh_ToUpper(e:Event):void
		{
			if (isHash == true && e.target.text.length < 1)
			{
				e.target.text = "#";
				e.target.setSelection(1, 1);
			}
			if (e.target.text.length >= 20)
			{
				e.target.text = e.target.text.substr(0, e.target.text.length - 1);
			}
			
			e.target.text = e.target.text.toUpperCase();
			TextField_NameEntry.TextEntryDispatcher.dispatchEvent(new Event(TextField_NameEntry.TEXT_ENTERED));
		}
	}
}
package com.bpi.gsoc.girlstablet.states.gsoc_08chooseanissue
{
	import com.bpi.gsoc.components.chooseissue.ChooseIssue_SA_Skin;
	import com.bpi.gsoc.framework.constants.AgeColors;
	import com.bpi.gsoc.framework.constants.AgeFontLinkages;
	import com.bpi.gsoc.framework.constants.TeamNameSizes;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.gsocui.GSOC_Header;
	
	public class ChooseIssue_App_SA extends ChooseIssue_App
	{
		public function ChooseIssue_App_SA()
		{
			m_Skin = new ChooseIssue_SA_Skin();
			m_HeaderFont = AgeFontLinkages.SA_HEADER_FONT;
			m_BodyFont = AgeFontLinkages.SA_BODY_FONT;
			m_BodyBoldFont = AgeFontLinkages.SA_BODYBOLD_FONT;
			m_Glow = AgeColors.SA_YELLOW;
			
			m_GSOCHeader = new GSOC_Header(m_Skin.header.paginationArea);
			GSOCheader.addTeamName(SessionInformation_GirlsTablet.TeamInfo.Team_Name,TeamNameSizes.SA_FONTSIZE);
			GSOCheader.addArrows(m_Skin.header.paginationArea.x, 0);
			
			super.Initialize();
		}
	}
}


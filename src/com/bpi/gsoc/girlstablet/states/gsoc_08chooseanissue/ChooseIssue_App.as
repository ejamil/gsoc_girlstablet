package com.bpi.gsoc.girlstablet.states.gsoc_08chooseanissue
{
	import com.bpi.gsoc.framework.states.app.GSOC_AppSkin;

	public class ChooseIssue_App extends GSOC_AppSkin
	{
		protected var _m_Glow:uint;
		
		public function ChooseIssue_App()
		{
			super(true);
		}
		
		public function get m_Glow():uint
		{
			return _m_Glow;
		}
		
		public function set m_Glow(value:uint):void
		{
			_m_Glow = value;
		}
	}
}
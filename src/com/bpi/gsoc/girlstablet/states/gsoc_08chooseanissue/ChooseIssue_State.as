package com.bpi.gsoc.girlstablet.states.gsoc_08chooseanissue
{
	import com.adobe.utils.StringUtil;
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.citrusas.input.inputcontroller.InputController;
	import com.bpi.citrusas.sound.SoundHandler;
	import com.bpi.citrusas.states.StateControllerManager;
	import com.bpi.citrusas.utils.Utils_Text;
	import com.bpi.citrusas.web.WebView;
	import com.bpi.gsoc.framework.constants.AgeConstants;
	import com.bpi.gsoc.framework.constants.AgeFontLinkages;
	import com.bpi.gsoc.framework.constants.JourneyConstants;
	import com.bpi.gsoc.framework.constants.Keys_FileNames;
	import com.bpi.gsoc.framework.data.sessioninformation.Issue_Data;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.data.userdata.GSOC_UserData_Team_ImageKeys;
	import com.bpi.gsoc.framework.gsocui.GSOC_WebView;
	import com.bpi.gsoc.framework.gsocui.IssueImage;
	import com.bpi.gsoc.framework.gsocui.STEM_Popups;
	import com.bpi.gsoc.framework.gsocui.keyboards.GSOC_Keyboards;
	import com.bpi.gsoc.framework.sfx.GSOC_SFX;
	import com.bpi.gsoc.framework.states.app.GSOC_AppState;
	import com.bpi.gsoc.framework.states.takeaction.Issue;
	import com.bpi.gsoc.framework.states.takeaction.IssueReference;
	import com.bpi.gsoc.girlstablet.global_data.events.Event_UploadDefaultData;
	import com.bpi.gsoc.girlstablet.global_data.upload.UploadManager;
	import com.bpi.gsoc.girlstablet.states.gsoc_08chooseanissue.classes.TextField_ChooseIssue;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_IDs;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_ChooseIssue;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_Main;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_MindMap;
	import com.greensock.TweenMax;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.events.TouchEvent;
	import flash.filters.DropShadowFilter;
	import flash.filters.GlowFilter;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.ui.Keyboard;

	public class ChooseIssue_State extends GSOC_AppState
	{
		private var m_AppState:ChooseIssue_App;
		private var submitY:int;
		private var theIssue:String = "";
		private var addYourOwnFields:Vector.<TextField_ChooseIssue>;
		private var _activeTextField:int;
		private var whichIcon:int = -1;
		private var saLeftIconSelectedIndex:int = -1;
		private var saRightIconSelectedIndex:int = -1;
		private var theGlow:GlowFilter;
		private var allSet:Boolean = false;
		private var activeBox:int;
		private var whatIssueSelected:int;
		
		private var m_WebView:WebView;
		
		public function ChooseIssue_State()
		{
			super(StateController_StateIDs_ChooseIssue.STATE_CHOOSEISSUE);
			m_WebView = new GSOC_WebView();
		}
		
		public override function Initialize():void
		{
			
		}
		
		protected override function Init():void
		{
			this.addYourOwnFields = new Vector.<TextField_ChooseIssue>();
			this._activeTextField = -1;
			
			saLeftIconSelectedIndex = -1;
			saRightIconSelectedIndex = -1;
			
			switch(SessionInformation_GirlsTablet.Age)
			{
				case AgeConstants.KEY_DAISYBROWNIE:
					m_AppState = new ChooseIssue_App_DB();
					parseIssues(0);
					break;
				case AgeConstants.KEY_JUNIORCADETTE:
					m_AppState = new ChooseIssue_App_JC();
					parseIssues(1);
					break;
				case AgeConstants.KEY_SENIORAMBASSADOR:
					m_AppState = new ChooseIssue_App_SA();
					parseIssues(2);
					break;	
			}
			m_AppState.Init();
			
			theGlow = new GlowFilter(this.m_AppState.m_Glow, 0.7, 200, 200, 3);
			
			submitY = m_AppState.skin.submit_Button.y;
			m_AppState.skin.submit_Button.y = submitY + m_AppState.skin.submit_Button.height;
			GSOC_Keyboards.SetKeyboardType(GSOC_Keyboards.KEYBOARDTYPE_UPPERCASE);
			
			switch(SessionInformation_GirlsTablet.Age)
			{
				case AgeConstants.KEY_DAISYBROWNIE:
					
					switch(SessionInformation_GirlsTablet.Journey)
					{
						case JourneyConstants.JOURNEY_PLANET:
							m_AppState.skin.box1.iconBox.gotoAndStop(1);
							m_AppState.skin.box2.iconBox.gotoAndStop(2);
							m_AppState.skin.box3.iconBox.gotoAndStop(3);
							break;
						case JourneyConstants.JOURNEY_STORY:
							m_AppState.skin.box1.iconBox.gotoAndStop(4);
							m_AppState.skin.box2.iconBox.gotoAndStop(5);
							m_AppState.skin.box3.iconBox.gotoAndStop(6);
							break;
						case JourneyConstants.JOURNEY_WORLD:
							m_AppState.skin.box1.iconBox.gotoAndStop(7);
							m_AppState.skin.box2.iconBox.gotoAndStop(8);
							m_AppState.skin.box3.iconBox.gotoAndStop(9);
							break;
					}
					
					
					m_AppState.skin.box1.iconBox.mouseEnabled = false;
					m_AppState.skin.box1.iconBox.mouseChildren = false;
					m_AppState.skin.box1.boarder.mouseEnabled = false;
					m_AppState.skin.box1.boarder.mouseChildren = false;
					m_AppState.skin.box1.txt.mouseEnabled = false;
					m_AppState.skin.box1.addEventListener(InputController.CLICK, chooseBox);
					
					m_AppState.skin.box2.iconBox.mouseEnabled = false;
					m_AppState.skin.box2.iconBox.mouseChildren = false;
					m_AppState.skin.box2.boarder.mouseEnabled = false;
					m_AppState.skin.box2.boarder.mouseChildren = false;
					m_AppState.skin.box2.txt.mouseEnabled = false;
					m_AppState.skin.box2.addEventListener(InputController.CLICK, chooseBox);
					
					m_AppState.skin.box3.iconBox.mouseEnabled = false;
					m_AppState.skin.box3.iconBox.mouseChildren = false;
					m_AppState.skin.box3.boarder.mouseEnabled = false;
					m_AppState.skin.box3.boarder.mouseChildren = false;
					m_AppState.skin.box3.txt.mouseEnabled = false;
					m_AppState.skin.box3.addEventListener(InputController.CLICK, chooseBox);
					
					Utils_Text.ConvertToEmbeddedFont(m_AppState.skin.box1.txt as TextField, m_AppState.bodyFont, true);
					Utils_Text.ConvertToEmbeddedFont(m_AppState.skin.box2.txt as TextField, m_AppState.bodyFont, true);
					Utils_Text.ConvertToEmbeddedFont(m_AppState.skin.box3.txt as TextField, m_AppState.bodyFont, true);
					
					
					m_AppState.skin.box1.txt.filters = [new DropShadowFilter(15, 45, 0, .4, 15, 15)];
					m_AppState.skin.box2.txt.filters = [new DropShadowFilter(15, 45, 0, .4, 15, 15)];
					m_AppState.skin.box3.txt.filters = [new DropShadowFilter(15, 45, 0, .4, 15, 15)];
					
					
					m_AppState.skin.box1.txt.text = IssueReference.GetIssuesByAgeAndJourney(0, SessionInformation_GirlsTablet.Journey)[0].IssueText;
					m_AppState.skin.box2.txt.text = IssueReference.GetIssuesByAgeAndJourney(0, SessionInformation_GirlsTablet.Journey)[1].IssueText;
					m_AppState.skin.box3.txt.text = IssueReference.GetIssuesByAgeAndJourney(0, SessionInformation_GirlsTablet.Journey)[2].IssueText;
					break;
				case AgeConstants.KEY_JUNIORCADETTE:
					switch(SessionInformation_GirlsTablet.Journey)
					{
						case JourneyConstants.JOURNEY_PLANET:
							m_AppState.skin.box1.iconBox.gotoAndStop(1);
							m_AppState.skin.box2.iconBox.gotoAndStop(2);
							m_AppState.skin.box3.iconBox.gotoAndStop(3);
							break;
						case JourneyConstants.JOURNEY_STORY:
							m_AppState.skin.box1.iconBox.gotoAndStop(4);
							m_AppState.skin.box2.iconBox.gotoAndStop(5);
							m_AppState.skin.box3.iconBox.gotoAndStop(6);
							break;
						case JourneyConstants.JOURNEY_WORLD:
							m_AppState.skin.box1.iconBox.gotoAndStop(7);
							m_AppState.skin.box2.iconBox.gotoAndStop(8);
							m_AppState.skin.box3.iconBox.gotoAndStop(9);
							break;
					}
					m_AppState.skin.box1.boarder.gotoAndStop(1);
					m_AppState.skin.box2.boarder.gotoAndStop(2);
					m_AppState.skin.box3.boarder.gotoAndStop(3);
					m_AppState.skin.box1.iconBox.mouseEnabled = false;
					m_AppState.skin.box1.iconBox.mouseChildren = false;
					m_AppState.skin.box1.boarder.mouseEnabled = false;
					m_AppState.skin.box1.boarder.mouseChildren = false;
					m_AppState.skin.box1.txt.mouseEnabled = false;
					m_AppState.skin.box1.nm.mouseEnabled = false;
					m_AppState.skin.box2.iconBox.mouseEnabled = false;
					m_AppState.skin.box2.iconBox.mouseChildren = false;
					m_AppState.skin.box2.boarder.mouseEnabled = false;
					m_AppState.skin.box2.boarder.mouseChildren = false;
					m_AppState.skin.box2.txt.mouseEnabled = false;
					m_AppState.skin.box2.nm.mouseEnabled = false;
					m_AppState.skin.box3.iconBox.mouseEnabled = false;
					m_AppState.skin.box3.iconBox.mouseChildren = false;
					m_AppState.skin.box3.boarder.mouseEnabled = false;
					m_AppState.skin.box3.boarder.mouseChildren = false;
					m_AppState.skin.box3.txt.mouseEnabled = false;
					m_AppState.skin.box3.nm.mouseEnabled = false;
					m_AppState.skin.box1.addEventListener(InputController.CLICK, chooseBox);
					m_AppState.skin.box2.addEventListener(InputController.CLICK, chooseBox);
					m_AppState.skin.box3.addEventListener(InputController.CLICK, chooseBox);
					m_AppState.skin.box1.boarder.mouseEnabled = false;
					m_AppState.skin.box1.boarder.mouseChildren = false;
					m_AppState.skin.box2.boarder.mouseEnabled = false;
					m_AppState.skin.box2.boarder.mouseChildren = false;
					m_AppState.skin.box3.boarder.mouseEnabled = false;
					m_AppState.skin.box3.boarder.mouseChildren = false;
					
					var tfJC:TextFormat = new TextFormat(m_AppState.bodyFont, (m_AppState.skin.box1.txt as TextField).defaultTextFormat.size, (m_AppState.skin.box1.txt as TextField).defaultTextFormat.color, false);
					var tfJC2:TextFormat = new TextFormat(m_AppState.bodyFont, (m_AppState.skin.box1.nm as TextField).defaultTextFormat.size, (m_AppState.skin.box1.nm as TextField).defaultTextFormat.color, false);
					
					var txtFields:Vector.<TextField> = new Vector.<TextField>();
					txtFields.push(m_AppState.skin.box1.txt as TextField, m_AppState.skin.box2.txt as TextField, m_AppState.skin.box3.txt as TextField);
					var nmFields:Vector.<TextField> = new Vector.<TextField>();
					nmFields.push(m_AppState.skin.box1.nm as TextField, m_AppState.skin.box2.nm as TextField, m_AppState.skin.box2.nm as TextField);
					
					
					for (var i:int = 0; i < txtFields.length; i++)
					{
						Utils_Text.ConvertToEmbeddedFont(txtFields[i], m_AppState.bodyFont, true);
						Utils_Text.ConvertToEmbeddedFont(nmFields[i], m_AppState.bodyFont);
					}
					
					//don't comment this out, it sets the text formats.  the above obviously didn't work because I got a bug about it
					(m_AppState.skin.box1.txt as TextField).defaultTextFormat = tfJC;
					(m_AppState.skin.box1.txt as TextField).embedFonts = true;
					(m_AppState.skin.box1.txt as TextField).setTextFormat(tfJC);
					
					(m_AppState.skin.box1.nm as TextField).defaultTextFormat = tfJC2;
					(m_AppState.skin.box1.nm as TextField).embedFonts = true;
					(m_AppState.skin.box1.nm as TextField).setTextFormat(tfJC2);
					
					(m_AppState.skin.box2.txt as TextField).defaultTextFormat = tfJC;
					(m_AppState.skin.box2.txt as TextField).embedFonts = true;
					(m_AppState.skin.box2.txt as TextField).setTextFormat(tfJC);
					
					(m_AppState.skin.box2.nm as TextField).defaultTextFormat = tfJC2;
					(m_AppState.skin.box2.nm as TextField).embedFonts = true;
					(m_AppState.skin.box2.nm as TextField).setTextFormat(tfJC2);
					
					(m_AppState.skin.box3.txt as TextField).defaultTextFormat = tfJC;
					(m_AppState.skin.box3.txt as TextField).embedFonts = true;
					(m_AppState.skin.box3.txt as TextField).setTextFormat(tfJC);
					
					(m_AppState.skin.box3.nm as TextField).defaultTextFormat = tfJC2;
					(m_AppState.skin.box3.nm as TextField).embedFonts = true;
					(m_AppState.skin.box3.nm as TextField).setTextFormat(tfJC2);
					
					
					m_AppState.skin.box1.txt.text = IssueReference.GetIssuesByAgeAndJourney(1, SessionInformation_GirlsTablet.Journey)[0].IssueText;
					m_AppState.skin.box2.txt.text = IssueReference.GetIssuesByAgeAndJourney(1, SessionInformation_GirlsTablet.Journey)[1].IssueText;
					m_AppState.skin.box3.txt.text = IssueReference.GetIssuesByAgeAndJourney(1, SessionInformation_GirlsTablet.Journey)[2].IssueText;
					m_AppState.skin.box1.nm.text = IssueReference.GetIssuesByAgeAndJourney(1, SessionInformation_GirlsTablet.Journey)[0].IssueName;
					m_AppState.skin.box2.nm.text = IssueReference.GetIssuesByAgeAndJourney(1, SessionInformation_GirlsTablet.Journey)[1].IssueName;
					m_AppState.skin.box3.nm.text = IssueReference.GetIssuesByAgeAndJourney(1, SessionInformation_GirlsTablet.Journey)[2].IssueName;
					
					
					break;
				case AgeConstants.KEY_SENIORAMBASSADOR:
					m_AppState.skin.box1.themeTxt.mouseEnabled = false;
					m_AppState.skin.box1.txt.mouseEnabled = false;
					m_AppState.skin.box1.keywordsuggestionsTxt.mouseEnabled = false;
					//m_AppState.skin.box1.socialMediaPost.mouseEnabled = false;
					//m_AppState.skin.box1.hashtagTxt.mouseEnabled = false;
					m_AppState.skin.box2.themeTxt.mouseEnabled = false;
					m_AppState.skin.box2.txt.mouseEnabled = false;
					m_AppState.skin.box2.keywordsuggestionsTxt.mouseEnabled = false;
					//m_AppState.skin.box2.socialMediaPost.mouseEnabled = false;
					//m_AppState.skin.box2.hashtagTxt.mouseEnabled = false;
					m_AppState.skin.box1.addEventListener(InputController.CLICK, chooseBox);
					m_AppState.skin.box2.addEventListener(InputController.CLICK, chooseBox);
				
				case AgeConstants.KEY_JUNIORCADETTE:
					switch(SessionInformation_GirlsTablet.Journey)
					{
						case JourneyConstants.JOURNEY_PLANET:
							m_AppState.skin.box1.icon1.gotoAndStop(1);
							m_AppState.skin.box1.icon2.gotoAndStop(2);
							m_AppState.skin.box1.icon3.gotoAndStop(3);
							m_AppState.skin.box1.icon4.gotoAndStop(4);
							m_AppState.skin.box1.icon5.gotoAndStop(5);
							m_AppState.skin.box1.icon6.gotoAndStop(6);
							
							m_AppState.skin.box2.icon1.gotoAndStop(7);
							m_AppState.skin.box2.icon2.gotoAndStop(8);
							m_AppState.skin.box2.icon3.gotoAndStop(9);
							m_AppState.skin.box2.icon4.gotoAndStop(10);
							m_AppState.skin.box2.icon5.gotoAndStop(11);
							m_AppState.skin.box2.icon6.gotoAndStop(12);
							break;
						case JourneyConstants.JOURNEY_STORY:
							m_AppState.skin.box1.icon1.gotoAndStop(13);
							m_AppState.skin.box1.icon2.gotoAndStop(14);
							m_AppState.skin.box1.icon3.gotoAndStop(15);
							m_AppState.skin.box1.icon4.gotoAndStop(16);
							m_AppState.skin.box1.icon5.gotoAndStop(17);
							m_AppState.skin.box1.icon6.gotoAndStop(18);
							
							m_AppState.skin.box2.icon1.gotoAndStop(19);
							m_AppState.skin.box2.icon2.gotoAndStop(20);
							m_AppState.skin.box2.icon3.gotoAndStop(21);
							m_AppState.skin.box2.icon4.gotoAndStop(22);
							m_AppState.skin.box2.icon5.gotoAndStop(23);
							m_AppState.skin.box2.icon6.gotoAndStop(24);
							break;
						case JourneyConstants.JOURNEY_WORLD:
							m_AppState.skin.box1.icon1.gotoAndStop(25);
							m_AppState.skin.box1.icon2.gotoAndStop(26);
							m_AppState.skin.box1.icon3.gotoAndStop(27);
							m_AppState.skin.box1.icon4.gotoAndStop(28);
							m_AppState.skin.box1.icon5.gotoAndStop(29);
							m_AppState.skin.box1.icon6.gotoAndStop(30);
							
							m_AppState.skin.box2.icon1.gotoAndStop(31);
							m_AppState.skin.box2.icon2.gotoAndStop(32);
							m_AppState.skin.box2.icon3.gotoAndStop(33);
							m_AppState.skin.box2.icon4.gotoAndStop(34);
							m_AppState.skin.box2.icon5.gotoAndStop(35);
							m_AppState.skin.box2.icon6.gotoAndStop(36);
							break;
					}
					
					m_AppState.skin.box1.icon1.addEventListener(InputController.CLICK, chooseIcon1);
					m_AppState.skin.box1.icon2.addEventListener(InputController.CLICK, chooseIcon1);
					m_AppState.skin.box1.icon3.addEventListener(InputController.CLICK, chooseIcon1);
					m_AppState.skin.box1.icon4.addEventListener(InputController.CLICK, chooseIcon1);
					m_AppState.skin.box1.icon5.addEventListener(InputController.CLICK, chooseIcon1);
					m_AppState.skin.box1.icon6.addEventListener(InputController.CLICK, chooseIcon1);
					
					m_AppState.skin.box2.icon1.addEventListener(InputController.CLICK, chooseIcon2);
					m_AppState.skin.box2.icon2.addEventListener(InputController.CLICK, chooseIcon2);
					m_AppState.skin.box2.icon3.addEventListener(InputController.CLICK, chooseIcon2);
					m_AppState.skin.box2.icon4.addEventListener(InputController.CLICK, chooseIcon2);
					m_AppState.skin.box2.icon5.addEventListener(InputController.CLICK, chooseIcon2);
					m_AppState.skin.box2.icon6.addEventListener(InputController.CLICK, chooseIcon2);
					
					var tfSA:TextFormat = new TextFormat(m_AppState.bodyFont, (m_AppState.skin.box1.themeTxt as TextField).defaultTextFormat.size, (m_AppState.skin.box1.themeTxt as TextField).defaultTextFormat.color, false);
					var tfSA2:TextFormat = new TextFormat(m_AppState.bodyBoldFont, (m_AppState.skin.box1.txt as TextField).defaultTextFormat.size, (m_AppState.skin.box1.txt as TextField).defaultTextFormat.color, false);
					var tfSA3:TextFormat = new TextFormat(m_AppState.bodyFont, (m_AppState.skin.box1.keywordsuggestionsTxt as TextField).defaultTextFormat.size, (m_AppState.skin.box1.keywordsuggestionsTxt as TextField).defaultTextFormat.color, false);
					//var tfSA4:TextFormat = new TextFormat(m_AppState.bodyFont, (m_AppState.skin.box1.socialMediaPost.txt as TextField).defaultTextFormat.size, (m_AppState.skin.box1.socialMediaPost.txt as TextField).defaultTextFormat.color, false);
					//var tfSA5:TextFormat = new TextFormat(m_AppState.bodyFont, (m_AppState.skin.box1.hashtag.txt as TextField).defaultTextFormat.size, (m_AppState.skin.box1.hashtag.txt as TextField).defaultTextFormat.color, false);
					
					if(SessionInformation_GirlsTablet.Age == AgeConstants.KEY_SENIORAMBASSADOR) tfSA.font = AgeFontLinkages.SA_HEADER_FONT;
					
					(m_AppState.skin.box1.themeTxt as TextField).defaultTextFormat = tfSA;
					(m_AppState.skin.box1.themeTxt as TextField).embedFonts = true;
					(m_AppState.skin.box1.themeTxt as TextField).setTextFormat(tfSA);
					m_AppState.skin.box1.themeTxt.filters = [new DropShadowFilter(4, 45, 0, .4)];
					
					(m_AppState.skin.box2.themeTxt as TextField).defaultTextFormat = tfSA;
					(m_AppState.skin.box2.themeTxt as TextField).embedFonts = true;
					(m_AppState.skin.box2.themeTxt as TextField).setTextFormat(tfSA);
					m_AppState.skin.box2.themeTxt.filters = [new DropShadowFilter(4, 45, 0, .4)];
					
					(m_AppState.skin.box1.txt as TextField).defaultTextFormat = tfSA2;
					(m_AppState.skin.box1.txt as TextField).embedFonts = true;
					(m_AppState.skin.box1.txt as TextField).setTextFormat(tfSA2);
					
					(m_AppState.skin.box2.txt as TextField).defaultTextFormat = tfSA2;
					(m_AppState.skin.box2.txt as TextField).embedFonts = true;
					(m_AppState.skin.box2.txt as TextField).setTextFormat(tfSA2);
					
					(m_AppState.skin.box1.keywordsuggestionsTxt as TextField).defaultTextFormat = tfSA3;
					(m_AppState.skin.box1.keywordsuggestionsTxt as TextField).embedFonts = true;
					(m_AppState.skin.box1.keywordsuggestionsTxt as TextField).setTextFormat(tfSA3);
					(m_AppState.skin.box1.keywordsuggestionsTxt as TextField).mouseEnabled = false;
					
					(m_AppState.skin.box2.keywordsuggestionsTxt as TextField).defaultTextFormat = tfSA3;
					(m_AppState.skin.box2.keywordsuggestionsTxt as TextField).embedFonts = true;
					(m_AppState.skin.box2.keywordsuggestionsTxt as TextField).setTextFormat(tfSA3);
					(m_AppState.skin.box2.keywordsuggestionsTxt as TextField).mouseEnabled = false;
					
					(m_AppState.skin.box1.keywordHeader as TextField).defaultTextFormat = tfSA3;
					(m_AppState.skin.box1.keywordHeader as TextField).embedFonts = true;
					(m_AppState.skin.box1.keywordHeader as TextField).setTextFormat(tfSA3);
					(m_AppState.skin.box1.keywordHeader as TextField).mouseEnabled = false;
					
					(m_AppState.skin.box2.keywordHeader as TextField).defaultTextFormat = tfSA3;
					(m_AppState.skin.box2.keywordHeader as TextField).embedFonts = true;
					(m_AppState.skin.box2.keywordHeader as TextField).setTextFormat(tfSA3);
					(m_AppState.skin.box2.keywordHeader as TextField).mouseEnabled = false;
					
					
					this.addYourOwnFields.push(
						new TextField_ChooseIssue(m_AppState.skin.box1.socialMediaPost, m_AppState.skin, m_AppState.bodyBoldFont, 233, 1210),
						new TextField_ChooseIssue(m_AppState.skin.box1.hashtag, m_AppState.skin, m_AppState.bodyBoldFont, 233, 1374, true),
						new TextField_ChooseIssue(m_AppState.skin.box2.socialMediaPost, m_AppState.skin, m_AppState.bodyBoldFont, 1615, 1210),
						new TextField_ChooseIssue(m_AppState.skin.box2.hashtag, m_AppState.skin, m_AppState.bodyBoldFont, 1615, 1374, true)
					);

					CitrusFramework.frameworkStage.addEventListener(KeyboardEvent.KEY_UP, this.receiveChar);
					CitrusFramework.frameworkStage.addEventListener(KeyboardEvent.KEY_DOWN, this.checkCaret);
					for (var index:int = 0; index < this.addYourOwnFields.length; index++) 
					{
						this.addYourOwnFields[index].addEventListener(InputController.DOWN, this.clearActiveTextField);
						this.addYourOwnFields[index].addEventListener(InputController.UP, this.focusActiveTextField);
					}
					
					m_AppState.skin.box1.themeTxt.text = "Theme #1";
					m_AppState.skin.box2.themeTxt.text = "Theme #2";
					m_AppState.skin.box1.txt.text = IssueReference.GetIssuesByAgeAndJourney(2, SessionInformation_GirlsTablet.Journey)[0].IssueText;
					m_AppState.skin.box2.txt.text = IssueReference.GetIssuesByAgeAndJourney(2, SessionInformation_GirlsTablet.Journey)[1].IssueText;
					m_AppState.skin.box1.keywordsuggestionsTxt.text = IssueReference.GetIssuesByAgeAndJourney(2, SessionInformation_GirlsTablet.Journey)[0].KeyWords[0] + "\n" + IssueReference.GetIssuesByAgeAndJourney(2, SessionInformation_GirlsTablet.Journey)[0].KeyWords[1] + "\n" + IssueReference.GetIssuesByAgeAndJourney(2, SessionInformation_GirlsTablet.Journey)[0].KeyWords[2];
					m_AppState.skin.box2.keywordsuggestionsTxt.text = IssueReference.GetIssuesByAgeAndJourney(2, SessionInformation_GirlsTablet.Journey)[1].KeyWords[0] + "\n" + IssueReference.GetIssuesByAgeAndJourney(2, SessionInformation_GirlsTablet.Journey)[1].KeyWords[1] + "\n" + IssueReference.GetIssuesByAgeAndJourney(2, SessionInformation_GirlsTablet.Journey)[1].KeyWords[2];
						
					m_AppState.skin.launchweb.addEventListener(InputController.CLICK, launchWebWindow);
					m_AppState.skin.header.mouseEnabled = false;
					m_AppState.skin.header.mouseChildren = false;
					break;
			}
			
			m_AppState.skin.submit_Button.addEventListener(InputController.CLICK, submitData);
			
			CitrusFramework.contentLayer.addChild(m_AppState);
			
			EndInit();
		}
		
		private function launchWebWindow(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_LAUNCHWEB);
			m_WebView.Initialize();
		}
		
		private function checkFilledTextFields(e:Event = null):void 
		{
			var i:int = 0;
			var numFilledFields:int = 0;
			
			for each (var field:TextField_ChooseIssue in this.addYourOwnFields) 
			{	
				if (field.getTextFieldLength() > 0) 
				{
					numFilledFields++;
				}
				i++;
			}
		}
		
		private function clearActiveTextField(e:Event):void
		{
			this._activeTextField = this.addYourOwnFields.indexOf(e.currentTarget as TextField_ChooseIssue);
			////////// TEMP
			
			if(this._activeTextField == -1)
			{
				return;
			}
			this.addYourOwnFields[this._activeTextField].clearField();
			////////////////
			this.activateSingleTextField(this._activeTextField);
			for (var i:int = 0; i < this.addYourOwnFields.length; i++)
			{
				if (i != this._activeTextField)
				{
					this.addYourOwnFields[i].resetText();
				}
				else
				{
					if (i < 2)
					{
						setBox("box1");
					}
					else
					{
						setBox("box2");
					}
				}
			}
		}
		
		private function focusActiveTextField(e:Event):void
		{
			this._activeTextField = this.addYourOwnFields.indexOf(e.currentTarget as TextField_ChooseIssue);
			
			if(this._activeTextField == -1)
			{
				return;
			}
			this.addYourOwnFields[this._activeTextField].clearField();
			////////////////
			this.activateSingleTextField(this._activeTextField);
			CitrusFramework.frameworkStage.focus = (e.currentTarget as TextField_ChooseIssue).asset.txt;
			
			checkDone();
		}
		
		private function activateSingleTextField(fieldNum:int):void
		{
			for each (var field:TextField_ChooseIssue in this.addYourOwnFields) 
			{
				field.deactivate();
			}
			if(fieldNum > 0 && fieldNum < this.addYourOwnFields.length)
			{
				this.addYourOwnFields[fieldNum].activate();
			}
		}
		
		private function checkCaret(e:KeyboardEvent):void
		{
			if(this._activeTextField < 0) return;

			if (this.addYourOwnFields[this._activeTextField].isHash && this.addYourOwnFields[this._activeTextField].asset.txt.caretIndex < 1)
			{
				this.addYourOwnFields[this._activeTextField].asset.txt.setSelection(1, 1);
			}
		}
		
		private function receiveChar(e:KeyboardEvent):void
		{
			if(e.keyCode == Keyboard.TAB)
			{
				this._activeTextField++;
				if(this._activeTextField >= this.addYourOwnFields.length) this._activeTextField = 0;
				
				if (this.addYourOwnFields[this._activeTextField].isHash)
				{
					this.addYourOwnFields[this._activeTextField].asset.txt.text = "#";
					this.addYourOwnFields[this._activeTextField].asset.txt.setSelection(1, 1);
				}
			}

			if(this._activeTextField >= 0)
			{
				this.addYourOwnFields[this._activeTextField].receiveChar(e);
				this.addYourOwnFields[this._activeTextField].caps();

				checkDone();
			}
		}
		
		private function parseIssues(i:int):void
		{
			
		}
		
		private function chooseIcon1(e:Event):void
		{
			var nameString:String = "";
			if(e is MouseEvent)
			{
				var mEvent:MouseEvent = (e as MouseEvent);
				nameString = mEvent.target.name;
			}
			
			if(e is TouchEvent)
			{
				var tEvent:TouchEvent = (e as TouchEvent);
				nameString = tEvent.target.name;
			}
			chooseIcon(nameString, 1);
		}
		
		private function chooseIcon2(e:Event):void
		{
			var nameString:String = "";
			if(e is MouseEvent)
			{
				var mEvent:MouseEvent = (e as MouseEvent);
				nameString = mEvent.target.name;
			}
			
			if(e is TouchEvent)
			{
				var tEvent:TouchEvent = (e as TouchEvent);
				nameString = tEvent.target.name;
			}
			chooseIcon(nameString, 2);
		}
		
		private function chooseIcon(nmString:String, whichBox:int):void
		{
			switch (whichBox)
			{
				case 1:
					TweenMax.to(m_AppState.skin.box1.icon1, .3, {tint:null});
					TweenMax.to(m_AppState.skin.box1.icon2, .3, {tint:null});
					TweenMax.to(m_AppState.skin.box1.icon3, .3, {tint:null});
					TweenMax.to(m_AppState.skin.box1.icon4, .3, {tint:null});
					TweenMax.to(m_AppState.skin.box1.icon5, .3, {tint:null});
					TweenMax.to(m_AppState.skin.box1.icon6, .3, {tint:null});
					setBox("box1")
					break;
				case 2:
					TweenMax.to(m_AppState.skin.box2.icon1, .3, {tint:null});
					TweenMax.to(m_AppState.skin.box2.icon2, .3, {tint:null});
					TweenMax.to(m_AppState.skin.box2.icon3, .3, {tint:null});
					TweenMax.to(m_AppState.skin.box2.icon4, .3, {tint:null});
					TweenMax.to(m_AppState.skin.box2.icon5, .3, {tint:null});
					TweenMax.to(m_AppState.skin.box2.icon6, .3, {tint:null});
					setBox("box2")
					break;
			}
			
			switch(nmString)
			{
				case "icon1":
					whichIcon = 1;
					switch (whichBox)
					{
						case 1:
							saLeftIconSelectedIndex = 1;
							TweenMax.to(m_AppState.skin.box1.icon1, .3, {tint:this.m_AppState.m_Glow, tintAmount:1});
							break;
						case 2:
							saRightIconSelectedIndex = 1;
							TweenMax.to(m_AppState.skin.box2.icon1, .3, {tint:this.m_AppState.m_Glow, tintAmount:1});
							break;
					}
					break;
				case "icon2":
					whichIcon = 2;
					switch (whichBox)
					{
						case 1:
							saLeftIconSelectedIndex = 2;
							TweenMax.to(m_AppState.skin.box1.icon2, .3, {tint:this.m_AppState.m_Glow, tintAmount:1});
							break;
						case 2:
							saRightIconSelectedIndex = 2;
							TweenMax.to(m_AppState.skin.box2.icon2, .3, {tint:this.m_AppState.m_Glow, tintAmount:1});
							break;
					}
					break;
				case "icon3":
					whichIcon = 3;
					switch (whichBox)
					{
						case 1:
							saLeftIconSelectedIndex = 3;
							TweenMax.to(m_AppState.skin.box1.icon3, .3, {tint:this.m_AppState.m_Glow, tintAmount:1});
							break;
						case 2:
							saRightIconSelectedIndex = 3;
							TweenMax.to(m_AppState.skin.box2.icon3, .3, {tint:this.m_AppState.m_Glow, tintAmount:1});
							break;
					}
					break;
				case "icon4":
					whichIcon = 4;
					switch (whichBox)
					{
						case 1:
							saLeftIconSelectedIndex = 4;
							TweenMax.to(m_AppState.skin.box1.icon4, .3, {tint:this.m_AppState.m_Glow, tintAmount:1});
							break;
						case 2:
							saRightIconSelectedIndex = 4;
							TweenMax.to(m_AppState.skin.box2.icon4, .3, {tint:this.m_AppState.m_Glow, tintAmount:1});
							break;
					}
					break;
				case "icon5":
					whichIcon = 5;
					switch (whichBox)
					{
						case 1:
							saLeftIconSelectedIndex = 5;
							TweenMax.to(m_AppState.skin.box1.icon5, .3, {tint:this.m_AppState.m_Glow, tintAmount:1});
							break;
						case 2:
							saRightIconSelectedIndex = 5;
							TweenMax.to(m_AppState.skin.box2.icon5, .3, {tint:this.m_AppState.m_Glow, tintAmount:1}); 
							break;
					}
					break;
				case "icon6":
					whichIcon = 6;
					switch (whichBox)
					{
						case 1:
							saLeftIconSelectedIndex = 6;
							TweenMax.to(m_AppState.skin.box1.icon6, .3, {tint:this.m_AppState.m_Glow, tintAmount:1});
							break;
						case 2:
							saRightIconSelectedIndex = 6;
							TweenMax.to(m_AppState.skin.box2.icon6, .3, {tint:this.m_AppState.m_Glow, tintAmount:1});
							break;
					}
					break;
			}
			
			checkDone();
		}
		
		private function chooseBox(e:Event):void
		{
			STEM_Popups.DisplayPopup(STEM_Popups.CHOOSE_ISSUE);
			
			var nameString:String = "";
			if(e is MouseEvent)
			{
				var mEvent:MouseEvent = (e as MouseEvent);
				nameString = mEvent.target.name;
			}
			
			if(e is TouchEvent)
			{
				var tEvent:TouchEvent = (e as TouchEvent);
				nameString = tEvent.target.name;
			}

			switch(nameString)
			{
				case "box1":
					switch(SessionInformation_GirlsTablet.Age)
					{
						case 0:
							switch(SessionInformation_GirlsTablet.Journey)
							{
								case 0:
									SoundHandler.PlaySound(GSOC_SFX.SOUND_WATERWASTE);
									break;
								case 1:
									SoundHandler.PlaySound(GSOC_SFX.SOUND_STRAYCATS);
									break;
								case 2:
									SoundHandler.PlaySound(GSOC_SFX.SOUND_UNHEALTHYLUNCH);
									break;
							}
						break;
						case 1:
							SoundHandler.PlaySound(GSOC_SFX.SOUND_CHOOSEISSUE);
						break;
						case 2:
							switch(SessionInformation_GirlsTablet.Journey)
							{
								case 0:
									SoundHandler.PlaySound(GSOC_SFX.SOUND_GRAFFITI);
									break;
								case 1:
									SoundHandler.PlaySound(GSOC_SFX.SOUND_LOCALRESTAURANTS);
									break;
								case 2:
									SoundHandler.PlaySound(GSOC_SFX.SOUND_BEAUTY);
									break;
							}
						break;
					}
					break;
				case "box2":
					switch(SessionInformation_GirlsTablet.Age)
					{
						case 0:
							switch(SessionInformation_GirlsTablet.Journey)
							{
								case 0:
									SoundHandler.PlaySound(GSOC_SFX.SOUND_ENDANGEREDANIMALS);
									break;
								case 1:
									SoundHandler.PlaySound(GSOC_SFX.SOUND_NOTENOUGHEXERCISE);
									break;
								case 2:
									SoundHandler.PlaySound(GSOC_SFX.SOUND_RIDINGBIKE);
									break;
							}
							break;
						case 1:
							SoundHandler.PlaySound(GSOC_SFX.SOUND_CHOOSEISSUE);
							break;
						case 2:
							switch(SessionInformation_GirlsTablet.Journey)
							{
								case 0:
									SoundHandler.PlaySound(GSOC_SFX.SOUND_CARACCIDENT);
									break;
								case 1:
									SoundHandler.PlaySound(GSOC_SFX.SOUND_IMPACTENVIRONMENT);
									break;
								case 2:
									SoundHandler.PlaySound(GSOC_SFX.SOUND_WORLDDREAMS);
									break;
							}
							break;
					}
					break;
				case "box3":
					switch(SessionInformation_GirlsTablet.Age)
					{
						case 0:
							switch(SessionInformation_GirlsTablet.Journey)
							{
								case 0:
									SoundHandler.PlaySound(GSOC_SFX.SOUND_WEEDSGROWING);
									break;
								case 1:
									SoundHandler.PlaySound(GSOC_SFX.SOUND_NOTREADING);
									break;
								case 2:
									SoundHandler.PlaySound(GSOC_SFX.SOUND_TROUBLEVEGETABLES);
									break;
							}
							break;
						case 1:
							SoundHandler.PlaySound(GSOC_SFX.SOUND_CHOOSEISSUE);
							break;
					}
					break;
			}
			setBox(nameString);
			checkDone();	
		}
		
		private function checkDone():void
		{
			if (SessionInformation_GirlsTablet.Age < 2)
			{
				TweenMax.to(m_AppState.skin.submit_Button, .5, {y:submitY});
			}
			else
			{
				var nameString:String = activeBox == 1 ? "box1" : "box2";
				whichIcon = activeBox == 1 ? saLeftIconSelectedIndex : saRightIconSelectedIndex;
				
				var socialMediaText:String = m_AppState.skin[nameString].socialMediaPost.txt.text;		
				var isSocialMediaTextValid:Boolean = StringUtil.trim(socialMediaText) && socialMediaText != "SOCIAL MEDIA POST";
				
				var hashTagText:String = m_AppState.skin[nameString].hashtag.txt.text;
				var isHashTagTextValid:Boolean = StringUtil.trim(hashTagText) != "#" && hashTagText != TextField_ChooseIssue.ABBREVIATE_TEXT;
				
				var isIssueValid:Boolean = whichIcon > -1 && isSocialMediaTextValid && isHashTagTextValid;
				var submitPositionY:int = submitY + (isIssueValid ? 0 : m_AppState.skin.submit_Button.height);
				TweenMax.to(m_AppState.skin.submit_Button, .5, {y:submitPositionY});
			}
			
			if (nameString == "box1" || nameString == "box2" || nameString == "box3")
			{
				setBox(nameString);
			}
		}
		
		private function setBox(nameString:String):void
		{
			var box1Filterable:Sprite;
			var box2Filterable:Sprite;
			var box3Filterable:Sprite;
			
			switch(SessionInformation_GirlsTablet.Age)
			{
				case AgeConstants.KEY_DAISYBROWNIE:
					box1Filterable = m_AppState.skin.box1;
					box2Filterable = m_AppState.skin.box2;
					box3Filterable = m_AppState.skin.box3;
					break;
				case AgeConstants.KEY_JUNIORCADETTE:
					box1Filterable = m_AppState.skin.box1.boarder;
					box2Filterable = m_AppState.skin.box2.boarder;
					box3Filterable = m_AppState.skin.box3.boarder;
					break;
				case AgeConstants.KEY_SENIORAMBASSADOR:
					box1Filterable = m_AppState.skin.box1;
					box2Filterable = m_AppState.skin.box2;
					break;
			}
			
			box1Filterable.filters = [];
			box2Filterable.filters = [];
			if(box3Filterable) box3Filterable.filters = [];
			
			switch(nameString)
			{
				case "box1":
					box1Filterable.filters = [theGlow]; 
					theIssue = m_AppState.skin.box1.txt.text;
					activeBox = 1;
					break;
				case "box2":
					box2Filterable.filters = [theGlow]; 
					theIssue = m_AppState.skin.box2.txt.text;
					activeBox = 2;
					break;
				case "box3":
					box3Filterable.filters = [theGlow]; 
					theIssue = m_AppState.skin.box3.txt.text;
					activeBox = 3;
					break;
			}
		}
		
		private function submitData(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_CLICKS[SessionInformation_GirlsTablet.Age]);
			var nmText:String = "";
			var hashText:String = "";
			var smText:String = "";
			var theText:String = "";
			
			var issueData:Issue_Data = SessionInformation_GirlsTablet.TeamInfo.Team_Issue;
			issueData.Reset();
			
			switch (SessionInformation_GirlsTablet.Age)
			{
				case AgeConstants.KEY_DAISYBROWNIE:
					switch(activeBox)
					{
						case 1:
							theText = m_AppState.skin.box1.txt.text;
							break;
						case 2:
							theText = m_AppState.skin.box2.txt.text;
							break;
						case 3:
							theText = m_AppState.skin.box3.txt.text;
							break;
					}
					break;
				case AgeConstants.KEY_JUNIORCADETTE:
					switch(activeBox)
					{
						case 1:
							nmText = m_AppState.skin.box1.nm.text;
							theText = m_AppState.skin.box1.txt.text;
							break;
						case 2:
							nmText = m_AppState.skin.box2.nm.text;
							theText = m_AppState.skin.box2.txt.text;
							break;
						case 3:
							nmText = m_AppState.skin.box3.nm.text;
							theText = m_AppState.skin.box3.txt.text;
							break;
					}
					break;
				case AgeConstants.KEY_SENIORAMBASSADOR:
					switch(activeBox)
					{
						case 1:
							theText = m_AppState.skin.box1.txt.text;
							hashText = m_AppState.skin.box1.hashtag.txt.text;
							smText = m_AppState.skin.box1.socialMediaPost.txt.text;
							break;
						case 2:
							theText = m_AppState.skin.box2.txt.text;
							hashText = m_AppState.skin.box2.hashtag.txt.text;
							smText = m_AppState.skin.box2.socialMediaPost.txt.text;
							break;
					}
					break;
			}
			
			var issue:Issue = IssueReference.GetIssueByText(theText);
			issueData.Key = issue.Key;
			issueData.Name = issue.IssueName;
			issueData.Text = theText;
			issueData.Hash = hashText.length == 0 ? issue.IssueHash : hashText;
			issueData.BackgroundIndex = activeBox;
			var iconIndex:int = whichIcon - 1;
			issueData.IconIndex = iconIndex >= 0 ? iconIndex : issue.IconIndex;
			
			saveData();
			
			if (SessionInformation_GirlsTablet.IsRunningLocally)
			{
				StateControllerManager.LeaveActiveState(StateController_IDs.STATECONTROLLER_CHOOSEISSUE, StateController_StateIDs_ChooseIssue.STATE_CHOOSEISSUE);
				StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_MINDMAP, StateController_StateIDs_MindMap.STATE_MINDMAP);
			}
			else
			{
				submitActivity();
			}
		}
		
		private function saveData():void
		{
			SessionInformation_GirlsTablet.Age == AgeConstants.KEY_DAISYBROWNIE ? uploadDBIssue() : uploadJCSAIssue();
			SessionInformation_GirlsTablet.UploadCurrentInformation();
		}
		
		private function uploadDBIssue():void
		{
			SessionInformation_GirlsTablet.SessionInformationEventDispatcher.dispatchEvent(new Event_UploadDefaultData(Event_UploadDefaultData.EVENT_UPLOAD_DEFAULT_MINDMAP_ISSUE, true));
		}
		
		private function uploadJCSAIssue():void
		{
			IssueImage.SaveIssue();
			UploadManager.UploadDisplayObject(SessionInformation_GirlsTablet.TeamInfo.Team_UserData.GetImage(GSOC_UserData_Team_ImageKeys.KEY_CHOOSEYOURISSUE_ISSUE), Keys_FileNames.KEY_CHOOSEYOURISSUE_ISSUE);
		}
		
		private function submitActivity():void
		{
			StateControllerManager.LeaveAllActiveStates();
			StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_MAIN, StateController_StateIDs_Main.STATE_SESSIONATTRACT);
			Document.TellServerActivityComplete();
		}
		
		protected override function DeInit():void
		{	
			whichIcon = -1;
			theIssue = "";
			CitrusFramework.frameworkStage.removeEventListener(KeyboardEvent.KEY_UP, this.receiveChar);
			CitrusFramework.frameworkStage.removeEventListener(KeyboardEvent.KEY_DOWN, this.checkCaret);
			m_AppState.skin.submit_Button.removeEventListener(InputController.CLICK, submitData);
			CitrusFramework.contentLayer.removeChild(m_AppState);
			m_AppState = null;
			
			m_WebView.DeInitialize();
			
			super.EndDeInit();
		}
	}
}
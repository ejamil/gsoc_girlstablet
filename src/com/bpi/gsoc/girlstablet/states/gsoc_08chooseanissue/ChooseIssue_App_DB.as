package com.bpi.gsoc.girlstablet.states.gsoc_08chooseanissue
{
	import com.bpi.gsoc.components.chooseissue.ChooseIssue_DB_Skin;
	import com.bpi.gsoc.framework.constants.AgeColors;
	import com.bpi.gsoc.framework.constants.AgeFontLinkages;
	import com.bpi.gsoc.framework.constants.TeamNameSizes;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.embeddedcontent.EmbeddedFonts;
	import com.bpi.gsoc.framework.gsocui.GSOC_Header;

	public class ChooseIssue_App_DB extends ChooseIssue_App
	{
		public function ChooseIssue_App_DB()
		{
			m_Skin = new ChooseIssue_DB_Skin();
			m_HeaderFont = AgeFontLinkages.DB_HEADER_FONT;
			m_BodyFont = EmbeddedFonts.FontName_ZalderdashTTF;
			m_Glow = AgeColors.DB_GLOW;
			
			m_GSOCHeader = new GSOC_Header(m_Skin.header.paginationArea);
			GSOCheader.addTeamName(SessionInformation_GirlsTablet.TeamInfo.Team_Name, TeamNameSizes.DB_FONTSIZE);
			GSOCheader.addArrows(m_Skin.header.paginationArea.x, 0);
			
			super.Initialize();
		}
	}
}
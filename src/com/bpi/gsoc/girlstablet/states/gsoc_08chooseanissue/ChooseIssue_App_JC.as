package com.bpi.gsoc.girlstablet.states.gsoc_08chooseanissue
{
	import com.bpi.gsoc.components.chooseissue.ChooseIssue_JC_Skin;
	import com.bpi.gsoc.framework.constants.AgeColors;
	import com.bpi.gsoc.framework.constants.AgeFontLinkages;
	import com.bpi.gsoc.framework.constants.TeamNameSizes;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.gsocui.GSOC_Header;
	
	public class ChooseIssue_App_JC extends ChooseIssue_App
	{
		public function ChooseIssue_App_JC()
		{
			m_Skin = new ChooseIssue_JC_Skin();
			m_HeaderFont = AgeFontLinkages.JC_HEADER_FONT;
			m_BodyFont = AgeFontLinkages.JC_BODY_FONT;
			m_Glow = AgeColors.JC_GLOW;
			
			m_GSOCHeader = new GSOC_Header(m_Skin.header.paginationArea);
			GSOCheader.addTeamName(SessionInformation_GirlsTablet.TeamInfo.Team_Name, TeamNameSizes.JC_FONTSIZE);
			GSOCheader.addArrows(m_Skin.header.paginationArea.x, 0);
			
			super.Initialize();
		}
	}
}


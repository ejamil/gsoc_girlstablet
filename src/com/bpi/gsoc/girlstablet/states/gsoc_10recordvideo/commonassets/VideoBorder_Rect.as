package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.commonassets
{
	import com.bpi.gsoc.girlstablet.global_data.TabletCamera;
	
	import flash.display.MovieClip;
	
	public class VideoBorder_Rect extends MovieClip
	{
		public function VideoBorder_Rect(color:uint)
		{
			super();
			
			this.graphics.lineStyle(VideoConstants.VIDEO_BORDER_WIDTH,color);
			this.graphics.beginFill(0xffffff, 0);
			this.graphics.drawRect(0, 0, TabletCamera.REAR_CAMERA_DIMS.x, TabletCamera.REAR_CAMERA_DIMS.y);
			this.graphics.endFill();
		}
	}
}
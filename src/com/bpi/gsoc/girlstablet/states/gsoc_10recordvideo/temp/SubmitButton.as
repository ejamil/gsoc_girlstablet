package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.temp
{
	import com.bpi.citrusas.text.TextBox;
	import com.bpi.gsoc.framework.constants.GSOC_Info;
	
	import flash.display.MovieClip;
	import flash.geom.Point;
	
	public class SubmitButton extends MovieClip
	{
		private static const BUTTON_DIMS:Point = new Point(250, 125);
		
		public function SubmitButton(text:String = "SUBMIT")
		{
			super();
			
			var tb:TextBox = new TextBox();
			tb.Initialize(new Point(BUTTON_DIMS.x * 0.75, BUTTON_DIMS.y * 0.75), 32, GSOC_Info.ColorOne, false);
			tb.Font = GSOC_Info.HeaderFont;
			tb.Text = text;
			
			this.addChild(tb);
			
			this.graphics.clear();
			this.graphics.beginFill(GSOC_Info.ColorTwo, 0.7);
			this.graphics.drawRect(0, 0, BUTTON_DIMS.x, BUTTON_DIMS.y);
			this.graphics.endFill();
			
			tb.x = this.width / 2 - tb.TextWidth / 2;
			tb.y = this.height / 2 - tb.height / 2 + 25;
		}
	}
}
package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo
{
	import com.bpi.citrusas.text.TextBox;
	import com.bpi.citrusas.utils.Utils_Image;
	import com.bpi.gsoc.framework.avatar.GSOC_Avatar_Builder;
	import com.bpi.gsoc.framework.avatar.GSOC_Avatar_Skin;
	import com.bpi.gsoc.framework.constants.AgeConstants;
	import com.bpi.gsoc.framework.constants.GSOC_Info;
	import com.bpi.gsoc.framework.data.sessioninformation.GirlInformation;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.data.userdata.GSOC_UserData_Girl_DataKeys;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.assignroles.roles.RoleKeys;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.assignroles.roles.RoleReference;
	import com.greensock.TweenLite;
	import com.greensock.easing.Elastic;
	
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.filters.DropShadowFilter;
	import flash.geom.Point;
	
	public class RoleCallout extends Sprite
	{
		private var m_GirlFaces:Sprite;
		private var m_Title:TextBox;
		private var m_ImageBlock:Sprite;
		private var m_RoleCalloutPos:Point;
		private var m_TintColor:uint;
		private var m_MaxWidth:int;
		private var m_MaxHeight:int;
		
		public static const ANIMATE_OUT_COMPLETE:String = "ANIMATE OUT COMPLETE";
		
		public function RoleCallout(x:int, y:int, maxWidth:int, maxHeight:int)
		{
			super();
			m_ImageBlock = new Sprite();
			m_MaxWidth = maxWidth;
			m_MaxHeight = maxHeight;
	
			switch(SessionInformation_GirlsTablet.Age)
			{
				case AgeConstants.KEY_DAISYBROWNIE:
					m_TintColor = 0xFFC40C;
					break;
				case AgeConstants.KEY_JUNIORCADETTE:
					m_TintColor = 0xb4d335;
					break;
				case AgeConstants.KEY_SENIORAMBASSADOR:
					m_TintColor = 0x6bcbd9;
					break;
			}
			
			m_Title = new TextBox();
			m_Title.Initialize(new Point(250, 100), 45, m_TintColor);
			m_Title.Font = GSOC_Info.BodyFont;
			m_Title.ToggleVerticalAlign(true);
			m_Title.ToggleAutoSize(true);
			m_Title.AlignTextCenter();
			
			this.x = x;
			this.y = y;
			
			this.visible = false;
			
			this.addChild(m_ImageBlock);		
		}
		
		public function FillForGuestAndHost():void
		{
			var icon:Sprite = RoleReference.GetIconForRole(RoleKeys.GUESTORHOST);
			setUpIcon(icon);
			
			var girlsWithRole:Vector.<GirlInformation> = new Vector.<GirlInformation>();
			
			for(var i:int = 0; i < SessionInformation_GirlsTablet.TeamInfo.GetTeamMemberCount(); i++)
			{
				var roles:Vector.<String> = SessionInformation_GirlsTablet.TeamInfo.GetGirlInformationFromIndex(i).GirlUserData.GetData(GSOC_UserData_Girl_DataKeys.KEY_ROLES) as Vector.<String>;
				
				if(roles)
				{
					if(roles.indexOf(RoleKeys.GUEST) != -1 || roles.indexOf(RoleKeys.HOST) != -1)
					{
						girlsWithRole.push(SessionInformation_GirlsTablet.TeamInfo.GetGirlInformationFromIndex(i));
					}
				}
			}
			
			m_Title.Text = "GUESTS & HOSTS";
			
			setUpFaceBlock(icon, girlsWithRole);
		}
		
		private function setUpIcon(icon:Sprite):void
		{
			TweenLite.to(icon, 0, {colorTransform:{tint:m_TintColor, tintAmount:1}});
			m_ImageBlock.removeChildren();
			m_ImageBlock.addChild(icon);
			m_ImageBlock.addChild(m_Title);
			
			// move title under icon
			m_Title.y =  icon.height - 25; // weird offset because current icons have lots of whitespace around them
			
			m_ImageBlock.filters = [new DropShadowFilter(4, 45, 0, .7, 20, 20)];
			
			m_ImageBlock.x = 50;
		}
		
		private function setUpFaceBlock(icon:Sprite, girlsWithRole:Vector.<GirlInformation>):void
		{
			icon.x = m_Title.width / 2 - icon.width / 2;
			m_GirlFaces = new Sprite();
			this.addChild(m_GirlFaces);
			
			if(girlsWithRole.length > 0)
			{
				for (var i:int = 0 ; i < girlsWithRole.length; i++)
				{
					var avatar:GSOC_Avatar_Skin = GSOC_Avatar_Builder.BuildAvatarFromUserData(true, girlsWithRole[i].GirlUserData);
					var snapshot:Bitmap = Utils_Image.ConvertDisplayObjectToBitmap(avatar);
					snapshot.x = i * avatar.width * 0.4;
					m_GirlFaces.addChild(snapshot);
					
				}
				
				var extraWidth:Number =  (m_ImageBlock.width + m_GirlFaces.width)- m_MaxWidth;
				if(extraWidth > 0)
				{
					// the girl faces sprite needs to shrink by extraWidth
					var scaleFactor:Number = (m_MaxWidth - m_ImageBlock.width) / m_GirlFaces.width;
					m_GirlFaces.scaleX = m_GirlFaces.scaleY = scaleFactor;
				}
				
				var extraHeight:Number = m_GirlFaces.height - m_MaxHeight;
				if(extraHeight > 0)
				{
					m_GirlFaces.height = m_MaxHeight;
					m_GirlFaces.scaleX = m_GirlFaces.scaleY;
				}
		
				this.visible = true;
			}
			
			m_ImageBlock.height = m_MaxHeight;
			m_ImageBlock.scaleX = m_ImageBlock.scaleY;

			m_GirlFaces.y = m_GirlFaces.parent.height / 2 - m_GirlFaces.height/2;
		}
		
		public function FillForRole(selectedRole:String):void
		{
			var icon:Sprite = RoleReference.GetIconForRole(selectedRole);
			setUpIcon(icon);
			
			var girlsWithRole:Vector.<GirlInformation> = new Vector.<GirlInformation>();
			
			for(var i:int = 0; i < SessionInformation_GirlsTablet.TeamInfo.GetTeamMemberCount(); i++)
			{
				var roles:Vector.<String> = SessionInformation_GirlsTablet.TeamInfo.GetGirlInformationFromIndex(i).GirlUserData.GetData(GSOC_UserData_Girl_DataKeys.KEY_ROLES) as Vector.<String>;
				
				if(roles)
				{
					if(roles.indexOf(selectedRole) != -1)
					{
						girlsWithRole.push(SessionInformation_GirlsTablet.TeamInfo.GetGirlInformationFromIndex(i));
					}
				}
			}
			
			m_Title.Text = selectedRole.toLocaleUpperCase() + "S";
		
			setUpFaceBlock(icon, girlsWithRole);
		}
		
		public function AnimateIn():void
		{
			TweenLite.fromTo(m_ImageBlock, 0.5, {y:-m_ImageBlock.height}, {y:0, ease:Elastic.easeOut.config(1, 1), delay:0.5});
			TweenLite.fromTo(m_GirlFaces, 0.5, {x:1500}, {x:m_ImageBlock.width, ease:Elastic.easeOut.config(1, 1), delay:0.5});
		}
		
		public function AnimateOut():void
		{
			TweenLite.to(m_ImageBlock, 0.3, {y:-m_ImageBlock.height});
			TweenLite.to(m_GirlFaces, 0.3,{x:1500, onComplete:this.notifyAnimateOutComplete});
		}
		
		private function notifyAnimateOutComplete():void
		{
			this.dispatchEvent(new Event(ANIMATE_OUT_COMPLETE));
		}
	}
}
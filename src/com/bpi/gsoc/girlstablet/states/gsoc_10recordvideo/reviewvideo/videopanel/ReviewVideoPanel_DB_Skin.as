package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.reviewvideo.videopanel
{
	import com.bpi.gsoc.components.recordvideo.DB_PauseButton;
	import com.bpi.gsoc.components.recordvideo.DB_PlayButton;
	import com.bpi.gsoc.components.recordvideo.DB_ProgressBarBackground;
	import com.bpi.gsoc.components.recordvideo.DB_ProgressBarForeground;
	import com.bpi.gsoc.components.recordvideo.DB_VideoMask;
	import com.bpi.gsoc.framework.constants.GSOC_Info;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.commonassets.VideoBorder_Rect;

	public class ReviewVideoPanel_DB_Skin extends ReviewVideoPanel_Skin
	{
		public function ReviewVideoPanel_DB_Skin()
		{
			super(new VideoBorder_Rect(GSOC_Info.ColorTwo), new DB_VideoMask(),new DB_PlayButton(), new DB_PauseButton(), new DB_ProgressBarForeground(), new DB_ProgressBarBackground());	
		}
	}
}
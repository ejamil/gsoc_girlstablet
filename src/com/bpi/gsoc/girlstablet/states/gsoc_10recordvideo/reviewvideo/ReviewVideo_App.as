package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.reviewvideo
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.citrusas.input.inputcontroller.InputController;
	import com.bpi.citrusas.sound.SoundHandler;
	import com.bpi.citrusas.states.StateControllerManager;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.gsocui.GSOC_ButtonToggle;
	import com.bpi.gsoc.framework.sfx.GSOC_SFX;
	import com.bpi.gsoc.framework.states.app.GSOC_AppSkin;
	import com.bpi.gsoc.girlstablet.global_data.TabletCamera;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.RoleCallout;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.assignroles.roles.RoleKeys;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.recordvideo.RecordVideo;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.reviewvideo.popup.RecordAgainPopup;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.reviewvideo.videopanel.ReviewVideoPanel;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.reviewvideo.videopanel.ReviewVideoPanel_Skin;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.reviewvideo.videopanel.events.Event_ReviewVideoPanel;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_IDs;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_RecordVideo;
	import com.greensock.TweenLite;
	import com.greensock.easing.Quad;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.filesystem.File;
	
	public class ReviewVideo_App extends GSOC_AppSkin
	{
		protected const STATE_SAVING_VIDEO:int = 0;
		protected const STATE_PREVIEWING_VIDEO:int = 1;
		
		protected var m_VideoPlayer:ReviewVideoPanel;
		protected var m_VideoPlayerSkin:ReviewVideoPanel_Skin;
		protected var m_RecordAgainButton:MovieClip;
		protected var m_ConfirmationPopup:RecordAgainPopup;
		protected var m_RoleCallout:RoleCallout;
		protected var m_CurrentState:int;
		
		public static const VID_VERTICAL_OFFSET:Number = 60;
		
		public function ReviewVideo_App(addPagination:Boolean=false)
		{
			super(this);
		}
		
		public override function Init():void
		{
			super.Init();
			m_Skin.header.gotoAndStop(SessionInformation_GirlsTablet.Track + 1);
			GSOCheader.resetLocations(m_Skin.header.paginationArea.x, m_Skin.header.paginationArea.width, m_Skin.header.paginationArea.height);
			GSOC_ButtonToggle.ToggleButton(false, m_Skin.nextBtn, null, null);
			
			this.enterPreviewState();
		}
		
		public override function DeInit():void
		{
			
			this.disableNext();
			m_ConfirmationPopup.removeEventListener(RecordAgainPopup.YES_CHOSEN, this.eh_GoToPreviousScreen);
			
			m_VideoPlayer.removeEventListener(Event_ReviewVideoPanel.EVENT_WATCHED_VIDEO_TO_END, this.eh_ShowNext);
			m_VideoPlayer.CleanUp();
			
			m_Skin.removeChild(m_VideoPlayer);
			this.exitPreviewState();
			this.removeChild(m_RoleCallout);
			m_RoleCallout = null;
			
			super.DeInit();
		}
		
		private function eh_ShowNext(e:Event):void
		{
			GSOC_ButtonToggle.ToggleButton(true, m_Skin.nextBtn, this.enableNext, null);
		}
		
		private function enableNext():void
		{
			m_Skin.nextBtn.addEventListener(InputController.CLICK, this.eh_GoToNextScreen);
		}
		
		private function disableNext():void
		{
			m_Skin.nextBtn.removeEventListener(InputController.CLICK, this.eh_GoToNextScreen);
		}
		
		private function showRecordAgainButton():void
		{
			TweenLite.to(m_RecordAgainButton, 0.5, {x:CitrusFramework.frameworkStageSize.x - 50 - m_RecordAgainButton.width, ease:Quad.easeIn, onComplete:this.enableRecordAgain});
		}
		
		private function hideRecordAgainButton():void
		{
			this.disableRecordAgain();
			TweenLite.to(m_RecordAgainButton, 0.5, {x:CitrusFramework.frameworkStageSize.x + m_RecordAgainButton.width + 50, ease:Quad.easeOut});
		}
		
		private function enableRecordAgain():void
		{
			m_RecordAgainButton.addEventListener(InputController.CLICK, m_ConfirmationPopup.Launch);
		}
		
		private function disableRecordAgain():void
		{
			m_RecordAgainButton.removeEventListener(InputController.CLICK, m_ConfirmationPopup.Launch);
		}
		
		private function eh_GoToNextScreen(e:Event = null):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_CLICKS[SessionInformation_GirlsTablet.Age]);
			StateControllerManager.LeaveActiveState(StateController_IDs.STATECONTROLLER_RECORDVIDEO, StateController_StateIDs_RecordVideo.STATE_REVIEWVIDEO);
			StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_RECORDVIDEO, StateController_StateIDs_RecordVideo.STATE_EDITVIDEO);
		}
		
		private function eh_GoToPreviousScreen(e:Event = null):void
		{
			StateControllerManager.LeaveActiveState(StateController_IDs.STATECONTROLLER_RECORDVIDEO, StateController_StateIDs_RecordVideo.STATE_REVIEWVIDEO);
			StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_RECORDVIDEO, StateController_StateIDs_RecordVideo.STATE_RECORDVIDEO);
		}
		
		private function enterPreviewState():void
		{
			m_VideoPlayer = new ReviewVideoPanel(m_VideoPlayerSkin);
			m_Skin.addChild(m_VideoPlayer);
			
			var path:String = CitrusFramework.frameworkConfigXML.videoEncodingConfigs.videoProcessingDirectory.toString() + CitrusFramework.frameworkConfigXML.videoEncodingConfigs.rawVideoPath.toString();
		
			var tmpFile:File = File.applicationDirectory.resolvePath(path);
			
			if(!tmpFile.exists)
			{
				// file wasn't saved properly. Relaunch the recorder.
				// this should never really happen because this state gets triggered on a successful save event, but it's just a safeguard
				CitrusFramework.localLog.LogEvent("Error saving video. When the preview screen tried to get the recorded video, it wasn't there.");
				RecordVideo.Launch(SessionInformation_GirlsTablet.Age);
				return;
			}
			
			m_VideoPlayer.Initialize(path, TabletCamera.REAR_CAMERA_DIMS);
			
			m_VideoPlayer.x = CitrusFramework.frameworkStageSize.x / 2  - m_VideoPlayer.width / 2;
			m_VideoPlayer.y = m_Skin.header.height + VID_VERTICAL_OFFSET;
			m_VideoPlayer.addEventListener(Event_ReviewVideoPanel.EVENT_WATCHED_VIDEO_TO_END, this.eh_ShowNext);
			
			m_Skin.addChild(m_RecordAgainButton);
			m_RecordAgainButton.x = CitrusFramework.frameworkStageSize.x + m_RecordAgainButton.width;
			m_RecordAgainButton.y = CitrusFramework.frameworkStageSize.y / 2;
			
			m_ConfirmationPopup.Initialize();
			m_Skin.addChild(m_ConfirmationPopup);
			m_ConfirmationPopup.addEventListener(RecordAgainPopup.YES_CHOSEN, this.eh_GoToPreviousScreen);
			
			
			m_RoleCallout.FillForRole(RoleKeys.DIRECTOR);
			m_RoleCallout.AnimateIn();
			this.addChild(m_RoleCallout);
			
			this.showRecordAgainButton();
		}
		
		private function exitPreviewState():void
		{
			if(m_VideoPlayer && m_Skin.contains(m_VideoPlayer)) m_Skin.removeChild(m_VideoPlayer);
			if(m_RecordAgainButton && m_Skin.contains(m_RecordAgainButton)) m_Skin.removeChild(m_RecordAgainButton);
			if(m_VideoPlayer) m_VideoPlayer.CleanUp();
			m_VideoPlayer = null;
			m_ConfirmationPopup.removeEventListener(RecordAgainPopup.YES_CHOSEN, this.eh_GoToPreviousScreen);
		}
	}
}
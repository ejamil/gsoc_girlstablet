package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.reviewvideo.videopanel
{
	import com.bpi.citrusas.video.InteractiveVideo;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.reviewvideo.videopanel.events.Event_ReviewVideoPanel;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.reviewvideo.videopanel.progressbar.ProgressBar;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.reviewvideo.videopanel.progressbar.events.Event_ProgressBar;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.filters.DropShadowFilter;
	import flash.geom.Point;
	
	public class ReviewVideoPanel extends Sprite
	{
		private static const PROGESS_BAR_VERTICAL_OFFSET:Number = 224;
		
		private static var ms_VideoLength:Number;
		
		private var m_Skin:ReviewVideoPanel_Skin;
		private var m_ProgressBar:ProgressBar;
		private var m_InteractiveVideo:InteractiveVideo;
		
		public function ReviewVideoPanel(skin:ReviewVideoPanel_Skin)
		{
			m_Skin = skin;
			m_ProgressBar = new ProgressBar();
			m_InteractiveVideo = new InteractiveVideo(true);
		}
		
		public function Initialize(videoPath:String, videoSize:Point, videoLoop:Boolean = false):void
		{	
			initializeInteractiveVideo(videoPath, videoSize, videoLoop);
			initializeProgressBar();
			initializeControlButtons();
			initializeSkinMask();
			
			initializeDropShadow();
			
			this.addEventListener(Event.ENTER_FRAME, eh_Update);
			m_InteractiveVideo.addEventListener(Event.COMPLETE, eh_VideoComplete);
			
			this.addChild(m_InteractiveVideo);
			this.addChild(m_Skin.border);
			this.addChild(m_Skin.mask);
			this.addChild(m_ProgressBar);
			
			m_Skin.border.mouseEnabled = false;
			m_Skin.mask.mouseEnabled = false;
		}
		
		public function CleanUp():void
		{		
			m_InteractiveVideo.Stop();
			m_Skin = null;
			cleanUpEventListeners();
			cleanUpProgressBar();
		}	
		
		private function eh_SetPercentage(evt:Event_ProgressBar):void
		{
			var position:Number = m_InteractiveVideo.Length * evt.E_Percentage;
			m_InteractiveVideo.Seek(position);
		}
		
		private function eh_Update(evt:Event):void
		{
			m_ProgressBar.Update();
			m_ProgressBar.UpdateWithTime(m_InteractiveVideo.Time, m_InteractiveVideo.Length);
			ms_VideoLength = m_InteractiveVideo.Length;
		}
		
		private function eh_VideoComplete(e:Event):void
		{	
			m_InteractiveVideo.StartPaused();
			this.dispatchEvent(new Event_ReviewVideoPanel(Event_ReviewVideoPanel.EVENT_WATCHED_VIDEO_TO_END));
		}
		
		private function initializeInteractiveVideo(videoPath:String, videoSize:Point, videoLoop:Boolean):void
		{
			m_InteractiveVideo.Initialize(videoPath, videoSize, videoLoop);
			m_InteractiveVideo.Play();
			ms_VideoLength = -1;
		}
		
		private function initializeControlButtons():void
		{
			m_InteractiveVideo.PlayButton = m_Skin.playButton;
			m_InteractiveVideo.PauseButton = m_Skin.pauseButton;
			
			// ht diff between ui buttons and prog bar so we can center the buttons vertically:
			var htDiff:Number = m_ProgressBar.Size.y - m_InteractiveVideo.PlayButton.height;
	
			m_InteractiveVideo.PlayButton.x = m_ProgressBar.x / 2 - m_InteractiveVideo.PlayButton.width / 2;
			m_InteractiveVideo.PlayButton.y = m_ProgressBar.y + htDiff;
			m_InteractiveVideo.PauseButton.y = m_InteractiveVideo.PlayButton.y;
			m_InteractiveVideo.PauseButton.x = m_InteractiveVideo.PlayButton.x;
		}
		
		private function initializeSkinMask():void
		{
			if(m_Skin.mask)
			{
				m_InteractiveVideo.VideoObject.mask = m_Skin.mask;
			}
		}
		
		private function initializeProgressBar():void
		{
			m_ProgressBar.Initialize(m_Skin.progressBarForeground, m_Skin.progressBarBackground, m_InteractiveVideo.width * 0.8);
			m_ProgressBar.addEventListener(Event_ProgressBar.EVENT_SET_POSITION, eh_SetPercentage);
			
			m_ProgressBar.ResetToEnd();
			m_ProgressBar.x = m_InteractiveVideo.width - m_ProgressBar.width * 1.1;
			m_ProgressBar.y = m_InteractiveVideo.height - PROGESS_BAR_VERTICAL_OFFSET;	
		}
		
		private function initializeDropShadow():void
		{
			var filter:DropShadowFilter = new DropShadowFilter();
			filter.distance = 15;
			filter.alpha = 0.5;
			filter.blurX = 30;
			filter.blurY = 30;
			
			this.filters = [filter];
		}
		
		private function cleanUpProgressBar():void
		{
			if(m_ProgressBar)
			{
				if(m_ProgressBar.hasEventListener(Event_ProgressBar.EVENT_SET_POSITION))
				{
					m_ProgressBar.removeEventListener(Event_ProgressBar.EVENT_SET_POSITION, eh_SetPercentage);
				}
				m_ProgressBar.CleanUp();
			}
			m_ProgressBar = null;
		}
		
		private function cleanUpEventListeners():void
		{
			if(this.hasEventListener(Event.ENTER_FRAME))
			{
				this.removeEventListener(Event.ENTER_FRAME, eh_Update);
			}
			
			if(m_InteractiveVideo.hasEventListener(Event.COMPLETE))
			{
				this.removeEventListener(Event.COMPLETE, eh_VideoComplete);
			}
		}
		
		public static function get VideoLength():Number
		{
			return ms_VideoLength;
		}
	}
}
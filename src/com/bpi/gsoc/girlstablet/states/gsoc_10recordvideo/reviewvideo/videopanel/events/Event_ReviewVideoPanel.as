package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.reviewvideo.videopanel.events
{
	import flash.events.Event;
	
	public class Event_ReviewVideoPanel extends Event
	{
		public static const EVENT_WATCHED_VIDEO_TO_END:String = "EVENT_WATCHED_VIDEO_TO_END";
		
		public function Event_ReviewVideoPanel(type:String) 
		{
			super(type, false, false);
		}
	}
}
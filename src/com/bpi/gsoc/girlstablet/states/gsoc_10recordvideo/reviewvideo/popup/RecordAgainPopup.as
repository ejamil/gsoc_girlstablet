package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.reviewvideo.popup
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.citrusas.input.inputcontroller.InputController;
	import com.bpi.citrusas.sound.SoundHandler;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.sfx.GSOC_SFX;
	import com.bpi.gsoc.girlstablet.global_data.FrameControlUtils;
	import com.greensock.TweenLite;
	
	import flash.display.Sprite;
	import flash.events.Event;
	
	public class RecordAgainPopup extends Sprite
	{
		private var m_Background:Sprite;
		private var m_Skin:RecordAgainPopup_Skin;
		private var m_SkinContainer:Sprite;
		private var m_AnswerChosen:String;
		
		public static const YES_CHOSEN:String = "YES_CHOSEN";
		public static const NO_CHOSEN:String = "NO_CHOSEN";
		
		public function RecordAgainPopup(skinGraphics:Class)
		{
			super();
			m_Skin = new RecordAgainPopup_Skin(skinGraphics);
		}
		
		public function Initialize():void
		{
			m_Background = new Sprite();
			m_Background.graphics.beginFill(0x000000, 0.7);
			m_Background.graphics.drawRect(0, 0, CitrusFramework.frameworkStageSize.x, CitrusFramework.frameworkStageSize.y);
			m_Background.graphics.endFill();
			
			m_SkinContainer = new Sprite();
			m_SkinContainer.addChild(m_Skin.skin);
			m_Skin.skin.x = -m_Skin.skin.width / 2;
			m_Skin.skin.y = -m_Skin.skin.height / 2;
			m_SkinContainer.x = CitrusFramework.frameworkStageSize.x / 2;
			m_SkinContainer.y = CitrusFramework.frameworkStageSize.y / 2;
			
			this.addChild(m_Background);
			this.addChild(m_SkinContainer);
			m_Background.alpha = 0;
			m_SkinContainer.alpha = 0;
			FrameControlUtils.StopAtFrameN(m_Skin.skin);
			this.mouseEnabled = false;
			this.mouseChildren = false;
		}
		
		public function Launch(e:Event = null):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_RERECORDBUTTONS[SessionInformation_GirlsTablet.Age]);
			this.mouseEnabled = true;
			this.mouseChildren = true;
			this.showBackground();
			this.showUI();
		}
		
		private function showBackground():void
		{
			TweenLite.fromTo(m_Background, .5, {alpha: 0}, {alpha: .6});
		}
		
		private function hideBackground():void
		{
			TweenLite.to(m_Background, .5, {alpha: 0, onComplete:this.endPopupInteraction});
		}
		
		private function showUI():void
		{
			TweenLite.fromTo(m_SkinContainer, .5, {scaleX: 1.3, scaleY: 1.3, alpha: 0}, {scaleX: 1, scaleY: 1, alpha: 1, onComplete:this.enableUI});	
		}
		
		private function hideUI():void
		{
			TweenLite.to(m_SkinContainer, .5, {scaleX: .4, scaleY: .4, alpha: 0});
		}
		
		private function enableUI():void
		{
			m_Skin.yesButton.addEventListener(InputController.CLICK, this.eh_YesClicked);
			m_Skin.noButton.addEventListener(InputController.CLICK, this.eh_NoClicked);
			
		}
		
		private function disableUI():void
		{
			m_Skin.yesButton.removeEventListener(InputController.CLICK, this.eh_YesClicked);
			m_Skin.noButton.removeEventListener(InputController.CLICK, this.eh_NoClicked);
		}
		
		private function eh_YesClicked(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_YESNOS[SessionInformation_GirlsTablet.Age]);
			m_AnswerChosen = YES_CHOSEN;
			close();
		}
		
		private function eh_NoClicked(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_YESNOS[SessionInformation_GirlsTablet.Age]);
			m_AnswerChosen = NO_CHOSEN;
			close();
		}
		
		private function endPopupInteraction():void
		{
			this.mouseEnabled = false;
			this.mouseChildren = false;
			this.dispatchEvent(new Event(m_AnswerChosen));
		}
		
		private function close():void
		{
			this.disableUI();
			this.hideUI();
			this.hideBackground();
		}
	}
}
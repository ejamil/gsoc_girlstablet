package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.reviewvideo.popup
{
	import flash.display.MovieClip;

	public class RecordAgainPopup_Skin
	{
		protected var m_Skin:MovieClip;
		
		public function RecordAgainPopup_Skin(skinGraphics:Class)
		{
			m_Skin = new skinGraphics();
		}
		
		public function get yesButton():MovieClip
		{
			return m_Skin.yesBtn;
		}
		
		public function get noButton():MovieClip
		{
			return m_Skin.noBtn;
		}
		
		public function get skin():MovieClip
		{
			return m_Skin;
		}
		
	}
}
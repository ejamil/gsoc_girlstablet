package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.reviewvideo
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.gsoc.framework.constants.AgeConstants;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.states.app.GSOC_AppState;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.reviewvideo.messaging.VideoSavingForceVisible;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_RecordVideo;
	
	public class ReviewVideo_State extends GSOC_AppState
	{
		private var m_App:ReviewVideo_App;
		protected var m_SavingGraphic:VideoSavingForceVisible;

		public function ReviewVideo_State()
		{
			super(StateController_StateIDs_RecordVideo.STATE_REVIEWVIDEO);
		}
		
		public override function Initialize():void
		{
			
		}
		
		protected override function Init():void
		{
			switch(SessionInformation_GirlsTablet.Age)
			{
				case AgeConstants.KEY_DAISYBROWNIE:
					m_App = new ReviewVideo_App_DB();
					break;
				case AgeConstants.KEY_JUNIORCADETTE:
					m_App = new ReviewVideo_App_JC();
					break;
				case AgeConstants.KEY_SENIORAMBASSADOR:
					m_App = new ReviewVideo_App_SA();
					break;
			}
			
			m_App.Initialize();
			super.Initialize();
			

			CitrusFramework.contentLayer.addChild(m_App);
			m_App.Init();
		}
		
		protected override function DeInit():void
		{	
			CitrusFramework.contentLayer.removeChild(m_App);
			m_App.DeInit();
			
			super.EndDeInit();
		}	
	}
}
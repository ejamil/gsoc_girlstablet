package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.reviewvideo
{
	import com.bpi.gsoc.components.recordvideo.DB_RecordAgainBtn;
	import com.bpi.gsoc.components.recordvideo.DB_areYouSurePopup;
	import com.bpi.gsoc.components.recordvideo.ReviewVideo_DB_Skin;
	import com.bpi.gsoc.framework.gsocui.GSOC_Header;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.RoleCallout;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.reviewvideo.popup.RecordAgainPopup;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.reviewvideo.videopanel.ReviewVideoPanel_DB_Skin;

	public class ReviewVideo_App_DB extends ReviewVideo_App
	{
		public function ReviewVideo_App_DB(addPagination:Boolean=false)
		{
			super(addPagination);
		}
		
		public override function Initialize():void
		{
			m_Skin = new ReviewVideo_DB_Skin();
			m_VideoPlayerSkin = new ReviewVideoPanel_DB_Skin();
			m_RecordAgainButton = new DB_RecordAgainBtn();
			m_ConfirmationPopup = new RecordAgainPopup(DB_areYouSurePopup);
			
			m_GSOCHeader = new GSOC_Header(m_Skin.header.paginationArea);
			GSOCheader.addArrows(m_Skin.header.paginationArea.x, 0);
			
			m_RoleCallout = new RoleCallout(m_Skin.header.paginationArea.x, m_Skin.header.paginationArea.y, m_Skin.header.paginationArea.width, m_Skin.header.paginationArea.height);
			
			super.Initialize();
		}
		
		public override function DeInit():void
		{
			super.DeInit();
		}
	}
}
package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.reviewvideo.videopanel
{
	import flash.display.MovieClip;

	public class ReviewVideoPanel_Skin
	{
		protected var m_Border:MovieClip;
		protected var m_Mask:MovieClip;
		protected var m_PlayButton:MovieClip;
		protected var m_PauseButton:MovieClip;
		protected var m_ProgressBarForeground:MovieClip;
		protected var m_ProgressBarBackground:MovieClip;
		
		public static const ROUNDED_EDGE_RADIUS:Number = 200;
		public static const BORDER_WIDTH:Number = 20;
	
		public function ReviewVideoPanel_Skin(border:MovieClip, mask:MovieClip, playButton:MovieClip, pauseButton:MovieClip, progBarFore:MovieClip, progBarBack:MovieClip)
		{
			m_Border = border;
			m_Mask = mask;
			m_PlayButton = playButton;
			m_PauseButton = pauseButton;
			m_ProgressBarForeground = progBarFore;
			m_ProgressBarBackground = progBarBack;
		}
		
		public function get border():MovieClip
		{
			return m_Border;
		}
		
		public function get mask():MovieClip
		{
			return m_Mask;
		}
		
		public function get playButton():MovieClip
		{
			return m_PlayButton;
		}
		
		public function get pauseButton():MovieClip
		{
			return m_PauseButton;
		}
		
		public function get progressBarForeground():MovieClip
		{
			return m_ProgressBarForeground;
		}
		
		public function get progressBarBackground():MovieClip
		{
			return m_ProgressBarBackground;
		}
		
	}
}
package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.reviewvideo.messaging
{
	import com.bpi.citrusas.images.AnimationClip;
	import com.bpi.citrusas.video.videorecorder.videosaving.VideoSaving;
	
	import flash.geom.Point;
	
	public class VideoSavingForceVisible extends VideoSaving
	{
		/**Duration in milliseconds**/
		private var m_TargetVisibleDuration:Number;
		private var m_VisibleTimeStart:Number;
		private var m_VisibleTimeEnd:Number;
		private var m_DurationShown:Number;
		
		public function VideoSavingForceVisible(duration:Number = 5000)
		{
			super();
			m_TargetVisibleDuration = duration;
			this.visible = false;
		}
		
		public override function Initialize(overlaySkin:Class, backgroundSkin:Class, msgFont:String):void
		{
			m_Overlay = new AnimationClip(overlaySkin);
			m_Background = new AnimationClip(backgroundSkin);
			
			m_VideoSaving_ProgressBar.Initialize();
			m_VideoSaving_ProgressBar.SetPosition(m_VideoSaving_ProgressBar_Position);
			
			super.initializeTextBox(msgFont);
			
			this.addChild(m_Overlay);
			this.addChild(m_Background);
			this.addChild(m_VideoSaving_ProgressBar);
			this.addChild(m_TextBox);
			
			m_VideoSaving_ProgressBar.SetPosition(new Point(m_Background.width / 2 - m_VideoSaving_ProgressBar.width / 2,
			m_Background.height / 2 - m_VideoSaving_ProgressBar.height / 2));
			
			m_TextBox.x = m_Background.width / 2 - m_TextBox.width / 2;
			m_TextBox.y = m_Background.height - m_TextBox.height * 1.5;
		}
		
		public function get targetVisibleDuration():Number
		{
			return m_TargetVisibleDuration;
		}
		
		/**
		 * Returns duration since the panel was made visible in milliseconds.
		 */
		public function GetCurrentVisibleDuration():Number
		{
			if(m_VisibleTimeStart)
			{
				return m_VisibleTimeStart - (new Date()).time;
			}
			else
			{
				throw new Error("You must start the visible duration before you can get its value.");
			}
		}
		
		public function StartVisibleDuration():void
		{
			this.visible = true;
			m_VisibleTimeStart = (new Date()).time;
			
		}
		
		public function StopVisibleDuration():void
		{
			this.visible = false;
			m_VisibleTimeEnd = new Date().time;
		}
		
		public function GetTimeRemainingInCurrentDuration():Number
		{
			return m_TargetVisibleDuration - (new Date().time - m_VisibleTimeStart);
		}
		
		public function CanBeHidden():Boolean
		{
			return ((new Date().time) - m_VisibleTimeStart) > m_TargetVisibleDuration;
		}
	}
}
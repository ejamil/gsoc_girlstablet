package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.reviewvideo.videopanel
{
	import com.bpi.gsoc.components.recordvideo.JC_PauseButton;
	import com.bpi.gsoc.components.recordvideo.JC_PlayButton;
	import com.bpi.gsoc.components.recordvideo.JC_ProgressBarBackground;
	import com.bpi.gsoc.components.recordvideo.JC_ProgressBarForeground;
	import com.bpi.gsoc.framework.constants.GSOC_Info;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.commonassets.VideoBorder_RoundRect;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.commonassets.VideoMask;

	public class ReviewVideoPanel_JC_Skin extends ReviewVideoPanel_Skin
	{
		public function ReviewVideoPanel_JC_Skin()
		{	
			super(new VideoBorder_RoundRect(GSOC_Info.ColorTwo), new VideoMask(), new JC_PlayButton(), new JC_PauseButton(), new JC_ProgressBarForeground(), new JC_ProgressBarBackground());
		}
	}
}
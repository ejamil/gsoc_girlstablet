package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.reviewvideo.videopanel.progressbar
{
	import com.bpi.citrusas.images.AnimationClip;
	import com.bpi.citrusas.input.singleinput.Input;
	import com.bpi.gsoc.components.Video_Scrub;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.reviewvideo.videopanel.progressbar.events.Event_ProgressBar;
	import com.greensock.TweenLite;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.geom.Point;
	
	public class ProgressBar extends Sprite
	{
		protected static const PROGRESSBAR_HEIGHT:int = 64;
		
		protected var m_Foreground:MovieClip;
		protected var m_Background:MovieClip;
		protected var m_TweenCompleted:Boolean;
		
		protected var m_Scrubber:AnimationClip;
		protected var m_Scrubber_Offset:Point;
		protected var m_Scrubber_Scale:Number = 0.5;
		
		public function ProgressBar()
		{
			m_Scrubber = new AnimationClip();
			m_Scrubber_Offset = new Point(0, 0);
		}
		
		public function Initialize(foreground:MovieClip, background:MovieClip, width:Number):void
		{
			m_Foreground = foreground;
			m_Background = background;
			
			m_TweenCompleted = true;
			
			m_Scrubber = new AnimationClip(Video_Scrub);
			m_Scrubber.Scale(m_Scrubber_Scale);
			m_Scrubber.Frame = SessionInformation_GirlsTablet.Age;
			
			m_Foreground.height = PROGRESSBAR_HEIGHT;
			m_Foreground.width = width;
			m_Background.height = PROGRESSBAR_HEIGHT;
			m_Background.width = width;
			
			m_Scrubber_Offset.x = m_Foreground.width - m_Scrubber.Size.x * 0.5;
			m_Scrubber_Offset.y = m_Foreground.height * 0.5 - m_Scrubber.Size.y * 0.5;
			
			var barMask:Sprite = new Sprite();
			barMask.graphics.beginFill(0x000000);
			barMask.graphics.drawRect(0, 0, m_Foreground.width, m_Foreground.height);
			barMask.graphics.endFill();
			
			this.addChild(m_Background);
			this.addChild(m_Foreground);
			this.addChild(barMask);
			this.addChild(m_Scrubber);
			
			m_Foreground.mask = barMask;
		}
		
		public function CleanUp():void
		{
		}
		
		public function Update():void
		{
			checkProgressBar();
		}
		
		private function checkProgressBar():void
		{
			if(Input.IsMouseDown || Input.IsMouseClicked)
			{
				if(m_Background.hitTestPoint(Input.MousePosition.x, Input.MousePosition.y))
				{
					var backgroundPosition:Point = this.localToGlobal(new Point(m_Background.x, m_Background.y));
					var percentage:Number = (Input.MousePosition.x - backgroundPosition.x) / m_Background.width;
					this.dispatchEvent(new Event_ProgressBar(Event_ProgressBar.EVENT_SET_POSITION, percentage));
				}
			}
		}
		
		private function refreshScrubber():void
		{
			m_Scrubber.x = m_Foreground.x + m_Scrubber_Offset.x;
			m_Scrubber.y = m_Foreground.y + m_Scrubber_Offset.y;
		}
		
		public function ResetToBeginning():void
		{
			TweenLite.killTweensOf(m_Foreground);
			m_Foreground.x = -m_Foreground.width;
		}
		
		public function ResetToEnd():void
		{
			TweenLite.killTweensOf(m_Foreground);
			m_Foreground.x = 0;
		}
		
		public function UpdateWithTime(currentTime:Number, videoLength:Number):void
		{
			m_Foreground.x = -(1 - currentTime / videoLength) * m_Foreground.width;
			refreshScrubber();
		}
		
		public function UpdateWithPercent(pct:int):void
		{
			var xPos:Number = -(1 - pct / 100) * m_Foreground.width
			TweenLite.to(m_Foreground, 0.3, {x:xPos});
			refreshScrubber();
		}
		
		public function Destroy():void
		{
			this.removeChildren();
			m_Foreground = null;
		}
		
		public function get Size():Point
		{
			return new Point(m_Foreground.width, m_Foreground.height);
		}
	}
}
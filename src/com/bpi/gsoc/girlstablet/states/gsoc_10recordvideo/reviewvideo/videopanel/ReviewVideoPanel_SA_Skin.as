package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.reviewvideo.videopanel
{
	import com.bpi.gsoc.components.recordvideo.SA_PauseButton;
	import com.bpi.gsoc.components.recordvideo.SA_PlayButton;
	import com.bpi.gsoc.components.recordvideo.SA_ProgressBarBackground;
	import com.bpi.gsoc.components.recordvideo.SA_ProgressBarForeground;
	import com.bpi.gsoc.framework.constants.GSOC_Info;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.commonassets.VideoBorder_RoundRect;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.commonassets.VideoMask;
	
	public class ReviewVideoPanel_SA_Skin extends ReviewVideoPanel_Skin
	{
		public function ReviewVideoPanel_SA_Skin()
		{
			super(new VideoBorder_RoundRect(GSOC_Info.ColorOne), new VideoMask(), new SA_PlayButton(), new SA_PauseButton(), new SA_ProgressBarForeground(), new SA_ProgressBarBackground());
		}
	}
}
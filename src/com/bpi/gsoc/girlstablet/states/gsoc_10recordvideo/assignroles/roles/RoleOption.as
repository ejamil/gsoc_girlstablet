package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.assignroles.roles
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.citrusas.input.inputcontroller.InputController;
	import com.bpi.citrusas.sound.SoundHandler;
	import com.bpi.citrusas.text.TextBox;
	import com.bpi.citrusas.ui.draggableobject.DraggableObject;
	import com.bpi.citrusas.utils.Utils_Text;
	import com.bpi.gsoc.framework.constants.AgeConstants;
	import com.bpi.gsoc.framework.constants.GSOC_Info;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.sfx.GSOC_SFX;
	import com.greensock.TweenLite;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TouchEvent;
	import flash.text.TextField;
	
	public class RoleOption extends MovieClip
	{
		private static const DORMANT:String = "Dormant";
		private static const IN_USE:String = "In use";
		
		private var m_Icon:DraggableObject;
		private var m_Background:MovieClip;
		private var m_Description:TextBox;
		private var m_State:String;
		private var m_IconColor:uint;
		
		public function RoleOption(label:String, mcTemplate:Class, iconTintColor:uint, icon:Class = null)
		{
			super();

			m_Background = new mcTemplate();
			this.addChild(m_Background);
			
			Utils_Text.ConvertToEmbeddedFont(m_Background.roleTxt, GSOC_Info.HeaderFont);
			Utils_Text.ConvertToEmbeddedFont(m_Background.instTxt, GSOC_Info.BodyFont);
			
			
			
			(m_Background.roleTxt as TextField).wordWrap = true;
			(m_Background.instTxt as TextField).wordWrap = true;
			
			m_IconColor = iconTintColor;
			m_Background.roleTxt.text = label;
			m_Background.instTxt.text = RoleReference.GetDescriptionForRole(label);
			
			Utils_Text.EliminateDanglingWord(m_Background.instTxt);
			Utils_Text.FitTextToTextfield(m_Background.instTxt);
		
			var iconContainer:Sprite = RoleReference.GetIconForRole(label);
			TweenLite.to(iconContainer, 0, {colorTransform:{tint:m_IconColor, tintAmount:1}});
			var scalingFactor:Number = 1;
			switch(SessionInformation_GirlsTablet.Age)
			{
				case AgeConstants.KEY_JUNIORCADETTE:
					scalingFactor = 0.75;
					break;
				case AgeConstants.KEY_SENIORAMBASSADOR:
					scalingFactor = 0.75;
					break;
			}
			iconContainer.scaleX = iconContainer.scaleY = scalingFactor;
			iconContainer.x = this.width / 2 - iconContainer.width / 2;
			iconContainer.y = 30; // arbitrary based on graphics. will change, probably

			this.addChild(iconContainer);
			
			m_State = DORMANT;
			this.Activate();
		}
		
		public function Activate():void
		{
			this.addEventListener(InputController.DOWN, this.eh_SpawnIcon);
		}
		
		public function Deactivate():void
		{
			this.removeEventListener(InputController.DOWN, this.eh_SpawnIcon);
			m_State = DORMANT;
		}
		
		private function eh_SpawnIcon(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_SELECTROLES[SessionInformation_GirlsTablet.Age]);
			m_State = IN_USE;
			var icon:Sprite = RoleReference.GetIconForRole(m_Background.roleTxt.text);

			TweenLite.to(icon, 0, {colorTransform:{tint:m_IconColor, tintAmount:1}});
			
			m_Icon = new DraggableRoleIcon(icon, m_Background.roleTxt.text);
			
			m_Icon.AddEventListeners();
			
			if(e is MouseEvent)
			{
				icon.x = (e as MouseEvent).stageX - icon.width / 2;
				icon.y = (e as MouseEvent).stageY - icon.height / 2;
			}
			else if(e is TouchEvent)
			{	icon.x = (e as TouchEvent).stageX - icon.width / 2;
				icon.y = (e as TouchEvent).stageY - icon.height / 2;			
			}
			
			CitrusFramework.frameworkStage.addChild(icon);
			
			m_Icon.ForceStartDrag(e);
		}
	}
}
package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.assignroles
{
	import com.bpi.gsoc.components.recordvideo.AssignRoles_JC_Skin;
	import com.bpi.gsoc.components.recordvideo.JC_IconHome;
	import com.bpi.gsoc.components.recordvideo.JC_IconTarget;
	import com.bpi.gsoc.framework.constants.GSOC_Info;
	import com.bpi.gsoc.framework.constants.TeamNameSizes;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.gsocui.GSOC_ButtonToggle;
	import com.bpi.gsoc.framework.gsocui.GSOC_Header;

	public class AssignRoles_App_JC extends AssignRoles_App
	{
		public function AssignRoles_App_JC(addPagination:Boolean=false)
		{
			super(addPagination);
		}
		
		public override function Initialize():void
		{
			m_Skin = new AssignRoles_JC_Skin();
			m_Skin.header.gotoAndStop(SessionInformation_GirlsTablet.Track + 1);
			m_SubmitButton = m_Skin.nextBtn;
			m_SelectedRoleDisplayBackground = JC_IconTarget;
			m_RoleDisplayBackground = JC_IconHome;
			m_IconTintColor = GSOC_Info.ColorOne;
			m_RoleOptionYPos = m_Skin.header.height + 50;
			GSOC_ButtonToggle.ToggleButton(false, m_Skin.nextBtn, null, null);
			this.addChild(m_Skin);
			
			m_GSOCHeader = new GSOC_Header(m_Skin.header.paginationArea);
			GSOCheader.addTeamName(SessionInformation_GirlsTablet.TeamInfo.Team_Name, TeamNameSizes.JC_FONTSIZE);
			GSOCheader.addArrows(m_Skin.header.paginationArea.x, 0);
			super.Initialize();
		}
	}
}
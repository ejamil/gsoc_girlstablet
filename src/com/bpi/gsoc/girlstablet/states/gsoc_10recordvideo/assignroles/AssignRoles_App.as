package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.assignroles
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.citrusas.display.DistributedContainer;
	import com.bpi.citrusas.input.inputcontroller.InputController;
	import com.bpi.citrusas.sound.SoundHandler;
	import com.bpi.citrusas.states.StateControllerManager;
	import com.bpi.gsoc.framework.avatar.GSOC_Avatar_Builder;
	import com.bpi.gsoc.framework.constants.AgeConstants;
	import com.bpi.gsoc.framework.constants.TrackConstants;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.data.userdata.GSOC_UserData_Girl_DataKeys;
	import com.bpi.gsoc.framework.gsocui.GSOC_ButtonToggle;
	import com.bpi.gsoc.framework.sfx.GSOC_SFX;
	import com.bpi.gsoc.framework.states.app.GSOC_AppSkin;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.assignroles.roles.RoleOption;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.assignroles.roles.RoleReference;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.assignroles.teamdisplay.SelectedRolesDisplay;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.assignroles.teamdisplay.TeamDisplay_AssignRoles;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_IDs;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_RecordVideo;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.filters.DropShadowFilter;
	import flash.geom.Point;
	
	public class AssignRoles_App extends GSOC_AppSkin
	{	
		protected var m_TeamDisplay:TeamDisplay_AssignRoles;
		protected var m_AvailableRolesDisplay:DistributedContainer;
		protected var m_SelectedRolesDisplay:SelectedRolesDisplay;
		protected var m_RoleOptionYPos:Number;
		protected var m_TeamDisplayYPos:Number;
		protected var m_SelectedRolesYPos:Number;
		public static var NumRoles:int;
		protected var m_SubmitButton:MovieClip;
		protected var m_SelectedRoleDisplayBackground:Class;
		protected var m_RoleDisplayBackground:Class;
		protected var m_IconTintColor:uint;
		protected var m_SubmitButtonAvailable:Boolean;
		public static const VERTICAL_CONTAINER_SPACER:Number = 25;
		
		private var m_MaxHeight:Number;
		
		public function AssignRoles_App(addPagination:Boolean=false)
		{
			super(addPagination);			
		}
		
		public override function Init():void
		{
			switch(SessionInformation_GirlsTablet.Track)
			{
				case TrackConstants.CAREER:
					NumRoles = 5;
					break;
				case TrackConstants.TAKE_ACTION:
					NumRoles = 4;
					break;
			}
			
			m_AvailableRolesDisplay = new DistributedContainer(new Point(2000, 300), true, 50);
			
			m_SelectedRolesDisplay = new SelectedRolesDisplay(SessionInformation_GirlsTablet.TeamInfo.GetTeamMemberCount(), m_SelectedRoleDisplayBackground);

			switch (SessionInformation_GirlsTablet.Age)
			{
				 case AgeConstants.KEY_DAISYBROWNIE:
					 m_MaxHeight = 625;
					 break;
				 case AgeConstants.KEY_JUNIORCADETTE:
					 m_MaxHeight = 600;
					 break;
				 case AgeConstants.KEY_SENIORAMBASSADOR:
					 m_MaxHeight = 650;
					 break;
			}
			
			m_TeamDisplay = new TeamDisplay_AssignRoles(new Point(2500, m_MaxHeight), m_SelectedRolesDisplay, true, 50);
			
			this.addChild(m_SubmitButton);
			
			m_TeamDisplay.addEventListener(TeamDisplay_AssignRoles.ROLE_ASSIGNMENT_COMPLETE, this.showSubmitButton);
			m_TeamDisplay.addEventListener(TeamDisplay_AssignRoles.ROLE_ASSIGNMENT_INCOMPLETE, this.hideSubmitButton);

			m_SubmitButtonAvailable = false;
			SetupScreen();
			super.Init();
		}
		
		public override function DeInit():void
		{
			GSOC_ButtonToggle.ToggleButton(false, m_SubmitButton, null, this.disableSubmit);
			m_TeamDisplay.removeEventListener(TeamDisplay_AssignRoles.ROLE_ASSIGNMENT_COMPLETE, this.showSubmitButton);
			m_TeamDisplay.removeEventListener(TeamDisplay_AssignRoles.ROLE_ASSIGNMENT_INCOMPLETE, this.hideSubmitButton);
			m_TeamDisplay.DeInit();
			m_SelectedRolesDisplay.DeInit();
			m_TeamDisplay.DeInit();
		}
		
		private function showSubmitButton(e:Event):void
		{
			m_SubmitButtonAvailable = true;
			GSOC_ButtonToggle.ToggleButton(true, m_SubmitButton, this.enableSubmit, null);	
		}
		
		private function hideSubmitButton(e:Event):void
		{
			if(m_SubmitButtonAvailable)
			{
				m_SubmitButtonAvailable = false;
				GSOC_ButtonToggle.ToggleButton(false, m_SubmitButton, null, this.disableSubmit);
			}
		}
		
		protected function SetupScreen():void
		{
			for(var i:int = 0; i < SessionInformation_GirlsTablet.TeamInfo.GetGirlInformationList().length; i++)
			{
				if(SessionInformation_GirlsTablet.Track == TrackConstants.CAREER)
				{
					m_TeamDisplay.AddAvatar(GSOC_Avatar_Builder.BuildAvatarFromUserData(false, 
						SessionInformation_GirlsTablet.TeamInfo.GetGirlInformationFromIndex(i).GirlUserData, true, TrackConstants.CAREER), 
						SessionInformation_GirlsTablet.TeamInfo.GetGirlInformationFromIndex(i).Name);	
				}
				else
				{
					m_TeamDisplay.AddAvatar(GSOC_Avatar_Builder.BuildAvatarFromUserData(false, 
						SessionInformation_GirlsTablet.TeamInfo.GetGirlInformationFromIndex(i).GirlUserData, true, TrackConstants.TAKE_ACTION), 
						SessionInformation_GirlsTablet.TeamInfo.GetGirlInformationFromIndex(i).Name);	
				}
				
			}
			
			this.addChild(m_TeamDisplay);
			this.addChild(m_SelectedRolesDisplay);
			
			this.CreateRoles();
			// temp position for display
			m_AvailableRolesDisplay.x = CitrusFramework.frameworkStageSize.x / 2 - m_AvailableRolesDisplay.width / 2;
			m_AvailableRolesDisplay.y = m_RoleOptionYPos;
			
			// center team display
			m_TeamDisplay.x = CitrusFramework.frameworkStageSize.x / 2 - m_TeamDisplay.width / 2;
			m_TeamDisplay.y = m_RoleOptionYPos + m_AvailableRolesDisplay.height + VERTICAL_CONTAINER_SPACER + (m_MaxHeight - m_TeamDisplay.height);

			m_TeamDisplay.ExpandIconHitTargets();
			m_TeamDisplay.PositionIconTargetBoxes();
		}
		
		protected function CreateRoles():void
		{
			var roles:Vector.<String>;
			switch(SessionInformation_GirlsTablet.Track)
			{
				case TrackConstants.CAREER:
					roles = RoleReference.GetRoles_Career();
					break;
				case TrackConstants.TAKE_ACTION:
					roles = RoleReference.GetRoles_TakeAction();
					break;
				default:
					roles = RoleReference.GetRoles_TakeAction();
					break;
			}
			
			for(var i:int = 0; i < roles.length; i++)
			{
				var role:RoleOption = new RoleOption(roles[i], m_RoleDisplayBackground, m_IconTintColor);
				role.filters = [new DropShadowFilter(10, 45, 0, 0.6, 20, 20)];
				m_AvailableRolesDisplay.AddItem(role);
			}
			
			this.addChild(m_AvailableRolesDisplay);
		}
				
		private function eh_EnableSubmit(e:Event):void
		{
			this.enableSubmit();
		}
		
		private function enableSubmit():void
		{
			m_SubmitButton.addEventListener(InputController.CLICK, this.eh_MoveOn);
		}
		
		private function eh_DisableSubmit(e:Event):void
		{
			this.disableSubmit();
		}
		
		private function disableSubmit():void
		{
			m_SubmitButton.removeEventListener(InputController.CLICK, this.eh_MoveOn);
		}
		
		private function eh_MoveOn(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_CLICKS[SessionInformation_GirlsTablet.Age]);
			for(var i:int = 0; i < SessionInformation_GirlsTablet.TeamInfo.GetTeamMemberCount(); i++)
			{
				SessionInformation_GirlsTablet.TeamInfo.GetGirlInformationFromIndex(i).GirlUserData.SetData(GSOC_UserData_Girl_DataKeys.KEY_ROLES, m_TeamDisplay.GetRoleVectorForGirlAtIndex(i));
			}
			StateControllerManager.LeaveActiveState(StateController_IDs.STATECONTROLLER_RECORDVIDEO, StateController_StateIDs_RecordVideo.STATE_ASSIGNROLES);
			StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_RECORDVIDEO, StateController_StateIDs_RecordVideo.STATE_WRITESCRIPT);
		}
	}
}
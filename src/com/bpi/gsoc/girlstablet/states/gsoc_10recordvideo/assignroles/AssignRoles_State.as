package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.assignroles
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.gsoc.framework.constants.AgeConstants;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.gsocui.keyboards.GSOC_Keyboards;
	import com.bpi.gsoc.framework.states.app.GSOC_AppState;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_RecordVideo;
	
	public class AssignRoles_State extends GSOC_AppState
	{
		private var m_App:AssignRoles_App;
		
		public function AssignRoles_State()
		{
			super(StateController_StateIDs_RecordVideo.STATE_ASSIGNROLES);
		}
		
		public override function Initialize():void
		{
			
		}
		
		protected override function Init():void
		{
			switch(SessionInformation_GirlsTablet.Age)
			{
				case AgeConstants.KEY_DAISYBROWNIE:
					m_App = new AssignRoles_App_DB();
					break;
				case AgeConstants.KEY_JUNIORCADETTE:
					m_App = new AssignRoles_App_JC();
					break;
				case AgeConstants.KEY_SENIORAMBASSADOR:
					m_App = new AssignRoles_App_SA();
					break;
			}
			
			m_App.Initialize();
			m_App.Init();	
			
			GSOC_Keyboards.SetKeyboardType(GSOC_Keyboards.KEYBOARDTYPE_REGULAR);
			CitrusFramework.contentLayer.addChild(m_App);
		}
		
		protected override function DeInit():void
		{	
			CitrusFramework.contentLayer.removeChild(m_App);
			m_App.DeInit();
			m_App = null;
			
			super.EndDeInit();
		}
	}
}
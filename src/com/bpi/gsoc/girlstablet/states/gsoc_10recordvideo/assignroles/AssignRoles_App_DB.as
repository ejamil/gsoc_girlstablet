package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.assignroles
{
	import com.bpi.gsoc.components.recordvideo.AssignRoles_DB_Skin;
	import com.bpi.gsoc.components.recordvideo.DB_IconHome;
	import com.bpi.gsoc.components.recordvideo.DB_IconTarget;
	import com.bpi.gsoc.framework.constants.GSOC_Info;
	import com.bpi.gsoc.framework.constants.TeamNameSizes;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.gsocui.GSOC_ButtonToggle;
	import com.bpi.gsoc.framework.gsocui.GSOC_Header;

	public class AssignRoles_App_DB extends AssignRoles_App
	{
		public function AssignRoles_App_DB(addPagination:Boolean=false)
		{
			super(addPagination);	
		}
		
		public override function Initialize():void
		{
			m_Skin = new AssignRoles_DB_Skin();
			m_Skin.header.gotoAndStop(SessionInformation_GirlsTablet.Track + 1);
			m_SubmitButton = m_Skin.submit_Button;
			m_SelectedRoleDisplayBackground = DB_IconTarget;
			m_IconTintColor = GSOC_Info.ColorTwo;
			m_RoleDisplayBackground = DB_IconHome;
			m_RoleOptionYPos = m_Skin.header.height + 50;
			GSOC_ButtonToggle.ToggleButton(false, m_Skin.submit_Button, null, null);
			this.addChild(m_Skin);
			
			m_GSOCHeader = new GSOC_Header(m_Skin.header.paginationArea);
			GSOCheader.addTeamName(SessionInformation_GirlsTablet.TeamInfo.Team_Name, TeamNameSizes.DB_FONTSIZE);
			GSOCheader.addArrows(m_Skin.header.paginationArea.x, 0);
			
			super.Initialize();
		}
	}
}
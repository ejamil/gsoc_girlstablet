package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.assignroles.events
{
	import com.bpi.citrusas.ui.draggableobject.events.Event_DraggableObject;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.assignroles.roles.DraggableRoleIcon;
	
	import flash.geom.Point;
	
	public class Event_RoleIconDropped extends Event_DraggableObject
	{
		public static const FREE_ROLE_ICON_DROPPED:String = "ROLE ICON DROPPED";
		public static const CAPTURED_ROLE_ICON_DROPPED:String = "CAPTURED ROLE ICON DROPPED";
		private var m_RoleIcon:DraggableRoleIcon;
		
		public function Event_RoleIconDropped(type:String, currentPosition:Point, roleIcon:DraggableRoleIcon, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, currentPosition, roleIcon.draggableObject, bubbles, cancelable);
			
			m_RoleIcon = roleIcon;
		}
		
		public function get roleIcon():DraggableRoleIcon
		{
			return m_RoleIcon;
		}
	}
}
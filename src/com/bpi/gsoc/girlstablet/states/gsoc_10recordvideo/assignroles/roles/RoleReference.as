package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.assignroles.roles
{
	import com.bpi.gsoc.components.recordvideo.ActorIcon;
	import com.bpi.gsoc.components.recordvideo.DirectorIcon;
	import com.bpi.gsoc.components.recordvideo.EditorIcon;
	import com.bpi.gsoc.components.recordvideo.GuestHostIcon;
	import com.bpi.gsoc.components.recordvideo.GuestIcon;
	import com.bpi.gsoc.components.recordvideo.HostIcon;
	import com.bpi.gsoc.components.recordvideo.WriterIcon;
	
	import flash.display.MovieClip;
	import flash.utils.Dictionary;

	public class RoleReference
	{
		private static var _images:Dictionary = new Dictionary();
		private static var _descriptions:Dictionary = new Dictionary();
		private static var _numRoles:int = 0;
		private static var _vect_Roles_TakeAction:Vector.<String>;
		private static var _vect_Roles_Career:Vector.<String>;
		
		private static function Initialize():void
		{
			_images[RoleKeys.ACTOR] = ActorIcon;
			_images[RoleKeys.DIRECTOR] = DirectorIcon;
			_images[RoleKeys.EDITOR] = EditorIcon;
			_images[RoleKeys.WRITER] = WriterIcon;
			_images[RoleKeys.GUEST] = GuestIcon;
			_images[RoleKeys.HOST] = HostIcon;
			_images[RoleKeys.GUESTORHOST] = GuestHostIcon;
			
			_descriptions[RoleKeys.ACTOR] = "Talk about Take Action in front of the camera";
			_descriptions[RoleKeys.DIRECTOR] = "Hold the INSPIRE pad & record the video";
			_descriptions[RoleKeys.EDITOR] = "Choose music & design";
			_descriptions[RoleKeys.WRITER] = "Use script suggestions and write your own ideas";
			_descriptions[RoleKeys.GUEST] = "Answer questions in front of the camera";
			_descriptions[RoleKeys.HOST] = "Ask guests questions in front of the camera";
			
			for(var role:String in _images)
			{
				_numRoles++;
			}
		}
		
		private static function InitCareerRoles():void
		{
			_vect_Roles_Career = new Vector.<String>();
			_vect_Roles_Career.push(RoleKeys.HOST);
			_vect_Roles_Career.push(RoleKeys.DIRECTOR);
			_vect_Roles_Career.push(RoleKeys.GUEST);
			_vect_Roles_Career.push(RoleKeys.EDITOR);
			_vect_Roles_Career.push(RoleKeys.WRITER);
		}
		
		private static function InitTakeActionRoles():void
		{
			_vect_Roles_TakeAction = new Vector.<String>();
			_vect_Roles_TakeAction.push(RoleKeys.ACTOR);
			_vect_Roles_TakeAction.push(RoleKeys.DIRECTOR);
			_vect_Roles_TakeAction.push(RoleKeys.EDITOR);
			_vect_Roles_TakeAction.push(RoleKeys.WRITER);
		}
	
		public static function GetIconForRole(roleKey:String):MovieClip
		{
			if(_numRoles == 0)
			{
				Initialize();
			}
			return new _images[roleKey]();
		}
		
		public static function GetDescriptionForRole(roleKey:String):String
		{
			if(_numRoles == 0)
			{
				Initialize();
			}
			return _descriptions[roleKey];
		}
		
		public static function GetRoles_Career():Vector.<String>
		{
			if(!_vect_Roles_Career)
			{
				InitCareerRoles();
			}
			return _vect_Roles_Career;
		}
		
		public static function GetRoles_TakeAction():Vector.<String>
		{
			if(!_vect_Roles_TakeAction)
			{
				InitTakeActionRoles();
			}
			return _vect_Roles_TakeAction;
		}
	}
}
package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.assignroles.roles
{
	import com.bpi.citrusas.input.inputcontroller.InputController;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.assignroles.events.Event_RoleIconDropped;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	import flash.events.TouchEvent;
	import flash.geom.Point;
	
	public class CapturedDraggableRoleIcon extends DraggableRoleIcon
	{
		public static var CapturedDraggableRoleEventDispatcher:EventDispatcher = new EventDispatcher();
		
		public function CapturedDraggableRoleIcon(objToDrag:DisplayObject, roleKey:String, index:int, objToClickToDrag:DisplayObject=null)
		{
			super(objToDrag, roleKey);
		}
		
		override protected function eh_StopDrag(e:Event):void
		{
			if(e is MouseEvent || (e is TouchEvent && (e as TouchEvent).touchPointID == m_TouchID))
			{
				this.m_DraggableObject.stage.removeEventListener(InputController.UP, eh_StopDrag);
				this.m_DraggableObject.stage.removeEventListener(InputController.MOVE, eh_UpdateShapePos);
			}
			
			// this event triggers icon removal and tweening
			this.dispatchEvent(new Event_RoleIconDropped(Event_RoleIconDropped.CAPTURED_ROLE_ICON_DROPPED, new Point(this.m_DraggableObject.x, this.m_DraggableObject.y), this));
			// this one triggers show/hide of submit button
			DraggableRoleEventDispatcher.dispatchEvent(new Event_RoleIconDropped(Event_RoleIconDropped.CAPTURED_ROLE_ICON_DROPPED, new Point(this.m_DraggableObject.x, this.m_DraggableObject.y), this));
		}
	}
}
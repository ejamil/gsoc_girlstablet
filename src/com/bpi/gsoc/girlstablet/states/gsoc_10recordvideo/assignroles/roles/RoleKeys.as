package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.assignroles.roles
{
	public class RoleKeys
	{
		public static const WRITER:String = "WRITER";
		public static const DIRECTOR:String = "DIRECTOR";
		public static const EDITOR:String = "EDITOR";
		public static const ACTOR:String = "ACTOR";
		public static const HOST:String = "HOST";
		public static const GUEST:String = "GUEST";
		public static const GUESTORHOST:String = "GUEST OR HOST"; // special case - GUEST and HOST icons appear on assign roles screen, but the callout shows the combined icon
	}
}
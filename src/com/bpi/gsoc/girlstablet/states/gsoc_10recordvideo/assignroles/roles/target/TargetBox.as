package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.assignroles.roles.target
{
	import com.bpi.citrusas.ui.draggableobject.DraggableObject;
	import com.bpi.citrusas.utils.Utils_Math;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.assignroles.events.Event_RoleIconDropped;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.assignroles.roles.CapturedDraggableRoleIcon;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.assignroles.roles.DraggableRoleIcon;
	import com.greensock.TweenLite;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.geom.Point;
	
	public class TargetBox extends Sprite
	{
		private static const DEFAULT_DIMS:Point = new Point(200, 175);
		
		private var m_Contents:Vector.<Sprite>;
		private var m_ContentIDs:Vector.<String>;
		private var m_Background:MovieClip;
		private var m_HitArea:Sprite;
		private var m_IconPositions:Vector.<Point>;
		
		public static var TargetBoxDispatcher:EventDispatcher = new EventDispatcher();
		
		public function TargetBox(background:MovieClip=null)
		{
			super();
			
			if(background)
			{
				m_Background = background;
				// keep this filter line until CB confirms (like, really confirms) that the target boxes don't need drop shadows
				//m_Background.filters = [new DropShadowFilter(10, 45, 0, 0.6, 20, 20)];
				this.addChild(m_Background);
			}
			else
			{
				// for debugging purposes, add a background
				m_Background = new MovieClip();
				m_Background.graphics.beginFill(0xff0000, 1);
				m_Background.graphics.drawRoundRect(0, 0, Utils_Math.RandomInt(100, 500), Utils_Math.RandomInt(100, 500), 50, 50);
				m_Background.graphics.endFill();
				this.addChild(m_Background);
			}
			
			m_HitArea = new Sprite();
			m_HitArea.graphics.beginFill(0x000000, 0);
			m_HitArea.graphics.drawRect(0, 0, m_Background.width, m_Background.height);
			m_HitArea.graphics.endFill();
			m_HitArea.mouseEnabled = false;
			this.addChild(m_HitArea);
			
			IconShuffler.Init(m_Background.getBounds(m_Background), 10);
			
			m_Contents = new Vector.<Sprite>();
			m_ContentIDs = new Vector.<String>();
			m_IconPositions = new Vector.<Point>();
		}
	
		public function AddItem(icon:DraggableRoleIcon):void
		{
			var s:Sprite = icon.draggableObject as Sprite;
			if(m_ContentIDs.indexOf(icon.roleName) > -1)
			{
				s.parent.removeChild(s);
				s = null;
				return;
			}
			if(! (m_Contents.indexOf(s) > -1))
			{
				m_ContentIDs.push(icon.roleName);
				m_Background.addChild(s);
				var localPt:Point = m_Background.globalToLocal(new Point(s.x, s.y));
				s.x = localPt.x;
				s.y = localPt.y;
				var draggableIcon:DraggableObject = new CapturedDraggableRoleIcon(s as Sprite, icon.roleName, m_ContentIDs.length - 1);
				draggableIcon.AddEventListeners();
				draggableIcon.addEventListener(Event_RoleIconDropped.CAPTURED_ROLE_ICON_DROPPED, this.eh_IconDropped);
				
				m_Contents.push(s);
				IconShuffler.MoveToIconConfiguration(m_Contents, this.eh_CalculateIconPositions);
			}
		}
		
		public function RemoveItem(s:DraggableRoleIcon):void
		{
			TweenLite.to(s.draggableObject, 0.5, {alpha:0, onComplete:popIcon, onCompleteParams:[s.draggableObject]});
			if(m_Contents.indexOf(s.draggableObject as Sprite) > -1) m_Contents.removeAt(m_Contents.indexOf(s.draggableObject as Sprite));
			if(m_ContentIDs.indexOf(s.roleName) > -1) m_ContentIDs.removeAt(m_ContentIDs.indexOf(s.roleName));
			IconShuffler.MoveToIconConfiguration(m_Contents, this.eh_CalculateIconPositions);
		}
		
		private function popIcon(s:Sprite):void
		{
			if(s.parent)
			{
				s.parent.removeChild(s);
			}
			s = null;
		}
		
		public function get iconHitArea():Sprite
		{
			return m_HitArea;
		}
		
		public function IsNotEmpty():Boolean
		{
			return m_Contents.length > 0;
		}
		
		public function GetRolesVector():Vector.<String>
		{
			return m_ContentIDs;
		}
		
		private function eh_CalculateIconPositions(e:Event=null):void
		{
			m_IconPositions = new Vector.<Point>();
			for(var j:int = 0; j < m_Contents.length; j++)
			{
				m_IconPositions.push(new Point(m_Contents[j].x, m_Contents[j].y));
			}
		}
		
		private function eh_IconDropped(e:Event_RoleIconDropped):void
		{
			if(e.type == Event_RoleIconDropped.FREE_ROLE_ICON_DROPPED) return; 

			var s:Sprite = e.draggableObject as Sprite;
			
			if(!(m_HitArea.hitTestObject(e.draggableObject)))
			{
				RemoveItem(e.roleIcon);
			}
			else
			{
				var index:int = m_Contents.indexOf(s);
				
				if(this.IsNotEmpty() && index < m_IconPositions.length && index != -1)
				{
					TweenLite.to(s, 0.5, {x:m_IconPositions[index].x, y:m_IconPositions[index].y});
				}				
				
			}
		}
		
		public function DeInit():void
		{
			m_ContentIDs = null;
			this.removeChild(m_Background);
			for (var i:int = 0; i < m_Background.numChildren; i++)
			{
				m_Background.removeChildAt(i);
			}
			m_Background = null;
			this.removeChild(m_HitArea);
			m_HitArea = null;
			m_IconPositions = null;
		}
	}
}
package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.writescript.classes
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.greensock.TimelineMax;
	import com.greensock.TweenAlign;
	import com.greensock.TweenLite;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	
	public class StaggeredAnimation extends Sprite
	{
		private var m_CascadeAnimation:TimelineMax;
		private static const TWEEN_DURATION:Number = 0.5;
		private static const STAGGER_DELAY:Number = .1;
		
		public function StaggeredAnimation(mc:MovieClip)
		{
			super();
			m_CascadeAnimation = new TimelineMax();
			var tweens:Array = [];
			
			for (var i:int = mc.numChildren - 1; i >= 0; i--)
			{
				mc.getChildAt(i).x = CitrusFramework.frameworkStageSize.x;
				tweens.push(TweenLite.fromTo(mc.getChildAt(i), TWEEN_DURATION, {x:CitrusFramework.frameworkStageSize.x}, {x:0}));
			}
			
			m_CascadeAnimation.appendMultiple(tweens, 0, TweenAlign.START, STAGGER_DELAY);
			tweens = [];
			this.AnimateOff();
		}
		
		public function AnimateOn():void
		{
			m_CascadeAnimation.play(0);
		}
		
		public function AnimateOff():void
		{
			m_CascadeAnimation.reverse(0);
		}
		
		public function Destroy():void
		{
			m_CascadeAnimation.kill();
		}
	}
}
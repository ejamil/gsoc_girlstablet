package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.writescript
{
	import com.bpi.citrusas.input.inputcontroller.InputController;
	import com.bpi.citrusas.sound.SoundHandler;
	import com.bpi.citrusas.states.StateControllerManager;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.sfx.GSOC_SFX;
	import com.bpi.gsoc.framework.states.app.GSOC_AppSkin;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.RoleCallout;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.assignroles.roles.RoleKeys;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.writescript.classes.StaggeredAnimation;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_IDs;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_RecordVideo;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	
	public class WriteScript_App extends GSOC_AppSkin
	{
		protected var m_NextButton:MovieClip;
		protected var m_RoleCallout:RoleCallout;
		protected var m_Instructions:StaggeredAnimation;
		
		public function WriteScript_App(addPagination:Boolean=false)
		{
			super(addPagination);	
		}
		
		public override function Initialize():void
		{
			super.Initialize();
		}
		
		public override function Init():void
		{
			m_NextButton.addEventListener(InputController.CLICK, this.nextScreen);
			SetupScreen();
			
			m_RoleCallout.AnimateIn();
			super.Init();
		}
		
		public override function DeInitialize():void
		{
			m_RoleCallout.AnimateOut();
			m_Instructions.Destroy();
		}
		
		protected function SetupScreen():void
		{
			m_RoleCallout.FillForRole(RoleKeys.WRITER);
			this.addChild(m_RoleCallout);
	
			m_GSOCHeader.arrows.animateArrows();
			m_Instructions.AnimateOn();
		}
		
		protected function nextScreen(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_CLICKS[SessionInformation_GirlsTablet.Age]);
			StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_RECORDVIDEO, StateController_StateIDs_RecordVideo.STATE_WARMUP);
		}
	}
}
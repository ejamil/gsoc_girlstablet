package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.writescript
{
	import com.bpi.citrusas.text.TextBox;
	import com.bpi.gsoc.components.recordvideo.WriteScript_DB_Skin;
	import com.bpi.gsoc.framework.constants.AgeColors;
	import com.bpi.gsoc.framework.constants.AgeFontLinkages;
	import com.bpi.gsoc.framework.constants.TrackConstants;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.data.userdata.GSOC_UserData_Team_DataKeys;
	import com.bpi.gsoc.framework.gsocui.GSOC_Header;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.RoleCallout;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.writescript.classes.StaggeredAnimation;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.geom.Point;

	public class WriteScript_App_DB extends WriteScript_App
	{
		public function WriteScript_App_DB(addPagination:Boolean=false)
		{
			super(addPagination);	
		}
		
		public override function Initialize():void
		{
			m_Skin = new WriteScript_DB_Skin();
			m_Skin.header.gotoAndStop(SessionInformation_GirlsTablet.Track + 1);
			m_Skin.scriptContent.gotoAndStop(SessionInformation_GirlsTablet.Track + 1);
			m_NextButton = m_Skin.nextBtn;
			m_RoleCallout = new RoleCallout(m_Skin.header.paginationArea.x, m_Skin.header.paginationArea.y, m_Skin.header.paginationArea.width, m_Skin.header.paginationArea.height);
			
			if(SessionInformation_GirlsTablet.Track == TrackConstants.TAKE_ACTION)
			{
				this.setUpTextFields();
			}
			
			m_Instructions = new StaggeredAnimation(m_Skin.scriptContent);
			this.addChild(m_Skin);
			
			m_GSOCHeader = new GSOC_Header(m_Skin.header.paginationArea);
			GSOCheader.addArrows(m_Skin.header.paginationArea.x, 0);
			super.Initialize();
		}
		
		private function formatScriptContent(container:MovieClip, fields:Vector.<TextBox>, markers:Vector.<MovieClip>):void
		{
			markers.push(container.fillInTarget1.textFieldMarker,
				container.fillInTarget2.textFieldMarker,
				container.fillInTarget3.textFieldMarker,
				container.fillInTarget4.textFieldMarker,
				container.fillInTarget5.textFieldMarker1, container.fillInTarget5.textFieldMarker2, container.fillInTarget5.textFieldMarker3);
			
			for (var i:int = 0; i < markers.length; i++)
			{
				var tb:TextBox = new TextBox();
				tb.Initialize(new Point(markers[i].width, markers[i].height), 45, AgeColors.DB_BLUE, false);
				tb.ToggleAutoSize(true);
				tb.Font = AgeFontLinkages.DB_HEADER_FONT;
				tb.x = markers[i].x;
				tb.y = markers[i].y;
				fields.push(tb);
			}
		}
		
		private function fillScriptContent(container:MovieClip, fields:Vector.<TextBox>, markers:Vector.<MovieClip>):void
		{
			// first space: team name
			fields[0].Text = SessionInformation_GirlsTablet.TeamInfo.Team_Name.toLocaleUpperCase();
			container.fillInTarget1.addChild(fields[0]);	
			
			//second space: project is called (not filled in)
			
			// third space: issue
			fields[1].Text = SessionInformation_GirlsTablet.TeamInfo.Team_Issue.Name.toLocaleUpperCase();
			if(fields[1].Text.length == 0)
			{
				// the issue name wasn't filled in, use the text instead
				fields[1].Text = SessionInformation_GirlsTablet.TeamInfo.Team_Issue.Text.toLocaleUpperCase();
			}
			container.fillInTarget2.addChild(fields[1]);
			
			// fourth space: important because
			fields[2].Text = SessionInformation_GirlsTablet.TeamInfo.Team_UserData.GetData(GSOC_UserData_Team_DataKeys.KEY_PROJECT_IMPORTANT_ANSWER) != null ? SessionInformation_GirlsTablet.TeamInfo.Team_UserData.GetData(GSOC_UserData_Team_DataKeys.KEY_PROJECT_IMPORTANT_ANSWER).toString().toLocaleUpperCase() : "";
			container.fillInTarget3.addChild(fields[2]);
			
			// fifth space: root cause
			fields[3].Text = SessionInformation_GirlsTablet.TeamInfo.Team_Issue.RootCause_Text.toLocaleUpperCase();
			container.fillInTarget3.addChild(fields[3]);
			
			// sixth, seventh and eighth spaces: community resources
			var building1:String = "Classroom";
			var building2:String = "Girl Scout Meeting";
			var building3:String = "Home";
			if(SessionInformation_GirlsTablet.TeamInfo.Team_UserData.GetData(GSOC_UserData_Team_DataKeys.KEY_COMMUNITYRESOURCES_BUILDING_LIST) != null)
			{
				building1 = SessionInformation_GirlsTablet.TeamInfo.Team_UserData.GetData(GSOC_UserData_Team_DataKeys.KEY_COMMUNITYRESOURCES_BUILDING_LIST)[0] != null ? SessionInformation_GirlsTablet.TeamInfo.Team_UserData.GetData(GSOC_UserData_Team_DataKeys.KEY_COMMUNITYRESOURCES_BUILDING_LIST)[0] : "Classroom";
				building2 = SessionInformation_GirlsTablet.TeamInfo.Team_UserData.GetData(GSOC_UserData_Team_DataKeys.KEY_COMMUNITYRESOURCES_BUILDING_LIST)[1] != null ? SessionInformation_GirlsTablet.TeamInfo.Team_UserData.GetData(GSOC_UserData_Team_DataKeys.KEY_COMMUNITYRESOURCES_BUILDING_LIST)[1] : "Girl Scout Meeting";
				building3 = SessionInformation_GirlsTablet.TeamInfo.Team_UserData.GetData(GSOC_UserData_Team_DataKeys.KEY_COMMUNITYRESOURCES_BUILDING_LIST)[2] != null ? SessionInformation_GirlsTablet.TeamInfo.Team_UserData.GetData(GSOC_UserData_Team_DataKeys.KEY_COMMUNITYRESOURCES_BUILDING_LIST)[2] : "Home";
			}
			
			if(building1) fields[4].Text = building1.toLocaleUpperCase();
			if(building2) fields[5].Text = building2.toLocaleUpperCase();
			if(building3) fields[6].Text = building3.toLocaleUpperCase();
			
			container.fillInTarget5.addChild(fields[4]);
			container.fillInTarget5.addChild(fields[5]);
			container.fillInTarget5.addChild(fields[6]);
			// ninth space is empty
			
			for(var i:int = 0; i < fields.length; i++)
			{
				if(fields[i].TextFieldSize.x > markers[i].width)
				{
					// make sure long entries don't exceed the bounds of the dotted lines
					this.sizeTextFieldToMarker(fields[i], markers[i]);
				}
				else
				{
					// make sure short entries are centered on the dotted line
					fields[i].x = fields[i].x + (markers[i].width / 2 - fields[i].width / 2);
				}
			}
		}
		
		private function setUpTextFields():void
		{
			var container:MovieClip = m_Skin.scriptContent;
			var fields:Vector.<TextBox> = new Vector.<TextBox>();
			var markers:Vector.<MovieClip> = new Vector.<MovieClip>();
			
			this.formatScriptContent(container, fields, markers);
			this.fillScriptContent(container, fields, markers);
		}
		
		private function sizeTextFieldToMarker(textField:TextBox, boundingMarker:Sprite):void
		{
			var origTFSize:Number = textField.TextFieldSize.y;
			while(textField.TextFieldSize.x > boundingMarker.width)
			{
				textField.FontSize -=1;
			}
			
			textField.x = textField.x + boundingMarker.width / 2 - textField.TextBoxTextField.textWidth / 2;
			textField.y = textField.y + (origTFSize - textField.TextFieldSize.y);
		}
	}
}
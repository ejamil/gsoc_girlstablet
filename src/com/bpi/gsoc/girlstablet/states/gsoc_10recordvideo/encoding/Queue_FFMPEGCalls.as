package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.encoding
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.citrusas.networking.client.Client;
	import com.bpi.citrusas.networking.events.Event_ClientNetworkMessage;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.data.videoupload.VideoUpload_Message;
	import com.bpi.gsoc.framework.data.videoupload.VideoUpload_MessageBuilder;
	import com.bpi.gsoc.framework.networking.NetworkMessage_Types;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.encoding.events.Event_FFMPEGProgress;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.encoding.ui.EncodingProgressGUI;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.filesystem.File;
	import flash.utils.ByteArray;

	public class Queue_FFMPEGCalls extends EventDispatcher
	{
		private static var ms_Queue:Vector.<FFMPEGCall>;
		private static var ms_CurrentIndex:int;
		private static var ms_GUI:EncodingProgressGUI;
		public static var QueueEventDispatcher:EventDispatcher;
		
		private static const ms_QUEUE_COMPLETE:String = "QUEUE COMPLETE";
		
		public static function Initialize():void
		{
			
			ms_Queue = new Vector.<FFMPEGCall>();
			ms_CurrentIndex = 0;
			ms_GUI = new EncodingProgressGUI();
			QueueEventDispatcher = new EventDispatcher();
			
			FFMPEGExporter.EventDispatcher_FFMPEGProgress.addEventListener(Event_FFMPEGProgress.ENCODING_PROGRESS, eh_ParseNetworkMessage);
		}
		
		public static function AddCall(call:FFMPEGCall):void
		{
			ms_Queue.push(call);
		}

		public static function StartQueue():void
		{
			if(CitrusFramework.frameworkConfigXML.videoEncodingConfigs.showEncodingProgressGUI == "true")
			{
				CitrusFramework.frameworkStage.addChild(ms_GUI);
			}
			
			FFMPEGExporter.Initialize(File.applicationDirectory.resolvePath("data\\ffmpeg\\ffmpeg.exe"));
			executeCurrentCall();
		}
		
		public static function eh_CallNext(e:Event):void
		{
			ms_CurrentIndex = ms_CurrentIndex + 1;
			executeCurrentCall();
		}
		
		public static function executeCurrentCall():void
		{
			if(ms_CurrentIndex >= ms_Queue.length) 
			{
				clearAndReset();
				// let the leader know we're done
				var networkMessageByteArray:ByteArray = VideoUpload_MessageBuilder.BuildNetworkMessage(100, FFMPEGCallIDs.FINISHED_ENCODING, false, SessionInformation_GirlsTablet.TabletIndex);
				Client.ClientEventDispatcher.dispatchEvent(new Event_ClientNetworkMessage(Event_ClientNetworkMessage.EVENT_SEND_NETWORK_MESSAGE, NetworkMessage_Types.TELL_SERVER_UPDATE_VIDEOUPLOADPROGRESS, networkMessageByteArray));
				// let the rest of the girls' tablet program know we're done
				QueueEventDispatcher.dispatchEvent(new Event_FFMPEGProgress(Event_FFMPEGProgress.ENCODING_PROGRESS, networkMessageByteArray));
				UploadVideo.beginVideoUpload();
				if(CitrusFramework.frameworkStage.contains(ms_GUI)) CitrusFramework.frameworkStage.removeChild(ms_GUI);
			}
			else
			{
				ms_GUI.Reset();
				FFMPEGExporter.StartFFMPEG(ms_Queue[ms_CurrentIndex], eh_CallNext, false);
			}
		}
		
		private static function clearAndReset():void
		{
			ms_CurrentIndex = 0;
			ms_Queue = new Vector.<FFMPEGCall>();
		}
		
		public static function ClearQueue():void
		{
			clearAndReset();
		}
		
		private static function eh_ParseNetworkMessage(e:Event_FFMPEGProgress):void
		{
			var videoUploadMessage:VideoUpload_Message = VideoUpload_MessageBuilder.ParseNetworkMessage(e.message);
			ms_GUI.Update(videoUploadMessage.Step_PercentComplete, videoUploadMessage.Step_Label);
		}
	}
}
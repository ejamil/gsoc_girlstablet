package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.encoding
{
	public class FFMPEGCall
	{
		private var m_CallArguments:Vector.<String>;
		private var m_TotalNumFrames:uint;
		private var m_CallID:String;
		
		public function FFMPEGCall(args:Vector.<String>, totalNumFrames:uint, progressID:String)
		{
			m_CallArguments = args;
			m_TotalNumFrames = totalNumFrames;
			m_CallID = progressID;
		}
		
		
		public function get arguments():Vector.<String>
		{
			return m_CallArguments;
		}
		
		public function get numFrames():uint
		{
			return m_TotalNumFrames;
		}
		
		public function get callID():String
		{
			return m_CallID;
		}
	}
}
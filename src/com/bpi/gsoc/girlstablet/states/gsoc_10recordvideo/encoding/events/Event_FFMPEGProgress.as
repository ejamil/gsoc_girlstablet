package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.encoding.events
{
	import flash.events.Event;
	import flash.utils.ByteArray;
	
	public class Event_FFMPEGProgress extends Event
	{
		private var m_NetworkMessage:ByteArray;
		
		public static const ENCODING_PROGRESS:String = "ENCODING PROGRESS";
		
		public function Event_FFMPEGProgress(type:String, ba:ByteArray, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			m_NetworkMessage = ba;
			
		}
		
		public function get message():ByteArray
		{
			return m_NetworkMessage;
		}
		
		
	}
}
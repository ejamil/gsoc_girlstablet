package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.encoding
{
	public class CardExportParams
	{
		private var m_SourcePath:String;
		private var m_OutputPath:String;
		private var m_CardDuration:Number;
		private var m_FadeInDuration:Number;
		private var m_FadeOutDuration:Number;
		private var m_FinalOutputLink:String;
		
		/**
		 * Data structure to consolidate parameters for exporting title/credtis/other cards via ffmpeg.
		 */
		public function CardExportParams(sourcePath:String, outputPath:String, cardDuration:Number, fadeInDuration:Number, fadeOutDuration:Number, finalOutput:String)
		{
			m_SourcePath = sourcePath;
			m_OutputPath = outputPath;
			m_CardDuration = cardDuration;
			m_FadeInDuration = fadeInDuration;
			m_FadeOutDuration = fadeOutDuration;
			m_FinalOutputLink = finalOutput;
		}
		
		public function GetParamsAsStringVector():Vector.<String>
		{
			var returnVect:Vector.<String> = new Vector.<String>();
			
			returnVect.push(sourcePath, outputPath, cardDuration.toString(), fadeInDuration.toString(), fadeOutDuration.toString());
			
			return returnVect;
		}
		
		public function get sourcePath():String
		{
			return m_SourcePath;
		}
		
		public function set sourcePath(s:String):void
		{
			m_SourcePath = s;
		}
		
		public function get outputPath():String
		{
			return m_OutputPath;
		}
		
		public function set outputPath(s:String):void
		{
			m_OutputPath = s;
		}
		
		public function get cardDuration():Number
		{
			return m_CardDuration;
		}
		
		public function set cardDuration(n:Number):void
		{
			m_CardDuration = n;
		}
		
		
		public function get fadeInDuration():Number
		{
			return m_FadeInDuration;
		}
		
		public function set fadeInDuration(n:Number):void
		{
			m_FadeInDuration = n;
		}
		
		
		public function get fadeOutDuration():Number
		{
			return m_FadeInDuration;
		}
		
		public function set fadeOutDuration(n:Number):void
		{
			m_FadeInDuration = n;
		}
		
		public function get finalOutput():String
		{
			return m_FinalOutputLink;
		}
		
		public function set finalOutput(s:String):void
		{
			m_FinalOutputLink = s;
		}
	}
}
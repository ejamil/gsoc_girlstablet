package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.encoding
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.data.userdata.GSOC_UserData_Team_DataKeys;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.commonassets.VideoConstants;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.filtering.FilterReference;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.filtering.filters.Filter;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.recordvideo.RecordVideo;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.reviewvideo.videopanel.ReviewVideoPanel;
	
	import flash.filesystem.File;

	/**
	 * Highly specialized class to piece together exact ffmpeg commands to execute for each girls' tablet use case.
	 */
	public class EncodingWrapper
	{
		
		public static function CreateEncodingBatchCalls():void
		{
			var basePath:String = CitrusFramework.frameworkConfigXML.videoEncodingConfigs.videoProcessingDirectory.toString();
			
			var src:String = basePath + CitrusFramework.frameworkConfigXML.videoEncodingConfigs.rawVideoPath.toString();
			var intermediateVidOutput:String = "tempVid.mp4";
			var output:String = basePath + CitrusFramework.frameworkConfigXML.videoEncodingConfigs.encodedVideoPath.toString();
			var filterString:String;
			
			//%%Framerate
			var framerate:int = VideoConstants.VIDEO_FRAMERATE;
			
			var args:Vector.<String> = new Vector.<String>();
			var titleExportParams:CardExportParams = new CardExportParams(basePath + CitrusFramework.frameworkConfigXML.videoEncodingConfigs.titleCardPath.toString(),
				"soundlessOverlay.mp4", 5, 1, 1, "titleCard.mp4");
			var creditsExportParams:CardExportParams = new CardExportParams(basePath + CitrusFramework.frameworkConfigXML.videoEncodingConfigs.creditsCardPath.toString(),
				"soundlessOverlay.mp4", 5, 1, 1, "credits.mp4")
			
	
			args.push("-y","-r", framerate.toString(), "-loop", "1", "-i", titleExportParams.sourcePath, "-frames:v", (titleExportParams.cardDuration * framerate).toString(), 
				"-vf", "fade=in:0:" + (1 + titleExportParams.fadeInDuration * framerate).toString() +",fade=out:" + ((titleExportParams.cardDuration -titleExportParams.fadeOutDuration) * framerate).toString() + ":" + (titleExportParams.fadeOutDuration * 30).toString(),
				"-shortest", "-pix_fmt", "yuv420p", titleExportParams.outputPath);
			
			var args2:Vector.<String> = new Vector.<String>();
			args2.push("-y", "-r", framerate.toString(), "-f", "lavfi", "-i", "anullsrc=channel_layout=stereo:sample_rate=44100", "-i", titleExportParams.outputPath, "-shortest", "-c:v",
				"copy", "-c:a", "aac", titleExportParams.finalOutput);
			
			var args3:Vector.<String> = new Vector.<String>();
			args3.push("-y", "-r", framerate.toString(), "-loop", "1", "-i", creditsExportParams.sourcePath, "-frames:v", (creditsExportParams.cardDuration * framerate).toString(), 
				"-vf", "fade=in:0:" + (creditsExportParams.fadeInDuration * framerate).toString() +",fade=out:" + ((creditsExportParams.cardDuration -creditsExportParams.fadeOutDuration - 1) * framerate).toString() + ":" + (creditsExportParams.fadeOutDuration * 30).toString(),
				"-shortest", "-pix_fmt", "yuv420p", creditsExportParams.outputPath);
			
			var args4:Vector.<String> = new Vector.<String>();
			args4.push("-y", "-r", framerate.toString(), "-f", "lavfi", "-i", "anullsrc=channel_layout=stereo:sample_rate=44100", "-i", creditsExportParams.outputPath, "-shortest", "-c:v",
				"copy", "-c:a", "aac", creditsExportParams.finalOutput);
			
			
			var args5:Vector.<String> = new Vector.<String>();
			var hasFilter:Boolean = SessionInformation_GirlsTablet.TeamInfo.Team_UserData.GetData(GSOC_UserData_Team_DataKeys.KEY_VIDEO_FILTER) ? true : false;
			var hasAudio:Boolean = SessionInformation_GirlsTablet.TeamInfo.Team_UserData.GetData(GSOC_UserData_Team_DataKeys.KEY_VIDEO_SONG) ? true : false;
			
			if(SessionInformation_GirlsTablet.TeamInfo.Team_UserData.GetData(GSOC_UserData_Team_DataKeys.KEY_VIDEO_FILTER)) 	var filter:Filter = FilterReference.GetFilterByKey(SessionInformation_GirlsTablet.TeamInfo.Team_UserData.GetData(GSOC_UserData_Team_DataKeys.KEY_VIDEO_FILTER) as String);
			
			if(hasFilter && hasAudio)
			{
				args5.push("-y", "-r", framerate.toString(), "-i", titleExportParams.finalOutput,
					"-i", src, "-i", creditsExportParams.finalOutput,
					"-i", File.applicationDirectory.nativePath.toString() + "\\" + SessionInformation_GirlsTablet.TeamInfo.Team_UserData.GetData(GSOC_UserData_Team_DataKeys.KEY_VIDEO_SONG),
					"-filter_complex", "[1:0]colorchannelmixer=" + filter.GetMatrixAsString(":") + "[tinted],[0:0][0:1][tinted][1:1][2:0][2:1] concat=n=3:v=1:a=1 [concatv] [concata], " +
					"[concata]aformat=sample_fmts=fltp:sample_rates=44100:channel_layouts=stereo,volume=5.0[loudconcata]," +
					"[3:0]aformat=sample_fmts=fltp:sample_rates=44100:channel_layouts=stereo,volume=0.1[quietmusic], [loudconcata][quietmusic]amix=inputs=2:duration=shortest[aout]",
					"-map", "[concatv]", "-map", "[aout]", "-pix_fmt", "yuv420p", "-c:a", "aac", "-level", "3.1", "-strict", "-2",output);
			}
			else if(hasFilter && !hasAudio)
			{
				args5.push("-y","-r", framerate.toString(), "-i", titleExportParams.finalOutput,
					"-i", src, "-i", creditsExportParams.finalOutput,
					"-filter_complex", "[1:0]colorchannelmixer=" + filter.GetMatrixAsString(":") + "[tinted],[0:0][0:1][tinted][1:1][2:0][2:1] concat=n=3:v=1:a=1 [v] [quieta]," +
					"[quieta]aformat=sample_fmts=fltp:sample_rates=44100:channel_layouts=stereo,volume=2[louda]",
					"-map", "[v]", "-map", "[louda]", "-pix_fmt", "yuv420p", "-c:a", "aac", "-level", "3.1", "-strict", "-2",output);
			}
			else if (!hasFilter && hasAudio)
			{
				args5.push("-y","-r", framerate.toString(), "-i", titleExportParams.finalOutput,
					"-i", src, "-i", creditsExportParams.finalOutput,
					"-i", File.applicationDirectory.nativePath.toString() + "\\" + SessionInformation_GirlsTablet.TeamInfo.Team_UserData.GetData(GSOC_UserData_Team_DataKeys.KEY_VIDEO_SONG),
					"-filter_complex", "[0:0][0:1][1:0][1:1][2:0][2:1] concat=n=3:v=1:a=1 [concatv] [concata], " +
					"[concata]aformat=sample_fmts=fltp:sample_rates=44100:channel_layouts=stereo,volume=5.0[loudconcata]," + 
					"[3:0]aformat=sample_fmts=fltp:sample_rates=44100:channel_layouts=stereo,volume=0.1[quietmusic], [loudconcata][quietmusic]amix=inputs=2:duration=shortest[aout]",
					"-map", "[concatv]", "-map", "[aout]", "-pix_fmt", "yuv420p", "-c:a", "aac", "-level", "3.1", "-strict", "-2",output);
			}
			else if(!hasFilter && !hasAudio)
			{
				
				args5.push("-y","-r", framerate.toString(), "-i", titleExportParams.finalOutput,
					"-i", src, "-i", creditsExportParams.finalOutput,
					"-filter_complex", "[0:0][0:1][1:0][1:1][2:0][2:1] concat=n=3:v=1:a=1 [v] [quieta], " +
					"[quieta]aformat=sample_fmts=fltp:sample_rates=44100:channel_layouts=stereo,volume=2[louda]",
					"-map", "[v]", "-map", "[louda]", "-pix_fmt", "yuv420p", "-c:a", "aac", "-level", "3.1", "-strict", "-2",output); 
			}
			else
			{
				// something has gone terribly wrong
			}
		
			
			Queue_FFMPEGCalls.AddCall(new FFMPEGCall(args, titleExportParams.cardDuration * 30, FFMPEGCallIDs.ENCODING_TITLE)); // title no sound
			Queue_FFMPEGCalls.AddCall(new FFMPEGCall(args2, titleExportParams.cardDuration * 30, FFMPEGCallIDs.AUDIO_OVERLAY_TITLE)); // add silent audio track to title clip
			Queue_FFMPEGCalls.AddCall(new FFMPEGCall(args3, creditsExportParams.cardDuration * 30, FFMPEGCallIDs.ENCODING_CREDITS)); // credits no sound
			Queue_FFMPEGCalls.AddCall(new FFMPEGCall(args4, creditsExportParams.cardDuration * 30, FFMPEGCallIDs.AUDIO_OVERLAY_CREDITS)); // add silent audio track to credits clip
			Queue_FFMPEGCalls.AddCall(new FFMPEGCall(args5, ReviewVideoPanel.VideoLength * 30 + titleExportParams.cardDuration * 30 + creditsExportParams.cardDuration * 30, FFMPEGCallIDs.ENCODING_MAIN)); // concatenate bookend cards and add filter <-- this takes 95% of the encoding time
			
		}
	}
}
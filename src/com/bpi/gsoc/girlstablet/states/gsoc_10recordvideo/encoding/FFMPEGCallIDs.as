package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.encoding
{
	public class FFMPEGCallIDs
	{
		public static const ENCODING_TITLE:String = "Converting title image to video";
		public static const AUDIO_OVERLAY_TITLE:String = "Adding audio track to title card";
		public static const ENCODING_CREDITS:String = "Converting credits image to video";
		public static const AUDIO_OVERLAY_CREDITS:String = "Adding audio track to credits";
		public static const ENCODING_MAIN:String = "Encoding main video";
		public static const FINISHED_ENCODING:String = "Encoding is complete; starting upload process";
	}
}
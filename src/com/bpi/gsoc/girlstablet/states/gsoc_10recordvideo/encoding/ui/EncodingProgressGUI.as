package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.encoding.ui
{
	import com.bpi.citrusas.text.TextBox;
	import com.bpi.gsoc.framework.constants.GSOC_Info;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.reviewvideo.videopanel.progressbar.ProgressBar;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.geom.Point;
	
	public class EncodingProgressGUI extends Sprite
	{
		private const m_DIMS:Point = new Point(1000, 400);
		private const m_BORDER_WIDTH:int = 20;
		private const m_TEXT_Y:int = m_DIMS.y* 0.25;
		
		private var m_StatusMessage:TextBox;
		private var m_ProgressBar:ProgressBar;
		
		public function EncodingProgressGUI(background:MovieClip=null, progBarForeground:MovieClip=null, progBarBackground:MovieClip=null)
		{
			super();
			
			if(!background)
			{
				this.graphics.beginFill(0xeeeeee, 0.9);
				this.graphics.lineStyle(m_BORDER_WIDTH, GSOC_Info.ColorTwo);
				this.graphics.drawRect(0, 0, m_DIMS.x, m_DIMS.y);
				this.graphics.endFill();
				this.y = m_BORDER_WIDTH / 2;
			}
			else
			{
				this.addChild(new background());
			}
			
			m_StatusMessage = new TextBox();
			m_StatusMessage.Initialize(new Point(m_DIMS.x * 0.8, 200), 50, 0x000000);
			m_StatusMessage.y = m_TEXT_Y;
			m_StatusMessage.Font = GSOC_Info.HeaderFont;
			m_StatusMessage.ToggleMultiLine(true);
			
			var fore:MovieClip = new MovieClip();
			var back:MovieClip = new MovieClip();
			if(!progBarForeground && !progBarBackground)
			{
				fore.graphics.beginFill(GSOC_Info.ColorOne);
				fore.graphics.drawRect(0, 0, m_DIMS.x * 0.8, m_DIMS.y * 0.1);
				fore.graphics.endFill();
				
				back.graphics.beginFill(0x999999);
				back.graphics.drawRect(0, 0, m_DIMS.x * 0.8, m_DIMS.y * 0.1);
				back.graphics.endFill();
				m_ProgressBar = new ProgressBar();
				m_ProgressBar.Initialize(fore, back, fore.width);
			}
			else
			{
				m_ProgressBar = new ProgressBar();
				m_ProgressBar.Initialize(progBarForeground, progBarBackground, progBarForeground.width);
			}
			m_ProgressBar.x = this.width / 2 - m_ProgressBar.width / 2;
			m_ProgressBar.y = m_StatusMessage.y + m_StatusMessage.height + 20;
		
			this.addChild(m_StatusMessage);
			this.addChild(m_ProgressBar);
		}
		
		public function Update(percentComplete:int, message:String):void
		{
			this.updateAndCenterMessage(message);
			m_ProgressBar.UpdateWithPercent(percentComplete);
		}
		
		public function Reset():void
		{
			m_StatusMessage.Text = "";
			m_ProgressBar.ResetToBeginning();
		}
		
		private function updateAndCenterMessage(m:String):void
		{
			m_StatusMessage.Text = m;
			m_StatusMessage.x = m_DIMS.x / 2 - m_StatusMessage.width / 2;
		}
	}
}
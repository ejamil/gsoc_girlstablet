package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.recordvideo
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.gsoc.framework.constants.AgeConstants;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.states.app.GSOC_AppState;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_RecordVideo;
	
	public class RecordVideo_State extends GSOC_AppState
	{
		private var m_App:RecordVideo_App;
		
		public function RecordVideo_State()
		{
			super(StateController_StateIDs_RecordVideo.STATE_RECORDVIDEO);
		}
		
		protected override function Init():void
		{
			switch(SessionInformation_GirlsTablet.Age)
			{
				case AgeConstants.KEY_DAISYBROWNIE:
					m_App = new RecordVideo_App_DB();
					break;
				case AgeConstants.KEY_JUNIORCADETTE:
					m_App = new RecordVideo_App_JC();
					break;
				case AgeConstants.KEY_SENIORAMBASSADOR:
					m_App = new RecordVideo_App_SA();
					break;
			}
			
			CitrusFramework.contentLayer.addChild(m_App);
			
			m_App.Initialize();
			m_App.Init();
		}
		
		protected override function DeInit():void
		{	
			m_App.visible = false;
			m_App.DeInit();
			
			super.EndDeInit();
		}
	}
}
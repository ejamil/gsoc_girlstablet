package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.recordvideo.ui
{
	import com.bpi.gsoc.framework.camera.PopupMessage;
	
	import flash.events.Event;
	
	public class VideoErrorPopup extends PopupMessage
	{
		private var m_CallbackOnButtonPress:Function;
		
		public function VideoErrorPopup(titleText:String, messageText:String, buttonText:String, onExit:Function)
		{
			super(titleText, messageText, buttonText);
			m_CallbackOnButtonPress = onExit;
		}
		
		override protected function eh_ButtonPressed(e:Event):void
		{
			m_CallbackOnButtonPress();
		}
	}
}
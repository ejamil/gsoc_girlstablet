package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.recordvideo
{
	import com.bpi.gsoc.components.recordvideo.RecordVideo_DB_Skin;
	import com.bpi.gsoc.framework.constants.TrackConstants;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.gsocui.GSOC_Header;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.RoleCallout;

	public class RecordVideo_App_DB extends RecordVideo_App
	{
		private var m_Skin:RecordVideo_DB_Skin;
		
		public function RecordVideo_App_DB(addPagination:Boolean=false)
		{
			super(addPagination);
		}
		
		public override function Initialize():void
		{
			m_Skin = new RecordVideo_DB_Skin();
			m_Skin.header.gotoAndStop(SessionInformation_GirlsTablet.Track + 1);	
			
			this.addChild(m_Skin);
			
			m_GSOCHeader = new GSOC_Header(m_Skin.header.paginationArea);
			GSOCheader.addArrows(m_Skin.header.paginationArea.x, 0);
			
			switch(SessionInformation_GirlsTablet.Track)
			{
				case TrackConstants.CAREER:
					m_RoleCallout = new RoleCallout(m_Skin.header.paginationArea.x, m_Skin.header.paginationArea.y, m_Skin.header.paginationArea.width, m_Skin.header.paginationArea.height);
					break;
				case TrackConstants.TAKE_ACTION:
					m_RoleCallout = new RoleCallout(m_Skin.header.paginationArea.x, m_Skin.header.paginationArea.y, m_Skin.header.paginationArea.width, m_Skin.header.paginationArea.height);
					break;
			}
			
			super.Initialize();
		}
	}
}
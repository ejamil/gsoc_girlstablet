package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.recordvideo.ui
{
	import com.bpi.citrusas.input.inputcontroller.InputController;
	import com.bpi.gsoc.components.recordvideo.DB_RecordBtn;
	import com.bpi.gsoc.components.recordvideo.DB_StopRecordingBtn;
	import com.greensock.TweenLite;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	
	public class RecordButton extends MovieClip
	{
		private var m_OuterRadius:Number;
		private var m_RecordImage:DB_RecordBtn;
		private var m_StopImage:DB_StopRecordingBtn;
		private var m_RecordImageContainer:Sprite;
		private var m_StopImageContainer:Sprite;
		private static const FLIP_DURATION:Number = .3;
		
		public function RecordButton(outerRadius:int = 75)
		{
			super();
			m_OuterRadius = outerRadius;
			m_RecordImage = new DB_RecordBtn();
			m_StopImage = new DB_StopRecordingBtn();
			m_RecordImageContainer = new Sprite();
			m_StopImageContainer = new Sprite();
			
			m_StopImageContainer.addChild(m_StopImage);
			m_RecordImageContainer.addChild(m_RecordImage);
			m_RecordImage.x = -m_RecordImage.width /2 ;
			m_StopImage.x = -m_StopImage.width / 2;
			
			this.addChild(m_StopImageContainer);
			this.addChild(m_RecordImageContainer);
			
			GoToReadyState();
		}
		
		public function GoToReadyState(e:Event = null):void
		{
			this.removeEventListener(InputController.CLICK, this.GoToReadyState);
			this.addEventListener(InputController.CLICK, this.GoToDisabledState);
			hideStop();
		}
		
		public function GoToDisabledState(e:Event = null):void
		{
			this.removeEventListener(InputController.CLICK, this.GoToDisabledState);
			this.addEventListener(InputController.CLICK, this.GoToReadyState);
			hideRecord();
		}
		
		private function hideRecord():void
		{
			TweenLite.to(m_RecordImageContainer, FLIP_DURATION, {rotationY:-66, onComplete:showStop});
		}
		
		private function showRecord():void
		{
			m_RecordImageContainer.rotationY = 90;
			TweenLite.to(m_RecordImageContainer, FLIP_DURATION, {rotationY:0});
		}
		
		private function hideStop():void
		{
			TweenLite.to(m_StopImageContainer, FLIP_DURATION, {rotationY:-66, onComplete:showRecord});
		}
		
		private function showStop():void
		{
			m_StopImageContainer.rotationY = 90;
			TweenLite.to(m_StopImageContainer, FLIP_DURATION, {rotationY:0});
		}
		
		public function Disable():void
		{
			this.removeEventListener(InputController.CLICK, this.GoToDisabledState);
			this.removeEventListener(InputController.CLICK, this.GoToReadyState);
		}
		
		public function HideCompletely():void
		{
			TweenLite.to(m_RecordImageContainer, FLIP_DURATION, {rotationY:-66});
			TweenLite.to(m_StopImageContainer, FLIP_DURATION, {rotationY:-66});
		}
	}
}
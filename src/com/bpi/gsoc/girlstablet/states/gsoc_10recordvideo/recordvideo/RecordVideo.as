package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.recordvideo
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.gsoc.framework.constants.AgeColors;
	import com.bpi.gsoc.framework.constants.AgeConstants;
	import com.bpi.gsoc.framework.constants.GSOC_Info;
	import com.bpi.gsoc.framework.constants.VideoExitCodes;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.girlstablet.global_data.TabletCamera;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.commonassets.VideoConstants;
	
	import flash.desktop.NativeProcess;
	import flash.desktop.NativeProcessStartupInfo;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.NativeProcessExitEvent;
	import flash.filesystem.File;
	import flash.geom.Point;

	public class RecordVideo
	{
		private static const ms_VideoRecorder_Location_Backup:String = "C:/bpi/GSOC_VideoRecorder.app/GSOC_VideoRecorder.exe"; // probably unused in real deploy. Jenkins puts the recorder at the location below. This path used for local development
		private static const ms_VideoRecorder_Location:String = "C:/bpi/GSOC_GirlsTabletBundle/GSOC_VideoRecorder.app/GSOC_VideoRecorder.exe"; 
		private static const MAX_VIDEO_DIMS:Point = new Point(1280, 720);
		private static var ms_VideoPos:Point = new Point();
		public static const EVENT_AIR_APP_CLOSED:String = "Video recorder app was closed";
		public static const EVENT_AIR_APP_CLOSED_FAILSAVE:String = "Video recorder app was closed but failed to save the video";
		public static const EVENT_AIR_APP_CLOSED_NOCAMERA:String = "No camera was detected on secondary app start"
		
		private static var ms_VideoRecorder_Process:NativeProcess;
		private static var ms_VideoRecorder_ProcessInfo:NativeProcessStartupInfo;
		private static var ms_Inited:Boolean = false;
		public static var RecordVideoDispatcher:EventDispatcher;
		
		
		public static function Init(age:int, vidSaveLocation:String):void
		{
			if(ms_Inited) return; // only init once

			var file:File = new File(ms_VideoRecorder_Location);
			if(!file.exists) // for development only. Path above is used by Jarvis, so it should always be there.
			{
				var backupFile:File = new File(ms_VideoRecorder_Location_Backup);
				if(!backupFile.exists)
				{
					throw new Error("The video recorder program could not be found in either the default or the backup location.");
				}
				file = new File(ms_VideoRecorder_Location_Backup);
			}
			var args:Vector.<String> = new Vector.<String>(); 
			
			ms_VideoRecorder_Process = new NativeProcess(); 
			ms_VideoRecorder_ProcessInfo = new NativeProcessStartupInfo(); 
			
			ms_VideoPos.x = CitrusFramework.frameworkStageSize.x / 2 - TabletCamera.REAR_CAMERA_DIMS.x / 2; 
			ms_VideoPos.y = 514; // taken from Review Video app. How to get this dynamically?
			
			var videoDims:Point = VideoConstants.VIDEO_DIMS;
			
			if(videoDims.x > MAX_VIDEO_DIMS.x || videoDims.y > MAX_VIDEO_DIMS.y)
			{
				CitrusFramework.localErrorLog.LogEvent("" +
					"The Flash encoder used to capture video does not handle resolutions above " +
					"1280x720 well. If you record at dimensions above this, you can expect to see " +
					"video lag problems, audio/video sync issues, and skipping on playback on " +
					"various platforms.");
			}
			
			ms_VideoRecorder_ProcessInfo.executable = file; 
			args[0] = age;
			args[1] = ms_VideoPos.x;
			args[2] = ms_VideoPos.y;
			args[3] = vidSaveLocation;
			args[4] = videoDims.x; 
			args[5] = videoDims.y;
			args[6] = GSOC_Info.ColorTwo; // border color
			
			
			// highlight color
			switch(SessionInformation_GirlsTablet.Age)
			{
				case AgeConstants.KEY_DAISYBROWNIE:
					args[7] = AgeColors.DB_YELLOW; // font color
					args[8] = 0xffffff;
					break;
				case AgeConstants.KEY_JUNIORCADETTE:
					args[7] = 0xffffff; // font color
					args[8] = AgeColors.JC_GREEN;
					break;
				case AgeConstants.KEY_SENIORAMBASSADOR:
					args[7] = 0xffffff; // font color
					args[8] = 0xffffff;
					break;
			}
			
			args[9] = VideoConstants.VIDEO_FRAMERATE;
			
			ms_VideoRecorder_ProcessInfo.arguments = args; 
			
			ms_VideoRecorder_Process.addEventListener(NativeProcessExitEvent.EXIT, eh_VideoClosed);
			ms_Inited = true;
			
			RecordVideoDispatcher = new EventDispatcher();
		}
		
		private static function eh_VideoClosed(e:NativeProcessExitEvent):void
		{
			if(e.exitCode == VideoExitCodes.EXIT_SUCCESSFUL_SAVE)
			{
				RecordVideoDispatcher.dispatchEvent(new Event(EVENT_AIR_APP_CLOSED));
				
			}
			else if (e.exitCode == VideoExitCodes.EXIT_UNSUCCESSFUL_SAVE)
			{
				RecordVideoDispatcher.dispatchEvent(new Event(EVENT_AIR_APP_CLOSED_FAILSAVE));
			}
			else if (e.exitCode == VideoExitCodes.EXIT_NOCAMERA)
			{
				RecordVideoDispatcher.dispatchEvent(new Event(EVENT_AIR_APP_CLOSED_NOCAMERA));
			}
			else
			{
				// this is probably when the video app has been closed manually and unintentionally. Restart it.
				startVideoRecorder();
			}
		}
		
		private static function startVideoRecorder():void
		{
			ms_VideoRecorder_Process.start(ms_VideoRecorder_ProcessInfo); 
		}
		
		public static function Launch(age:int):void
		{
			// delete the previous video so we can detect its existence on the next screen as an error check
			var tmpFile:File;
			
			tmpFile = File.applicationDirectory.resolvePath(CitrusFramework.frameworkConfigXML.videoEncodingConfigs.videoProcessingDirectory.toString() + 
				CitrusFramework.frameworkConfigXML.videoEncodingConfigs.rawVideoPath.toString());

			if(tmpFile.exists)
			{
				tmpFile.deleteFile();
			}
			if(!ms_Inited) throw new Error("You must call Init before launching the recorder");
			startVideoRecorder();
		}
		
		public static function Close():void
		{
			// at this point we are explicitly closing the app and should not handle the accidental close situation
			ms_VideoRecorder_Process.removeEventListener(NativeProcessExitEvent.EXIT, eh_VideoClosed);
			ms_VideoRecorder_Process.exit();
			ms_VideoRecorder_Process = null;
			ms_Inited = false;
		}
	}
}
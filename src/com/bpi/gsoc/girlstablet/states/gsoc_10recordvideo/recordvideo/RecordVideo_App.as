package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.recordvideo
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.citrusas.networking.client.Client;
	import com.bpi.citrusas.networking.events.Event_ClientNetworkMessage;
	import com.bpi.citrusas.sound.SoundHandler;
	import com.bpi.citrusas.states.StateControllerManager;
	import com.bpi.gsoc.framework.constants.GSOC_Info;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.data.videoupload.VideoUpload_MessageBuilder;
	import com.bpi.gsoc.framework.networking.NetworkMessage_Types;
	import com.bpi.gsoc.framework.sfx.GSOC_SFX;
	import com.bpi.gsoc.framework.states.app.GSOC_AppSkin;
	import com.bpi.gsoc.girlstablet.global_data.TabletCamera;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.RoleCallout;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.assignroles.roles.RoleKeys;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.encoding.UploadVideo;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.recordvideo.ui.VideoErrorPopup;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.reviewvideo.messaging.VideoSaveBackground;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.reviewvideo.messaging.VideoSavingForceVisible;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_IDs;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_Main;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_Poster;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_RecordVideo;
	import com.greensock.TweenLite;
	
	import flash.desktop.NativeApplication;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.utils.ByteArray;
	
	public class RecordVideo_App extends GSOC_AppSkin
	{
		protected var m_RoleCallout:RoleCallout;
		protected var m_SavingGraphic:VideoSavingForceVisible;
		protected static const FORCE_SAVE_VISIBLE_DURATION:Number = 1.5;

		public function RecordVideo_App(addPagination:Boolean=false)
		{
			super(addPagination);
		}
		
		public override function Initialize():void
		{
			super.Initialize();
		}
		
		public override function Init():void
		{
			m_SavingGraphic = new VideoSavingForceVisible();
			m_SavingGraphic.Initialize(MovieClip, VideoSaveBackground, GSOC_Info.HeaderFont);
			m_SavingGraphic.Init();
			m_SavingGraphic.x  = CitrusFramework.frameworkStageSize.x / 2 - m_SavingGraphic.width / 2;
			m_SavingGraphic.y = CitrusFramework.frameworkStageSize.y / 2 - m_SavingGraphic.height / 2;
			this.addChild(m_SavingGraphic);
			this.hideSavingGraphic();
			
			RecordVideo.Init(SessionInformation_GirlsTablet.Age,
				CitrusFramework.frameworkConfigXML.videoEncodingConfigs.videoProcessingDirectory.toString() + CitrusFramework.frameworkConfigXML.videoEncodingConfigs.rawVideoPath.toString());
			NativeApplication.nativeApplication.addEventListener(Event.EXITING, this.eh_AppExitGuard);
			this.setUpScreen();
			this.launchRecordingAIRApp();
		}

		public override function DeInit():void
		{
			NativeApplication.nativeApplication.removeEventListener(Event.EXITING, this.eh_AppExitGuard);
			m_SavingGraphic.DeInit();
			m_SavingGraphic = null;
			RecordVideo.Close();
			
			super.DeInit();
		}
		

		private function setUpScreen():void
		{
			this.addChild(m_RoleCallout);
			m_RoleCallout.FillForRole(RoleKeys.DIRECTOR);
			m_RoleCallout.AnimateIn();
			m_GSOCHeader.arrows.animateArrows();
		}
		
	
		private function launchRecordingAIRApp(e:Event = null):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_RECORDBUTTONS[SessionInformation_GirlsTablet.Age]);
			RecordVideo.Launch(SessionInformation_GirlsTablet.Age);
			TweenLite.delayedCall(3, this.showSavingGraphic); // the video takes a second to pop up, so delay showing the saving graphic for a bit
			RecordVideo.RecordVideoDispatcher.addEventListener(RecordVideo.EVENT_AIR_APP_CLOSED, this.eh_MoveOn);
			RecordVideo.RecordVideoDispatcher.addEventListener(RecordVideo.EVENT_AIR_APP_CLOSED_FAILSAVE, this.eh_HandleFailedSave);
			RecordVideo.RecordVideoDispatcher.addEventListener(RecordVideo.EVENT_AIR_APP_CLOSED_NOCAMERA, this.eh_HandleNoCamera);

		}
		
		private function eh_MoveOn(e:Event):void
		{
			RecordVideo.RecordVideoDispatcher.removeEventListener(RecordVideo.EVENT_AIR_APP_CLOSED, this.eh_MoveOn);
			// force the saving graphic to remain visible for a short duration
			TweenLite.delayedCall(FORCE_SAVE_VISIBLE_DURATION, this.delayedMoveOn);
		}
		
		private function eh_HandleFailedSave(e:Event):void
		{
			CitrusFramework.localLog.LogEvent("Video failed to save due to a RecordVideo.EVENT_AIR_APP_CLOSED_FAILSAVE event. The secondary" +
				" video app throws this error if there is not enough disk space available.");
			RecordVideo.RecordVideoDispatcher.removeEventListener(RecordVideo.EVENT_AIR_APP_CLOSED_FAILSAVE, this.eh_HandleFailedSave);
			if(m_SavingGraphic.visible)
			{
				this.hideSavingGraphic();
			}
			else 
			{
				TweenLite.killDelayedCallsTo(this.showSavingGraphic);
			}
			var graphic:VideoErrorPopup = new VideoErrorPopup("Video Error", "The video failed to save, possibly because there is not enough disk space. This activity will be skipped.", "OK",
				handleVideoErrorStateChanges);
			graphic.Initialize();
			this.addChild(graphic);
		}
		
		private function eh_HandleNoCamera(e:Event):void
		{
			CitrusFramework.localLog.LogEvent("Video failed to save due to a RecordVideo.EVENT_AIR_APP_CLOSED_NOCAMERA event. The secondary" +
				" video app throws this error if no camera is detected on launch.");
			RecordVideo.RecordVideoDispatcher.removeEventListener(RecordVideo.EVENT_AIR_APP_CLOSED_NOCAMERA, this.eh_HandleNoCamera);
			if(m_SavingGraphic.visible)
			{
				this.hideSavingGraphic();
			}
			else 
			{
				TweenLite.killDelayedCallsTo(this.showSavingGraphic);
			}
			var graphic:VideoErrorPopup = new VideoErrorPopup("Video Error", "No camera was found to record the video. This activity will be skipped.", "OK",
				handleVideoErrorStateChanges);
			graphic.Initialize();
			this.addChild(graphic);	
		}
		
		
		private function eh_AppExitGuard(exitingEvent:Event):void
		{
			// make sure to close vid recording app when this app closes as well
			RecordVideo.Close();
		}
		
		private function delayedMoveOn():void
		{
			this.hideSavingGraphic();
			StateControllerManager.LeaveActiveState(StateController_IDs.STATECONTROLLER_RECORDVIDEO, StateController_StateIDs_RecordVideo.STATE_RECORDVIDEO);
			StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_RECORDVIDEO, StateController_StateIDs_RecordVideo.STATE_REVIEWVIDEO);
		}
		
		private function handleVideoErrorStateChanges():void
		{
			// if there's an error either with the video feed or with saving the video:
			if(SessionInformation_GirlsTablet.IsRunningLocally)
			{
				StateControllerManager.LeaveAllActiveStates();
				StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_POSTER, StateController_StateIDs_Poster.STATE_POSTER);
			}
			else
			{
				// send message to leader tablet that there's something wrong
				var networkMessageByteArray:ByteArray = VideoUpload_MessageBuilder.BuildNetworkMessage(100, UploadVideo.UPLOAD_FAILED, false, SessionInformation_GirlsTablet.TabletIndex);
				Client.ClientEventDispatcher.dispatchEvent(new Event_ClientNetworkMessage(Event_ClientNetworkMessage.EVENT_SEND_NETWORK_MESSAGE, NetworkMessage_Types.TELL_SERVER_UPDATE_VIDEOUPLOADPROGRESS, networkMessageByteArray));
				// submit state early
				this.submitActivity();
				
			}
		}
		
		private function submitActivity():void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_CLICKS[SessionInformation_GirlsTablet.Age]);
			StateControllerManager.LeaveAllActiveStates();
			StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_MAIN, StateController_StateIDs_Main.STATE_SESSIONATTRACT);
			Document.TellServerActivityComplete();
		}
		
		private function showSavingGraphic():void
		{
			toggleSavingGraphicVisible(true);
			this.startSavingAnimation();
		}
		
		private function hideSavingGraphic():void
		{
			toggleSavingGraphicVisible(false);
			this.stopSavingAnimation();
		}
		
		private function toggleSavingGraphicVisible(toggle:Boolean):void
		{
			if(m_SavingGraphic)
			{
				m_SavingGraphic.visible = toggle;
			}
		}
		
		private function startSavingAnimation():void
		{
			if(m_SavingGraphic)
			{
				this.stage.addEventListener(Event.ENTER_FRAME, m_SavingGraphic.Update);
			}
		}
		
		private function stopSavingAnimation():void
		{
			if(m_SavingGraphic)
			{
				this.stage.removeEventListener(Event.ENTER_FRAME, m_SavingGraphic.Update);
			}
		}
	}
}
package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.playbackvideo
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.citrusas.input.inputcontroller.InputController;
	import com.bpi.citrusas.states.StateControllerManager;
	import com.bpi.citrusas.video.InteractiveVideo;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.states.app.GSOC_AppSkin;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_IDs;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_Main;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_Poster;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_RecordVideo;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Point;
	
	public class PlayBackVideo_App extends GSOC_AppSkin
	{
		private var m_VideoPlayer:InteractiveVideo;
		private var m_NextButton:Sprite;
		
		public function PlayBackVideo_App(addPagination:Boolean=false)
		{
			super(this);
		}
		
		public override function Initialize():void
		{
			super.Initialize();
			m_VideoPlayer = new InteractiveVideo(false);
			m_NextButton = new Sprite();

			SetupScreen();
		}
		
		public override function DeInitialize():void
		{
			
		}
		
		protected function SetupScreen():void
		{
			CitrusFramework.contentLayer.addChild(m_VideoPlayer);
			m_VideoPlayer.Initialize(CitrusFramework.frameworkConfigXML.encodedVideoPath, new Point(1920, 1080), true);
			m_VideoPlayer.Play();
			
			m_VideoPlayer.x = 200;
			m_VideoPlayer.y = 200;

			m_NextButton = new MovieClip();
			m_NextButton.graphics.beginFill(0x004499);
			m_NextButton.graphics.drawRect(0, 0, 200, 100);
			m_NextButton.graphics.endFill();
			m_NextButton.x = CitrusFramework.frameworkStageSize.x - m_NextButton.width - 20;
			m_NextButton.y = CitrusFramework.frameworkStageSize.y - m_NextButton.height - 20;
			
			//nextButton.visible = false;
			m_NextButton.addEventListener(InputController.CLICK, eh_MoveOn);
			
			CitrusFramework.contentLayer.addChild(m_NextButton);
			
			m_GSOCHeader.arrows.animateArrows();
		}
				
	
		private function eh_MoveOn(e:Event):void
		{
			this.DeInitialize();
			
			if(SessionInformation_GirlsTablet.IsRunningLocally)
			{
				StateControllerManager.LeaveActiveState(StateController_IDs.STATECONTROLLER_RECORDVIDEO, StateController_StateIDs_RecordVideo.STATE_PLAYBACKVIDEO);
				StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_POSTER, StateController_StateIDs_Poster.STATE_POSTER);
			}
			else
			{
				submitActivity();
			}
		}
		
		private function submitActivity():void
		{
			StateControllerManager.LeaveAllActiveStates();
			StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_MAIN, StateController_StateIDs_Main.STATE_SESSIONATTRACT);
			Document.TellServerActivityComplete();
		}
		
		public function get BodyFont():String
		{
			return m_BodyFont;
		}
		
		public function get HeaderFont():String
		{
			return m_HeaderFont;
		}
	}
}
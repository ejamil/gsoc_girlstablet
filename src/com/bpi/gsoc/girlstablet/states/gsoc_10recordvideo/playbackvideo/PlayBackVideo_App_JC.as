package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.playbackvideo
{
	import com.bpi.gsoc.framework.constants.AgeFontLinkages;
	import com.bpi.gsoc.framework.gsocui.GSOC_Header;
	
	import flash.display.MovieClip;

	public class PlayBackVideo_App_JC extends PlayBackVideo_App
	{
		private var m_Skin:MovieClip;
		
		public function PlayBackVideo_App_JC(addPagination:Boolean=false)
		{
			super(addPagination);
			m_Skin = new MovieClip();
			m_HeaderFont = AgeFontLinkages.JC_HEADER_FONT;
			m_BodyFont = AgeFontLinkages.JC_BODY_FONT;
			
			m_GSOCHeader = new GSOC_Header(m_Skin.header.paginationArea);
			GSOCheader.addArrows(m_Skin.header.paginationArea.x, 0);
		}
	}
}
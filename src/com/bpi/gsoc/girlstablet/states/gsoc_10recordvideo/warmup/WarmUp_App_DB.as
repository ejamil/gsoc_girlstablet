package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.warmup
{
	import com.bpi.gsoc.components.recordvideo.WarmUp_DB_Skin;
	import com.bpi.gsoc.framework.gsocui.GSOC_Header;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.RoleCallout;

	public class WarmUp_App_DB extends WarmUp_App
	{
		public function WarmUp_App_DB(addPagination:Boolean=false)
		{
			super(addPagination);
		}
		
		public override function Initialize():void
		{
			m_Skin = new WarmUp_DB_Skin();
			
			m_GSOCHeader = new GSOC_Header(m_Skin.header.paginationArea);
			GSOCheader.addArrows(m_Skin.header.paginationArea.x, 0);
			
			m_RoleCallout = new RoleCallout(m_Skin.header.paginationArea.x, m_Skin.header.paginationArea.y, m_Skin.header.paginationArea.width, m_Skin.header.paginationArea.height);
			
			super.Initialize();
		}
	}
}
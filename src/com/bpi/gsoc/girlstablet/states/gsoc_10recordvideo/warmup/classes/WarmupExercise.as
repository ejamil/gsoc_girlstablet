package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.warmup.classes
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.greensock.TweenLite;
	import com.greensock.easing.Expo;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.geom.Point;
	
	public class WarmupExercise extends EventDispatcher
	{
		private var m_Asset:MovieClip;
		private var m_HidePointRight:Point;
		private var m_HidePointLeft:Point;
		private var m_TweenOnScreenFromRight:TweenLite;
		private var m_TweenOnScreenFromLeft:TweenLite;
		private var m_TweenOffScreenToLeft:TweenLite;
		private var m_TweenOffScreenToRight:TweenLite;
		
		public static const TWEEN_ON_COMPLETE:String = "TWEEN_ON_COMPLETE";
		
		private static const m_TWEEN_DURATION:Number = 0.5;
		
		public function WarmupExercise(asset:MovieClip)
		{
			super();
			m_Asset = asset;
			m_HidePointRight = new Point(CitrusFramework.frameworkStageSize.x + m_Asset.width + 100, m_Asset.y);
			m_HidePointLeft = new Point(-m_Asset.width - 100, m_Asset.y);
			
			m_TweenOnScreenFromRight = TweenLite.fromTo(m_Asset, m_TWEEN_DURATION, {x:m_HidePointRight.x}, {x:CitrusFramework.frameworkStageSize.x / 2 - m_Asset.width / 2, paused:true, onComplete: this.tweenOnComplete, ease:Expo.easeOut});
			m_TweenOnScreenFromLeft = TweenLite.fromTo(m_Asset, m_TWEEN_DURATION, {x:m_HidePointLeft.x}, {x:CitrusFramework.frameworkStageSize.x / 2 - m_Asset.width / 2, paused:true, onComplete: this.tweenOnComplete, ease:Expo.easeOut });
			m_TweenOffScreenToLeft = TweenLite.to(m_Asset, m_TWEEN_DURATION, {x:m_HidePointLeft.x, paused:true, onComplete:this.tweenOffComplete});
			m_TweenOffScreenToRight = TweenLite.to(m_Asset, m_TWEEN_DURATION, {x:m_HidePointRight.x, paused:true, onComplete:this.tweenOffComplete});
			
			m_Asset.x = m_HidePointRight.x;
		}
		
		public function TweenOnScreenFromRight():void
		{
			m_TweenOnScreenFromRight.play(0);
		}
		
		public function TweenOnScreenFromLeft():void
		{
			m_TweenOnScreenFromLeft.play(0);
		}
		
		public function TweenOffScreenToLeft():void
		{
			m_TweenOffScreenToLeft.play(0);
		}
	
		public function TweenOffScreenToRight():void
		{
			m_TweenOffScreenToRight.play(0);
		}
		
		private function tweenOnComplete():void
		{
			// reenable next button here
			this.dispatchEvent(new Event(TWEEN_ON_COMPLETE));
		}
		
		private function tweenOffComplete():void
		{
			
		}
		
		public function Destroy():void
		{
			m_TweenOnScreenFromLeft.kill();
			m_TweenOnScreenFromRight.kill();
			m_TweenOffScreenToLeft.kill();
			m_TweenOffScreenToRight.kill();
			m_TweenOnScreenFromRight = null;
			m_TweenOffScreenToLeft = null;	
		}
	}
}
package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.warmup
{
	import com.bpi.citrusas.input.inputcontroller.InputController;
	import com.bpi.citrusas.sound.SoundHandler;
	import com.bpi.citrusas.states.StateControllerManager;
	import com.bpi.gsoc.framework.constants.TrackConstants;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.gsocui.GSOC_ButtonToggle;
	import com.bpi.gsoc.framework.sfx.GSOC_SFX;
	import com.bpi.gsoc.framework.states.app.GSOC_AppSkin;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.RoleCallout;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.assignroles.roles.RoleKeys;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.warmup.classes.WarmupExerciseController;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_IDs;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_Main;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_RecordVideo;
	import com.greensock.TweenLite;
	
	import flash.events.Event;
	import flash.geom.Point;
	
	public class WarmUp_App extends GSOC_AppSkin
	{
		private var m_ExerciseController:WarmupExerciseController;
		protected var m_RoleCallout:RoleCallout;
		
		public static var HIDE_POINT_RIGHT:Point;
		public static var HIDE_POINT_LEFT:Point;
		
		public function WarmUp_App(addPagination:Boolean=false)
		{
			super(this);
		}
		
		public override function Init():void
		{
			m_Skin.header.gotoAndStop(SessionInformation_GirlsTablet.Track + 1);
			this.enableNextButton();
			
			m_ExerciseController = new WarmupExerciseController(m_Skin);
			m_ExerciseController.GoToFirstExercise();
			m_GSOCHeader.arrows.animateArrows();
			var role1:String = SessionInformation_GirlsTablet.Track == TrackConstants.CAREER ? RoleKeys.GUEST : RoleKeys.ACTOR;
			var role2:String = SessionInformation_GirlsTablet.Track == TrackConstants.CAREER ? RoleKeys.HOST : "EMPTY";
			
			if(SessionInformation_GirlsTablet.Track == TrackConstants.TAKE_ACTION)
			{
				m_RoleCallout.FillForRole(RoleKeys.ACTOR);
			}
			else
			{
				m_RoleCallout.FillForGuestAndHost();
			}

			m_RoleCallout.AnimateIn();
			m_GSOCHeader.addChild(m_RoleCallout);
			super.Init();
			
		}
		
		private function eh_GoToNextExercise(e:Event):void
		{	
			SoundHandler.PlaySound(GSOC_SFX.SOUND_CLICKS[SessionInformation_GirlsTablet.Age]);
			if(m_ExerciseController.isTweening)return;
			m_ExerciseController.GoToNextExercise();
			this.controlButtons();
		}
		
		private function eh_GoToPreviousExercise(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_CLICKS[SessionInformation_GirlsTablet.Age]);
			if(m_ExerciseController.isTweening)return;
			m_ExerciseController.GoToPreviousExercise();
			this.controlButtons();	
		}
		
		private function controlButtons():void
		{
			if(!m_ExerciseController.hasMoreExercises)
			{
				// show submit and hide next
				GSOC_ButtonToggle.ToggleButton(false, m_Skin.nextBtn, null, this.disableNextButton);
				TweenLite.delayedCall(0.5, function():void{});
				GSOC_ButtonToggle.ToggleButton(true, m_Skin.submitBtn, this.enableSubmitButton, null);
			}
			else
			{
				//show next and hide submit
				GSOC_ButtonToggle.ToggleButton(false, m_Skin.submitBtn, null, this.disableSubmitButton);
				TweenLite.delayedCall(0.5, function():void{});
				GSOC_ButtonToggle.ToggleButton(true, m_Skin.nextBtn, this.enableNextButton, null);
			}
			
			if(m_ExerciseController.hasPreviousExercises)
			{
				GSOC_ButtonToggle.ToggleButton(true, m_Skin.backButton, this.enableBackButton, null);
			}
			else
			{
				GSOC_ButtonToggle.ToggleButton(false, m_Skin.backButton, null, this.disableBackButton);
			}
		}
		
		private function disableNextButton():void
		{
			m_Skin.nextBtn.removeEventListener(InputController.CLICK, this.eh_GoToNextExercise);
		}
		
		private function enableNextButton():void
		{
			m_Skin.nextBtn.addEventListener(InputController.CLICK, this.eh_GoToNextExercise);
		}
		
		private function disableSubmitButton():void
		{
			m_Skin.submitBtn.removeEventListener(InputController.CLICK, this.eh_GoToNextActivity);
		}
		
		private function enableSubmitButton():void
		{
			m_Skin.submitBtn.addEventListener(InputController.CLICK, this.eh_GoToNextActivity);
		}
		
		private function enableBackButton():void
		{
			m_Skin.backButton.addEventListener(InputController.CLICK, this.eh_GoToPreviousExercise);
		}
		
		private function disableBackButton():void
		{
			m_Skin.backButton.removeEventListener(InputController.CLICK, this.eh_GoToPreviousExercise);
		}
		
		private function eh_GoToNextActivity(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_CLICKS[SessionInformation_GirlsTablet.Age]);
			if (SessionInformation_GirlsTablet.IsRunningLocally)
			{
				StateControllerManager.LeaveActiveState(StateController_IDs.STATECONTROLLER_RECORDVIDEO, StateController_StateIDs_RecordVideo.STATE_WARMUP);
				StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_RECORDVIDEO, StateController_StateIDs_RecordVideo.STATE_RECORDVIDEO);
			}
			else
			{
				submitActivity();
			}
		}
		
		private function submitActivity():void
		{
			StateControllerManager.LeaveAllActiveStates();
			StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_MAIN, StateController_StateIDs_Main.STATE_SESSIONATTRACT);
			Document.TellServerActivityComplete();
		}
		
		public override function DeInit():void
		{
			m_ExerciseController.Destroy();
			m_Skin.nextBtn.removeEventListener(InputController.CLICK, this.eh_GoToNextExercise);	
			m_Skin.header.removeChild(m_RoleCallout);
			m_RoleCallout = null;
		}
	}
}
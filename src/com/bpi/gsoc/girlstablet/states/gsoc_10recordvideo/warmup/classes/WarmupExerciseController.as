package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.warmup.classes
{
	import com.bpi.gsoc.framework.constants.TrackConstants;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	
	public class WarmupExerciseController extends EventDispatcher
	{
		private var m_WarmupExercises:Vector.<WarmupExercise>;
		private var m_CurrentExerciseNum:int = 0;
		private var m_IsTweening:Boolean;
		
		public function WarmupExerciseController(appSkin:MovieClip, target:IEventDispatcher=null)
		{
			super(target);
			m_WarmupExercises = new Vector.<WarmupExercise>();
			m_WarmupExercises.push(new WarmupExercise(appSkin.tip1));
			m_WarmupExercises.push(new WarmupExercise(appSkin.tip2));
			m_WarmupExercises.push(new WarmupExercise(appSkin.tip3));
			m_WarmupExercises.push(new WarmupExercise(appSkin.tip4));
			m_WarmupExercises.push(new WarmupExercise(appSkin.tip5));
			
			appSkin.tip5.gotoAndStop(SessionInformation_GirlsTablet.Track + 1); // the only one that has a different symbol for each track
		}
		
		public function GoToFirstExercise():void
		{
			m_CurrentExerciseNum = 0;
			m_WarmupExercises[m_CurrentExerciseNum].addEventListener(WarmupExercise.TWEEN_ON_COMPLETE, this.eh_TweenOnComplete);
			m_WarmupExercises[m_CurrentExerciseNum].TweenOnScreenFromRight();
		}
		
		public function GoToNextExercise():void
		{
			if(this.hasMoreExercises)
			{
				m_CurrentExerciseNum++;
				m_WarmupExercises[m_CurrentExerciseNum - 1].TweenOffScreenToLeft();
				m_IsTweening = true;
				m_WarmupExercises[m_CurrentExerciseNum].addEventListener(WarmupExercise.TWEEN_ON_COMPLETE, this.eh_TweenOnComplete);
				m_WarmupExercises[m_CurrentExerciseNum].TweenOnScreenFromRight();
			}
		}
		
		public function GoToPreviousExercise():void
		{
			if(this.hasPreviousExercises)
			{
				m_CurrentExerciseNum--;
				
				m_WarmupExercises[m_CurrentExerciseNum + 1].TweenOffScreenToRight();
				m_IsTweening = true;
				m_WarmupExercises[m_CurrentExerciseNum].addEventListener(WarmupExercise.TWEEN_ON_COMPLETE, this.eh_TweenOnComplete);
				m_WarmupExercises[m_CurrentExerciseNum].TweenOnScreenFromLeft();
			}
		}
		
		private function eh_TweenOnComplete(e:Event):void
		{
			m_IsTweening = false;
		}
		
		public function get isTweening():Boolean
		{
			return m_IsTweening;
		}
		
		public function get hasMoreExercises():Boolean
		{
			return (m_CurrentExerciseNum + 1 < m_WarmupExercises.length);
		}
		
		public function get hasPreviousExercises():Boolean
		{
			return (m_CurrentExerciseNum >= 1); 
		}
		
		public function Destroy():void
		{
			for each(var warmup:WarmupExercise in m_WarmupExercises)
			{
				warmup.Destroy();
			}
		}
	}
}
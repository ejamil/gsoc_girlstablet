package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.screens.creditsscreen
{
	import com.bpi.gsoc.framework.constants.TrackConstants;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.data.userdata.GSOC_UserData_Girl_DataKeys;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.assignroles.roles.RoleKeys;

	public class CreditsBuilder
	{
		private var m_CreditsString:String = "";
		
		public function CreditsBuilder()
		{	
			var directorString:String = "";
			var numDirectors:int = 0;
			
			var editorString:String = "";
			var numEditors:int = 0;
			
			var writerString:String = "";
			var numWriters:int = 0;
			
			var actorString:String = "";
			var numActors:int = 0;
			
			var hostString:String = "";
			var numHosts:int = 0;
			
			var guestString:String = "";
			var numGuests:int = 0;
			
			for(var i:int = 0; i < SessionInformation_GirlsTablet.TeamInfo.GetTeamMemberCount(); i++)
			{
				var roles:Vector.<String> = SessionInformation_GirlsTablet.TeamInfo.GetGirlInformationFromIndex(i).GirlUserData.GetData(GSOC_UserData_Girl_DataKeys.KEY_ROLES) as Vector.<String>;

				if(roles)
				{
					if(roles.indexOf(RoleKeys.DIRECTOR) != -1)
					{
						numDirectors++;
						directorString += SessionInformation_GirlsTablet.TeamInfo.GetGirlInformationFromIndex(i).Name;
						directorString += ", ";
					}
					if(roles.indexOf(RoleKeys.ACTOR) != -1)
					{
						numActors++;
						actorString += SessionInformation_GirlsTablet.TeamInfo.GetGirlInformationFromIndex(i).Name;
						actorString += ", ";
					}
					if(roles.indexOf(RoleKeys.EDITOR) != -1)
					{
						numEditors++;
						editorString += SessionInformation_GirlsTablet.TeamInfo.GetGirlInformationFromIndex(i).Name;
						editorString += ", ";
					}
					if(roles.indexOf(RoleKeys.WRITER) != -1)
					{
						numWriters++;
						writerString += SessionInformation_GirlsTablet.TeamInfo.GetGirlInformationFromIndex(i).Name;
						writerString += ", ";
					}
					if(roles.indexOf(RoleKeys.HOST) != -1)
					{
						numHosts++;
						hostString += SessionInformation_GirlsTablet.TeamInfo.GetGirlInformationFromIndex(i).Name;
						hostString += ", ";
					}
					if(roles.indexOf(RoleKeys.GUEST) != -1)
					{
						numGuests++;
						guestString += SessionInformation_GirlsTablet.TeamInfo.GetGirlInformationFromIndex(i).Name;
						guestString += ", ";
					}
				}
			}
			
			if (directorString.length > 2)
			{
				directorString = directorString.slice(0, directorString.length - 2);
			}
			if (actorString.length > 2)
			{
				actorString = actorString.slice(0, actorString.length - 2);
			}
			if (editorString.length > 2)
			{
				editorString = editorString.slice(0, editorString.length - 2);
			}
			if (writerString.length > 2)
			{
				writerString = writerString.slice(0, writerString.length - 2);
			}
			if (hostString.length > 2)
			{
				hostString = hostString.slice(0, hostString.length - 2);
			}
			if (guestString.length > 2)
			{
				guestString = guestString.slice(0, guestString.length - 2);
			}
			
			m_CreditsString = "Credits\n";
			m_CreditsString += RoleKeys.DIRECTOR;
			if (numDirectors > 1)
			{
				m_CreditsString += "S";
			}
			m_CreditsString += " - ";
			m_CreditsString += directorString;
			m_CreditsString += "\n";
			
			m_CreditsString += RoleKeys.EDITOR;
			if (numEditors > 1)
			{
				m_CreditsString += "S";
			}
			m_CreditsString += " - ";
			m_CreditsString += editorString;
			m_CreditsString += "\n";
			
			if(SessionInformation_GirlsTablet.Track == TrackConstants.TAKE_ACTION)
			{
				m_CreditsString += RoleKeys.ACTOR;
				if (numActors > 1)
				{
					m_CreditsString += "S";
				}
				m_CreditsString += " - ";
				m_CreditsString += actorString;
				m_CreditsString += "\n";
			}
			
			if(SessionInformation_GirlsTablet.Track == TrackConstants.CAREER)
			{
				m_CreditsString += RoleKeys.HOST;
				if (numHosts > 1)
				{
					m_CreditsString += "S";
				}
				m_CreditsString += " - ";
				m_CreditsString += hostString;
				m_CreditsString += "\n";
				
				m_CreditsString += RoleKeys.GUEST;
				if (numGuests > 1)
				{
					m_CreditsString += "S";
				}
				m_CreditsString += " - ";
				m_CreditsString += guestString;
				m_CreditsString += "\n";
			}
			
			m_CreditsString += RoleKeys.WRITER;
			if (numWriters > 1)
			{
				m_CreditsString += "S";
			}
			m_CreditsString += " - ";
			m_CreditsString += writerString;
			
		}

		public function get CreditsString():String
		{
			return m_CreditsString;
		}

		public function set CreditsString(value:String):void
		{
			m_CreditsString = value;
		}
	}
}
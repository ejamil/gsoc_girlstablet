package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.filtering.events
{
	import flash.events.Event;
	
	public class Event_FilterChosen extends Event
	{
		public static const FILTER_CHOSEN:String = "Filter chosen";
		private var m_FilterKey:String;
		
		public function Event_FilterChosen(type:String, filterKey:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			m_FilterKey = filterKey;
		}
		
		public function get filterKey():String
		{
			return m_FilterKey;
		}
	}
}
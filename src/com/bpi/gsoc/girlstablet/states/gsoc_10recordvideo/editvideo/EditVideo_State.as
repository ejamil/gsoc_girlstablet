package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.gsoc.framework.constants.AgeConstants;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.states.app.GSOC_AppState;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_RecordVideo;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	
	public class EditVideo_State extends GSOC_AppState
	{
		protected var m_App:EditVideo_App;
		private var nextButton:MovieClip
		
		public function EditVideo_State()
		{
			super(StateController_StateIDs_RecordVideo.STATE_EDITVIDEO);
		}
		
		public override function Initialize():void
		{
			
		}
		
		protected override function Init():void
		{
			switch(SessionInformation_GirlsTablet.Age)
			{
				case AgeConstants.KEY_DAISYBROWNIE:
					m_App = new EditVideo_App_DB();
					break;
				case AgeConstants.KEY_JUNIORCADETTE:
					m_App = new EditVideo_App_JC();
					break;
				case AgeConstants.KEY_SENIORAMBASSADOR:
					m_App = new EditVideo_App_SA();
					break;
			}
			m_App.Initialize();
			super.Initialize();
			
			CitrusFramework.contentLayer.addChild(m_App);
			
			m_App.Init();
		}
		
		private function nextScreen(e:Event):void
		{
			//StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_RECORDVIDEO, StateController_StateIDs_RecordVideo.STATE_PLAYBACKVIDEO);
		}
		
		protected override function DeInit():void
		{	
			m_App.DeInit();
			
			super.EndDeInit();
		}
	}
}
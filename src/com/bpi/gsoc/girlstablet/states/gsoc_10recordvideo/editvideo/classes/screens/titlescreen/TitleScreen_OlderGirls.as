package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.screens.titlescreen
{
	import com.adobe.utils.StringUtil;
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.citrusas.input.inputcontroller.InputController;
	import com.bpi.citrusas.sound.SoundHandler;
	import com.bpi.citrusas.text.TextBox;
	import com.bpi.citrusas.utils.Utils_Text;
	import com.bpi.gsoc.framework.components.NoTitleImage;
	import com.bpi.gsoc.framework.constants.AgeConstants;
	import com.bpi.gsoc.framework.constants.GSOC_Info;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.sfx.GSOC_SFX;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.commonassets.VideoConstants;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.screens.exporting.PNGExportCall;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.screens.exporting.Queue_PNGExport;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.encoding.UploadVideo;
	import com.greensock.TweenLite;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.geom.ColorTransform;
	import flash.geom.Point;
	import flash.text.TextField;

	public class TitleScreen_OlderGirls extends TitleScreen
	{
		protected var m_FontNames:Vector.<String>;
		protected var m_CurrentlySelectedFont:String;
		protected var m_NoneSelected:Boolean;
		
		public function TitleScreen_OlderGirls()
		{
			super();
			m_FontNames = new Vector.<String>();
			m_CurrentlySelectedFont = "";   
		}
		
		override public function Init():void
		{
			m_SelectableItems = new Vector.<MovieClip>();
			m_SelectableItems.push(m_Skin.title1, m_Skin.title2, m_Skin.title3, m_Skin.title4);
			Utils_Text.ConvertToEmbeddedFont(m_Skin.titleTxt, GSOC_Info.BodyFont);
			Utils_Text.ConvertToEmbeddedFont(m_Skin.watermark, GSOC_Info.BodyFont);

			m_NoneSelected = false;
			
			super.Init();
			(m_Skin.titleTxt as TextField).maxChars = 32;
			m_Skin.titleTxt.text = "";
			TweenLite.delayedCall(0.5, this.setTextFieldFocus); // because the glow tweens steal focus. Reset focus a little later
			m_Skin.titleTxt.addEventListener(InputController.CLICK, this.eh_inputFieldClicked);
			m_Skin.watermark.mouseEnabled = false;
			m_Skin.watermark.tabEnabled = false;
			m_Skin.titleTxt.tabEnabled = false;
			this.toggleFontChoices(false);
			CitrusFramework.frameworkStage.addEventListener(KeyboardEvent.KEY_UP, this.eh_KeyUpHandler);
			CitrusFramework.frameworkStage.addEventListener(KeyboardEvent.KEY_DOWN, this.eh_KeyDownHandler);	
		}
		
		private function setTextFieldFocus():void
		{
			CitrusFramework.frameworkStage.focus = m_Skin.titleTxt;
		}
		
		private function eh_inputFieldClicked(e:Event):void
		{
			this.hideWatermark();
			m_NoneSelected = false;
			super.unhighlightNoneButton();
			this.checkReadyToMoveOn(e);
			this.setTextFieldFocus();
		}
		
		override protected function eh_NoneButtonSelected(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_SELECTTITLES[SessionInformation_GirlsTablet.Age]);
			m_NoneSelected = true;
			m_Skin.titleTxt.text = "";
			super.clearSelections();
			this.showWatermark();
			this.toggleFontChoices(false);
			this.checkReadyToMoveOn(e);
			this.highlightNoneButton();
		}
		
		override protected function eh_OptionSelected(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_SELECTTITLES[SessionInformation_GirlsTablet.Age]);
			m_NoneSelected = false;
			m_CurrentlySelectedFont = m_FontNames[m_SelectableItems.indexOf(e.target as MovieClip)];
			Utils_Text.ConvertToEmbeddedFont(m_Skin.titleTxt, m_CurrentlySelectedFont);
			
			this.checkReadyToMoveOn(e);
			super.eh_OptionSelected(e);
			(e.target as MovieClip).transform.colorTransform = new ColorTransform(((m_GlowColor >> 16) & 0xff) / 0xff, ((m_GlowColor >> 8) & 0xff) / 0xff, ((m_GlowColor) & 0xff) / 0xff);
			TweenLite.delayedCall(0.1, this.setTextFieldFocus);
		}
		
		private function eh_KeyUpHandler(e:KeyboardEvent):void
		{
			toggleFontChoices(isTitleTextPopulated);
			this.checkReadyToMoveOn(e);
		}
		
		private function get isTitleTextPopulated():Boolean
		{
			return StringUtil.trim(m_Skin.titleTxt.text);
		}
		
		private function get isTitleFontSelected():Boolean
		{
			return m_CurrentlySelectedFont != "";
		}
		
		private function get isScreenComplete():Boolean
		{
			return (isTitleTextPopulated && isTitleFontSelected || m_NoneSelected);
		}
		
		private function eh_KeyDownHandler(e:KeyboardEvent):void
		{
			if(CitrusFramework.frameworkStage.focus == m_Skin.titleTxt) this.hideWatermarkFast();
			CitrusFramework.frameworkStage.removeEventListener(KeyboardEvent.KEY_DOWN, this.eh_KeyDownHandler); // only worry about this situation on the very first key pressed
		}
		
		private function toggleFontChoices(toggle:Boolean):void
		{
			var alphaValue:Number = toggle ? 1 : 0.5;
			var isMouseEnabled:Boolean = toggle;
			for each(var fontChoice:MovieClip in m_SelectableItems)
			{
				fontChoice.mouseEnabled = isMouseEnabled;
				TweenLite.to(fontChoice, 0.3, {alpha:alphaValue});
			}
		}
		
		private function checkReadyToMoveOn(e:Event):void
		{	
			isScreenComplete ? onScreenComplete() : onScreenIncomplete();
		}
		
		private function showWatermark():void
		{
			TweenLite.to(m_Skin.watermark, 0.2, {alpha:1});
		}
		
		private function hideWatermark():void
		{
			TweenLite.to(m_Skin.watermark, 0.2, {alpha:0, onComplete:this.setTextFieldFocus});
		}
		
		private function hideWatermarkFast():void
		{
			TweenLite.to(m_Skin.watermark, 0, {alpha:0, onComplete:this.setTextFieldFocus});
		}
		
		override protected function onScreenComplete(e:Event = null):void
		{
			super.onScreenComplete();
		}
		
		override protected function onScreenIncomplete(e:Event = null):void
		{
			super.onScreenIncomplete();
		}
		
		override public function DeInit():void
		{
			CitrusFramework.frameworkStage.removeEventListener(KeyboardEvent.KEY_UP, this.eh_KeyUpHandler);
			CitrusFramework.frameworkStage.removeEventListener(KeyboardEvent.KEY_DOWN, this.eh_KeyDownHandler);
			Queue_PNGExport.Initialize();
			
			var textContainer:MovieClip = new MovieClip();
			// create title card
			if(!m_NoneSelected) 
			{
				
				var titleText:TextBox = new TextBox();
				var videoDims:Point = VideoConstants.VIDEO_DIMS;
				titleText.Initialize(new Point(videoDims.x * 0.7, videoDims.y * 0.7), 150);
				titleText.Font = m_CurrentlySelectedFont;
				titleText.Text = m_Skin.titleTxt.text;
				// set the title to the upload video class
				UploadVideo.currentTitle = titleText.Text;
				titleText.ToggleMultiLine(true);
				titleText.ToggleAutoSize(true);
				textContainer.addChild(titleText);
				
				if(textContainer.width > videoDims.x)
				{
					textContainer.width = videoDims.x;
					textContainer.scaleY = textContainer.scaleX;
				}
				
				if(textContainer.height > videoDims.y)
				{
					textContainer.height = videoDims.y;
					textContainer.scaleX = textContainer.scaleY;
				}
				
			}
			else
			{
				// save GSOC logo
				var placeholder:MovieClip = new NoTitleImage();
				textContainer.addChild(placeholder);
			}
			
			// save title card			
			var titlePath:String = CitrusFramework.frameworkConfigXML.videoEncodingConfigs.videoProcessingDirectory.toString() + CitrusFramework.frameworkConfigXML.videoEncodingConfigs.titleCardPath;
			
			Queue_PNGExport.AddCall(new PNGExportCall(textContainer, titlePath));
			// save out placeholder credit screen for JC
			if (SessionInformation_GirlsTablet.Age == AgeConstants.KEY_JUNIORCADETTE)
			{
				var creditsPath:String = CitrusFramework.frameworkConfigXML.videoEncodingConfigs.videoProcessingDirectory.toString() + CitrusFramework.frameworkConfigXML.videoEncodingConfigs.creditsCardPath;;
		
				textContainer = m_NoTitleImage;
				Queue_PNGExport.AddCall(new PNGExportCall(textContainer, creditsPath));
				Queue_PNGExport.StartQueue();
			}
			
			super.DeInit();
		}
	}
}
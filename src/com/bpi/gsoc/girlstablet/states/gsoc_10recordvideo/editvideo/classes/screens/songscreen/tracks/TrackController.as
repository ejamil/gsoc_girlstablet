package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.screens.songscreen.tracks
{
	import com.bpi.citrusas.sound.Event_SoundHolder;
	import com.bpi.citrusas.sound.SoundHandler;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.data.userdata.GSOC_UserData_Team_DataKeys;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.screens.songscreen.events.Event_TrackInteraction;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.media.Sound;
	import flash.net.URLRequest;

	public class TrackController extends EventDispatcher
	{
		private var m_Tracks:Vector.<TrackSelection>;
		private var m_TrackSelected:TrackSelection;
		private var m_Assets:Vector.<MovieClip>;
		
		private static const ms_ALL_TRACKS_LOADED:String = "ALL TRACKS LOADED";
		
		public function TrackController()
		{
			m_Tracks = new Vector.<TrackSelection>();
		}
		
		public function Initialize(selectableItems:Vector.<MovieClip>):void
		{
			m_Tracks = new Vector.<TrackSelection>();
			m_TrackSelected = null;
			m_Assets = selectableItems;
			
			this.loadTracks(); // more initialization happens after the tracks are loaded
			
		}
		
		private function loadTracks():void
		{
			this.addEventListener(ms_ALL_TRACKS_LOADED, eh_OnTracksLoaded);
			this.loadNextTrack(); // recursive; next track defaults to zero
		}
		
		private function loadNextTrack(index:int = 0):void
		{
			if(index >= TrackReference.GetNumTracksByAge(SessionInformation_GirlsTablet.Age))
			{
				this.dispatchEvent(new Event(ms_ALL_TRACKS_LOADED));
				return;
			}
			
			var trackSound:Sound = new Sound();
			trackSound.addEventListener(Event.COMPLETE, function(e:Event):void
			{
	
				SoundHandler.AddSoundFromObject(TrackReference.GetSongByAge(SessionInformation_GirlsTablet.Age, index).title, e.target as Sound, false);
				loadNextTrack(++index);
			});
			
			trackSound.load(new URLRequest(TrackReference.GetSongByAge(SessionInformation_GirlsTablet.Age, index).sampleURL));
			
		}
		
		private function eh_OnTracksLoaded(e:Event):void
		{
			this.removeEventListener(ms_ALL_TRACKS_LOADED, eh_OnTracksLoaded);
			
			for(var i:int = 0; i < m_Assets.length; i++)
			{
				var trackData:TrackData = TrackReference.GetSongByAge(SessionInformation_GirlsTablet.Age, i);
				var newTrackSelection:TrackSelection = new TrackSelection(m_Assets[i], trackData, SoundHandler.GetSoundLengthInSeconds(trackData.title))
				newTrackSelection.addEventListener(Event_TrackInteraction.CHECK_SELECTED, this.eh_TrackSelected);
				newTrackSelection.addEventListener(Event_TrackInteraction.PLAY_SELECTED, this.eh_PlaySelected);
				newTrackSelection.addEventListener(Event_TrackInteraction.PAUSE_SELECTED, this.eh_PauseSelected);
				m_Tracks.push(newTrackSelection);
			}
		
		}
		
		private function clearAllPlayButtons():void
		{
			for each(var track:TrackSelection in m_Tracks)
			{
				track.ClearPlay();
			}	
		}
		
		private function clearAllCheckButtons():void
		{
			for each (var track:TrackSelection in m_Tracks)
			{
				track.ClearCheck();
			}
		}
		
		public function PauseAllTracks():void
		{
			this.pauseAllTracks();
		}
		
		private function pauseAllTracks(exceptFor:TrackSelection=null):void
		{
			for each (var track:TrackSelection in m_Tracks)
			{
				if(exceptFor)
				{
					if(track != exceptFor) 
					{
						track.PauseTrack();
						SoundHandler.PauseSound(track.trackData.title);
					}
				}
				else
				{
					track.PauseTrack();
					SoundHandler.PauseSound(track.trackData.title);
				}
			}
		}
		
		private function eh_TrackSelected(e:Event_TrackInteraction):void
		{
			this.dispatchEvent(e);
			this.clearAllCheckButtons();
			m_TrackSelected = e.track;
			e.track.HighlightCheck();
			SessionInformation_GirlsTablet.TeamInfo.Team_UserData.SetData(GSOC_UserData_Team_DataKeys.KEY_VIDEO_SONG, e.track.trackData.fullLengthURL);
		}
		
		private function eh_PlaySelected(e:Event_TrackInteraction):void
		{
			this.clearAllPlayButtons();
			this.pauseAllTracks(e.track);
			e.track.HighlightPlay();
			
			if(SoundHandler.IsSoundPaused(e.track.trackData.title))
			{
				SoundHandler.ResumeSound(e.track.trackData.title);
			}
			else
			{
				SoundHandler.GetSoundHolderByName(e.track.trackData.title).addEventListener(Event_SoundHolder.EVENT_COMPLETE_SOUND, this.eh_SoundComplete);
				SoundHandler.PlaySound(e.track.trackData.title,false);
			}
		}
		
		private function eh_PauseSelected(e:Event_TrackInteraction):void
		{	
			this.pauseAllTracks();
			e.track.ClearPlay();
			SoundHandler.PauseSound(e.track.trackData.title);
		}
		
		private function eh_SoundComplete(e:Event_SoundHolder):void
		{
			SoundHandler.GetSoundHolderByName(e.E_SoundName).removeEventListener(Event_SoundHolder.EVENT_COMPLETE_SOUND, this.eh_SoundComplete);
			SoundHandler.GetSoundHolderByName(e.E_SoundName).Stop();
		}
		
		public function OnNoneSelected():void
		{
			m_TrackSelected = null;
			this.clearAllCheckButtons();
			this.clearAllPlayButtons();
			this.pauseAllTracks();
			SessionInformation_GirlsTablet.TeamInfo.Team_UserData.SetData(GSOC_UserData_Team_DataKeys.KEY_VIDEO_SONG, null);
		}
		
		public function GetSelectedTrack():TrackSelection
		{
			return m_TrackSelected;
		}
		
		public function Destroy():void
		{
			for each(var track:TrackSelection in m_Tracks)
			{
				track.removeEventListener(Event_TrackInteraction.CHECK_SELECTED, this.eh_TrackSelected);
				track.removeEventListener(Event_TrackInteraction.PLAY_SELECTED, this.eh_PlaySelected);
				track.removeEventListener(Event_TrackInteraction.PAUSE_SELECTED, this.eh_PauseSelected);
				track.Destroy();
			}
			m_Tracks = null;
			m_TrackSelected = null;
			m_Assets = null;
			
		}
	}
}
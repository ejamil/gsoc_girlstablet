package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo
{
	import com.bpi.gsoc.components.recordvideo.EditVideo_DB_Skin;
	import com.bpi.gsoc.framework.gsocui.GSOC_Header;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.RoleCallout;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.screens.songscreen.SongScreen_DB;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.screens.titlescreen.TitleScreen_DB;

	public class EditVideo_App_DB extends EditVideo_App
	{
		public function EditVideo_App_DB(addPagination:Boolean=false)
		{
			super(addPagination);
		}

		public override function Initialize():void
		{
			m_Skin = new EditVideo_DB_Skin();
			m_EditingStateController.CreateStates(new SongScreen_DB(), new TitleScreen_DB());
			
			m_GSOCHeader = new GSOC_Header(m_Skin.header.paginationArea);
			GSOCheader.addArrows(m_Skin.header.paginationArea.x, 0);
			
			m_RoleCallout = new RoleCallout(m_Skin.header.paginationArea.x, m_Skin.header.paginationArea.y, m_Skin.header.paginationArea.width, m_Skin.header.paginationArea.height);
			
			super.Initialize();
		}
		
		public override function Init():void
		{
			super.Init();
		}
		
		public override function DeInitialize():void
		{
			super.DeInitialize();
		}
	}
}
package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.filtering.filters
{
	import flash.filters.ColorMatrixFilter;
	
	public class Filter
	{
		private static const IDENTITY_MATRIX:Array = [
			1,0,0,0,0,
			0,1,0,0,0,
			0,0,1,0,0,
			0,0,0,1,0,
			0,0,0,0,1
		];
		
		protected static var m_Matrix:Array;
		
		public function GetMatrix():Array
		{
			return m_Matrix;
		}
		
		public function GetMatrixAsString(delimiter:String):String
		{
			var str:String = "";
			
			for(var i:int = 0; i < m_Matrix.length; i++)
			{
				// only take the first four columns of the matrix, because that's what ffmpeg needs
				if((i + 1) % 5 == 0)
				{
					continue;
				}
				str = str.concat( m_Matrix[i].toString());
				if(i < m_Matrix.length - 1)
				{
					str = str.concat(delimiter);
				}
			}
			
			return str;
		}
		
		public function GetColorMatrixFilter():ColorMatrixFilter
		{
			return new ColorMatrixFilter(m_Matrix);
		}
		
		public static function GetIdentityMatrix():Array
		{
			return IDENTITY_MATRIX;
		}
		
		public static function GetNoneFilter():ColorMatrixFilter
		{
			return new ColorMatrixFilter(IDENTITY_MATRIX);
		}
	}
}
package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.screens.songscreen
{
	import com.bpi.gsoc.components.recordvideo.DB_SongSelection;
	import com.bpi.gsoc.framework.constants.GSOC_Info;
	
	import flash.display.MovieClip;

	public class SongScreen_DB extends SongScreen
	{
		public function SongScreen_DB()
		{
			super();
		}
		
		public override function Init():void
		{
			m_Skin = new DB_SongSelection();
			m_GlowColor = GSOC_Info.ColorOne;
		
			m_SelectableItems = new Vector.<MovieClip>();
			m_SelectableItems.push(m_Skin.track1, m_Skin.track2, m_Skin.track3, m_Skin.track4);
				
			super.Init();
		}
		
		public override function DeInit():void
		{
			m_SelectableItems = null;
			super.DeInit();
		}
	}
}
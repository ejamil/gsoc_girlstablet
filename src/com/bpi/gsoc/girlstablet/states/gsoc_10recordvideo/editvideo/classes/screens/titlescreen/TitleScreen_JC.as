package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.screens.titlescreen
{
	import com.bpi.gsoc.components.recordvideo.JC_TitleSelection;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.screens.titlescreen.fonts.EmbeddedVideoFonts;


	public class TitleScreen_JC extends TitleScreen_OlderGirls
	{
		public function TitleScreen_JC()
		{
			super();
		}
		
		override public function Init():void
		{
			m_Skin = new JC_TitleSelection();
			m_GlowColor = 0x0CB1A5;
			m_FontNames.push(EmbeddedVideoFonts.FontName_CarringtonTTF, EmbeddedVideoFonts.FontName_AmericanTypewriterTTF, EmbeddedVideoFonts.FontName_ChunkfiveOTF, EmbeddedVideoFonts.FontName_AirstreamOTF);
			super.Init();
		}
	}
}
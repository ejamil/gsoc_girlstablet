package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.statemanagement
{
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.screens.EditScreen;
	
	import flash.events.Event;

	public class EditScreenStateController
	{
		private var m_Screens:Vector.<EditScreen>;
		private var m_CurrentScreenIndex:int;
		
		public function EditScreenStateController()
		{
			m_Screens = new Vector.<EditScreen>();
			m_CurrentScreenIndex = -1;
		}
		
		public function CreateStates(... states):void
		{
			m_Screens = new Vector.<EditScreen>();
			for each (var state:EditScreen in states)
			{
				m_Screens.push(state);
			}
		}
		
		public function HasNextState():Boolean
		{
			return hasNextState();
		}
	
		private function hasNextState():Boolean
		{
			return (m_Screens.length -1 ) > m_CurrentScreenIndex;
		}
		
		public function GoToNextState(e:Event = null):void
		{	
			if(this.hasNextState())
			{
				if(m_CurrentScreenIndex >= 0) 
				{
					m_Screens[m_CurrentScreenIndex].DeInit();
				}
				
				m_CurrentScreenIndex ++;
				m_Screens[m_CurrentScreenIndex].Init();
			}
		}
		
		public function GoToFirstState():void
		{
			m_CurrentScreenIndex = 0;
			m_Screens[0].Init();
		}
		
		public function GetCurrentEditScreen():EditScreen
		{
			return m_Screens[m_CurrentScreenIndex];
		}
	}
}
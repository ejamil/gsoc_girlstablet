package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.filtering
{
	public class FilterKeys
	{
		public static const BLACK_AND_WHITE:String = "Black & White";
		public static const VINTAGE:String = "Vintage";
		public static const SATURATED:String = "Saturated";
		public static const UNDER_THE_SEA:String = "Cool";
		public static const ON_FIRE:String = "Warm";
		public static const NONE:String = "None";
	}
}
package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.screens.songscreen
{
	import com.bpi.citrusas.input.inputcontroller.InputController;
	import com.bpi.citrusas.sound.SoundHandler;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.sfx.GSOC_SFX;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.screens.EditScreen;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.screens.songscreen.events.Event_TrackInteraction;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.screens.songscreen.tracks.TrackController;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.statemanagement.EditVideoStateIDs;
	import com.greensock.plugins.GlowFilterPlugin;
	import com.greensock.plugins.TweenPlugin;
	
	import flash.events.Event;

	public class SongScreen extends EditScreen
	{	
		protected var m_SelectionMade:Boolean; 
		protected var m_TrackController:TrackController;
		
		public static const NO_TRACK_SELECTED:String = "no track selected";
		
		public function SongScreen()
		{
			super(EditVideoStateIDs.STATE_SONG);
		}
		
		public override function Init():void
		{
			TweenPlugin.activate([GlowFilterPlugin]);
			m_SelectionMade = false;
			m_TrackController = new TrackController();
			m_TrackController.Initialize(m_SelectableItems);
			m_TrackController.addEventListener(Event_TrackInteraction.CHECK_SELECTED, this.eh_OptionSelected);
		
			m_Skin.noneBtn.addEventListener(InputController.CLICK, this.eh_NoneButtonSelected);
			
			this.unhighlightNoneButton();
			super.Init();
		}
		
		private function detectInteractionOnce():void
		{
			var priorSelectionMade:Boolean = m_SelectionMade;
			m_SelectionMade = true;
			if(!priorSelectionMade) super.onScreenComplete();
		}
		
		public override function DeInit():void
		{	
			m_Skin.noneBtn.addEventListener(InputController.CLICK, this.eh_NoneButtonSelected);
			m_TrackController.removeEventListener(Event_TrackInteraction.CHECK_SELECTED, this.eh_OptionSelected);
			m_TrackController.PauseAllTracks();
			m_TrackController.Destroy();
			m_TrackController = null;
			super.DeInit();
			m_Skin = null;
		}
		
		override protected function eh_OptionSelected(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_SELECTMUSICS[SessionInformation_GirlsTablet.Age]);
			this.unhighlightNoneButton();
			this.detectInteractionOnce();	
		}
	
		override protected function eh_NoneButtonSelected(e:Event):void
		{	
			SoundHandler.PlaySound(GSOC_SFX.SOUND_SELECTMUSICS[SessionInformation_GirlsTablet.Age]);
			this.detectInteractionOnce();
			m_TrackController.OnNoneSelected();
			super.eh_NoneButtonSelected(e);
		}
	}
}
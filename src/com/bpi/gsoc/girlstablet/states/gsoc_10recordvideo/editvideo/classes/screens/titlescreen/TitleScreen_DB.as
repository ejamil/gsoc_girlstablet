package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.screens.titlescreen
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.citrusas.sound.SoundHandler;
	import com.bpi.gsoc.components.recordvideo.DB_TitleSelection;
	import com.bpi.gsoc.framework.constants.GSOC_Info;
	import com.bpi.gsoc.framework.constants.TrackConstants;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.sfx.GSOC_SFX;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.screens.exporting.PNGExportCall;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.screens.exporting.Queue_PNGExport;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.encoding.UploadVideo;
	import com.greensock.TweenLite;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.geom.ColorTransform;
	
	public class TitleScreen_DB extends TitleScreen
	{
		private var m_TypedSkin:DB_TitleSelection;
		private var m_CurrentlySelectedTitle:MovieClip;
		private var m_SelectableTitles:Vector.<String>;
		private var m_CurrentlySelectedTitleString:String;
		
		public function TitleScreen_DB()
		{
			super();
		}
		
		override public function Init():void
		{
			m_Skin = new DB_TitleSelection();
			m_TypedSkin = m_Skin as DB_TitleSelection;
			m_GlowColor = GSOC_Info.ColorOne;
			m_SelectableItems = new Vector.<MovieClip>();
			
			m_SelectableTitles = new Vector.<String>();
			
			if(SessionInformation_GirlsTablet.Track == TrackConstants.CAREER)
			{
				m_TypedSkin.gotoAndStop(1);
				m_SelectableItems.push(m_TypedSkin.title1, m_TypedSkin.title2, m_TypedSkin.title3);
				m_SelectableTitles.push("GOOD MORNING girl scouts!", "GIRL SCOUT CHAT", "CAREERTALK");
			}
			else if(SessionInformation_GirlsTablet.Track == TrackConstants.TAKE_ACTION)
			{
				m_TypedSkin.gotoAndStop(2);
				m_SelectableItems.push(m_TypedSkin.title1, m_TypedSkin.title2, m_TypedSkin.title3);
				m_SelectableItems.push(m_TypedSkin.title4);
				m_SelectableTitles.push("MISSION: ISSUE RESOLVED", "Make An Impact", "Take Action Time!", "Community Commercial");
			}
			
			m_CurrentlySelectedTitleString = m_SelectableTitles[0];
		
			super.Init(); // this has to go prior to the gotoAndStop command, otherwise the title card will always show Career
			m_TypedSkin.gotoAndStop(SessionInformation_GirlsTablet.Track + 1);	
		}
		
		override protected function eh_OptionSelected(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_SELECTTITLES[SessionInformation_GirlsTablet.Age]);
			this.clearSelections();
			this.unhighlightNoneButton();
			TweenLite.killTweensOf(e.target);
//			TweenPlugin.activate([GlowFilterPlugin]);
//			TweenLite.to(e.currentTarget, 0.75, {ease: Elastic.easeOut.config(2, 1), glowFilter:{color:m_GlowColor, blurX:80, blurY:80, strength:1.5, alpha:1}});
			(e.target as MovieClip).transform.colorTransform = new ColorTransform(((m_GlowColor >> 16) & 0xff) / 0xff, ((m_GlowColor >> 8) & 0xff) / 0xff, ((m_GlowColor) & 0xff) / 0xff);
			super.onScreenComplete();
			m_CurrentlySelectedTitle = e.target as MovieClip;
			
			// get the index of the title clip and set the title to that clip's text
			var index:int = m_SelectableItems.indexOf(m_CurrentlySelectedTitle);
			if(index != -1 && index < m_SelectableTitles.length)
			{
				m_CurrentlySelectedTitleString = m_SelectableTitles[index];
			}
			else
			{
				m_CurrentlySelectedTitleString = m_SelectableTitles[0];
			}
		}
		
		override protected function eh_NoneButtonSelected(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_SELECTTITLES[SessionInformation_GirlsTablet.Age]);
			m_CurrentlySelectedTitle = m_NoTitleImage;
			super.onScreenComplete();
			super.eh_NoneButtonSelected(e);
		}
		
		override public function DeInit():void
		{
			// just for DB, do this ahead of time so the saved image doesn't have the glow
			for each(var option:MovieClip in m_SelectableItems)
			{
				//option.removeEventListener(InputController.CLICK, eh_OptionSelected);
				option.filters = [];
			}
			
			// save out title image
			Queue_PNGExport.Initialize();
			var titlePath:String = CitrusFramework.frameworkConfigXML.videoEncodingConfigs.videoProcessingDirectory.toString() + CitrusFramework.frameworkConfigXML.videoEncodingConfigs.titleCardPath;
			var creditsPath:String  = CitrusFramework.frameworkConfigXML.videoEncodingConfigs.videoProcessingDirectory.toString() + CitrusFramework.frameworkConfigXML.videoEncodingConfigs.creditsCardPath;
	
			//Utils_Export.ExportAsPngAsynchronous(m_CurrentlySelectedTitle, TabletCamera.REAR_CAMERA_DIMS, titlePath);
			Queue_PNGExport.AddCall(new PNGExportCall(m_CurrentlySelectedTitle, titlePath));
			
			UploadVideo.currentTitle = m_CurrentlySelectedTitleString;
			
			// save out placeholder credit screen
			m_CurrentlySelectedTitle = m_NoTitleImage;
			//Utils_Export.hasAnotherItem(m_CurrentlySelectedTitle, TabletCamera.REAR_CAMERA_DIMS, creditsPath);
			Queue_PNGExport.AddCall(new PNGExportCall(m_CurrentlySelectedTitle, creditsPath));
			Queue_PNGExport.StartQueue();
			super.DeInit();
		}
	}
}
package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.screens
{
	import com.greensock.TweenLite;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.geom.ColorTransform;

	public class EditScreen extends MovieClip
	{
		protected var m_Skin:MovieClip;
		protected var m_ID:String;
		protected var m_SelectableItems:Vector.<MovieClip>;
		protected var m_GlowColor:uint;
		
		public static const SCREEN_COMPLETE:String = "SCREEN_COMPLETE";
		public static const SCREEN_INCOMPLETE:String = "SCREEN_INCOMPLETE";
		
		public function EditScreen(id:String)
		{
			m_ID = id;
		}
		
		public function SaveAssets():void
		{
			
		}
	
		public function Init():void
		{
			m_Skin.stopAllMovieClips();
			this.addChild(m_Skin);
			this.unhighlightNoneButton();
		}
	
		public function DeInit():void
		{
			if(this.contains(m_Skin)) this.removeChild(m_Skin);
		}
		
		protected function eh_OptionSelected(e:Event):void
		{
			this.clearSelections();
			this.unhighlightNoneButton();
			TweenLite.killTweensOf(e.target);
		}
		
		protected function eh_NoneButtonSelected(e:Event):void
		{
			this.clearSelections();
			this.highlightNoneButton();
		}
		
		protected function highlightNoneButton():void
		{
			m_Skin.noneBtn.gotoAndStop(2);
		}
		
		protected function unhighlightNoneButton():void
		{
			m_Skin.noneBtn.gotoAndStop(1);
		}
		
		protected function clearSelections():void
		{
			for each(var option:MovieClip in m_SelectableItems)
			{
				option.transform.colorTransform = new ColorTransform();
			}
		}

		protected function onScreenComplete(e:Event = null):void
		{
			this.dispatchEvent(new Event(EditScreen.SCREEN_COMPLETE));
		}
		
		protected function onScreenIncomplete(e:Event = null):void
		{
			this.dispatchEvent(new Event(EditScreen.SCREEN_INCOMPLETE));
		}
		
		public function get id():String
		{
			return m_ID;
		}
	}
}
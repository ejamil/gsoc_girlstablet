package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.screens.filterscreen
{
	import com.bpi.citrusas.input.inputcontroller.InputController;
	import com.bpi.gsoc.components.recordvideo.FilterBorder;
	import com.bpi.gsoc.components.recordvideo.FilterMask;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.filtering.FilterReference;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.filtering.events.Event_FilterChosen;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.geom.ColorTransform;
	import flash.geom.Point;
	import flash.text.TextField;
	
	public class FilterButton extends MovieClip
	{
		public var titleText:TextField;
		private var unselectedColor:uint = 0xFAA61B;
		private var selectedColor:uint = 0x9D91C6;
		private var m_FilterKey:String;
		private var m_FilterBorder:MovieClip;
		public static var FilterChosenDispatcher:EventDispatcher = new EventDispatcher();
		
		public function FilterButton(background:Sprite, previewImg:Sprite, filterKey:String)
		{
			var tempXY:Point = new Point(background.x, background.y);
			this.addChild(background);
			this.x = tempXY.x;
			this.y = tempXY.y; 
			background.x = background.y = 0;
	
			// size preview to fit
			previewImg.width = background.width - 5; // source image is very slightly too wide
 			previewImg.height = background.height - 5;
			previewImg.y = previewImg.x = 1; // // mask does not match border by like 1 pixel. 
			background.addChild(previewImg);
			var roundedMask:FilterMask = new FilterMask();
			roundedMask.cacheAsBitmap = true;
			this.addChild(roundedMask);
			this.mask  = roundedMask;
			m_FilterBorder = new FilterBorder;
			this.addChild(m_FilterBorder);
			
			this.m_FilterKey = filterKey;
			
			this.GoToUnhighlightedState();
			
			previewImg.filters = [FilterReference.GetFilterByKey(filterKey).GetColorMatrixFilter()];
			this.addEventListener(InputController.CLICK, this.eh_FilterChosen);
		}
		
		private function eh_FilterChosen(e:Event):void
		{
			FilterChosenDispatcher.dispatchEvent(new Event_FilterChosen(Event_FilterChosen.FILTER_CHOSEN, m_FilterKey));
		}
		
		public function get filterKey():String
		{
			return m_FilterKey;
		}
		
		public function GoToHighlightedState():void
		{
			m_FilterBorder.transform.colorTransform = new ColorTransform(((selectedColor >> 16) & 0xff) / 0xff, ((selectedColor >> 8) & 0xff) / 0xff, ((selectedColor) & 0xff) / 0xff);		
		}
		
		public function GoToUnhighlightedState():void
		{
			m_FilterBorder.transform.colorTransform = new ColorTransform(((unselectedColor >> 16) & 0xff) / 0xff, ((unselectedColor >> 8) & 0xff) / 0xff, ((unselectedColor) & 0xff) / 0xff);
		}
	}
}


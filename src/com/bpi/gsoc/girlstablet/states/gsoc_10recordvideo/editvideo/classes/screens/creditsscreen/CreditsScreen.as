package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.screens.creditsscreen
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.citrusas.input.inputcontroller.InputController;
	import com.bpi.citrusas.sound.SoundHandler;
	import com.bpi.citrusas.text.TextBox;
	import com.bpi.citrusas.utils.Utils_Text;
	import com.bpi.gsoc.components.recordvideo.SA_CreditsSelection;
	import com.bpi.gsoc.framework.components.NoTitleImage;
	import com.bpi.gsoc.framework.constants.GSOC_Info;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.sfx.GSOC_SFX;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.commonassets.VideoConstants;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.screens.EditScreen;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.screens.exporting.PNGExportCall;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.screens.exporting.Queue_PNGExport;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.statemanagement.EditVideoStateIDs;
	import com.greensock.TweenLite;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.geom.Point;

	public class CreditsScreen extends EditScreen
	{
		private var m_YesButton:MovieClip;
		private var m_NoButton:MovieClip;
		private var m_TypedSkin:SA_CreditsSelection;
		private var m_AddCredits:Boolean;
		private var m_TempButton:MovieClip;
		
		public function CreditsScreen()
		{
			super(EditVideoStateIDs.STATE_CREDITS);
		}
		
		public override function Init():void
		{
			m_Skin = new SA_CreditsSelection();
			m_TypedSkin = m_Skin as SA_CreditsSelection;
			m_YesButton = m_TypedSkin.yesButton;
			m_NoButton = m_TypedSkin.noneBtn;
			m_GlowColor = GSOC_Info.ColorTwo;
			m_AddCredits = false;
			
			m_SelectableItems = new Vector.<MovieClip>();
			m_SelectableItems.push(m_YesButton);
			
			m_YesButton.addEventListener(InputController.CLICK, this.eh_OptionSelected);
			m_NoButton.addEventListener(InputController.CLICK, this.eh_NoneButtonSelected);
			
			TweenLite.to(m_YesButton, 0.3, {glowFilter:{color:m_GlowColor, blurX:0, blurY:0, strength:1, alpha:0}}); // to fix weird white glow on first tween
			
			super.Init();
		}
		
		override protected function eh_OptionSelected(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_YESNOS[SessionInformation_GirlsTablet.Age]);
			m_AddCredits = true;
			answerChosen();
			m_NoButton.gotoAndStop(1);
			m_YesButton.gotoAndStop(2);
			super.eh_OptionSelected(e);
		}
		
		override protected function eh_NoneButtonSelected(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_YESNOS[SessionInformation_GirlsTablet.Age]);
			m_AddCredits = false;
			answerChosen();
			m_NoButton.gotoAndStop(2);
			m_YesButton.gotoAndStop(1);
			super.eh_NoneButtonSelected(e);
		}
		
		private function answerChosen():void
		{
			this.dispatchEvent(new Event(EditScreen.SCREEN_COMPLETE));
		}
		
		private function exportCredits(e:Event = null):void
		{
			var creditsContainer:MovieClip = new MovieClip();
			if(m_AddCredits)
			{
				var creditsBuilder:CreditsBuilder = new CreditsBuilder();
				var credits:TextBox = new TextBox();
				var videoDims:Point = VideoConstants.VIDEO_DIMS;
				credits.Initialize(new Point(videoDims.x * 0.7, videoDims.y * 0.7), 100);
				credits.ToggleMultiLine(true);
				credits.Font = GSOC_Info.BodyFont;
				credits.Text = creditsBuilder.CreditsString;
				Utils_Text.FitTextToTextfield(credits.TextBoxTextField, 4, true, credits.Size.x, credits.Size.y);
				creditsContainer.addChild(credits);
			}
			else
			{
				var tempCredits:MovieClip  = new NoTitleImage();
				creditsContainer.addChild(tempCredits);
			}
			
			var creditsPath:String = CitrusFramework.frameworkConfigXML.videoEncodingConfigs.videoProcessingDirectory.toString() + CitrusFramework.frameworkConfigXML.videoEncodingConfigs.creditsCardPath;

			Queue_PNGExport.AddCall(new PNGExportCall(creditsContainer, creditsPath));
		}
		
		public override function DeInit():void
		{
			this.exportCredits()
			m_YesButton.removeEventListener(InputController.CLICK, this.eh_OptionSelected);
			m_NoButton.removeEventListener(InputController.CLICK, this.eh_NoneButtonSelected);
			
			Queue_PNGExport.StartQueue();
		}
	}
}
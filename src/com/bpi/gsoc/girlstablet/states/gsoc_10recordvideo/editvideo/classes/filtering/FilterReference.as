package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.filtering
{
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.filtering.filters.Filter;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.filtering.filters.Filter_BlackAndWhite;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.filtering.filters.Filter_None;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.filtering.filters.Filter_OnFire;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.filtering.filters.Filter_Saturated;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.filtering.filters.Filter_UnderTheSea;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.filtering.filters.Filter_Vintage;
	
	import flash.utils.Dictionary;

	public class FilterReference
	{
		private static var m_Filters:Dictionary;
		private static var m_NumFilters:int = 0;
		
		public static function InitFilters():void
		{
			m_Filters = new Dictionary();
			m_Filters[FilterKeys.BLACK_AND_WHITE] = Filter_BlackAndWhite;
			m_Filters[FilterKeys.SATURATED] = Filter_Saturated;
			m_Filters[FilterKeys.VINTAGE] = Filter_Vintage;
			m_Filters[FilterKeys.UNDER_THE_SEA] = Filter_UnderTheSea;
			m_Filters[FilterKeys.ON_FIRE] = Filter_OnFire;
			m_Filters[FilterKeys.NONE] = Filter_None;
			
			for(var filter:String in m_Filters)
			{
				m_NumFilters++;
			}
		}
		
		public static function GetFilterByKey(key:String):Filter
		{
			if(m_NumFilters == 0)
			{
				InitFilters();
			}
		
			return new m_Filters[key]();	
		}
	}
}
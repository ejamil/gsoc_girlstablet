package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.screens.songscreen.tracks
{
	import com.bpi.citrusas.input.inputcontroller.InputController;
	import com.bpi.citrusas.utils.Utils_Image;
	import com.bpi.citrusas.utils.Utils_Text;
	import com.bpi.gsoc.framework.constants.AgeConstants;
	import com.bpi.gsoc.framework.constants.GSOC_Info;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.embeddedcontent.EmbeddedFonts;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.screens.songscreen.events.Event_TrackInteraction;
	import com.greensock.TweenLite;
	import com.greensock.easing.Linear;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.geom.Point;

	public class TrackSelection extends EventDispatcher
	{
		private var m_Background:MovieClip;
		private var m_TrackData:TrackData;
		private var m_ProgBarTween:TweenLite;
		private var m_ProgBarStartPos:Point;
		private var m_Completed:Boolean;

		public function TrackSelection(asset:MovieClip, track:TrackData, soundLength:Number)
		{
			m_Background = asset;
			m_TrackData = track;
			m_Completed = true;
		
			m_Background.checkBoxBtn.addEventListener(InputController.CLICK, this.eh_CheckBoxSelected);
			this.EnablePlay();
			
			m_ProgBarStartPos = new Point(m_Background.progressBar.x, m_Background.progressBar.y);
			var progBarMask:Sprite = new Sprite();
			progBarMask.graphics.beginFill(0x000000);
			progBarMask.graphics.drawRect(m_ProgBarStartPos.x, m_ProgBarStartPos.y, m_Background.progressBar.width, m_Background.progressBar.height);
			progBarMask.graphics.endFill();
			m_Background.addChild(progBarMask);
			m_Background.progressBar.mask = progBarMask;
			
			m_ProgBarTween = new TweenLite(m_Background.progressBar, soundLength, {x:m_ProgBarStartPos.x, paused:true, onComplete:this.songFinishedPlaying, ease:Linear.easeInOut});
			m_Background.playTrackBtn.graphics.beginFill(0xffffff, 0);
			m_Background.playTrackBtn.graphics.drawRect(0, 0, m_Background.playTrackBtn.width, m_Background.playTrackBtn.height);
			m_Background.playTrackBtn.graphics.endFill();
			
			var fontName:String;
			switch(SessionInformation_GirlsTablet.Age)
			{
				case AgeConstants.KEY_DAISYBROWNIE:
					fontName = EmbeddedFonts.FontName_PostinoTTF;
					break;
				default:
					fontName = GSOC_Info.BodyFont;
			}
			Utils_Text.ConvertToEmbeddedFont(m_Background.trackTxt, fontName);
		    m_Background.playTrackBtn.graphics.clear();
			m_Background.trackTxt.text = m_TrackData.title;
			
			m_Background.progressBar.x = - m_Background.progressBar.width;
		}
		
		public function PauseTrack():void
		{
			if(m_ProgBarTween.isActive())
			{
				m_ProgBarTween.pause();
			}
			
			this.ClearPlay();
		}
		
		public function StopTrack():void
		{
			PauseTrack();
			m_ProgBarTween.seek(0);
			m_Background.progressBar.x = m_ProgBarStartPos.x;
		}
		
		private function eh_PlayButtonSelected(e:Event):void
		{
			if(m_ProgBarTween.isActive())
			{
				this.PauseTrack();
				this.dispatchEvent(new Event_TrackInteraction(Event_TrackInteraction.PAUSE_SELECTED, this));
				return;
			}
			
			m_Background.progressBar.x = m_ProgBarStartPos.x - m_Background.progressBar.width;
			
			if(m_ProgBarTween.paused() && m_ProgBarTween._pauseTime > 0)
			{
				m_ProgBarTween.resume();
				this.dispatchEvent(new Event_TrackInteraction(Event_TrackInteraction.PLAY_SELECTED, this));
			}
			else
			{
				m_ProgBarTween.play(0);
				this.dispatchEvent(new Event_TrackInteraction(Event_TrackInteraction.PLAY_SELECTED, this));
			}
		}
		
		private function eh_CheckBoxSelected(e:Event):void
		{
			this.dispatchEvent(new Event_TrackInteraction(Event_TrackInteraction.CHECK_SELECTED, this));
		}
		
		private function songFinishedPlaying():void
		{
			m_Completed = true;
			this.ClearPlay();
		}
		
		public function ClearCheck():void
		{
			m_Background.checkBoxBtn.gotoAndStop(1);
		}
		
		public function ClearPlay():void
		{
			
			m_Background.playTrackBtn.gotoAndStop(1);
		}
		
		public function HighlightPlay():void
		{
			m_Background.playTrackBtn.gotoAndStop(2);
		}
		
		public function HighlightCheck():void
		{
			m_Background.checkBoxBtn.gotoAndStop(2);
		}
		
		public function EnablePlay():void
		{
			m_Background.playTrackBtn.addEventListener(InputController.CLICK, this.eh_PlayButtonSelected);
		}
		
		public function DisablePlay():void
		{
			m_Background.playTrackBtn.removeEventListener(InputController.CLICK, this.eh_PlayButtonSelected);
		}
		
		public function Destroy():void
		{
			m_Background.checkBoxBtn.removeEventListener(InputController.CLICK, this.eh_CheckBoxSelected);
			m_Background.playTrackBtn.removeEventListener(InputController.CLICK, this.eh_PlayButtonSelected);
			m_Background.removeChildren();
			
			m_Background.playTrackBtn = null;
			m_Background = null;
			m_TrackData = null;
		}
		
		public function get trackData():TrackData
		{
			return m_TrackData;
		}	
	}
}
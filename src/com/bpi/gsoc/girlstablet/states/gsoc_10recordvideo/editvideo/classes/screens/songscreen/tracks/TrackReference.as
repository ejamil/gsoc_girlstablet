package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.screens.songscreen.tracks
{
	import com.bpi.gsoc.framework.constants.AgeConstants;

	public class TrackReference
	{
		private static var m_DBTracks:Vector.<TrackData> = new Vector.<TrackData>;
		private static var m_JCTracks:Vector.<TrackData> = new Vector.<TrackData>;
		private static var m_SATracks:Vector.<TrackData> = new Vector.<TrackData>;
		
		public static function Initialize():void
		{
			m_DBTracks.push(new TrackData("data/videosongs/DB/FASTSample.mp3", "data/videosongs/DB/FASTMusic.mp3", "Fast Music"),
				new TrackData("data/videosongs/DB/FUNSample.mp3", "data/videosongs/DB/FUNMusic.mp3",  "Fun Music"), 
				new TrackData("data/videosongs/DB/SERIOUSSample.mp3","data/videosongs/DB/SERIOUSMusic.mp3", "Serious Music"), 
				new TrackData("data/videosongs/DB/SLOWSample.mp3", "data/videosongs/DB/SLOWMusic.mp3", "Slow Music"));
			m_JCTracks.push(new TrackData("data/videosongs/JC/Track 1_Sample.mp3", "data/videosongs/JC/Track 1.mp3", "Feel the Beat"), 
				new TrackData("data/videosongs/JC/Track 2_Sample.mp3", "data/videosongs/JC/Track 2.mp3", "Electronic Tunes"), 
				new TrackData("data/videosongs/JC/Track 3_Sample.mp3", "data/videosongs/JC/Track 3.mp3", "Magical Melody"), 
				new TrackData("data/videosongs/JC/Track 4_Sample.mp3", "data/videosongs/JC/Track 4.mp3", "Dance Party"), 
				new TrackData("data/videosongs/JC/Track 5_Sample.mp3", "data/videosongs/JC/Track 5.mp3", "Get in the Groove"));
			m_SATracks.push(new TrackData("data/videosongs/SA/Track 1_Sample.mp3", "data/videosongs/SA/Track 1.mp3", "Piano Melody"), 
				new TrackData("data/videosongs/SA/Track 2_Sample.mp3", "data/videosongs/SA/Track 2.mp3","Rock & Jam"), 
				new TrackData("data/videosongs/SA/Track 3_Sample.mp3", "data/videosongs/SA/Track 3.mp3", "Electronic Beat"), 
				new TrackData("data/videosongs/SA/Track 4_Sample.mp3", "data/videosongs/SA/Track 4.mp3", "Peaceful Tones"), 
				new TrackData("data/videosongs/SA/Track 5_Sample.mp3", "data/videosongs/SA/Track 5.mp3", "Rhythmic Guitar"));

			
		}
		
		public static function GetSongByAge(age:int, index:int):TrackData
		{
			if(m_DBTracks.length == 0 || m_JCTracks.length == 0 || m_SATracks.length == 0)
			{
				Initialize();
			}
			
			var lookup:Vector.<TrackData>;
			
			switch(age)
			{
				case AgeConstants.KEY_DAISYBROWNIE:
					lookup = m_DBTracks;
					break;
				case AgeConstants.KEY_JUNIORCADETTE:
					lookup = m_JCTracks;
					break;
				case AgeConstants.KEY_SENIORAMBASSADOR:
					lookup = m_SATracks;					
					break;
			}
			
			return lookup[index];
		}
		
		public static function GetNumTracksByAge(age:int):int
		{
			if(m_DBTracks.length == 0 || m_JCTracks.length == 0 || m_SATracks.length == 0)
			{
				Initialize();
			}
			
			var numTracks:int;
			switch(age)
			{
				case AgeConstants.KEY_DAISYBROWNIE:
					numTracks = m_DBTracks.length;
					break;
				case AgeConstants.KEY_JUNIORCADETTE:
					numTracks = m_JCTracks.length;
					break;
				case AgeConstants.KEY_SENIORAMBASSADOR:
					numTracks = m_SATracks.length;					
					break;
			}
			return numTracks;
		}
	}
}
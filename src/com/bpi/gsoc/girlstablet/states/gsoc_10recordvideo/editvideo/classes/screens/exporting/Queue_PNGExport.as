package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.screens.exporting
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.citrusas.utils.Utils_Export;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.commonassets.VideoConstants;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.geom.Point;

	public class Queue_PNGExport
	{
		private static var ms_Queue:Vector.<PNGExportCall>;
		
		private static var ms_CurrentIndex:int;
		public static const PNG_QUEUE_COMPLETE:String = "PNG QUEUE COMPLETE";
		
		public static var QueueEventDispatcher:EventDispatcher = new EventDispatcher();
		
		public static function Initialize():void
		{
			ms_Queue = new Vector.<PNGExportCall>();
			ms_CurrentIndex = 0;
		}
		
		public static function AddCall(call:PNGExportCall):void
		{
			ms_Queue.push(call);		
		}
		
		public static function StartQueue():void
		{
			executeCurrentCall();
		}
		
		public static function eh_CallNext(e:Event):void
		{
			ms_CurrentIndex = ms_CurrentIndex + 1;
			executeCurrentCall();
		}
		
		public static function executeCurrentCall():void
		{
			if(ms_CurrentIndex >= ms_Queue.length) 
			{
				clearAndReset();
				QueueEventDispatcher.dispatchEvent(new Event(PNG_QUEUE_COMPLETE));
			}
			else
			{
				Utils_Export.EventDispatcher_PNGEncoder.addEventListener(Utils_Export.ASYNC_ENCODE_COMPLETE, eh_onImageEncodeComplete);
				Utils_Export.ExportAsPngAsynchronous(ms_Queue[ms_CurrentIndex].source, VideoConstants.VIDEO_DIMS, ms_Queue[ms_CurrentIndex].targetPath);
			}
		}
		
		private static function eh_onImageEncodeComplete(e:Event):void
		{
			eh_CallNext(e);
		}
		
		private static function clearAndReset():void
		{
			ms_CurrentIndex = 0;
			ms_Queue = new Vector.<PNGExportCall>();
		}
		
		public static function ClearQueue():void
		{
			clearAndReset();
		}
	}
}
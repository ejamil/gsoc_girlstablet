package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.statemanagement
{
	public class EditVideoStateIDs
	{
		public static const STATE_SONG:String = "SONG";
		public static const STATE_TITLE:String = "TITLE";
		public static const STATE_FILTER:String = "FILTER";
		public static const STATE_CREDITS:String = "CREDITS";
	}
}
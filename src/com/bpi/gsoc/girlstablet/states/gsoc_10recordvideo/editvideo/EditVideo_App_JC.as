package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo
{
	import com.bpi.gsoc.components.recordvideo.EditVideo_JC_Skin;
	import com.bpi.gsoc.framework.gsocui.GSOC_Header;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.RoleCallout;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.screens.songscreen.SongScreen_OlderGirls;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.screens.titlescreen.TitleScreen_JC;

	public class EditVideo_App_JC extends EditVideo_App
	{
		public function EditVideo_App_JC(addPagination:Boolean=false)
		{
			super(addPagination);
		}
		
		public override function Initialize():void
		{
			m_Skin = new EditVideo_JC_Skin();
			m_EditingStateController.CreateStates(new SongScreen_OlderGirls(),	new TitleScreen_JC());
			
			
			m_GSOCHeader = new GSOC_Header(m_Skin.header.paginationArea);
			GSOCheader.addArrows(m_Skin.header.paginationArea.x, 0);
			
			m_RoleCallout = new RoleCallout(m_Skin.header.paginationArea.x, m_Skin.header.paginationArea.y, m_Skin.header.paginationArea.width, m_Skin.header.paginationArea.height);
			super.Initialize();
		}
		
		public override function DeInitialize():void
		{
			super.DeInitialize();
		}
	}
}
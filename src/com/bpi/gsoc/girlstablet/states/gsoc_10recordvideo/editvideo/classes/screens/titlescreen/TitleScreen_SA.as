package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.screens.titlescreen
{
	import com.bpi.gsoc.components.recordvideo.SA_TitleSelection;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.screens.titlescreen.fonts.EmbeddedVideoFonts;

	public class TitleScreen_SA extends TitleScreen_OlderGirls
	{
		public function TitleScreen_SA()
		{
			super();
		}
		
		override public function Init():void
		{
			m_Skin = new SA_TitleSelection();
			m_GlowColor = 0x9D91C6;
			m_FontNames.push(EmbeddedVideoFonts.FontName_ChalkdusterTTF, EmbeddedVideoFonts.FontName_MailartTTF, EmbeddedVideoFonts.FontName_ChunkfiveOTF, EmbeddedVideoFonts.FontName_ThirstyScriptOTF);
			super.Init();
		}
	}
}
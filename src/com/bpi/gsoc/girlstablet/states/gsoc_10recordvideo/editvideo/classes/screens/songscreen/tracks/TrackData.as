package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.screens.songscreen.tracks
{
	public class TrackData
	{
		private var m_URL_Sample:String;
		private var m_URL_FullLength:String;
		private var m_Title:String;
		
		public function TrackData(sampleUrl:String, fullLengthURL:String, title:String)
		{
			m_URL_Sample = sampleUrl;
			m_URL_FullLength = fullLengthURL;
			m_Title = title;
		}
		
		public function get sampleURL():String
		{
			return m_URL_Sample;
		}
		
		public function get title():String
		{
			return m_Title;
		}
		
		public function get fullLengthURL():String
		{
			return m_URL_FullLength;
		}
	}
}
package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.screens.titlescreen.fonts
{
	import flash.text.TextFormat;

	public class EmbeddedVideoFonts
	{
		// JC Fonts
		[Embed(source="../../../../../../../../../../../data/videofonts/AIRSTREA.ttf", fontName = "Airstream", mimeType = "application/x-font", advancedAntiAliasing="true", embedAsCFF="false")]
		private static const AirstreamTTF:Class;
		public static const FontName_AirstreamOTF:String = "Airstream";
		
		[Embed(source="../../../../../../../../../../../data/videofonts/American Typewriter Regular.ttf", fontName = "American Typewriter", mimeType = "application/x-font", advancedAntiAliasing="true", embedAsCFF="false")]
		private static const AmericanTypewriterTTF:Class;
		public static const FontName_AmericanTypewriterTTF:String = "American Typewriter";
		
		[Embed(source="../../../../../../../../../../../data/videofonts/Carrington.ttf", fontName = "Carrington", mimeType = "application/x-font", advancedAntiAliasing="true", embedAsCFF="false")]
		private static const CarringtonTTF:Class;
		public static const FontName_CarringtonTTF:String = "Carrington";
		
		[Embed(source="../../../../../../../../../../../data/videofonts/Chunkfive.otf", fontName = "Chunkfive", mimeType = "application/x-font", advancedAntiAliasing="true", embedAsCFF="false")]
		private static const ChunkfiveOTF:Class;
		public static const FontName_ChunkfiveOTF:String = "Chunkfive";
		
		// SA Fonts
		[Embed(source="../../../../../../../../../../../data/videofonts/Chalkduster.ttf", fontName = "Chalkduster", mimeType = "application/x-font", advancedAntiAliasing="true", embedAsCFF="false")]
		private static const ChalkdusterTTF:Class;
		public static const FontName_ChalkdusterTTF:String = "Chalkduster";
		
		[Embed(source="../../../../../../../../../../../data/videofonts/MailartRubberstamp.ttf", fontName = "Mailart", mimeType = "application/x-font", advancedAntiAliasing="true", embedAsCFF="false")]
		private static const MailartTTF:Class;
		public static const FontName_MailartTTF:String = "Mailart";
		
//		[Embed(source="../../../../../../../../../../../data/videofonts/Stone Sans ITF TT", fontName = "Mailart", mimeType = "application/x-font", advancedAntiAliasing="true", embedAsCFF="false")]
//		private static const StoneSans:Class;
//		public static const FontName_StoneSans
		
		[Embed(source="../../../../../../../../../../../data/videofonts/ThirstyScriptExtraBoldDemo.otf", fontName = "ThirstyScript", mimeType = "application/x-font", advancedAntiAliasing="true", embedAsCFF="false")]
		private static const ThirstyScriptOTF:Class;
		public static const FontName_ThirstyScriptOTF:String = "ThirstyScript";
		
		/**
		 * This will allow Flash to import the fonts correctly.
		 */
		public static function InitializeFonts():void
		{
			// YOU MUST INITIALIZE A NEW TextFormat OBJECT WITH THE FONT YOU ARE EMBEDDING TO MAKE SURE IT WORKS
			var tf:TextFormat = new TextFormat(FontName_AirstreamOTF);
			tf = new TextFormat(FontName_AmericanTypewriterTTF);
			tf = new TextFormat(FontName_CarringtonTTF);
			tf = new TextFormat(FontName_ChunkfiveOTF);
		
		}
	}
}
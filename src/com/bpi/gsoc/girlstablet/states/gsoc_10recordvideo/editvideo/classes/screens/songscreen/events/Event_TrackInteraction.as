package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.screens.songscreen.events
{
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.screens.songscreen.tracks.TrackSelection;
	
	import flash.events.Event;
	
	public class Event_TrackInteraction extends Event
	{
		public static const PLAY_SELECTED:String = "PLAY SELECTED";
		public static const PAUSE_SELECTED:String = "PAUSE SELECTED";
		public static const CHECK_SELECTED:String = "CHECK SELECTED";

		private var m_AssociatedTrack:TrackSelection;
		
		public function Event_TrackInteraction(type:String, track:TrackSelection, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			m_AssociatedTrack = track;
		}
		
		public function get track():TrackSelection
		{
			return m_AssociatedTrack;
		}
	}
}
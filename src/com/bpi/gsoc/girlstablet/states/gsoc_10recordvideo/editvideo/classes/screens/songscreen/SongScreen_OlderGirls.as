package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.screens.songscreen
{
	import com.bpi.gsoc.components.recordvideo.JC_SongSelection;
	import com.bpi.gsoc.components.recordvideo.SA_SongSelection;
	import com.bpi.gsoc.framework.constants.AgeConstants;
	import com.bpi.gsoc.framework.constants.GSOC_Info;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	
	import flash.display.MovieClip;

	public class SongScreen_OlderGirls extends SongScreen
	{
		public function SongScreen_OlderGirls()
		{
			super();
		}
		
		public override function Init():void
		{
			switch(SessionInformation_GirlsTablet.Age)
			{
				case AgeConstants.KEY_JUNIORCADETTE:
					m_Skin = new JC_SongSelection();
					m_GlowColor = GSOC_Info.ColorOne;
					break;
				case AgeConstants.KEY_SENIORAMBASSADOR:
					m_Skin = new SA_SongSelection();
					m_GlowColor = GSOC_Info.ColorTwo;
					break;
			}
			
			m_SelectableItems = new Vector.<MovieClip>();
			m_SelectableItems.push(m_Skin.track1, m_Skin.track2, m_Skin.track3, m_Skin.track4, m_Skin.track5);
			super.Init();
		}
	}
}
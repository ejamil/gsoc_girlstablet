package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo
{
	import com.bpi.gsoc.components.recordvideo.EditVideo_SA_Skin;
	import com.bpi.gsoc.framework.gsocui.GSOC_Header;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.RoleCallout;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.screens.creditsscreen.CreditsScreen;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.screens.filterscreen.FilterScreen;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.screens.songscreen.SongScreen_OlderGirls;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.screens.titlescreen.TitleScreen_SA;

	public class EditVideo_App_SA extends EditVideo_App
	{
		public function EditVideo_App_SA(addPagination:Boolean=false)
		{
			super(addPagination);	
		}
			
		public override function Initialize():void
		{
			m_Skin = new EditVideo_SA_Skin();
			m_EditingStateController.CreateStates(new SongScreen_OlderGirls(),new TitleScreen_SA(), 
				new FilterScreen(), 
				new CreditsScreen());
			
			m_GSOCHeader = new GSOC_Header(m_Skin.header.paginationArea);
			GSOCheader.addArrows(m_Skin.header.paginationArea.x, 0);
			
			m_RoleCallout = new RoleCallout(m_Skin.header.paginationArea.x, m_Skin.header.paginationArea.y, m_Skin.header.paginationArea.width, m_Skin.header.paginationArea.height);
			
			super.Initialize();
		}
		
		public override function DeInitialize():void
		{
			super.DeInitialize();
		}
	}
}
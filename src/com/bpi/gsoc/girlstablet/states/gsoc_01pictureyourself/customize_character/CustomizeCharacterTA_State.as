package com.bpi.gsoc.girlstablet.states.gsoc_01pictureyourself.customize_character
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.citrusas.input.inputcontroller.InputController;
	import com.bpi.citrusas.sound.SoundHandler;
	import com.bpi.gsoc.framework.avatar.GSOC_Avatar_Builder;
	import com.bpi.gsoc.framework.avatar.OutfitReference;
	import com.bpi.gsoc.framework.constants.AgeConstants;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.data.userdata.GSOC_UserData;
	import com.bpi.gsoc.framework.data.userdata.GSOC_UserData_Girl_DataKeys;
	import com.bpi.gsoc.framework.data.userdata.GSOC_UserData_Girl_ImageKeys;
	import com.bpi.gsoc.framework.gsocui.keyboards.GSOC_Keyboards;
	import com.bpi.gsoc.framework.sfx.GSOC_SFX;
	import com.bpi.gsoc.girlstablet.global_data.FrameControlUtils;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.filters.GlowFilter;
	import flash.utils.Dictionary;

	public class CustomizeCharacterTA_State extends CustomizeCharacter_State
	{
		private var _outfitLookup:Dictionary;
		private var _outfitChosen:Boolean;
		private var _outfits:Vector.<MovieClip>;

		public function CustomizeCharacterTA_State()
		{
			super();
			ScaleHairstyle = 0.4;
			OffsetHairstyle = - 60;
			this._outfitLookup = new Dictionary();
		}
		
		protected override function Init():void
		{
			switch(SessionInformation_GirlsTablet.Age)
			{
				case AgeConstants.KEY_DAISYBROWNIE:
					m_StateGraphics = new CustomizeCharacterTA_App_DB();
					break;
				case AgeConstants.KEY_JUNIORCADETTE:
					m_StateGraphics = new CustomizeCharacterTA_App_JC();
					break;
				case AgeConstants.KEY_SENIORAMBASSADOR:
					m_StateGraphics = new CustomizeCharacterTA_App_SA();
					break;
				default:
					m_StateGraphics = new CustomizeCharacterTA_App_DB();
					
			}

			m_StateGraphics.Init();
			FrameControlUtils.StopAtFrameN(m_StateGraphics);
			GSOC_Keyboards.SetKeyboardType(GSOC_Keyboards.KEYBOARDTYPE_UPPERCASE);
			CitrusFramework.contentLayer.addChild(m_StateGraphics);
				
			this._outfits = new Vector.<MovieClip>();
			for(var i:int = 0; i < (m_StateGraphics.skin.outfitStyles as MovieClip).numChildren; i++)
			{
				
				(m_StateGraphics.skin.outfitStyles as MovieClip).addEventListener(InputController.CLICK, this.eh_outfitChosen);
				
				
				if((m_StateGraphics.skin.outfitStyles as MovieClip).getChildAt(i) is MovieClip)
				{
					this._outfits.push((m_StateGraphics.skin.outfitStyles as MovieClip).getChildAt(i));
				}
			}
			
			this.initOutfitLookup();
			this._outfitChosen = false;
			
			super.commonInit();
		}
		
		private function initOutfitLookup():void
		{
			// key matches outfit instance name.
			// this remaps the outfits on the screen to the  index into a list of TA outfits in the reference
			this._outfitLookup["outfit1"] = 3;
			this._outfitLookup["outfit2"] = 4;
			this._outfitLookup["outfit3"] = 5;
			this._outfitLookup["outfit4"] = 0;
			this._outfitLookup["outfit5"] = 1;
			this._outfitLookup["outfit6"] = 2;
			
			//TODO - change for Take Action when they can actually choose an outfit
			//ud.userData[GSOC_UserData_DataKeys.KEY_OUTFIT] = this._avatar.currentOutfitIndex;
		}
		
		private function eh_outfitChosen(e:Event):void
		{
			if(this._outfitLookup[e.target.name] != null)
			{
				SoundHandler.PlaySound(GSOC_SFX.SOUND_SELECTOUTFITS[SessionInformation_GirlsTablet.Age]);
				this._outfitChosen = true;
				
				for each(var outfit:MovieClip in this._outfits)
				{
					outfit.filters = [];
				}
				
				(e.target as Sprite).filters = [new GlowFilter(m_StateGraphics.m_Glow, 0.5, 30.0, 30.0, 2)];
				
				this._avatar.outfit.removeChildAt(0);
				
				this._avatar.outfit.addChild(new (OutfitReference.GetTakeActionOutfits()[this._outfitLookup[e.target.name]])());
				this._avatar.currentOutfitIndex = this._outfitLookup[e.target.name];
			}
		}
		
		override protected function saveUserData():void
		{
			super.saveUserData();
			var ud:GSOC_UserData = SessionInformation_GirlsTablet.TeamInfo.GetActiveGirlInformation().GirlUserData;
			if(this._outfitChosen)
			{
				ud.userData[GSOC_UserData_Girl_DataKeys.KEY_OUTFIT] = this._avatar.currentOutfitIndex;
			}
			else
			{
				ud.userData[GSOC_UserData_Girl_DataKeys.KEY_OUTFIT] = GSOC_Avatar_Builder.DEFAULT_OUTFIT_INDEX_TAKEACTION;
			}
		}
		
		override protected function createAvatar():void
		{
			var bmp:Sprite = SessionInformation_GirlsTablet.TeamInfo.GetActiveGirlInformation().GirlUserData.GetImage(GSOC_UserData_Girl_ImageKeys.KEY_PICTUREYOURSELF_GIRL_PHOTO);

			this._avatar = GSOC_Avatar_Builder.BuildAvatar(false, bmp, 
				GSOC_Avatar_Builder.DEFAULT_HAIR_INDEX,
				GSOC_Avatar_Builder.DEFAULT_SKIN_COLOR,
				GSOC_Avatar_Builder.DEFAULT_HAIR_COLOR,
				OutfitReference.GetOutfitForTakeActionByIndex(5));
		}
	}
}
package com.bpi.gsoc.girlstablet.states.gsoc_01pictureyourself.customize_character
{
	import com.bpi.gsoc.components.pictureyourself.CustomizeCharacter_SA_Skin;
	import com.bpi.gsoc.framework.constants.AgeColors;
	import com.bpi.gsoc.framework.gsocui.GSOC_Header;
	import com.bpi.gsoc.girlstablet.states.gsoc_01pictureyourself.customize_character.classes.SkinToneChoiceBox;

	public class CustomizeCharacter_App_SA extends CustomizeCharacter_App
	{
		public function CustomizeCharacter_App_SA()
		{
			m_Skin = new CustomizeCharacter_SA_Skin();
			
			m_GSOCHeader = new GSOC_Header(m_Skin.header.paginationArea);
			GSOCheader.addPagination();
			GSOCheader.addArrows(m_Skin.header.paginationArea.x, 0);
			
			m_Glow = AgeColors.SA_GLOW;
			m_SkinToneChoiceShape = SkinToneChoiceBox.SHAPE_ROUND_RECT;
			m_SkinToneChoiceBorderColor =AgeColors.SA_ORANGE;
			super.Initialize();

		}
	}
}
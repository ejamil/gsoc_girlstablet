package com.bpi.gsoc.girlstablet.states.gsoc_01pictureyourself.customize_character.classes
{
	import flash.events.Event;
	
	public class Event_SkinColorChoice extends Event
	{
		public static const SKIN_COLOR_CHOSEN:String = "Skin color chosen";
		private var m_SkinColor:uint;
		private var m_Initiator:SkinToneChoiceBox;
		
		public function Event_SkinColorChoice(type:String, color:uint, sender:SkinToneChoiceBox, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			m_SkinColor = color;
			m_Initiator = sender;
		}
		
		public function get color():uint
		{
			return m_SkinColor;
		}
		
		public function get sender():SkinToneChoiceBox
		{
			return m_Initiator;
		}
	}
}
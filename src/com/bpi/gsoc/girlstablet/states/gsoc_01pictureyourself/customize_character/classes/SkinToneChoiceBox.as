package com.bpi.gsoc.girlstablet.states.gsoc_01pictureyourself.customize_character.classes
{
	import com.bpi.citrusas.input.inputcontroller.InputController;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.filters.DropShadowFilter;
	import flash.filters.GlowFilter;
	
	public class SkinToneChoiceBox extends Sprite
	{
		public static const SHAPE_SQUARE:String = "Square";
		public static const SHAPE_ROUND_RECT:String = "Round rect";
		public static const SHAPE_CIRCLE:String = "Circle";
		
		private var m_ReferenceShape:Sprite;
		private var m_FillColor:uint;
		private var m_BorderColor:uint;
		private var m_GlowColor:uint;
		private var m_Shape:String;
		
		public function SkinToneChoiceBox(referenceSprite:Sprite, fillColor:uint, borderColor:uint, glowColor:uint, shape:String)
		{
			super();
			
			m_ReferenceShape = referenceSprite;
			m_FillColor = fillColor;
			m_BorderColor = borderColor;
			m_GlowColor = glowColor;
			m_Shape = shape;
			
			switch(m_Shape)
			{
				case SHAPE_SQUARE:
					m_ReferenceShape.graphics.lineStyle(m_ReferenceShape.width / 10, m_BorderColor);
					m_ReferenceShape.graphics.beginFill(m_FillColor);
					m_ReferenceShape.graphics.drawRect(0, 0, m_ReferenceShape.width, m_ReferenceShape.width);
					break;
				case SHAPE_ROUND_RECT:
					m_ReferenceShape.graphics.lineStyle(m_ReferenceShape.width / 10, m_BorderColor);
					m_ReferenceShape.graphics.beginFill(m_FillColor);
					m_ReferenceShape.graphics.drawRoundRect(0, 0, m_ReferenceShape.width, m_ReferenceShape.width, m_ReferenceShape.width / 2, m_ReferenceShape.width / 2);
					break;
				case SHAPE_CIRCLE:
					m_ReferenceShape.graphics.lineStyle(m_ReferenceShape.width / 10, m_BorderColor);
					m_ReferenceShape.graphics.beginFill(m_FillColor);
					m_ReferenceShape.graphics.drawCircle(m_ReferenceShape.width/2, m_ReferenceShape.width/2, m_ReferenceShape.width/2);
					break;
				default:
					break;
			}
			
			m_ReferenceShape.graphics.endFill();
			m_ReferenceShape.filters = [new DropShadowFilter(4,45, 0, 0.5, 10, 10, 0.5)];
			
			m_ReferenceShape.addEventListener(InputController.CLICK, this.eh_ReportColor);
		}
		
		private function eh_ReportColor(e:Event):void
		{
			this.dispatchEvent(new Event_SkinColorChoice(Event_SkinColorChoice.SKIN_COLOR_CHOSEN, m_FillColor, this));
		}
		
		public function Destroy():void
		{
			this.graphics.clear();
			m_ReferenceShape.removeEventListener(InputController.CLICK, this.eh_ReportColor);
		}
		
		public function Glow():void
		{
			m_ReferenceShape.filters = [new GlowFilter(m_GlowColor, 0.9, m_ReferenceShape.width / 5, m_ReferenceShape.width / 5)]
		}
		
		public function UnGlow():void
		{
			m_ReferenceShape.filters = [new DropShadowFilter(4,45, 0, 0.5, 10, 10, 0.5)];
		}
	}
}
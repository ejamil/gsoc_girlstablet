package com.bpi.gsoc.girlstablet.states.gsoc_01pictureyourself.customize_character
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.citrusas.events.CourierEvent;
	import com.bpi.citrusas.input.inputcontroller.InputController;
	import com.bpi.citrusas.sound.SoundHandler;
	import com.bpi.citrusas.states.StateControllerManager;
	import com.bpi.gsoc.framework.avatar.GSOC_Avatar_Builder;
	import com.bpi.gsoc.framework.avatar.GSOC_Avatar_Skin;
	import com.bpi.gsoc.framework.avatar.HairstyleReference;
	import com.bpi.gsoc.framework.constants.AgeConstants;
	import com.bpi.gsoc.framework.constants.Keys_FileNames;
	import com.bpi.gsoc.framework.constants.TrackConstants;
	import com.bpi.gsoc.framework.data.sessioninformation.GirlInformation;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.data.userdata.GSOC_UserData;
	import com.bpi.gsoc.framework.data.userdata.GSOC_UserData_Girl_DataKeys;
	import com.bpi.gsoc.framework.data.userdata.GSOC_UserData_Girl_ImageKeys;
	import com.bpi.gsoc.framework.gsocui.ColorPicker;
	import com.bpi.gsoc.framework.gsocui.GSOC_ButtonToggle;
	import com.bpi.gsoc.framework.gsocui.keyboards.GSOC_Keyboards;
	import com.bpi.gsoc.framework.sfx.GSOC_SFX;
	import com.bpi.gsoc.girlstablet.global_data.FrameControlUtils;
	import com.bpi.gsoc.girlstablet.global_data.upload.UploadManager;
	import com.bpi.gsoc.girlstablet.states.gsoc_01pictureyourself.PictureYourself_AppState;
	import com.bpi.gsoc.girlstablet.states.gsoc_01pictureyourself.customize_character.classes.Event_SkinColorChoice;
	import com.bpi.gsoc.girlstablet.states.gsoc_01pictureyourself.customize_character.classes.SkinColorLookup;
	import com.bpi.gsoc.girlstablet.states.gsoc_01pictureyourself.customize_character.classes.SkinToneChoiceContainer;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_IDs;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_AssembleYourTeam;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_ExploreYourPassion;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_Main;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_PictureYourself;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.filters.GlowFilter;
	import flash.geom.ColorTransform;

	public class CustomizeCharacter_State extends PictureYourself_AppState
	{
		private var m_CustomizeCharacter_Group:Vector.<CustomizeCharacter_App>;
		protected var m_StateGraphics:CustomizeCharacter_App;
		protected var _avatar:GSOC_Avatar_Skin;
		private var _hairstyles:Vector.<MovieClip>;
		private var _skinColorChosen:Boolean;
		private var _hairColorChosen:Boolean;
		private var _hairstyleChosen:Boolean;
		private var m_HairColorPicker:ColorPicker;
		private var m_SkinToneColorPicker:SkinToneChoiceContainer;
		protected var ScaleHairstyle:Number = 0.6;
		protected var OffsetHairstyle:Number = -100;
		
		public function CustomizeCharacter_State()
		{
			super(StateController_StateIDs_PictureYourself.STATE_CUSTOMIZECHARACTER);
		}
		
		protected override function Init():void
		{
			switch(SessionInformation_GirlsTablet.Age)
			{
				case AgeConstants.KEY_DAISYBROWNIE:
					m_StateGraphics = new CustomizeCharacter_App_DB();
					break;
				case AgeConstants.KEY_JUNIORCADETTE:
					m_StateGraphics = new CustomizeCharacter_App_JC();
					break;
				case AgeConstants.KEY_SENIORAMBASSADOR:
					m_StateGraphics = new CustomizeCharacter_App_SA();
					break;
				default:
					m_StateGraphics = new CustomizeCharacter_App_DB();		
			}

			m_StateGraphics.Init();
			FrameControlUtils.StopAtFrameN(m_StateGraphics);
			GSOC_Keyboards.SetKeyboardType(GSOC_Keyboards.KEYBOARDTYPE_UPPERCASE);
		
			CitrusFramework.contentLayer.addChild(m_StateGraphics);
			
			this.commonInit();
		}
		
		protected function commonInit():void
		{
			var tempHairstyles:Vector.<MovieClip> = new Vector.<MovieClip>()
			this._hairstyles = new Vector.<MovieClip>();
			tempHairstyles.push((m_StateGraphics.skin.hair_Style_1) as MovieClip);
			tempHairstyles.push((m_StateGraphics.skin.hair_Style_2) as MovieClip);
			tempHairstyles.push((m_StateGraphics.skin.hair_Style_3) as MovieClip);
			tempHairstyles.push((m_StateGraphics.skin.hair_Style_4) as MovieClip);
			tempHairstyles.push((m_StateGraphics.skin.hair_Style_5) as MovieClip);
			if(m_StateGraphics.skin.hair_Style_6) tempHairstyles.push((m_StateGraphics.skin.hair_Style_6) as MovieClip);

			for(var i:int = 0; i < tempHairstyles.length; i++)
			{
				tempHairstyles[i].removeChildAt(0);
				var nextHairstyle:Sprite;
				nextHairstyle = HairstyleReference.GetHairstyleAtIndex(i);
				nextHairstyle.hitArea = (nextHairstyle as Object).hitTarget;
				(nextHairstyle as Object).hitTarget.alpha = 0;
				
				nextHairstyle.x = tempHairstyles[i].x + OffsetHairstyle;
				nextHairstyle.y = tempHairstyles[i].y;
				nextHairstyle.scaleX = nextHairstyle.scaleY = ScaleHairstyle;
				nextHairstyle.transform.colorTransform = GSOC_Avatar_Builder.RGBToColorTransform(GSOC_Avatar_Builder.DEFAULT_HAIR_COLOR);
				
				m_StateGraphics.skin.addChild(nextHairstyle);
				nextHairstyle.addEventListener(InputController.CLICK, this.eh_hairstyleChosen, false, 0, true);

				this._hairstyles.push(nextHairstyle);
			}
			
			this.createAvatar();
			this._avatar.x = CitrusFramework.frameworkStageSize.x / 2 - this._avatar.width / 2;
			this._avatar.y = 400;
			this._avatar.mouseEnabled = false;
			this._avatar.mouseChildren = false;
			m_StateGraphics.skin.addChild(this._avatar);
			
			this._hairColorChosen = this._hairstyleChosen = this._skinColorChosen = false;
			
			m_HairColorPicker = new ColorPicker(m_StateGraphics.skin.hairColorPicker.gradient, m_StateGraphics.skin, "HAIR");
			m_HairColorPicker.addEventListener(InputController.UP, this.eh_hairColorChosenFinal);
			m_SkinToneColorPicker = new SkinToneChoiceContainer();
			
			var colorFunctionCall:Function
			switch(SessionInformation_GirlsTablet.Age)
			{
				case AgeConstants.KEY_DAISYBROWNIE:
					colorFunctionCall = SkinColorLookup.GetDBColorAtIndex;
					break;
				case AgeConstants.KEY_JUNIORCADETTE:
					colorFunctionCall = SkinColorLookup.GetJCColorAtIndex;
					break;
				case AgeConstants.KEY_SENIORAMBASSADOR:
					colorFunctionCall = SkinColorLookup.GetSAColorAtIndex;
					break;
			}
			
			if(m_StateGraphics.skin.skinColorPickerBorder) m_StateGraphics.skin.skinColorPickerBorder.mouseEnabled = false;
			m_StateGraphics.skin.skinColorPicker.mouseEnabled = false;

			for (var index:int = 0; index < m_StateGraphics.skin.skinColorPicker.numChildren; index++)
			{
				if(index == 0) continue;
				m_SkinToneColorPicker.AddSkinColor(m_StateGraphics.skin.skinColorPicker.getChildAt(index),colorFunctionCall(index-1), 
					m_StateGraphics.skinToneChoiceBorderColor, m_StateGraphics.m_Glow, m_StateGraphics.skinToneChoiceShape);
			}
			
			m_SkinToneColorPicker.addEventListener(Event_SkinColorChoice.SKIN_COLOR_CHOSEN, this.eh_skinColorChosen);
			
			
			m_HairColorPicker.addEventListener(m_HairColorPicker.getColorChangeEventName(), this.eh_hairColorChosen, false, 0, true);
			m_HairColorPicker.addEventListener(InputController.UP, this.eh_hairColorChosenFinal, false, 0, true);
			
			m_StateGraphics.GSOCheader.pagination.setNumMembers(SessionInformation_GirlsTablet.TeamInfo.GetTeamMemberCount());
			m_StateGraphics.GSOCheader.pagination.goToIndex(SessionInformation_GirlsTablet.TeamInfo.GetActiveIndex());
			m_StateGraphics.GSOCheader.pagination.setActiveName(SessionInformation_GirlsTablet.TeamInfo.GetActiveGirlInformation().Name);
			
			if(SessionInformation_GirlsTablet.TeamInfo.HasNextTeamMember())
			{
				// hide submit
				GSOC_ButtonToggle.ToggleButton(false, m_StateGraphics.skin.submitButton, null, this.eh_DeactivateSubmit);
				// show next button
				GSOC_ButtonToggle.ToggleButton(true, m_StateGraphics.skin.next_Girl_Button, this.eh_ActivateNext, null);
			}
			else
			{
				// hide next
				GSOC_ButtonToggle.ToggleButton(false, m_StateGraphics.skin.next_Girl_Button, null, this.eh_DeactivateNext);
				//show submit
				GSOC_ButtonToggle.ToggleButton(true, m_StateGraphics.skin.submitButton, this.eh_ActivateSubmit, null);
			}
			EndInit();
		}
		
		protected override function DeInit():void
		{			
			this._avatar = null;
			m_SkinToneColorPicker.removeEventListener(Event_SkinColorChoice.SKIN_COLOR_CHOSEN, this.eh_skinColorChosen);
			m_HairColorPicker.removeEventListener(m_HairColorPicker.getColorChangeEventName(), this.eh_hairColorChosen);
			m_HairColorPicker.removeEventListener(InputController.UP, this.eh_hairColorChosenFinal);
			GSOC_ButtonToggle.ToggleButton(false, m_StateGraphics.skin.next_Girl_Button, null, this.eh_DeactivateNext);
			GSOC_ButtonToggle.ToggleButton(false, m_StateGraphics.skin.submitButton, null, this.eh_DeactivateSubmit);
			CitrusFramework.contentLayer.removeChild(m_StateGraphics);
			
			super.DeInit();
		}
		
		protected function eh_MoveOn(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_CLICKS[SessionInformation_GirlsTablet.Age]);
			this.saveUserData();
			
			if(SessionInformation_GirlsTablet.TeamInfo.HasNextTeamMember())
			{
				SessionInformation_GirlsTablet.TeamInfo.IncrementActiveIndex();
				
				if(SessionInformation_GirlsTablet.Track == TrackConstants.CAREER)
				{
					StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_PICTUREYOURSELF, StateController_StateIDs_PictureYourself.STATE_TAKEPICTURE);
				}
				else
				{
					StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_ASSEMBLEYOURTEAM, StateController_StateIDs_PictureYourself.STATE_TAKEPICTURE);
				}
			}
			else // if we're in career, go on to mc questions. If we're in take action, there's one more screen in this activity
			{
				SessionInformation_GirlsTablet.TeamInfo.ResetActiveIndex();
				
				if(SessionInformation_GirlsTablet.IsRunningLocally)
				{
					if(SessionInformation_GirlsTablet.Track == TrackConstants.CAREER)
					{
						StateControllerManager.LeaveAllActiveStates();
						StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_EXPLOREYOURPASSION, StateController_StateIDs_ExploreYourPassion.STATE_MULTIPLEQUESTIONS);
					}
					else
					{
						StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_ASSEMBLEYOURTEAM, StateController_StateIDs_AssembleYourTeam.STATE_CHOOSETEAMNAME);
					}	
				}
				else
				{
					if(SessionInformation_GirlsTablet.Track == TrackConstants.CAREER)
					{
						submitActivity();
					}
					else
					{
						StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_ASSEMBLEYOURTEAM, StateController_StateIDs_AssembleYourTeam.STATE_CHOOSETEAMNAME);
					}
				}
			}
		}
		
		private function submitActivity():void
		{
			StateControllerManager.LeaveAllActiveStates();
			StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_MAIN, StateController_StateIDs_Main.STATE_SESSIONATTRACT);
			Document.TellServerActivityComplete();
		}
		
		protected function saveUserData():void
		{
			var girlInformation:GirlInformation = SessionInformation_GirlsTablet.TeamInfo.GetActiveGirlInformation();
			var ud:GSOC_UserData = girlInformation.GirlUserData;
			
			ud.userData[GSOC_UserData_Girl_DataKeys.KEY_HAIR_COLOR] = this._hairColorChosen ? this._avatar.currentHairColor : GSOC_Avatar_Builder.DEFAULT_HAIR_COLOR;
			ud.userData[GSOC_UserData_Girl_DataKeys.KEY_SKIN_COLOR] = this._skinColorChosen ? this._avatar.currentSkinColor : GSOC_Avatar_Builder.DEFAULT_SKIN_COLOR;
			ud.userData[GSOC_UserData_Girl_DataKeys.KEY_HAIR_STYLE] = this._hairstyleChosen ? this._avatar.currentHairIndex : GSOC_Avatar_Builder.DEFAULT_HAIR_INDEX;
			ud.userData[GSOC_UserData_Girl_DataKeys.KEY_AVATAR_COMPLETE] = true;
			SessionInformation_GirlsTablet.UploadCurrentInformation();
			ud.SetImage(GSOC_UserData_Girl_ImageKeys.KEY_PICTUREYOURSELF_GIRL_AVATAR, this._avatar);
			UploadManager.UploadDisplayObject(this._avatar, Keys_FileNames.KEY_PICTUREYOURSELF_GIRL_AVATAR, girlInformation.UID);
		}
		
		protected function createAvatar():void
		{
			var bmp:Sprite = SessionInformation_GirlsTablet.TeamInfo.GetActiveGirlInformation().GirlUserData.GetImage(GSOC_UserData_Girl_ImageKeys.KEY_PICTUREYOURSELF_GIRL_PHOTO);
			this._avatar = GSOC_Avatar_Builder.BuildAvatar(false, bmp);
		}
		
		private function eh_hairColorChosenFinal(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_CLICKS[SessionInformation_GirlsTablet.Age]);
		}
		
		private function eh_skinColorChosen(e:Event_SkinColorChoice):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_CLICKS[SessionInformation_GirlsTablet.Age]);
			this._skinColorChosen = true;
			var r:Number = (e.color >> 16) & 0xff;
			var g:Number = (e.color >> 8) & 0xff;
			var b:Number = e.color & 0xff;
			var ct:ColorTransform = new ColorTransform(r / 255, g / 255, b / 255);
			this._avatar.skin.transform.colorTransform = ct;
			this._avatar.currentSkinColor = e.color;
		}
		
		private function eh_hairColorChosen(e:CourierEvent):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_CLICKS[SessionInformation_GirlsTablet.Age]);
			this._hairColorChosen = true;
			var ct:ColorTransform = new ColorTransform(e.payload[0]/255, e.payload[1]/255, e.payload[2]/255);
			this._avatar.currentHairColor = e.payload[0] << 16 | e.payload[1] << 8 | e.payload[2];
			this._avatar.hairstyle.transform.colorTransform = ct;
			for each(var h:Sprite in this._hairstyles)
			{
				h.transform.colorTransform = ct;
			}	
		}
		
		private function eh_hairstyleChosen(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_SELECTHAIRSTYLES[SessionInformation_GirlsTablet.Age]);
			this._hairstyleChosen = true;
			for each(var hs:MovieClip in this._hairstyles)
			{
				hs.filters = [];
			}
			
			(e.currentTarget as Sprite).filters = [new GlowFilter(m_StateGraphics.m_Glow, 0.5, 30.0, 30.0, 2)];
			
			var ct:ColorTransform = this._avatar.hairstyle.transform.colorTransform;
			this._avatar.hairstyle.removeChildren();
			var hairStyle:MovieClip = HairstyleReference.GetHairstyleAtIndex(this._hairstyles.indexOf(e.currentTarget as MovieClip));
			hairStyle.hitTarget.alpha = 0;
			this._avatar.hairstyle.addChild(hairStyle);
			this._avatar.currentHairIndex = this._hairstyles.indexOf(e.currentTarget as MovieClip);
		}
		
		private function eh_ActivateNext(e:Event = null):void
		{
			m_StateGraphics.skin.next_Girl_Button.addEventListener(InputController.CLICK, this.eh_MoveOn);
		}
		
		private function eh_DeactivateNext(e:Event = null):void
		{
			m_StateGraphics.skin.next_Girl_Button.removeEventListener(InputController.CLICK, this.eh_MoveOn);
		}
		
		private function eh_ActivateSubmit(e:Event = null):void
		{
			m_StateGraphics.skin.submitButton.addEventListener(InputController.CLICK, this.eh_MoveOn);
		}
		
		private function eh_DeactivateSubmit(e:Event = null):void
		{
			m_StateGraphics.skin.submitButton.removeEventListener(InputController.CLICK, this.eh_MoveOn);
		}
	}
}
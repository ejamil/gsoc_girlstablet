package com.bpi.gsoc.girlstablet.states.gsoc_01pictureyourself.customize_character
{
	import com.bpi.gsoc.framework.states.app.GSOC_AppSkin;
	
	public class CustomizeCharacter_App extends GSOC_AppSkin
	{
		private var _m_Glow:uint;
		protected var m_SkinToneChoiceShape:String;
		protected var m_SkinToneChoiceBorderColor:uint;
		
		public function CustomizeCharacter_App()
		{
			super(true);
		}

		public function get m_Glow():uint
		{
			return _m_Glow;
		}

		public function set m_Glow(value:uint):void
		{
			_m_Glow = value;
		}
		
		public function get skinToneChoiceShape():String
		{
			return m_SkinToneChoiceShape;
		}
		
		public function get skinToneChoiceBorderColor():uint
		{
			return m_SkinToneChoiceBorderColor;
		}
	}
}
package com.bpi.gsoc.girlstablet.states.gsoc_01pictureyourself.customize_character
{
	import com.bpi.gsoc.components.pictureyourself.CustomizeCharacter_DB_Skin;
	import com.bpi.gsoc.framework.constants.AgeColors;
	import com.bpi.gsoc.framework.constants.GSOC_Info;
	import com.bpi.gsoc.framework.gsocui.GSOC_Header;
	import com.bpi.gsoc.girlstablet.states.gsoc_01pictureyourself.customize_character.classes.SkinToneChoiceBox;

	public class CustomizeCharacter_App_DB extends CustomizeCharacter_App
	{
		public function CustomizeCharacter_App_DB()
		{
			m_Skin = new CustomizeCharacter_DB_Skin();
			
			m_GSOCHeader = new GSOC_Header(m_Skin.header.paginationArea);
			GSOCheader.addPagination();
			GSOCheader.addArrows(m_Skin.header.paginationArea.x, 0);
			
			m_Glow = AgeColors.DB_BLUE;
			m_SkinToneChoiceShape = SkinToneChoiceBox.SHAPE_SQUARE;
			m_SkinToneChoiceBorderColor = GSOC_Info.ColorTwo;
			super.Initialize();
		}
	}
}
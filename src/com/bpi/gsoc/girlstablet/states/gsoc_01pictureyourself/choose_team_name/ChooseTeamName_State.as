package com.bpi.gsoc.girlstablet.states.gsoc_01pictureyourself.choose_team_name
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.citrusas.input.inputcontroller.InputController;
	import com.bpi.citrusas.sound.SoundHandler;
	import com.bpi.citrusas.states.StateControllerManager;
	import com.bpi.citrusas.text.InputTextfieldWithBackground;
	import com.bpi.citrusas.utils.Utils_Image;
	import com.bpi.citrusas.utils.Utils_Text;
	import com.bpi.gsoc.framework.avatar.GSOC_Avatar_Builder;
	import com.bpi.gsoc.framework.constants.AgeColors;
	import com.bpi.gsoc.framework.constants.AgeConstants;
	import com.bpi.gsoc.framework.constants.GSOC_Info;
	import com.bpi.gsoc.framework.constants.Keys_FileNames;
	import com.bpi.gsoc.framework.constants.TrackConstants;
	import com.bpi.gsoc.framework.data.sessioninformation.GirlInformation;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.data.sessioninformation.TeamInformation;
	import com.bpi.gsoc.framework.data.userdata.GSOC_UserData_Team_DataKeys;
	import com.bpi.gsoc.framework.data.userdata.GSOC_UserData_Team_ImageKeys;
	import com.bpi.gsoc.framework.gsocui.GSOC_ButtonToggle;
	import com.bpi.gsoc.framework.gsocui.STEM_Popups;
	import com.bpi.gsoc.framework.gsocui.TeamDisplay;
	import com.bpi.gsoc.framework.gsocui.keyboards.GSOC_Keyboards;
	import com.bpi.gsoc.framework.sfx.GSOC_SFX;
	import com.bpi.gsoc.girlstablet.global_data.FrameControlUtils;
	import com.bpi.gsoc.girlstablet.global_data.upload.UploadManager;
	import com.bpi.gsoc.girlstablet.states.gsoc_01pictureyourself.PictureYourself_AppState;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_IDs;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_AssembleYourTeam;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_ChooseIssue;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_Main;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_PictureYourself;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.geom.Point;
	import flash.text.TextField;
	
	public class ChooseTeamName_State extends PictureYourself_AppState
	{
		private static const MAX_NUM_TEAM_NAME_CHARS:int = 50;
		
		private static var m_MaxAvatarBlockHeight:int;
		private static var m_TeamNameField:InputTextfieldWithBackground;
		
		private var m_TeamDisplay:TeamDisplay;
		
		public function ChooseTeamName_State()
		{
			super(StateController_StateIDs_AssembleYourTeam.STATE_CHOOSETEAMNAME);
		}
		
		public override function Initialize():void
		{
			m_MaxAvatarBlockHeight  = 750; // taken from xfl (loosely)
		}
		
		protected override function Init():void
		{
			STEM_Popups.Reset();
			
			switch(SessionInformation_GirlsTablet.Age)
			{
				case AgeConstants.KEY_DAISYBROWNIE:
					m_Skin = new ChooseTeamName_App_DB();
					break;
				case AgeConstants.KEY_JUNIORCADETTE:
					m_Skin = new ChooseTeamName_App_JC();
					break;
				case AgeConstants.KEY_SENIORAMBASSADOR:
					m_Skin = new ChooseTeamName_App_SA();
					break;
				default:
					m_Skin = new ChooseTeamName_App_DB();	
			}

			m_Skin.Init();
			GSOC_Keyboards.SetKeyboardType(GSOC_Keyboards.KEYBOARDTYPE_UPPERCASE);
			FrameControlUtils.StopAtFrameN(m_Skin);
			toggleGSOCButton(false);
	
			CitrusFramework.contentLayer.addChild(m_Skin);
			this.m_TeamDisplay = new TeamDisplay(new Point(CitrusFramework.frameworkStageSize.x, m_MaxAvatarBlockHeight), true);
			this.populateAvatars();
			this.setUpTeamNameField();
			CitrusFramework.frameworkStage.addEventListener(KeyboardEvent.KEY_DOWN, this.eh_checkTeamNameContents, false, 0, true);
			CitrusFramework.frameworkStage.addEventListener(KeyboardEvent.KEY_UP, this.eh_checkTeamNameContents, false, 0, true);

			EndInit();
		}
	
		protected override function DeInit():void
		{
			CitrusFramework.frameworkStage.removeEventListener(KeyboardEvent.KEY_DOWN, this.eh_checkTeamNameContents);
			CitrusFramework.frameworkStage.removeEventListener(KeyboardEvent.KEY_UP, this.eh_checkTeamNameContents);
			CitrusFramework.contentLayer.removeChild(m_Skin);
			this.eh_deactivateSubmitButton();
			this.m_TeamDisplay.RemoveAvatars();
			
			super.DeInit();
		}
		
		private function populateAvatars():void
		{
			var girlInformationList:Vector.<GirlInformation> = SessionInformation_GirlsTablet.TeamInfo.GetGirlInformationList();
			var color:uint = GSOC_Info.ColorOne;
			
			if(SessionInformation_GirlsTablet.Age == AgeConstants.KEY_DAISYBROWNIE) color = AgeColors.DB_BROWN;
			
			for each(var girlInformation:GirlInformation in girlInformationList)
			{
				this.m_TeamDisplay.AddAvatar(GSOC_Avatar_Builder.BuildAvatarFromUserData(false, girlInformation.GirlUserData, true, TrackConstants.TAKE_ACTION), girlInformation.Name, color);
			}

			// Center it
			this.m_TeamDisplay.y = 500 + m_MaxAvatarBlockHeight - this.m_TeamDisplay.height; // taken from xfl (loosely)
			this.m_TeamDisplay.x = CitrusFramework.frameworkStageSize.x / 2 - this.m_TeamDisplay.width / 2 ;

			m_Skin.addChild(this.m_TeamDisplay);	
		}
		
		private function setUpTeamNameField():void
		{
			// team name label
			Utils_Text.ConvertToEmbeddedFont(m_Skin.skin.teamNameLabel as TextField, GSOC_Info.HeaderFont);
		
			ChooseTeamName_State.m_TeamNameField = new InputTextfieldWithBackground(m_Skin.skin.teamName.nameLabel, m_Skin.skin.teamName, GSOC_Info.ColorOne, true);
			var tf:TextField = m_Skin.skin.teamName.nameLabel as TextField;
			ChooseTeamName_State.m_TeamNameField.SetTextfieldFormat(tf.defaultTextFormat.size as int, tf.defaultTextFormat.color as uint, GSOC_Info.HeaderFont);
			tf.maxChars = MAX_NUM_TEAM_NAME_CHARS; 
			ChooseTeamName_State.m_TeamNameField.InitializeEventListeners();
		}
		
		private function eh_checkTeamNameContents(e:KeyboardEvent):void
		{
			toggleGSOCButton(ChooseTeamName_State.m_TeamNameField.IsPopulated);
		}
		
		private function toggleGSOCButton(toggle:Boolean):void
		{
			GSOC_ButtonToggle.ToggleButton(toggle, m_Skin.skin.submitButton, this.eh_activateSubmitButton, this.eh_deactivateSubmitButton);
		}
		
		private function eh_activateSubmitButton(e:Event = null):void
		{
			m_Skin.skin.submitButton.addEventListener(InputController.CLICK, this.eh_moveOn);
		}
		
		private function eh_deactivateSubmitButton(e:Event = null):void
		{
			m_Skin.skin.submitButton.removeEventListener(InputController.CLICK, this.eh_moveOn);
		}
		
		private function eh_moveOn(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_CLICKS[SessionInformation_GirlsTablet.Age]);
			saveData();
			
			var teamInformation:TeamInformation = SessionInformation_GirlsTablet.TeamInfo;
			teamInformation.Team_UserData.SetData(GSOC_UserData_Team_DataKeys.KEY_TEAM_NAME, m_TeamNameField.Text);
			teamInformation.Team_UserData.SetData(GSOC_UserData_Team_DataKeys.KEY_TEAMNAME_PICKED, true);
			teamInformation.ResetActiveIndex();
			
			if(SessionInformation_GirlsTablet.IsRunningLocally)
			{
				StateControllerManager.LeaveActiveState(StateController_IDs.STATECONTROLLER_ASSEMBLEYOURTEAM, StateController_StateIDs_PictureYourself.STATE_CUSTOMIZECHARACTER);
				StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_CHOOSEISSUE, StateController_StateIDs_ChooseIssue.STATE_CHOOSEISSUE);
			}
			else
			{
				submitActivity();
			}
			
			SessionInformation_GirlsTablet.UploadCurrentInformation();
		}
		
		private function saveData():void
		{
			var mc:MovieClip = new MovieClip();
			mc.addChild(Utils_Image.GetAccurateBitmapOfObject(m_TeamDisplay));
			
			SessionInformation_GirlsTablet.TeamInfo.Team_UserData.SetImage(GSOC_UserData_Team_ImageKeys.KEY_ASSEMBLEYOURTEAM_TEAM, mc);
			UploadManager.UploadDisplayObject(mc, Keys_FileNames.KEY_ASSEMBLEYOURTEAM_TEAM);
		}
		
		private function submitActivity():void
		{
			StateControllerManager.LeaveAllActiveStates();
			StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_MAIN, StateController_StateIDs_Main.STATE_SESSIONATTRACT);
			Document.TellServerActivityComplete();
		}
	}
}
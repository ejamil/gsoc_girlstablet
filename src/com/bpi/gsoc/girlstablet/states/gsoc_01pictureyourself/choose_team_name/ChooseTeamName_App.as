package com.bpi.gsoc.girlstablet.states.gsoc_01pictureyourself.choose_team_name
{
	import com.bpi.gsoc.framework.states.app.GSOC_AppSkin;
	
	public class ChooseTeamName_App extends GSOC_AppSkin
	{
		public function ChooseTeamName_App(addPagination:Boolean=false)
		{
			super(addPagination);
		}
	}
}
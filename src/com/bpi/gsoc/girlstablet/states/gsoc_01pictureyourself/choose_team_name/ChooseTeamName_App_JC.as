package com.bpi.gsoc.girlstablet.states.gsoc_01pictureyourself.choose_team_name
{
	import com.bpi.gsoc.components.buildyourteam.TeamName_JC_Skin;
	import com.bpi.gsoc.framework.constants.AgeFontLinkages;
	import com.bpi.gsoc.framework.gsocui.GSOC_Header;

	public class ChooseTeamName_App_JC extends ChooseTeamName_App
	{
		public function ChooseTeamName_App_JC(addPagination:Boolean=false)
		{
			m_Skin = new TeamName_JC_Skin();
			m_HeaderFont = AgeFontLinkages.JC_HEADER_FONT;
			m_BodyFont = AgeFontLinkages.JC_BODY_FONT;
			m_GSOCHeader = new GSOC_Header(m_Skin.header.paginationArea);
			GSOCheader.addArrows(m_Skin.header.paginationArea.x, 0);
			super.Initialize();
		}
	}
}
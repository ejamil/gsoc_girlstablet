package com.bpi.gsoc.girlstablet.states.gsoc_01pictureyourself.take_picture
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.citrusas.input.inputcontroller.InputController;
	import com.bpi.citrusas.input.multitouch.MultiTouchInput;
	import com.bpi.citrusas.input.multitouch.MultiTouchInput_TouchPoint;
	import com.bpi.citrusas.sound.SoundHandler;
	import com.bpi.citrusas.states.StateControllerManager;
	import com.bpi.gsoc.components.pictureyourself.CameraMask;
	import com.bpi.gsoc.framework.avatar.GSOC_Avatar_Builder;
	import com.bpi.gsoc.framework.avatar.GSOC_Avatar_Skin;
	import com.bpi.gsoc.framework.constants.AgeConstants;
	import com.bpi.gsoc.framework.constants.GSOC_Info;
	import com.bpi.gsoc.framework.constants.Keys_FileNames;
	import com.bpi.gsoc.framework.constants.TrackConstants;
	import com.bpi.gsoc.framework.data.sessioninformation.GirlInformation;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.data.userdata.GSOC_UserData_Girl_ImageKeys;
	import com.bpi.gsoc.framework.gsocui.GSOC_ButtonToggle;
	import com.bpi.gsoc.framework.gsocui.keyboards.GSOC_Keyboards;
	import com.bpi.gsoc.framework.sfx.GSOC_SFX;
	import com.bpi.gsoc.girlstablet.global_data.FrameControlUtils;
	import com.bpi.gsoc.girlstablet.global_data.TabletCamera;
	import com.bpi.gsoc.girlstablet.global_data.upload.UploadManager;
	import com.bpi.gsoc.girlstablet.states.gsoc_01pictureyourself.PictureYourself_AppState;
	import com.bpi.gsoc.girlstablet.states.gsoc_01pictureyourself.take_picture.classes.DragArrows;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_IDs;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_AssembleYourTeam;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_PictureYourself;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.filters.DropShadowFilter;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.media.Camera;
	import flash.media.Video;
	
	public class TakePicture_State extends PictureYourself_AppState
	{
		private var _capturedImageContainer:Sprite;
		private var _origImageContainerPos:Point;
		private var _avatar:GSOC_Avatar_Skin;
		private var _camera:Camera;
		private var _vid:Video;
		private var _capturedImage:BitmapData;
		private var _vidContainer:Sprite;
		public static var InitialScalingFactor:Number;
		private var _previousPanPos:Point;
		private var _previousPinchDistance:Number;
		private var _ovalDims:Point;
		private var _ovalMask:CameraMask;
		private var _origOvalPos:Point;
		private var _videoMaskPos:Point;
		private var _testPic:Sprite;
		private var _currentState:String;
		private static const STATE_LIVE:String = "LIVE";
		private static const STATE_STATIC:String = "STATIC";
		private static const STATE_DECLINE:String = "DECLINE";
		private static const MAX_PIXELS_IN_BMP:uint = 15000000; // smaller than actual value (16,777,215), but safe
		private static var MAX_SCALE:Number;
		private static var MIN_SCALE:Number = 1;
		private var _arrowUI:DragArrows;
		private var _dImagePosition:Point;
		private var _stageHitTarget:Sprite;
		private var _pictureFrame:Sprite;
		
		private var m_GirlPicture:Sprite;
		
		public function TakePicture_State()
		{
			super(StateController_StateIDs_PictureYourself.STATE_TAKEPICTURE);
		}
		
		public override function Initialize():void
		{
			this._avatar = GSOC_Avatar_Builder.BuildAvatar(false);
			MAX_SCALE = Math.sqrt(MAX_PIXELS_IN_BMP / (TabletCamera.FRONT_CAMERA_DIMS.x * TabletCamera.FRONT_CAMERA_DIMS.y));
		}
		
		protected override function Init():void
		{	
			switch(SessionInformation_GirlsTablet.Age)
			{
				case AgeConstants.KEY_DAISYBROWNIE:
					m_Skin = new TakePicture_App_DB();
					break;
				case AgeConstants.KEY_JUNIORCADETTE:
					m_Skin = new TakePicture_App_JC();
					break;
				case AgeConstants.KEY_SENIORAMBASSADOR:
					m_Skin = new TakePicture_App_SA();
					break;
				default:
					m_Skin = new TakePicture_App_DB();	
			}
		
			m_Skin.Init();
			GSOC_Keyboards.SetKeyboardType(GSOC_Keyboards.KEYBOARDTYPE_UPPERCASE);
			if(SessionInformation_GirlsTablet.Track == TrackConstants.CAREER)
			{
				FrameControlUtils.StopAtFrameN(m_Skin, 1);
			}
			else
			{
				FrameControlUtils.StopAtFrameN(m_Skin, 2);
			}
			
			CitrusFramework.contentLayer.addChild(m_Skin);
			
			this._capturedImageContainer = new Sprite();
			this._ovalMask = new CameraMask();
			
			TabletCamera.Init(); // under the surface, this should do nothing if the camera is already initialized
			this._vid = TabletCamera.GetFrontVideo();
			
			this.setUpCamera();
			this.setUpPagination();
			GSOC_ButtonToggle.ToggleButton(false, m_Skin.skin.design_YourCharacter_Button, null, null);
			this.enableOptOutButton();
			this.enterLiveCamView();

			EndInit();
		}
		
		protected override function DeInit():void
		{
			this.disableDesignButton();
			this.disableOptOutButton();
			if(this._capturedImage) this._capturedImage.dispose();
			if(this._arrowUI) this._arrowUI.removeEventListener(Event.ENTER_FRAME, this.animateArrows);
			//this._capturedImage = new BitmapData(0,0);
			CitrusFramework.contentLayer.removeChild(m_Skin);
			m_Skin.visible = false;
			this._vid = null;
			
			super.DeInit();
		}
		
		private function setUpPagination():void
		{
			m_Skin.GSOCheader.pagination.setNumMembers(SessionInformation_GirlsTablet.TeamInfo.GetTeamMemberCount());
			m_Skin.GSOCheader.pagination.goToIndex(SessionInformation_GirlsTablet.TeamInfo.GetActiveIndex());
			m_Skin.GSOCheader.pagination.setActiveName(SessionInformation_GirlsTablet.TeamInfo.GetActiveGirlInformation().Name);
		}
		
		private function setUpCamera():void
		{
			var videoMask:Sprite = new CameraMask();
			this._vidContainer = new Sprite();
			
			this._ovalDims = new Point(m_Skin.skin.oval.width, m_Skin.skin.oval.height);
			
			this.m_Skin.skin.addChild(this._avatar);
			this._avatar.x = CitrusFramework.frameworkStageSize.x / 2 - this._avatar.width / 2;
			this._avatar.y = 400;
			this._avatar.visible = false;

			m_Skin.skin.oval.addChild(videoMask);
			
			if(this._vid)
			{
				this._vidContainer.addChild(this._vid);
			}
			
			TakePicture_State.InitialScalingFactor = (1 + (m_Skin.skin.oval.height - TabletCamera.FRONT_CAMERA_DIMS.y) / TabletCamera.FRONT_CAMERA_DIMS.y) ;
			this._vidContainer.scaleX = -TakePicture_State.InitialScalingFactor;
			this._vidContainer.scaleY = TakePicture_State.InitialScalingFactor;
			this._vidContainer.x = 3 * this._vidContainer.width / 4;
			this._vidContainer.mask = videoMask;
			
			this._videoMaskPos = new Point();
			this._videoMaskPos.x = videoMask.x;
			this._videoMaskPos.y = videoMask.y;
			
			this._arrowUI = new DragArrows();
			this._arrowUI.Initialize(350, 8, GSOC_Info.ColorOne);
			
			m_Skin.skin.oval.addChildAt(this._vidContainer, 0);
			m_Skin.skin.oval.addChildAt(this._capturedImageContainer, 1);
			m_Skin.skin.oval.filters = [new DropShadowFilter(30, 45, 0, .3)];
			this._origImageContainerPos = new Point(this._capturedImageContainer.x, this._capturedImageContainer.y);
			this._origOvalPos = new Point(m_Skin.skin.oval.x, m_Skin.skin.oval.y);
		
			this._arrowUI.x = m_Skin.skin.oval.pictureFrame.width / 2;
			this._arrowUI.y = m_Skin.skin.oval.pictureFrame.height / 2;
			_pictureFrame = m_Skin.skin.oval.pictureFrame;
			m_Skin.skin.oval.addChild(this._arrowUI);	
		}

		private function takePicture(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_TAKEPICTURES[SessionInformation_GirlsTablet.Age]);
			this._capturedImage = new BitmapData(TabletCamera.FRONT_CAMERA_DIMS.x, TabletCamera.FRONT_CAMERA_DIMS.y);
			
			if(this._vid)
			{
				this._capturedImage.draw(this._vid);
			}
			
			this.enterStaticPicView();
		}
		
		private function enterLiveCamView(e:Event = null):void
		{
			this.exitStaticPicView();
			this.exitOptedOutView();
			this._currentState = STATE_LIVE;
			this._vidContainer.visible = true;
			m_Skin.skin.photoLines.visible = true;
			m_Skin.skin.take_Photo_Button.addEventListener(InputController.CLICK, this.takePicture);
			
			SessionInformation_GirlsTablet.TeamInfo.GetActiveGirlInformation().GirlUserData.SetImage(GSOC_UserData_Girl_ImageKeys.KEY_PICTUREYOURSELF_GIRL_PHOTO, null);
			m_GirlPicture = null;
			//this is the only state where you can't save
			GSOC_ButtonToggle.ToggleButton(false, this.m_Skin.skin.design_YourCharacter_Button, this.enableDesignButton, this.disableDesignButton);
		}
		
		private function exitLiveCamView(e:Event = null):void
		{
			m_Skin.skin.take_Photo_Button.removeEventListener(InputController.CLICK, this.takePicture);
			m_Skin.skin.decline_Photo_Button.removeEventListener(InputController.CLICK, this.enterOptedOutView);
			this._vidContainer.visible = false;
			// either of the other states should allow you to save
			GSOC_ButtonToggle.ToggleButton(true, this.m_Skin.skin.design_YourCharacter_Button, this.enableDesignButton, this.disableDesignButton);
		}
		
		private function enterStaticPicView(e:Event = null):void
		{
			this.exitLiveCamView();
			this.exitOptedOutView();
			this._currentState = STATE_STATIC;
			
			var bmd:BitmapData = this._capturedImage;
			var camMask:CameraMask = new CameraMask();
			camMask.x = m_Skin.skin.oval.x; 
			camMask.y = m_Skin.skin.oval.y;
			
			this._capturedImageContainer.addChild(new Bitmap(bmd));
			this._capturedImageContainer.x = this._origImageContainerPos.x;
			this._capturedImageContainer.y = this._origImageContainerPos.y;
			this._capturedImageContainer.scaleX = -TakePicture_State.InitialScalingFactor;
			this._capturedImageContainer.scaleY = TakePicture_State.InitialScalingFactor;
			this._capturedImageContainer.x = 3 * this._vidContainer.width / 4;
			this._capturedImageContainer.mask = camMask;
			
			this._origImageContainerPos = new Point(this._capturedImageContainer.x, this._capturedImageContainer.y);
			
			SessionInformation_GirlsTablet.TeamInfo.GetActiveGirlInformation().GirlUserData.SetImage(GSOC_UserData_Girl_ImageKeys.KEY_PICTUREYOURSELF_GIRL_PHOTO, this._capturedImageContainer);
			m_GirlPicture = this._capturedImageContainer;
			
			// show captured image
			this._capturedImageContainer.visible = true;
			
			// show pinch to zoom ui
			this._arrowUI.ToggleVisibility(true);
			this._arrowUI.addEventListener(Event.ENTER_FRAME, this.animateArrows);
			
			// this is the only state where pinch-to=zoom works
			MultiTouchInput.InitializeEventListeners(CitrusFramework.frameworkStage);
			this.addEventListener(Event.ENTER_FRAME, this.updateTouch);
			
			// now the take pic button takes you back to the live view
			m_Skin.skin.take_Photo_Button.addEventListener(InputController.CLICK, this.enterLiveCamView);
			m_Skin.skin.decline_Photo_Button.addEventListener(InputController.CLICK, this.enterOptedOutView);	
		}
		
		private function exitStaticPicView(e:Event = null):void
		{
			this.removeEventListener(Event.ENTER_FRAME, this.updateTouch);
			this._arrowUI.ToggleVisibility(false);
	
			this._capturedImageContainer.visible = false;
			if(this._capturedImage) 
			{
				this._capturedImage.dispose();
			}
			m_Skin.skin.take_Photo_Button.removeEventListener(InputController.CLICK, this.enterLiveCamView);
		}
		
		private function enterOptedOutView(e:Event = null):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_NOPICTURES[SessionInformation_GirlsTablet.Age]);
			this.exitLiveCamView();
			this.exitStaticPicView();
			this._currentState = STATE_DECLINE;
			
			SessionInformation_GirlsTablet.TeamInfo.GetActiveGirlInformation().GirlUserData.SetImage(GSOC_UserData_Girl_ImageKeys.KEY_PICTUREYOURSELF_GIRL_PHOTO, null);
			m_GirlPicture = null;
			m_Skin.skin.oval.visible = false;
			m_Skin.skin.mouth_Graphic.visible = false;
			m_Skin.skin.eyes_Graphic.visible = false;
			m_Skin.skin.photoLines.visible = false;
			this._avatar.visible = true;
			
			m_Skin.skin.take_Photo_Button.addEventListener(InputController.CLICK, this.enterLiveCamView);
			GSOC_ButtonToggle.ToggleButton(false, m_Skin.skin.decline_Photo_Button, this.enableOptOutButton, this.disableOptOutButton);
		}
		
		private function exitOptedOutView(e:Event = null):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_NOPICTURES[SessionInformation_GirlsTablet.Age]);
			m_Skin.skin.oval.visible = true;
			this._avatar.visible = false;
			m_Skin.skin.mouth_Graphic.visible = true;
			m_Skin.skin.eyes_Graphic.visible = true;
			
			m_Skin.skin.take_Photo_Button.removeEventListener(InputController.CLICK, this.enterLiveCamView);
			GSOC_ButtonToggle.ToggleButton(true, m_Skin.skin.decline_Photo_Button, this.enableOptOutButton, this.disableOptOutButton);
		}
		
		private function nextScreen(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_CLICKS[SessionInformation_GirlsTablet.Age]);
			var activeGirlInformation:GirlInformation = SessionInformation_GirlsTablet.TeamInfo.GetActiveGirlInformation();
			if(this._currentState == STATE_DECLINE)
			{
				activeGirlInformation.GirlUserData.SetImage(GSOC_UserData_Girl_ImageKeys.KEY_PICTUREYOURSELF_GIRL_PHOTO, null); 
				m_GirlPicture = null;
			}
			else if(this._currentState == STATE_STATIC)
			{
				activeGirlInformation.GirlUserData.SetImage(GSOC_UserData_Girl_ImageKeys.KEY_PICTUREYOURSELF_GIRL_PHOTO, this.buildAvatarPicture());
				m_GirlPicture = this.buildAvatarPicture();
			}
			
			if(m_GirlPicture)
			{
				UploadManager.UploadDisplayObject(m_GirlPicture, Keys_FileNames.KEY_PICTUREYOURSELF_GIRL_PHOTO, activeGirlInformation.UID);
			}
			SessionInformation_GirlsTablet.UploadCurrentInformation();
			
			if(SessionInformation_GirlsTablet.Track == TrackConstants.CAREER)
			{
				StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_PICTUREYOURSELF, StateController_StateIDs_PictureYourself.STATE_CUSTOMIZECHARACTER);
			}
			else
			{
				StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_ASSEMBLEYOURTEAM, StateController_StateIDs_AssembleYourTeam.STATE_CUSTOMIZECHARACTER);
			}
		}
		
		private function removeTouch():void
		{
			this._previousPanPos = null;	
			this._previousPinchDistance = 0;
			this._arrowUI.ToggleVisibility(true);
		}
		
		private function updateTouchPan(touchPoint:MultiTouchInput_TouchPoint):void
		{
			var hasTouchPoint:Boolean = hasTouchPoint = _pictureFrame.getBounds(CitrusFramework.frameworkStage).containsPoint(touchPoint.Position);
			
			if(hasTouchPoint)
			{
				this._arrowUI.ToggleVisibility(false);
				
				if(this._previousPanPos)
				{
					var currentPanPos:Point = touchPoint.Position;
					var dist:Number = Point.distance(this._previousPanPos, currentPanPos);
					if(this._previousPinchDistance == 0) // prevent image jump when transitioning from zoom to pan
					{
						this._capturedImageContainer.x -= (this._previousPanPos.x - currentPanPos.x);
						this._capturedImageContainer.y -= (this._previousPanPos.y - currentPanPos.y);
					}	
				}
				
				this._previousPanPos = touchPoint.Position;
				
				this.checkEdgeLimits();
				
				this._previousPinchDistance = 0;
			}
			else
			{
				removeTouch();
			}
		}
		
		private function updateTouchZoom(touchPointOne:MultiTouchInput_TouchPoint, touchPointTwo:MultiTouchInput_TouchPoint):void
		{			
			var hasTouchPointOne:Boolean = _pictureFrame.getBounds(CitrusFramework.frameworkStage).containsPoint(touchPointOne.Position);;
			var hasTouchPointTwo:Boolean = _pictureFrame.getBounds(CitrusFramework.frameworkStage).containsPoint(touchPointTwo.Position);;
			
			//CitrusFramework.localLog.LogEvent("Updating touch zoom: has touch point one? " + hasTouchPointOne + ", and has touch point two? " + hasTouchPointTwo); 
			
			if(hasTouchPointOne && hasTouchPointTwo)
			{
				this._arrowUI.ToggleVisibility(false);
				
				var currentPinchDistance:Number = MultiTouchInput.GetDistanceBetweenTouchPoints(touchPointOne.TouchPointID, touchPointTwo.TouchPointID);
				
				//CitrusFramework.localLog.LogEvent("Previous pinch distance: " + this._previousPinchDistance);
				if(this._previousPinchDistance)
				{
					//CitrusFramework.localLog.LogEvent("Calculating new scale...");
					var deltaPinchDistance:Number = (currentPinchDistance - this._previousPinchDistance);
					var pctScale:Number = deltaPinchDistance / this._previousPinchDistance;
					// calculate new scale
					var newScale:Number = this._capturedImageContainer.scaleX + this._capturedImageContainer.scaleX * pctScale;

					if(Math.abs(newScale) >= TakePicture_State.InitialScalingFactor && Math.abs(newScale) < MAX_SCALE)
					{
						//CitrusFramework.localLog.LogEvent("Updating image to new scale: " + newScale);
						// grab old info
						var oldWidth:Number = this._capturedImageContainer.width;
						var oldHeight:Number = this._capturedImageContainer.height;
						
						this._capturedImageContainer.scaleX = newScale;
						this._capturedImageContainer.scaleY = -newScale; // scaleX was already inverted. uninvert Y so the image doesn't turn upside down
						
						// move the image so it stays centered
						var dWidth:Number = this._capturedImageContainer.width - oldWidth;
						var dHeight:Number = this._capturedImageContainer.height - oldHeight;
						
						this._capturedImageContainer.x += dWidth / 2;
						this._capturedImageContainer.y -= dHeight / 2;
					}
				}
				this._previousPinchDistance = currentPinchDistance;
			}
			else
			{
				removeTouch();
			}
		}
		
		private function updateTouch(e:Event):void
		{
			var currentTouchPoints:Vector.<MultiTouchInput_TouchPoint> = MultiTouchInput.GetAllTouchPoints_Down();

			switch(currentTouchPoints.length)
			{
				case 0:
					removeTouch();
					break
				case 1:
					updateTouchPan(currentTouchPoints[0]);
					break;
				default:
					updateTouchZoom(currentTouchPoints[0], currentTouchPoints[1]);
					break;
			}
		}
		
		private function buildAvatarPicture():Sprite
		{
			var storedMask:DisplayObject = this._capturedImageContainer.mask;
			_capturedImageContainer.mask = null;
			
			var pictureFrame:Sprite = m_Skin.skin.oval.pictureFrame;		
			var bitmapData:BitmapData = new BitmapData(pictureFrame.width, pictureFrame.height);
			
			var pictureMatrix:Matrix = new Matrix();
			pictureMatrix.scale(_capturedImageContainer.scaleX, _capturedImageContainer.scaleY);
			pictureMatrix.translate(_capturedImageContainer.x, _capturedImageContainer.y);
			bitmapData.draw(_capturedImageContainer, pictureMatrix);
			
			var returnAvatarSprite:Sprite = new Sprite();
			returnAvatarSprite.graphics.beginBitmapFill(bitmapData);
			returnAvatarSprite.graphics.drawEllipse(0, 0, bitmapData.width, bitmapData.height);
			returnAvatarSprite.graphics.endFill();
			
			this._capturedImageContainer.mask = storedMask;
	
			return returnAvatarSprite;
		}
		
		private function enableDesignButton(e:Event = null):void
		{
			m_Skin.skin.design_YourCharacter_Button.addEventListener(InputController.CLICK, this.nextScreen);
		}
		
		private function disableDesignButton(e:Event = null):void
		{
			m_Skin.skin.design_YourCharacter_Button.removeEventListener(InputController.CLICK, this.nextScreen);
		}
		
		private function enableOptOutButton(e:Event = null):void
		{
			m_Skin.skin.decline_Photo_Button.addEventListener(InputController.CLICK, this.enterOptedOutView);
		}
		
		private function disableOptOutButton(e:Event = null):void
		{
			m_Skin.skin.decline_Photo_Button.removeEventListener(InputController.CLICK, this.enterOptedOutView);
		}
		
		private function checkEdgeLimits():void
		{
			// left edge
			if(this._capturedImageContainer.x > this._capturedImageContainer.width)
			{
				this._capturedImageContainer.x = this._capturedImageContainer.width;
			}
			
			// right edge
			if(this._capturedImageContainer.x  < this._ovalDims.x) 
			{
				this._capturedImageContainer.x = this._ovalDims.x;
			}
			
			// top edge
			if(this._capturedImageContainer.y > 0)
			{
				this._capturedImageContainer.y = 0;
			}
			
			// bottom edge
			if(this._capturedImageContainer.y + this._capturedImageContainer.height < this._ovalDims.y)
			{
				this._capturedImageContainer.y = this._ovalDims.y - this._capturedImageContainer.height;
			}
		}
		
		private function animateArrows(e:Event):void
		{
			this._arrowUI.Update();
		}
		
		private function capturedImageZeroIsOnStage():Boolean
		{
			var currentPos:Point = this._capturedImageContainer.localToGlobal(new Point());
			return this._stageHitTarget.hitTestPoint(currentPos.x - this._capturedImageContainer.width, currentPos.y);
		}
	}
}
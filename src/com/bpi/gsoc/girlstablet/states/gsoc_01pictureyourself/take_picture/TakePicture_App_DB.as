package com.bpi.gsoc.girlstablet.states.gsoc_01pictureyourself.take_picture
{
	import com.bpi.gsoc.components.pictureyourself.TakePicture_DB_Skin;
	import com.bpi.gsoc.framework.constants.TrackConstants;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.gsocui.GSOC_Header;

	public class TakePicture_App_DB extends TakePicture_App
	{
		public function TakePicture_App_DB()
		{
			m_Skin = new TakePicture_DB_Skin();
			if(SessionInformation_GirlsTablet.Track == TrackConstants.CAREER)
			{
				m_Skin.header.gotoAndStop(1);
			}
			else
			{
				m_Skin.header.gotoAndStop(2);
			}
			m_GSOCHeader = new GSOC_Header(m_Skin.header.paginationArea);
			GSOCheader.addPagination();
			GSOCheader.addArrows(m_Skin.header.paginationArea.x, 0);
			
			super.Initialize();
		}
	}
}
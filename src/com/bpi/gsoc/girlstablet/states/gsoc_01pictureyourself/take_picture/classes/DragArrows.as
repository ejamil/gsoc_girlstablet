package com.bpi.gsoc.girlstablet.states.gsoc_01pictureyourself.take_picture.classes
{
	import com.bpi.citrusas.images.AnimationClip;
	import com.bpi.citrusas.text.TextBox;
	import com.bpi.citrusas.time.DeltaTime;
	import com.bpi.citrusas.utils.Utils_Image;
	import com.bpi.citrusas.utils.Utils_Math;
	import com.bpi.gsoc.framework.components.ArrowButton;
	import com.bpi.gsoc.framework.constants.GSOC_Info;
	
	import flash.display.Sprite;
	import flash.geom.ColorTransform;
	import flash.geom.Point;
	
	public class DragArrows extends Sprite
	{
		private static const ms_MaxDegrees:Number = 360;
		
		private var m_TextBox:TextBox;
		private var m_TextBox_Font_Size:int = 64;
		private var m_TextBox_Font_Color:uint = 0xFFFFFF;
		private var m_TextBox_Text:String = "\nPinch Here to Zoom!\n\n\nDrag Here to Center!";
		
		private var m_Arrows:Vector.<AnimationClip>;
		private var m_Arrows_Count:int;
		
		private var m_Radius:Number;
		
		private var m_Lerp_Amount:Number;
		private var m_Lerp_Direction:Number;
		
		private var m_TintColor:uint;
		
		public function DragArrows()
		{
			m_TextBox = new TextBox();
			m_Arrows = new Vector.<AnimationClip>();
		}
		
		public function Initialize(radius:Number, arrowCount:int = 4, tintColor:uint=0xffffff):void
		{
			m_Radius = radius;
			m_Arrows_Count = arrowCount;
			m_TintColor = tintColor;
			
			reset();
			initializeTextBox();
			initializeArrows();
			
			this.addChild(m_TextBox);
		}
		
		public function Update():void
		{
			animateArrows();
		}
		
		public function Reset():void
		{
			reset();
		}
		
		private function reset():void
		{
			m_Lerp_Amount = 0;
			m_Lerp_Direction = 1;
			
			lerpElements();
		}
		
		private function initializeTextBox():void
		{
			m_TextBox.Initialize(new Point(m_Radius * 2, m_Radius * 2), m_TextBox_Font_Size, m_TextBox_Font_Color);
			m_TextBox.SetPosition(new Point(-m_Radius, -m_Radius));
			m_TextBox.Font = GSOC_Info.HeaderFont;
			m_TextBox.Text = m_TextBox_Text;
			m_TextBox.AlignTextCenter();
			m_TextBox.ToggleVerticalAlign(true);
			m_TextBox.AutoScaleText();
		}
		
		private function initializeArrows():void
		{
			var degreeIncrement:Number = ms_MaxDegrees / m_Arrows_Count;
			var currentDegrees:Number = 0;
			for(var index:int = 0; index < m_Arrows_Count; index++)
			{
//				var arrow:AnimationClip = new AnimationClip(TakePicture_Drag);
				var arrow:AnimationClip = new AnimationClip(ArrowButton);
				if(m_TintColor != 0xffffff)
				{
				
					arrow.transform.colorTransform = new ColorTransform(((m_TintColor >> 16) & 0xff)/255, ((m_TintColor >> 8) & 0xff) / 255, (m_TintColor & 0xff) / 255);
				}
				
				var radians:Number = Utils_Math.DegreeToRadian(currentDegrees);
				var arrowPosition:Point = new Point(m_Radius * Math.cos(radians), m_Radius * Math.sin(radians));
				arrow.SetPosition(arrowPosition);
				Utils_Image.Rotation(arrow, currentDegrees, new Point(arrowPosition.x, arrowPosition.y));
				
				this.addChild(arrow);
				m_Arrows.push(arrow);
				
				currentDegrees += degreeIncrement;
			}
		}
		
		private function animateArrows():void
		{
			lerpElements();
			
			m_Lerp_Amount += (m_Lerp_Direction * DeltaTime.ElapsedDeltaTimeSeconds);
			
			if(m_Lerp_Direction == -1 && m_Lerp_Amount <= 0)
			{
				m_Lerp_Amount = 0;
				m_Lerp_Direction = 1;
			}
			else if(m_Lerp_Direction == 1 && m_Lerp_Amount >= 1)
			{
				m_Lerp_Amount = 1;
				m_Lerp_Direction = -1;
			}
		}
		
		private function lerpElements():void
		{
			for each(var arrow:AnimationClip in m_Arrows)
			{
				arrow.alpha = Utils_Math.LerpValue(0, 1, m_Lerp_Amount);
			}
			m_TextBox.alpha = Utils_Math.LerpValue(0, 1, m_Lerp_Amount);
		}
		
		public function ToggleVisibility(toggle:Boolean):void
		{
			this.visible = toggle;
		}
	}
}
package com.bpi.gsoc.girlstablet.states.gsoc_01pictureyourself.take_picture
{
	import com.bpi.gsoc.framework.states.app.GSOC_AppSkin;
	
	public class TakePicture_App extends GSOC_AppSkin
	{	
		public function TakePicture_App()
		{
			super(true);	
		}
	}
}
package com.bpi.gsoc.girlstablet.states.gsoc_01pictureyourself.loading
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.citrusas.images.AnimationClip;
	import com.bpi.citrusas.text.TextBox;
	import com.bpi.gsoc.components.attractvideos.PictureYourself_Loading_Attract;
	import com.bpi.gsoc.components.attractvideos.PictureYourself_Loading_Box;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.embeddedcontent.EmbeddedFonts_TreFoil;
	import com.bpi.gsoc.framework.gsocui.keyboards.GSOC_Keyboards;
	import com.bpi.gsoc.framework.states.general.servercommunication.states.update.content.ProgressCircle;
	import com.bpi.gsoc.girlstablet.states.gsoc_01pictureyourself.PictureYourself_AppState;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_PictureYourself;
	
	import flash.geom.Point;
	
	public class Loading_State extends PictureYourself_AppState
	{
		private var m_Background:AnimationClip;
		
		private var m_Spinner:ProgressCircle;
		private var m_Spinner_Position:Point = new Point(1370, 950);
		private var m_Spinner_Background:AnimationClip;
		private var m_Spinner_Background_Position:Point = new Point(1070, 650);
		
		protected var m_TextBox:TextBox;
		protected var m_TextBox_Position:Point = new Point(1240, 932);
		protected var m_TextBox_Text:String = "CREATING THE TEAM";
		protected var m_TextBox_Font_Size:int = 64;
		protected var m_TextBox_Font_Color:uint = 0x01a850;
		protected var m_TextBox_Size:Point = new Point(256, 128);
		
		public function Loading_State()
		{
			super(StateController_StateIDs_PictureYourself.STATE_LOADING);
			
			m_Background = new AnimationClip();
			m_Spinner = new ProgressCircle();
			m_Spinner_Background = new AnimationClip();
			m_TextBox = new TextBox();
		}
		
		public override function Initialize():void
		{
			m_Background = new AnimationClip(PictureYourself_Loading_Attract);
			
			m_Spinner.Initialize();
			m_Spinner.SetPosition(m_Spinner_Position);
			
			m_Spinner_Background = new AnimationClip(PictureYourself_Loading_Box);
			m_Spinner_Background.SetPosition(m_Spinner_Background_Position);
			
			initializeTextBox();
		}
		
		protected override function Init():void
		{
			m_Background.Frame = SessionInformation_GirlsTablet.Age;
			
			GSOC_Keyboards.SetKeyboardType(GSOC_Keyboards.KEYBOARDTYPE_UPPERCASE);
			
			CitrusFramework.contentLayer.addChild(m_Background);
			CitrusFramework.contentLayer.addChild(m_Spinner_Background);
			CitrusFramework.contentLayer.addChild(m_Spinner);
			CitrusFramework.contentLayer.addChild(m_TextBox);
			
			EndInit();
		}
		
		protected override function DeInit():void
		{
			CitrusFramework.contentLayer.removeChild(m_Background);
			CitrusFramework.contentLayer.removeChild(m_Spinner_Background);
			CitrusFramework.contentLayer.removeChild(m_Spinner);
			CitrusFramework.contentLayer.removeChild(m_TextBox);
			
			super.DeInit();
		}
		
		public override function Update():void
		{
			m_Spinner.Update();
		}
		
		private function initializeTextBox():void
		{
			m_TextBox.Initialize(m_TextBox_Size, m_TextBox_Font_Size, m_TextBox_Font_Color);
			m_TextBox.Font = EmbeddedFonts_TreFoil.FontName_TreFoil_Sans_Bd;
			m_TextBox.AlignTextCenter();
			m_TextBox.Text = m_TextBox_Text;
			m_TextBox.SetPosition(m_TextBox_Position);
			m_TextBox.AutoScaleText();
		}
	}
}
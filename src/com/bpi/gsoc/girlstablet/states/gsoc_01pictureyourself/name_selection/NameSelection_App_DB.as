package com.bpi.gsoc.girlstablet.states.gsoc_01pictureyourself.name_selection
{
	import com.bpi.gsoc.components.pictureyourself.NameSelection_DB_Skin;
	import com.bpi.gsoc.framework.constants.AgeFontLinkages;
	import com.bpi.gsoc.framework.gsocui.GSOC_Header;

	public class NameSelection_App_DB extends NameSelection_Skin
	{
		public function NameSelection_App_DB()
		{
			m_Skin = new NameSelection_DB_Skin();
			m_HeaderFont = AgeFontLinkages.DB_HEADER_FONT;
			m_BodyFont = AgeFontLinkages.DB_BODY_FONT;
			
			m_GSOCHeader = new GSOC_Header(m_Skin.header.paginationArea);
			GSOCheader.addArrows(m_Skin.header.paginationArea.x, 0);
			
			super.Initialize();			
		}
	}
}
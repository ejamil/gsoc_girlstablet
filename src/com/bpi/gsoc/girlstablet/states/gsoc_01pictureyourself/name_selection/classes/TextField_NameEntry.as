package com.bpi.gsoc.girlstablet.states.gsoc_01pictureyourself.name_selection.classes
{
	import flash.display.DisplayObjectContainer;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.FocusEvent;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	public class TextField_NameEntry extends Sprite
	{
		private var asset:MovieClip;
		private var originalPosition:Point;
		private var maxTextWidth:int;
		
		public static const TEXT_ENTERED:String = "TEXT_ENTERED";
		public static var TextEntryDispatcher:EventDispatcher = new EventDispatcher();
		
		public function TextField_NameEntry(skin:MovieClip, context:DisplayObjectContainer, font:String, useUpper:Boolean = true, tabIndex=0)
		{			
			this.addChild(skin);
			context.addChild(this);
			
			this.asset = skin;
			
			this.originalPosition = new Point();
			this.originalPosition.y = this.asset.nameLabel.y;
			this.originalPosition.x = this.asset.nameLabel.x; 
			this.maxTextWidth = skin.width * 0.8;
			var tf:TextFormat = new TextFormat(font, (this.asset.nameLabel as TextField).defaultTextFormat.size, (this.asset.nameLabel as TextField).defaultTextFormat.color, false);
			
			(this.asset.nameLabel as TextField).defaultTextFormat = tf;
			(this.asset.nameLabel as TextField).embedFonts = true;
			(this.asset.nameLabel as TextField).setTextFormat(tf);
			(this.asset.nameLabel as TextField).tabIndex = tabIndex;
			(this.asset.nameLabel as TextField).addEventListener(FocusEvent.FOCUS_IN, this.eh_Highlight);
			(this.asset.nameLabel as TextField).addEventListener(FocusEvent.FOCUS_OUT, this.eh_Unhighlight);
			
			if(useUpper)
			{
				(this.asset.nameLabel as TextField).addEventListener(Event.CHANGE, eh_ToUpper, false, 0, true);
			}
		}
		
		public function clearField():void
		{
			this.asset.nameLabel.text = "";
			this.asset.nameLabel.scaleX = this.asset.nameLabel.scaleY = 1;
			
		}

		
		private function scaleText(backspace:Boolean = false):void
		{
			while((this.asset.nameLabel as TextField).textWidth * this.asset.nameLabel.scaleX > this.maxTextWidth)
			{
				this.asset.nameLabel.scaleX -= 0.01;
				this.asset.nameLabel.scaleY = this.asset.nameLabel.scaleX;
			}
			
			if(this.asset.nameLabel.scaleX < 1)
			{
				if(!backspace) 
				{
					this.asset.nameLabel.x += (1 - this.asset.nameLabel.scaleX)/0.5;
				}
				else
				{
					this.asset.nameLabel.x -= (1 - this.asset.nameLabel.scaleX)/0.5;
				}
				
				this.asset.nameLabel.y = this.originalPosition.y + ((1 - this.asset.nameLabel.scaleX)/0.03);
			}	
		}
		
		public function getTextFieldLength():Number
		{
			return this.asset.nameLabel.text.length;
		}
		
		public function getText():String
		{
			return this.asset.nameLabel.text;
		}
		
		public function isShown():Boolean
		{
			return this.asset.visible;
		}
		
		public function hide():void
		{
			this.asset.nameLabel.text = "";
			this.asset.visible = false;
		}
		
		public function show():void
		{
			this.asset.visible = true;
		}
		
		public function eh_Highlight(e:Event = null):void
		{
			
			this.asset.gotoAndStop(2);
		}
		
		public function eh_Unhighlight(e:Event = null):void
		{
			this.asset.gotoAndStop(1);
		}
		
		public function enableMouseClick():void
		{
			this.addEventListener(MouseEvent.CLICK, this.eh_Highlight);
		}
		
		public function disableMouseClick():void
		{
			this.removeEventListener(MouseEvent.CLICK, this.eh_Highlight);
		}
		
		private function eh_ToUpper(e:Event):void
		{
			e.target.text = e.target.text.toUpperCase();
			TextField_NameEntry.TextEntryDispatcher.dispatchEvent(new Event(TextField_NameEntry.TEXT_ENTERED));
		}
		
		public function get textField():TextField
		{
			return this.asset.nameLabel as TextField;
		}
	}
}
package com.bpi.gsoc.girlstablet.states.gsoc_01pictureyourself.name_selection
{
	import com.bpi.gsoc.components.pictureyourself.NameSelection_JC_Skin;
	import com.bpi.gsoc.framework.constants.AgeFontLinkages;
	import com.bpi.gsoc.framework.gsocui.GSOC_Header;

	public class NameSelection_App_JC extends NameSelection_Skin
	{
		public function NameSelection_App_JC()
		{
			m_Skin = new NameSelection_JC_Skin();
			m_HeaderFont = AgeFontLinkages.JC_HEADER_FONT;
			m_BodyFont = AgeFontLinkages.JC_BODY_FONT;
			
			m_GSOCHeader = new GSOC_Header(m_Skin.header.paginationArea);
			GSOCheader.addArrows(m_Skin.header.paginationArea.x - 100, 0);
			
			super.Initialize();
		}
	}
}
package com.bpi.gsoc.girlstablet.states.sessionattract
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.citrusas.citrus.framework.layers.LayerIDs;
	import com.bpi.citrusas.states.AppState;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_Main;

	public class AppState_SessionAttract extends AppState
	{
		private var m_AppSessionAttract:App_SessionAttract;
		
		public function AppState_SessionAttract()
		{
			super(StateController_StateIDs_Main.STATE_SESSIONATTRACT);
			
			m_AppSessionAttract = new App_SessionAttract();
		}
		
		public override function Initialize():void
		{
			m_AppSessionAttract.InitializeEventListeners();
			m_AppSessionAttract.Initialize();
			
			super.Initialize();
		}
		
		public override function CleanUp():void
		{
			m_AppSessionAttract.CleanUp();
			
			super.CleanUp();
		}
		
		public override function Update():void
		{
			m_AppSessionAttract.Update();
			
			super.Update();
		}
		
		protected override function Init():void
		{
			m_AppSessionAttract.Init();
			
			CitrusFramework.layerController.GetLayerByID(LayerIDs.CONTENT).addChild(m_AppSessionAttract);
			
			super.Init();
		}
		
		protected override function DeInit():void
		{
			m_AppSessionAttract.DeInit();
			
			CitrusFramework.layerController.GetLayerByID(LayerIDs.CONTENT).removeChild(m_AppSessionAttract);
			
			super.DeInit();
		}
		
		public override function QueuedUpdate():void
		{
			
		}
	}
}
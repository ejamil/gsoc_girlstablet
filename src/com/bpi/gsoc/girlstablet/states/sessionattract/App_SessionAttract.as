package com.bpi.gsoc.girlstablet.states.sessionattract
{
	import com.bpi.citrusas.images.AnimationClip;
	import com.bpi.gsoc.compoments.attractvideos.SessionAttract_Attract_SeniorAmbassador;
	import com.bpi.gsoc.components.attractvideos.SessionAttract_Attract_DaisyBrownie;
	import com.bpi.gsoc.components.attractvideos.SessionAttract_Attract_JuniorCadette;
	import com.bpi.gsoc.components.girlstablet.general.SessionAttract_Background;
	import com.bpi.gsoc.framework.constants.AgeConstants;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.data.sessioninformation.events.Event_SessionInformation_Client;
	import com.bpi.gsoc.framework.states.general.servercommunication.states.update.content.ProgressCircle;
	
	import flash.display.Sprite;
	import flash.geom.Point;

	public class App_SessionAttract extends Sprite
	{
		private static const ms_SpinnerPosition:Point = new Point(1370, 950);
		
		private var m_Spinner:ProgressCircle;
		private var m_Background:AnimationClip;
		
		private var m_Attract:AnimationClip;
		private var m_Attract_Age:int;
		
		public function App_SessionAttract()
		{
			m_Background = new AnimationClip(SessionAttract_Background);
			m_Spinner = new ProgressCircle();
			m_Attract = new AnimationClip();
			m_Attract_Age = -1;
		}
		
		public function InitializeEventListeners():void
		{
			SessionInformation_GirlsTablet.SessionInformationEventDispatcher.addEventListener(Event_SessionInformation_Client.EVENT_NOTIFY_SESSIONINFORMATION_UPDATE, eh_NotifySessionInformationUpdate);
		}
		
		public function Initialize():void
		{	
			m_Spinner.Initialize();
			
			this.addChild(m_Background);
			this.addChild(m_Spinner);
		}
		
		public function CleanUp():void
		{
		}
		
		public function Init():void
		{
			m_Attract.Play();
			m_Spinner.SetPosition(ms_SpinnerPosition);
		}
		
		public function DeInit():void
		{
			m_Attract.Stop();
		}
		
		public function Update():void
		{
			m_Spinner.Update();
		}
		
		public function QueuedUpdate():void
		{
			
		}
		
		public function Resume():void
		{
		}
		
		public function Pause():void
		{
			
		}
		
		private function eh_NotifySessionInformationUpdate(evt:Event_SessionInformation_Client):void
		{
			refreshAttract();
		}
		
		private function refreshAttract():void
		{
			if(m_Attract_Age != SessionInformation_GirlsTablet.Age)
			{
				m_Attract_Age = SessionInformation_GirlsTablet.Age;
				if(this.contains(m_Attract))
				{
					this.removeChild(m_Attract);
				}
				m_Attract = new AnimationClip(getAttractClass());
				this.addChild(m_Attract);
				m_Attract.Play();
			}
		}
		
		private function getAttractClass():Class
		{
			var attractClass:Class = SessionAttract_Attract_DaisyBrownie;
			switch(SessionInformation_GirlsTablet.Age)
			{
				case AgeConstants.KEY_DAISYBROWNIE:
					attractClass = SessionAttract_Attract_DaisyBrownie;
					break;
				case AgeConstants.KEY_JUNIORCADETTE:
					attractClass = SessionAttract_Attract_JuniorCadette;
					break;
				case AgeConstants.KEY_SENIORAMBASSADOR:
					attractClass = SessionAttract_Attract_SeniorAmbassador;
					break;
			}
			return attractClass;
		}
	}
}
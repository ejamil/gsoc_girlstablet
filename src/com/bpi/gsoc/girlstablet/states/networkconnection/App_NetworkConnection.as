package com.bpi.gsoc.girlstablet.states.networkconnection
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.citrusas.images.AnimationClip;
	import com.bpi.citrusas.input.singleinput.Input;
	import com.bpi.citrusas.states.StateControllerManager;
	import com.bpi.gsoc.framework.components.NetworkConnection_Background;
	import com.bpi.gsoc.framework.states.general.networkconnection.ConnectionField;
	import com.bpi.gsoc.girlstablet.states.networkconnection.tabletcolorselection.TabletColorSelection;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_IDs;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_Main;
	
	import flash.display.Sprite;
	import flash.ui.Keyboard;

	public class App_NetworkConnection extends Sprite
	{
		private var m_Background:AnimationClip;
		
		private var m_ConnectionField:ConnectionField;
		
		private var m_TabletColorSelection:TabletColorSelection;
		
		public function App_NetworkConnection()
		{
			m_Background = new AnimationClip(NetworkConnection_Background);
			m_ConnectionField = new ConnectionField();
			m_TabletColorSelection = new TabletColorSelection();
		}
		
		public function InitializeEventListeners():void
		{
			m_ConnectionField.InitializeEventListeners();
			m_TabletColorSelection.InitializeEventListeners();
		}
		
		public function Initialize():void
		{
			m_ConnectionField.Initialize(CitrusFramework.frameworkStageSize);
			m_TabletColorSelection.Initialize();
			
			this.addChild(m_Background);
			this.addChild(m_ConnectionField);
			this.addChild(m_TabletColorSelection);
		}
		
		public function CleanUp():void
		{
			
		}
		
		public function Update():void
		{
			m_ConnectionField.Update();
			m_TabletColorSelection.Update();
			checkExitNetworkConnection();
		}
		
		public function Init():void
		{
			m_TabletColorSelection.Init();
			m_ConnectionField.Init();
		}
		
		public function DeInit():void
		{
			m_TabletColorSelection.DeInit();
			m_ConnectionField.DeInit();
		}
		
		public function Resume():void
		{
			
		}
		
		public function Pause():void
		{
			
		}
		
		private function checkExitNetworkConnection():void
		{
			if(((Input.IsKeyPressed(Keyboard.N) && Input.IsKeyDown(Keyboard.CONTROL))) || Input.IsKeyPressed(Keyboard.ENTER))
			{
				StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_MAIN, StateController_StateIDs_Main.STATE_ATTRACT);
			}
		}
	}
}
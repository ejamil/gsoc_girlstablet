package com.bpi.gsoc.girlstablet.states.networkconnection
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.citrusas.citrus.framework.layers.LayerIDs;
	import com.bpi.citrusas.states.AppState;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_Main;
	
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;

	public class AppState_NetworkConnection extends AppState
	{
		private static var ms_EventDispatcher:IEventDispatcher = new EventDispatcher();
		
		private var m_App_NetworkConnection:App_NetworkConnection;
		
		public function AppState_NetworkConnection()
		{
			super(StateController_StateIDs_Main.STATE_NETWORKCONNECTION);
			
			m_App_NetworkConnection = new App_NetworkConnection();
		}
		
		public override function Initialize():void
		{
			m_App_NetworkConnection.InitializeEventListeners();
			m_App_NetworkConnection.Initialize();
			
			this.addChild(m_App_NetworkConnection);
			
			super.Initialize();
		}
		
		public override function CleanUp():void
		{
			m_App_NetworkConnection.CleanUp();
			
			super.CleanUp();
		}
		
		public override function Update():void
		{
			m_App_NetworkConnection.Update();
			
			super.Update();
		}
		
		protected override function Init():void
		{
			m_App_NetworkConnection.Init();
			
			CitrusFramework.layerController.AddChildToLayer(LayerIDs.CONTENT, m_App_NetworkConnection);
			
			super.Init();
		}
		
		protected override function DeInit():void
		{		
			m_App_NetworkConnection.DeInit();
			
			CitrusFramework.layerController.RemoveChildFromLayer(LayerIDs.CONTENT, m_App_NetworkConnection);
			CitrusFramework.frameworkStage.focus = null;
			
			super.DeInit();
		}
		
		public override function Resume():void
		{
			super.Resume();
		}
		
		public override function Pause():void
		{
			super.Pause();
		}
		
		public static function get NetworkConnectionEventDispatcher():IEventDispatcher
		{
			return ms_EventDispatcher;
		}
	}
}
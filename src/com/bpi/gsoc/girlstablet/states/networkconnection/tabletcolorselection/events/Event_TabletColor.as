﻿package com.bpi.gsoc.girlstablet.states.networkconnection.tabletcolorselection.events
{
	import flash.events.Event;
	
	public class Event_TabletColor extends Event
	{
		public static const EVENT_SET_ACTIVE_TABLETCOLOR:String = "EVENT_SET_ACTIVE_TABLETCOLOR";
		public static const EVENT_SAVE_ACTIVE_TABLETCOLOR:String = "EVENT_SAVE_ACTIVE_TABLETCOLOR";
		
		private var m_Index:int;
		
		public function Event_TabletColor(type:String, index:int) 
		{
			super(type, false, false);
			
			m_Index = index;
		}
		
		public function get E_Index():int
		{
			return m_Index;
		}
	}
}
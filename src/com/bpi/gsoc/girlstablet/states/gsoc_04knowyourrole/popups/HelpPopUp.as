package com.bpi.gsoc.girlstablet.states.gsoc_04knowyourrole.popups
{
	import com.bpi.citrusas.utils.Utils_Text;
	import com.bpi.gsoc.framework.constants.GSOC_Info;
	import com.bpi.gsoc.framework.gsocui.SkillMap.Popups.SkillMap_Popup;
	import com.greensock.TweenMax;
	import com.greensock.easing.Back;
	
	import flash.display.MovieClip;
	import flash.text.TextField;

	public class HelpPopUp extends SkillMap_Popup
	{
		private var m_Textfield:TextField;
		private var m_BubblePos_Y:Number;
		
		public function HelpPopUp(skin:MovieClip)
		{
			super(skin, skin.closePopUp);
			m_Textfield = skin.textbox;
			m_BubblePos_Y = skin.bubble.y;
		}
		
		public override function Initialize():void
		{
			Utils_Text.EmbedText(m_Textfield, GSOC_Info.BodyFont, 60, GSOC_Info.ColorOne); 
			
			m_Textfield.height = 160;
			m_Textfield.multiline = true;
			m_Textfield.wordWrap = true;
			m_Skin.bubble.mouseEnabled = false;
			
			super.Initialize();
		}
		
		public override function DisplayPopUp():void
		{
			super.DisplayPopUp();
			TweenMax.allFromTo([m_Skin.bubble, m_Textfield], .5, {alpha: 0, scaleX: 3, scaleY: 3}, {alpha: 1, delay: .25, scaleX:1, scaleY: 1, ease: Back.easeOut.config(1)});
		}
		
		public function set Text(text:String):void
		{
			m_Textfield.text = text;
			Utils_Text.FitTextToTextfield(m_Textfield, 20);
		}
	}
}
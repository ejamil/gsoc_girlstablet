package com.bpi.gsoc.girlstablet.states.gsoc_04knowyourrole.choice_web
{
	import com.bpi.citrusas.input.inputcontroller.InputController;
	import com.bpi.citrusas.sound.SoundHandler;
	import com.bpi.citrusas.states.StateControllerManager;
	import com.bpi.citrusas.utils.Utils_Image;
	import com.bpi.citrusas.utils.Utils_Text;
	import com.bpi.gsoc.framework.avatar.GSOC_Avatar_Builder;
	import com.bpi.gsoc.framework.constants.ButtonPlacement;
	import com.bpi.gsoc.framework.constants.Keys_FileNames;
	import com.bpi.gsoc.framework.data.sessioninformation.GirlInformation;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.data.userdata.GSOC_UserData_Girl_DataKeys;
	import com.bpi.gsoc.framework.data.userdata.GSOC_UserData_Girl_ImageKeys;
	import com.bpi.gsoc.framework.gsocui.GSOC_ButtonToggle;
	import com.bpi.gsoc.framework.gsocui.SkillMap.SkillMap;
	import com.bpi.gsoc.framework.gsocui.SkillMap.Events.Event_AddYourOwnSelected;
	import com.bpi.gsoc.framework.gsocui.SkillMap.Events.Event_MapStatus;
	import com.bpi.gsoc.framework.gsocui.SkillMap.Menus.CustomizeColorMenu;
	import com.bpi.gsoc.framework.gsocui.SkillMap.Menus.CustomizeShapeMenu;
	import com.bpi.gsoc.framework.gsocui.SkillMap.Popups.AddSkills_PopUp;
	import com.bpi.gsoc.framework.sfx.GSOC_SFX;
	import com.bpi.gsoc.framework.states.app.GSOC_AppSkin;
	import com.bpi.gsoc.girlstablet.global_data.upload.UploadManager;
	import com.bpi.gsoc.girlstablet.states.gsoc_04knowyourrole.popups.HelpPopUp;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_IDs;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_KnowYourRole;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_Main;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_RecordVideo;
	import com.greensock.TimelineMax;
	import com.greensock.easing.Back;
	import com.greensock.easing.Bounce;
	import com.greensock.easing.Power3;
	import com.greensock.layout.AlignMode;
	
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.text.TextField;

	public class ChoiceWeb_App extends GSOC_AppSkin
	{
		private static var m_Instance:ChoiceWeb_App;
		
		private var m_Avatar:DisplayObject;
		
		protected var m_Header:MovieClip;
		
		protected var m_Help_Popup:HelpPopUp;
		protected var m_Help_Button:MovieClip;
		
		protected var m_CustomizeColor_Button:MovieClip;
		protected var m_CustomizeShape_Button:MovieClip;
		protected var m_CustomizeColor_Menu:CustomizeColorMenu;
		protected var m_CustomizeShape_Menu:CustomizeShapeMenu;
		
		protected var m_SkillMap:SkillMap;
		protected var m_AvatarHolder:MovieClip;
		
		protected var m_NextGirl_Button:MovieClip;
		protected var m_NextGirlPosY:Number;
		
		protected var m_EditSkill_Popup:AddSkills_PopUp;
		
		protected var m_GirlName_Header:TextField;
		protected var m_GirlName_Footer:TextField;
		protected var m_CareerTitle:MovieClip;
		
		protected var m_ColorOne:uint;
		protected var m_ColorTwo:uint;
		
		private var m_CustomizeButtonY:Number;
		private var m_Timeline:TimelineMax;
		
		private var m_DragLayer:Sprite;
		
		public function ChoiceWeb_App()
		{
			if(Instance == null)
			{
				m_Instance = this;
			}
			else
			{
				throw new Error("You can only have one instance of choiceWeb_App");
			}
		}
		
		public override function Initialize():void
		{
			super.Initialize();
			
			m_DragLayer = new Sprite();
			
			m_Help_Popup.Initialize();
			m_CustomizeColor_Menu.Initialize();
			m_CustomizeShape_Menu.Initialize();
			m_SkillMap.Initialize(m_DragLayer);
			
			this.addChild(m_SkillMap);
			this.addChild(m_AvatarHolder);
			this.addChild(m_CareerTitle);
			this.addChild(m_DragLayer);
			this.addChild(m_CustomizeColor_Menu);
			this.addChild(m_CustomizeShape_Menu);
			this.addChild(m_Header);
			this.addChild(m_CustomizeColor_Button);
			this.addChild(m_CustomizeShape_Button);
			this.addChild(m_GSOCHeader);
			this.addChild(m_GirlName_Header);
			this.addChild(m_Help_Popup);

			SetupScreen();
		}
		
		public function InitializeEventListeners():void
		{
			SkillMap.Dispatcher.addEventListener(Event_MapStatus.MAP_CHANGED, eh_MapChanged);
			SkillMap.Dispatcher.addEventListener(Event_AddYourOwnSelected.ADDYOUROWN_SELECTED, eh_AddYourOwnSelected);
			
			m_Help_Button.addEventListener(InputController.CLICK, eh_HelpButtonClicked);
			m_CustomizeColor_Button.addEventListener(InputController.CLICK, eh_CustomizeColorButtonClicked);
			m_CustomizeShape_Button.addEventListener(InputController.CLICK, eh_CustomizeShapeButtonClicked);

			m_CustomizeColor_Menu.InitializeEventListeners();
			m_CustomizeShape_Menu.InitializeEventListeners();
			m_SkillMap.InitializeEventListeners();
		}
		
		public override function DeInitialize():void
		{
			this.removeChild(m_CustomizeColor_Menu);
			this.removeChild(m_CustomizeShape_Menu);
			this.removeChild(m_Header);
			this.removeChild(m_CustomizeColor_Button);
			this.removeChild(m_CustomizeShape_Button);
			this.removeChild(m_SkillMap);
			this.removeChild(m_GirlName_Header);
			this.removeChild(m_CareerTitle);
			this.removeChild(m_Help_Popup);
			
			m_Help_Popup.DeInitialize();
			m_CustomizeColor_Menu.DeInitialize();
			m_CustomizeShape_Menu.DeInitialize();
			m_SkillMap.DeInitialize();
			
			if(m_EditSkill_Popup)
			{
				m_EditSkill_Popup.DeInitialize();
				this.removeChild(m_EditSkill_Popup);
			}
			
			SkillMap.Dispatcher.removeEventListener(Event_MapStatus.MAP_CHANGED, eh_MapChanged);
			SkillMap.Dispatcher.removeEventListener(Event_AddYourOwnSelected.ADDYOUROWN_SELECTED, eh_AddYourOwnSelected);
			
			m_NextGirl_Button.removeEventListener(InputController.CLICK, eh_NextGirlButtonClicked);
			m_NextGirl_Button.removeEventListener(InputController.CLICK, this.eh_SubmitButtonClicked);
			m_Help_Button.removeEventListener(InputController.CLICK, eh_HelpButtonClicked);
			m_CustomizeColor_Button.removeEventListener(InputController.CLICK, eh_CustomizeColorButtonClicked);
			m_CustomizeShape_Button.removeEventListener(InputController.CLICK, eh_CustomizeShapeButtonClicked);

			m_Instance = null;
		}
		
		protected function SetupScreen():void
		{
			m_Header.mouseEnabled = false;

			m_NextGirlPosY = m_NextGirl_Button.y;
			m_NextGirl_Button.x = ButtonPlacement.NEXT_X;
			m_CustomizeButtonY = m_CustomizeColor_Button.y;

			GSOC_ButtonToggle.ToggleButton(false, m_NextGirl_Button, this.activateNextGirlButton, this.deactivateNextGirlButton); 
			
			Utils_Text.EmbedText(m_GirlName_Header, HeaderFont, 80, 0xffffff);
			m_GirlName_Header.text = SessionInformation_GirlsTablet.TeamInfo.GetActiveGirlInformation().Name;
			Utils_Text.FitTextToTextfield(m_GirlName_Header);
			
			Utils_Text.EmbedText(m_GirlName_Footer, HeaderFont, 80, 0xffffff);
			m_GirlName_Footer.text = SessionInformation_GirlsTablet.TeamInfo.GetActiveGirlInformation().Name;
			Utils_Text.FitTextToTextfield(m_GirlName_Footer);
			
			Utils_Text.EmbedText(m_CareerTitle.careerTitle, BodyFont, 60, 0xffffff, AlignMode.CENTER);
			m_CareerTitle.careerTitle.text = (SessionInformation_GirlsTablet.TeamInfo.GetActiveGirlInformation().GirlUserData.GetData(GSOC_UserData_Girl_DataKeys.KEY_CAREER_TITLE));
			Utils_Text.FitTextToTextfield(m_CareerTitle.careerTitle);
			
			m_Avatar = GSOC_Avatar_Builder.BuildAvatarFromUserData(false, SessionInformation_GirlsTablet.TeamInfo.GetActiveGirlInformation().GirlUserData, true);
			m_AvatarHolder.addChild(m_Avatar);
			m_AvatarHolder.mouseEnabled = false;
			m_AvatarHolder.mouseChildren = false;
			
			if(m_EditSkill_Popup)
			{
				m_EditSkill_Popup.Initialize()
				this.addChild(m_EditSkill_Popup);
			}

			m_Timeline = new TimelineMax();
			m_Timeline.to([m_CustomizeShape_Button, m_CustomizeColor_Button], 3, {
				rotationx: 0, rotationY: 0, scaleX: 1, scaleY: 1, y: m_CustomizeButtonY, ease: Power3.easeOut});
			m_Timeline.to([m_CustomizeShape_Button, m_CustomizeColor_Button], .5, {
				rotationX: 10, rotationY: 10, scaleX: 1.1, scaleY: 1.1, y: m_CustomizeButtonY - 20, ease: Back.easeIn});
			m_Timeline.to([m_CustomizeShape_Button, m_CustomizeColor_Button], .5, {
				rotationX: 0, rotationY: 0, scaleX: 1, scaleY: 1, y: m_CustomizeButtonY, ease: Bounce.easeOut, onComplete: repeatAnimation});
		}
		
		private function repeatAnimation():void
		{
			m_Timeline.play(0);
		}

		protected function eh_MapChanged(e:Event_MapStatus):void
		{
			if(e.m_IsComplete && SessionInformation_GirlsTablet.TeamInfo.HasNextTeamMember())
			{
				GSOC_ButtonToggle.ToggleButton(true, m_NextGirl_Button, this.activateNextGirlButton, this.deactivateNextGirlButton);
			}
			else if(e.m_IsComplete && !SessionInformation_GirlsTablet.TeamInfo.HasNextTeamMember())
			{
				m_NextGirl_Button.gotoAndStop(2);
				GSOC_ButtonToggle.ToggleButton(true, m_NextGirl_Button, this.activateSubmitButton, this.deactivateSubmitButton);
			}
			else
			{
				GSOC_ButtonToggle.ToggleButton(false, m_NextGirl_Button, this.activateNextGirlButton, this.deactivateNextGirlButton);
			}
		}
		
		private function activateNextGirlButton(e:Event = null):void
		{
			m_NextGirl_Button.addEventListener(InputController.CLICK, this.eh_NextGirlButtonClicked);
		}
		
		private function deactivateNextGirlButton(e:Event = null):void
		{
			m_NextGirl_Button.removeEventListener(InputController.CLICK, this.eh_NextGirlButtonClicked);
		}
		
		private function activateSubmitButton(e:Event = null):void
		{
			m_NextGirl_Button.addEventListener(InputController.CLICK, this.eh_SubmitButtonClicked);
		}
		
		private function deactivateSubmitButton(e:Event = null):void
		{
			m_NextGirl_Button.removeEventListener(InputController.CLICK, this.eh_SubmitButtonClicked);
		}

		private function eh_NextGirlButtonClicked(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_CLICKS[SessionInformation_GirlsTablet.Age]);
			saveData();
			
			if(SessionInformation_GirlsTablet.TeamInfo.HasNextTeamMember())
			{
				SessionInformation_GirlsTablet.TeamInfo.IncrementActiveIndex();
				StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_KNOWYOURROLE, StateController_StateIDs_KnowYourRole.STATE_CHOICEWEB);
			}
		}
		
		private function saveData():void
		{
			m_SkillMap.RemoveUnusedShapes();
			m_SkillMap.addChild(m_DragLayer);

			var tempSprite:Sprite = new Sprite();
			var tempSkillMap:Bitmap = Utils_Image.ConvertDisplayObjectToBitmap(m_SkillMap);
			var tempCareerTitle:Bitmap = Utils_Image.GetNonTransparentAreaOfDisplayObject(m_CareerTitle);
			var tempAvatarHolder:Bitmap = Utils_Image.ConvertDisplayObjectToBitmap(m_AvatarHolder);

			tempSkillMap.x = 0;
			tempSkillMap.y = 0;

			tempAvatarHolder.x =  tempSkillMap.width * .5 - m_Avatar.width * .5 - 10;
			tempAvatarHolder.y = 0;
			tempCareerTitle.x = tempAvatarHolder.x  -tempCareerTitle.width * .23 + 200;
			tempCareerTitle.y = m_CareerTitle.y - m_CareerTitle.width * .5 + 50;
			
			tempSprite.addChild(tempSkillMap);
			tempSprite.addChild(tempAvatarHolder);
			tempSprite.addChild(tempCareerTitle);
			
			tempSprite.scaleX = tempSprite.scaleY = 0.5;
			
			var scaledSprite:Sprite = new Sprite();
			var scaledBitmap:Bitmap = new Bitmap(Utils_Image.CopyBitmapDataWithScaling(tempSprite));
			scaledSprite.addChild(scaledBitmap);
			
			var finalBitmap:Bitmap = Utils_Image.GetNonTransparentAreaOfDisplayObject(scaledSprite);
			var finalSprite:Sprite = new Sprite();
			finalSprite.addChild(finalBitmap);

			var girlInformation:GirlInformation = SessionInformation_GirlsTablet.TeamInfo.GetActiveGirlInformation();
			girlInformation.GirlUserData.SetImage(GSOC_UserData_Girl_ImageKeys.KEY_KNOWYOURROLE_WEB, finalSprite);
			UploadManager.UploadDisplayObject(finalSprite, Keys_FileNames.KEY_KNOWYOURROLE_WEB, girlInformation.UID);
			
			this.addChild(m_DragLayer);
		}
		
		private function eh_SubmitButtonClicked(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_CLICKS[SessionInformation_GirlsTablet.Age]);
			saveData();
			
			if(SessionInformation_GirlsTablet.IsRunningLocally)
			{
				StateControllerManager.LeaveActiveState(StateController_IDs.STATECONTROLLER_KNOWYOURROLE, StateController_StateIDs_KnowYourRole.STATE_CHOICEWEB);
				StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_RECORDVIDEO, StateController_StateIDs_RecordVideo.STATE_ASSIGNROLES);
			}
			else
			{
				submitActivity();
			}
		}
		
		private function submitActivity():void
		{
			StateControllerManager.LeaveAllActiveStates();
			StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_MAIN, StateController_StateIDs_Main.STATE_SESSIONATTRACT);
			Document.TellServerActivityComplete();
		}
		
		private function eh_HelpButtonClicked(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_ASKAFRIENDPOPUPS[SessionInformation_GirlsTablet.Age]);
			m_Help_Popup.DisplayPopUp();
		}
		
		private function eh_CustomizeColorButtonClicked(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_COLORDESIGNDROPDOWNS[SessionInformation_GirlsTablet.Age]);
			m_CustomizeColor_Menu.ToggleDisplay();
		}
		
		private function eh_CustomizeShapeButtonClicked(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_COLORDESIGNDROPDOWNS[SessionInformation_GirlsTablet.Age]);
			m_CustomizeShape_Menu.ToggleDisplay();
		}
		
		private function eh_AddYourOwnSelected(e:Event_AddYourOwnSelected):void
		{
			m_EditSkill_Popup.MapPos = e.m_MapPos;
			m_EditSkill_Popup.DisplayPopUp();
		}
		
		public function get BodyFont():String
		{
			return m_BodyFont;
		}
		
		public function get HeaderFont():String
		{
			return m_HeaderFont;
		}
		
		public function get ColorOne():uint
		{
			return m_ColorOne;
		}
		
		public function get ColorTwo():uint
		{
			return m_ColorTwo;
		}
		
		public static function get Instance():ChoiceWeb_App
		{
			return m_Instance;
		}
	}
}
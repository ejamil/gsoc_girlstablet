package com.bpi.gsoc.girlstablet.states.gsoc_04knowyourrole.choice_web
{
	import com.bpi.citrusas.utils.Utils_Image;
	import com.bpi.components.knowyourrole.SkillShape_Skin;
	import com.bpi.gsoc.components.knowyourrole.ChoiceWebJCSkin;
	import com.bpi.gsoc.framework.career.Career;
	import com.bpi.gsoc.framework.career.CareerReference;
	import com.bpi.gsoc.framework.constants.AgeFontLinkages;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.data.userdata.GSOC_UserData_Girl_DataKeys;
	import com.bpi.gsoc.framework.gsocui.GSOC_Header;
	import com.bpi.gsoc.framework.gsocui.SkillMap.SkillMap_JC;
	import com.bpi.gsoc.framework.gsocui.SkillMap.Menus.CustomizeColorMenu;
	import com.bpi.gsoc.framework.gsocui.SkillMap.Menus.CustomizeShapeMenu;
	import com.bpi.gsoc.framework.gsocui.SkillMap.Popups.AddSkills_PopUp;
	import com.bpi.gsoc.framework.skill.Skill;
	import com.bpi.gsoc.girlstablet.global_data.FrameControlUtils;
	import com.bpi.gsoc.girlstablet.states.gsoc_04knowyourrole.popups.HelpPopUp;

	public class ChoiceWeb_App_JC extends ChoiceWeb_App
	{
		public function ChoiceWeb_App_JC()
		{
		}
		
		public override function Initialize():void
		{
			m_Skin = new ChoiceWebJCSkin();
			m_Header = m_Skin.Header;
			
			m_BodyFont = AgeFontLinkages.JC_BODY_FONT;
			m_HeaderFont = AgeFontLinkages.JC_HEADER_FONT;
			
			m_Help_Popup = new HelpPopUp(m_Skin.helpButtonPopUp);
			m_Help_Button = m_Skin.helpButton;
			
			m_GSOCHeader = new GSOC_Header(m_Skin.Header.paginationArea);
			GSOCheader.addArrows(m_Skin.Header.paginationArea.x, 0);
			
			m_EditSkill_Popup = new AddSkills_PopUp(m_Skin.addSkillPopUp, SkillShape_Skin);
			
			m_CustomizeColor_Button = m_Skin.customizeColor;
			m_CustomizeShape_Button = m_Skin.customizeDesign;
			m_CustomizeColor_Menu = new CustomizeColorMenu(m_Skin.customizeColorMenu);
			m_CustomizeShape_Menu = new CustomizeShapeMenu(m_Skin.customizeDesignMenu);
			
			m_GirlName_Footer = m_Skin.careerTitleFrame.girlName;
			m_GirlName_Header = m_Skin.girlName;
			m_CareerTitle = m_Skin.careerTitleFrame;
			
			m_ColorOne = 0x8d2a8e;
			m_ColorTwo = 0xDF2131;
			
			m_AvatarHolder = m_Skin.AvatarHolder;
			m_NextGirl_Button = m_Skin.next_Girl_Button;
			
			
			m_SkillMap = new SkillMap_JC(m_Skin.SkillMap);

			FrameControlUtils.StopAtFrameN(m_Skin);
			this.addChild(m_Skin);
			
			super.Initialize();
		}
		
		protected override function SetupScreen():void
		{
			super.SetupScreen();
			
			var career:Career = CareerReference.GetJCCareer((SessionInformation_GirlsTablet.TeamInfo.GetActiveGirlInformation().GirlUserData.GetData(GSOC_UserData_Girl_DataKeys.KEY_CAREER_TITLE)) as String);
			
			var skills:Vector.<Skill> = career.skillsList;
			for(var i:int = 0 ; i < m_SkillMap.SkillShapes.length; i++)
			{
				m_SkillMap.SkillShapes[i].Icon = Utils_Image.ConvertDisplayObjectToMovieClip(skills[i].graphic);
				m_SkillMap.SkillShapes[i].Text = skills[i].skillName;
			}
			
			m_Help_Popup.Text = career.askAFriendText;
		}
	}
}
package com.bpi.gsoc.girlstablet.states.gsoc_11poster.classes
{
	import com.bpi.gsoc.framework.avatar.GSOC_Avatar_Builder;
	import com.bpi.gsoc.framework.avatar.GSOC_Avatar_Skin;
	import com.bpi.gsoc.framework.career.group.CareerGroupReference;
	import com.bpi.gsoc.framework.data.userdata.GSOC_UserData;
	import com.bpi.gsoc.framework.data.userdata.GSOC_UserData_Girl_DataKeys;
	import com.bpi.gsoc.poster.components.CareerDisplay_Mask;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.geom.Point;

	public class CareerDisplay extends Sprite
	{
		private static var ms_Avatar_Offset:Point = new Point(116, 128);
		private static var ms_Avatar_Scale:Number = 0.5;
		
		private static var ms_Scale:Number = .5;
		
		private var m_Image_Background:MovieClip;
		private var m_Image_Avatar:GSOC_Avatar_Skin;
		private var m_Image_Mask:MovieClip;
		
		private var m_CareerTitle:String;
		
		public function CareerDisplay()
		{
			m_Image_Mask = new CareerDisplay_Mask();
		}
		
		private function generateCareerAvatar(girlUserData:GSOC_UserData):void
		{
			m_CareerTitle = girlUserData.GetData(GSOC_UserData_Girl_DataKeys.KEY_CAREER_TITLE) as String;
			
			createBackground(girlUserData);
			createAvatar(girlUserData);
			
			this.scaleX = ms_Scale;
			this.scaleY = ms_Scale;
		}
		
		private function createBackground(girlUserData:GSOC_UserData):void
		{
			if(m_Image_Background && this.contains(m_Image_Background))
			{
				this.removeChild(m_Image_Background);
			}
			
			m_Image_Background = CareerGroupReference.GetBackgroundForCareer(m_CareerTitle);
			this.addChild(m_Image_Background);
		}
		
		private function createAvatar(girlUserData:GSOC_UserData):void
		{
			if(m_Image_Avatar && this.contains(m_Image_Avatar))
			{
				this.removeChild(m_Image_Avatar);
			}
			
			m_Image_Avatar = GSOC_Avatar_Builder.BuildAvatarFromUserData(false, girlUserData);
			
			m_Image_Avatar.scaleX = ms_Avatar_Scale;
			m_Image_Avatar.scaleY = ms_Avatar_Scale;
			m_Image_Avatar.x += ms_Avatar_Offset.x;
			m_Image_Avatar.y += ms_Avatar_Offset.y;
			m_Image_Avatar.mask = m_Image_Mask;
			
			this.addChild(m_Image_Avatar);
			this.addChild(m_Image_Mask);
		}
		
		public function GenerateAvatar(girlUserData:GSOC_UserData):void
		{
			generateCareerAvatar(girlUserData);
		}
		
		public function CheckCareerTitleUpdate(girlUserData:GSOC_UserData):void
		{
			if(m_CareerTitle != girlUserData.GetData(GSOC_UserData_Girl_DataKeys.KEY_CAREER_TITLE) as String)
			{
				generateCareerAvatar(girlUserData);
			}
		}
		
		public function get Size():Point
		{
			return new Point(m_Image_Background.width * this.scaleX, m_Image_Background.height * this.scaleY);
		}
	}
}
package com.bpi.gsoc.girlstablet.states.gsoc_11poster.classes.popups.typing_popup
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.citrusas.sound.SoundHandler;
	import com.bpi.citrusas.utils.Utils_Text;
	import com.bpi.gsoc.framework.constants.GSOC_Info;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.sfx.GSOC_SFX;
	import com.bpi.gsoc.girlstablet.states.gsoc_11poster.classes.popups.PosterPopup;
	import com.greensock.TweenLite;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.text.TextField;

	public class Typing_Popup extends PosterPopup
	{
		public static const MAX_CHARACTER_LIMIT:int = 160;
		public static const DEFAULT_TEXT:String = "Start typing your message!";
		
		private var m_Textfield:TextField;
		
		public function Typing_Popup(skin:MovieClip)
		{
			m_Textfield = skin.textbox;
			Utils_Text.EmbedText(m_Textfield, GSOC_Info.BodyFont, 60, GSOC_Info.ColorTwo);
			m_Textfield.multiline = true;
			m_Textfield.wordWrap = true;
			m_Textfield.maxChars = 25;
			
			super(skin);
		}
		
		public override function DisplayPopUp():void
		{
			m_Skin.visible = true;
			m_Background.visible = true;
			
			TweenLite.fromTo(m_Skin, .5, {scaleX: 1.3, scaleY: 1.3, alpha: 0}, {scaleX: 1, scaleY: 1, alpha: 1, onComplete: setFocus});
			TweenLite.fromTo(m_Background, .5, {alpha: 0}, {alpha: .6});
			
			m_Textfield.text = DEFAULT_TEXT;
			
			CitrusFramework.frameworkStage.addEventListener(KeyboardEvent.KEY_DOWN, eh_KeyPressed);
		}
		
		public override function HidePopUp():void
		{
			CitrusFramework.frameworkStage.removeEventListener(KeyboardEvent.KEY_DOWN, eh_KeyPressed);
			CitrusFramework.frameworkStage.focus = null;
			
			super.HidePopUp();
		}
		
		private function setFocus():void
		{
			CitrusFramework.frameworkStage.focus = m_Textfield;
		}
		
		protected override function eh_ClearButton_Pressed(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_CLICKS[SessionInformation_GirlsTablet.Age]);
			m_Textfield.text = "";
			
			super.eh_ClearButton_Pressed(e);
		}
		
		protected override function eh_SaveButton_Pressed(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_SAVEBUTTONS[SessionInformation_GirlsTablet.Age]);
			checkText();
			super.eh_SaveButton_Pressed(e);
		}
		
		private function eh_KeyPressed(e:Event):void
		{
			checkText();	
		}
		
		private function checkText():void
		{
			if(m_Textfield.text == DEFAULT_TEXT)
			{
				m_Textfield.text = "";
			}
		}
		
		public function get Text():String
		{
			return m_Textfield.text;
		}
		
	}
}
package com.bpi.gsoc.girlstablet.states.gsoc_11poster.classes
{
	import com.bpi.citrusas.ui.draggableobject.interactivedisplayobject.InteractiveDisplayObject;
	
	import flash.display.DisplayObject;
	import flash.events.TouchEvent;
	import flash.geom.Point;

	public class PosterObject extends InteractiveDisplayObject
	{
		private static const ms_MaxWidth:Number = 2000;	
		
		private var m_IsTrashable:Boolean;
		
		public function PosterObject(obj:DisplayObject, isTrashable:Boolean = false, canRotate:Boolean = true, canZoom:Boolean = true)
		{
			super(obj, canRotate, canZoom);
			
			m_IsTrashable = isTrashable;
		}
		
		public function get IsTrashable():Boolean
		{
			return m_IsTrashable;
		}
		
		protected override function zoom(e:TouchEvent):void
		{
			var distance:Number = Point.distance(m_TouchPoints[0].Position, m_TouchPoints[1].Position);
			
			if(m_PrevDistance)
			{
				m_Container.width += distance - m_PrevDistance;
				m_Container.scaleY = m_Container.scaleX;
			}
			
			if(m_Container.width >= ms_MaxWidth)
			{
				m_Container.width = ms_MaxWidth;
				m_Container.scaleY = m_Container.scaleX;
			}
			
			m_PrevDistance = distance;
		}
	}
}
package com.bpi.gsoc.girlstablet.states.gsoc_11poster.classes.popups
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.citrusas.input.inputcontroller.InputController;
	import com.bpi.citrusas.sound.SoundHandler;
	import com.bpi.citrusas.ui.primitives.Primitive_Rectangle;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.sfx.GSOC_SFX;
	import com.greensock.TweenLite;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Point;
	
	public class PosterPopup extends Sprite
	{
		public static const SAVE_BUTTON_PRESSED:String = "SAVE_BUTTON_PRESSED";
		public static const CLEAR_BUTTON_PRESSED:String = "CLEAR_BUTTON_PRESSED";
		public static const CLOSE_BUTTON_PRESSED:String = "CLOSE_BUTTON_PRESSED";
		
		protected var m_ClearButton:MovieClip;
		protected var m_SaveButton:MovieClip;
		protected var m_CloseButton:MovieClip;
		
		protected var m_Skin:MovieClip;
		
		protected var m_Background:Primitive_Rectangle;
		
		public function PosterPopup(skin:MovieClip)
		{
			m_Background = new Primitive_Rectangle(new Point(CitrusFramework.frameworkStageSize.x, CitrusFramework.frameworkStageSize.y), 0x000000);
			m_Background.alpha = .6;
			
			m_Skin = skin;
			m_CloseButton = skin.close;
			m_ClearButton = skin.clear;
			m_SaveButton = skin.save;
			
			m_Skin.visible = false;
			m_Background.visible = false;
		}
		
		public function Initialize():void
		{
			if(m_ClearButton) m_ClearButton.addEventListener(InputController.DOWN, eh_ClearButton_Pressed);
			if(m_SaveButton) m_SaveButton.addEventListener(InputController.DOWN, eh_SaveButton_Pressed);
			if(m_CloseButton) m_CloseButton.addEventListener(InputController.CLICK, eh_ClosePopup);
			
			CitrusFramework.contentLayer.addChild(m_Background);
			CitrusFramework.contentLayer.addChild(m_Skin);
		}
		
		public function DeInitialize():void
		{
			if(m_ClearButton) m_ClearButton.removeEventListener(InputController.DOWN, eh_ClearButton_Pressed);
			if(m_SaveButton) m_SaveButton.removeEventListener(InputController.DOWN, eh_SaveButton_Pressed);
			if(m_CloseButton) m_CloseButton.removeEventListener(InputController.CLICK, eh_ClosePopup);
			
			if(CitrusFramework.contentLayer.contains(m_Background)) CitrusFramework.contentLayer.removeChild(m_Background);
		}
		
		private function eh_ClosePopup(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_CLOSEBUTTONS[SessionInformation_GirlsTablet.Age]);
			HidePopUp();
			this.dispatchEvent(new Event(CLOSE_BUTTON_PRESSED));
		}
		
		public function DisplayPopUp():void
		{
			m_Skin.visible = true;
			m_Background.visible = true;
			
			TweenLite.fromTo(m_Skin, .5, {scaleX: 1.3, scaleY: 1.3, alpha: 0}, {scaleX: 1, scaleY: 1, alpha: 1});
			TweenLite.fromTo(m_Background, .5, {alpha: 0}, {alpha: .6});
		}
		
		public function HidePopUp():void
		{
			TweenLite.to(m_Skin, .5, {scaleX: .4, scaleY: .4, alpha: 0, onComplete: removePopup});
			TweenLite.to(m_Background, .5, {alpha: 0, onComplete: removePopup});
		}
		
		private function removePopup():void
		{
			m_Skin.visible = false;
			m_Background.visible = false;
		}
		
		protected function eh_ClearButton_Pressed(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_CLICKS[SessionInformation_GirlsTablet.Age]);
			this.dispatchEvent(new Event(CLEAR_BUTTON_PRESSED));
		}
		
		protected function eh_SaveButton_Pressed(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_SAVEBUTTONS[SessionInformation_GirlsTablet.Age]);
			HidePopUp();
			
			this.dispatchEvent(new Event(SAVE_BUTTON_PRESSED));
		}
	}
}
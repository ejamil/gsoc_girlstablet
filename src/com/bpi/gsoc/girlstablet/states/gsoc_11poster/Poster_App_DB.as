package com.bpi.gsoc.girlstablet.states.gsoc_11poster
{
	import com.bpi.components.poster.Poster_DB_Skin;
	import com.bpi.gsoc.framework.gsocui.ColorPicker;
	import com.bpi.gsoc.framework.gsocui.GSOC_Header;

	public class Poster_App_DB extends Poster_App
	{
		private var m_TypedSkin:Poster_DB_Skin;
		
		public function Poster_App_DB()
		{
			m_Skin = new Poster_DB_Skin();
			m_TypedSkin = (m_Skin as Poster_DB_Skin);
			m_GSOCHeader = new GSOC_Header(m_Skin.header.paginationArea);
			GSOCheader.addArrows(m_Skin.header.paginationArea.x, 0);
		}
		
		public override function Initialize():void
		{
			m_Poster = m_TypedSkin.poster;
			m_SubmitButton = m_TypedSkin.submit;
			m_Border = m_TypedSkin.poster.borders;
			m_Pattern = m_TypedSkin.poster.background;
			m_Background = m_TypedSkin.poster.maxArea;
			m_ColorSelection = new ColorPicker(m_TypedSkin.gradientBar_DB, m_TypedSkin, "COLOR_CHANGED");
			
			m_Border_Buttons.push(m_TypedSkin.borderButton1);
			m_Border_Buttons.push(m_TypedSkin.borderButton2);
			m_Border_Buttons.push(m_TypedSkin.borderButton3);
			m_Border_Buttons.push(m_TypedSkin.borderButton4);
			m_Border_Buttons.push(m_TypedSkin.borderButton5);
			
			m_CanResize = false;
			m_CanRotate = false;
			
			super.Initialize();
		}
	}
}
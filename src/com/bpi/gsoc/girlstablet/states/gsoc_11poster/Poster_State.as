package com.bpi.gsoc.girlstablet.states.gsoc_11poster
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.gsoc.framework.constants.AgeConstants;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.gsocui.keyboards.GSOC_Keyboards;
	import com.bpi.gsoc.framework.states.app.GSOC_AppState;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_Poster;

	public class Poster_State extends GSOC_AppState
	{
		private var m_AppState:Poster_App;
		public function Poster_State()
		{
			super(StateController_StateIDs_Poster.STATE_POSTER);
		}
		
		protected override function Init():void
		{
			switch(SessionInformation_GirlsTablet.Age)
			{
				case AgeConstants.KEY_DAISYBROWNIE:
					m_AppState = new Poster_App_DB();
					break;
				case AgeConstants.KEY_JUNIORCADETTE:
					m_AppState = new Poster_App_JC();
					break;
				case AgeConstants.KEY_SENIORAMBASSADOR:
					m_AppState = new Poster_App_SA();
					break;
			}
			
			CitrusFramework.contentLayer.addChild(m_AppState);
			GSOC_Keyboards.SetKeyboardType(GSOC_Keyboards.KEYBOARDTYPE_REGULAR);
			
			m_AppState.Initialize();
			m_AppState.Init()
			m_AppState.SetupScreen();
		}
		
		protected override function DeInit():void
		{	
			m_AppState.DeInitialize();
			CitrusFramework.contentLayer.removeChild(m_AppState);
			m_AppState = null;
			
			super.EndDeInit();
		}
	}
}
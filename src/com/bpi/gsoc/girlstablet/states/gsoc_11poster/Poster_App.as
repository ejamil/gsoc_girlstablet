package com.bpi.gsoc.girlstablet.states.gsoc_11poster
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.citrusas.console.Console;
	import com.bpi.citrusas.events.CourierEvent;
	import com.bpi.citrusas.input.inputcontroller.InputController;
	import com.bpi.citrusas.sound.SoundHandler;
	import com.bpi.citrusas.states.StateControllerManager;
	import com.bpi.citrusas.ui.draggableobject.interactivedisplayobject.InteractiveDisplayObject;
	import com.bpi.citrusas.utils.Utils_BPI;
	import com.bpi.citrusas.utils.Utils_Image;
	import com.bpi.citrusas.utils.Utils_Text;
	import com.bpi.gsoc.framework.constants.ButtonPlacement;
	import com.bpi.gsoc.framework.constants.GSOC_Info;
	import com.bpi.gsoc.framework.constants.Keys_FileNames;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.data.userdata.GSOC_UserData;
	import com.bpi.gsoc.framework.data.userdata.GSOC_UserData_Girl_ImageKeys;
	import com.bpi.gsoc.framework.data.userdata.GSOC_UserData_Team_ImageKeys;
	import com.bpi.gsoc.framework.gsocui.ColorPicker;
	import com.bpi.gsoc.framework.gsocui.GSOC_ButtonToggle;
	import com.bpi.gsoc.framework.sfx.GSOC_SFX;
	import com.bpi.gsoc.framework.states.app.GSOC_AppSkin;
	import com.bpi.gsoc.girlstablet.global_data.upload.UploadManager;
	import com.bpi.gsoc.girlstablet.states.gsoc_11poster.classes.PosterObject;
	import com.bpi.gsoc.girlstablet.states.gsoc_11poster.classes.popups.PosterPopup;
	import com.bpi.gsoc.girlstablet.states.gsoc_11poster.classes.popups.Poster_PopupIDs;
	import com.bpi.gsoc.girlstablet.states.gsoc_11poster.classes.popups.icons_popup.Icons_Popup;
	import com.bpi.gsoc.girlstablet.states.gsoc_11poster.classes.popups.icons_popup.Poster_Icon;
	import com.bpi.gsoc.girlstablet.states.gsoc_11poster.classes.popups.typing_popup.Typing_Popup;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_IDs;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_Main;
	import com.greensock.TweenLite;
	import com.greensock.TweenMax;
	import com.greensock.easing.Back;
	import com.greensock.plugins.ColorTransformPlugin;
	import com.greensock.plugins.GlowFilterPlugin;
	import com.greensock.plugins.TweenPlugin;
	
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TouchEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.text.TextField;

	public class Poster_App extends GSOC_AppSkin
	{
		private static const ms_MinDistanceToTrash:Number = 100;
		private static const ms_BufferBetweenImages:Number = 15;
		private static const ms_PercentOfPoster:Number = 0.9;
		
		protected var m_DisplayObjects:Vector.<PosterObject>;
		protected var m_Pattern_Buttons:Vector.<MovieClip>;
		protected var m_Border_Buttons:Vector.<MovieClip>;
		protected var m_Popup_Buttons:Vector.<MovieClip>;
		protected var m_Popups:Vector.<PosterPopup>;
		
		protected var m_Poster:MovieClip;
		protected var m_SubmitButton:MovieClip;
		protected var m_Border:MovieClip;
		protected var m_Pattern:MovieClip;
		protected var m_Background:MovieClip;
		protected var m_TrashCan:MovieClip;
		
		protected var m_CanResize:Boolean;
		protected var m_CanRotate:Boolean;
		
		protected var m_ColorSelection:ColorPicker;
		
		public function Poster_App()
		{
			m_DisplayObjects = new Vector.<PosterObject>();
			m_Pattern_Buttons = new Vector.<MovieClip>();
			m_Border_Buttons = new Vector.<MovieClip>();
			m_Popup_Buttons = new Vector.<MovieClip>();
			m_Popups = new Vector.<PosterPopup>();

			TweenPlugin.activate([ColorTransformPlugin]);
			TweenPlugin.activate([GlowFilterPlugin]);
		}
		
		public override function Initialize():void
		{
			super.Initialize();
			
			var i:int;
			for(i = 0; i < m_Popup_Buttons.length; i++)
			{
				m_Popups[i].Initialize();
				m_Popups[i].addEventListener(PosterPopup.SAVE_BUTTON_PRESSED, eh_PopupSaved);
				m_Popup_Buttons[i].addEventListener(InputController.DOWN, eh_PopupButtonClicked);
				m_Popup_Buttons[i].mouseChildren = false;
			}
			for(i = 0; i < m_Border_Buttons.length; i++)
			{
				m_Border_Buttons[i].addEventListener(InputController.DOWN, eh_BorderButtonClicked);
				m_Border_Buttons[i].gotoAndStop(i + 1);
				m_Border_Buttons[i].mouseChildren = false;
				selectBorderButton(m_Border_Buttons[0]);
			}
			for(i = 0; i < m_Pattern_Buttons.length; i++)
			{
				m_Pattern_Buttons[i].addEventListener(InputController.DOWN, eh_PatternButtonClicked);
				m_Pattern_Buttons[i].gotoAndStop(i + 1);
				m_Pattern_Buttons[i].mouseChildren = false;
				selectPatternButton(m_Pattern_Buttons[0]);
			}
			
			if(m_ColorSelection)
			{
				m_ColorSelection.addEventListener(m_ColorSelection.getColorChangeEventName(), eh_ColorChanged);
			}
			
			m_Border.mouseEnabled = false;
		}
		
		public function SetupScreen():void
		{
			m_SubmitButton.x = ButtonPlacement.SUBMIT_X;
			GSOC_ButtonToggle.ToggleButton(false, m_SubmitButton, this.activateNextGirlButton, this.deactivateNextGirlButton);
			
			m_Skin.stopAllMovieClips();
			m_Skin.header.gotoAndStop(SessionInformation_GirlsTablet.Track + 1);
			
			var displayObjects:Vector.<DisplayObject>;
			
			Console.Log("__________" + Utils_BPI.GetTime() + "____________");
		
			if(SessionInformation_GirlsTablet.Track == 0)
			{
				Console.Log("### Adding CAREER Objects ###");
				displayObjects = addCareerDisplayObjects();
			}
			else
			{
				Console.Log("### Adding TAKE_ACTION Objects ###");
				displayObjects = addTakeActionDisplayObjects();
			}
			
			var SizeRatio:Number = (m_Background.width/m_Background.height) * ms_PercentOfPoster;
			
			var row_Num:int = Math.round(Math.sqrt(displayObjects.length / SizeRatio));
			var column_Num:int = Math.ceil(displayObjects.length / row_Num);
			
			var areaWidth:Number = ((m_Background.width * ms_PercentOfPoster) / column_Num) - ms_BufferBetweenImages - (ms_BufferBetweenImages/column_Num);
			var areaHeight:Number = ((m_Background.height * ms_PercentOfPoster) / row_Num) - ms_BufferBetweenImages - (ms_BufferBetweenImages/row_Num);
			
			for(var index:int = 0; index < displayObjects.length; index++)
			{
				var displayObject:DisplayObject = displayObjects[index];
				if(displayObject)
				{
					Console.Log("Full obj #" + index + "- width: " + displayObject.width + " height: " + displayObject.height, "Poster");
					var obj:PosterObject = new PosterObject(Utils_Image.GetAccurateBitmapOfObject(displayObject), false, m_CanRotate, m_CanResize);
					Console.Log("Obj w/o transparency #" + index + "- width: " + obj.width + " height: " + obj.height, "Poster");
					
					if(obj.Object.width / areaWidth > obj.Object.height / areaHeight)
					{
						obj.Object.width = areaWidth;
						obj.Object.scaleY = obj.Object.scaleX;
					}
					else
					{
						obj.Object.height = areaHeight;
						obj.Object.scaleX = obj.Object.scaleY;
					}
					var row:int = Math.floor(index/column_Num);
					var column:int = (index%column_Num);
					
					obj.y = (row * areaHeight + m_Background.y) + ((row + 1) * ms_BufferBetweenImages) + (areaHeight - obj.height)*.5;
					obj.x = (column * areaWidth + m_Background.x) + ((column + 1) * ms_BufferBetweenImages) + (areaWidth - obj.width)*.5;
					
					addDisplayObject(obj);
				}
				else
				{
					Console.Log("Obj #" + index + " does not exist", "Poster");
				}
			}
		}
		
		private function addTakeActionDisplayObjects():Vector.<DisplayObject>
		{
			var displayObjects:Vector.<DisplayObject> = new Vector.<DisplayObject>();
			displayObjects.push(SessionInformation_GirlsTablet.TeamInfo.Team_UserData.GetImage(GSOC_UserData_Team_ImageKeys.KEY_BRAINSTORM_BOARD));
			displayObjects.push(SessionInformation_GirlsTablet.TeamInfo.Team_UserData.GetImage(GSOC_UserData_Team_ImageKeys.KEY_BRAINSTORM_PROJECTGOALS_BOARD));
			displayObjects.push(SessionInformation_GirlsTablet.TeamInfo.Team_UserData.GetImage(GSOC_UserData_Team_ImageKeys.KEY_MINDMAP_WEB));
			displayObjects.push(SessionInformation_GirlsTablet.TeamInfo.Team_UserData.GetImage(GSOC_UserData_Team_ImageKeys.KEY_COMMUNITYRESOURCES_MAP));
			displayObjects.push(SessionInformation_GirlsTablet.TeamInfo.Team_UserData.GetImage(GSOC_UserData_Team_ImageKeys.KEY_PROJECTPLANNING_PUZZLE));
			displayObjects.push(SessionInformation_GirlsTablet.TeamInfo.Team_UserData.GetImage(GSOC_UserData_Team_ImageKeys.KEY_ASSEMBLEYOURTEAM_TEAM));
			
			return displayObjects;
		}
		
		private function addCareerDisplayObjects():Vector.<DisplayObject>
		{
			var displayObjects:Vector.<DisplayObject> = new Vector.<DisplayObject>();
			
			for(var i:int = 0; i <SessionInformation_GirlsTablet.TeamInfo.GetTeamMemberCount(); i++)
			{
				var userData:GSOC_UserData = SessionInformation_GirlsTablet.TeamInfo.GetGirlInformationFromIndex(i).GirlUserData;
				
				displayObjects.push(userData.GetImage(GSOC_UserData_Girl_ImageKeys.KEY_KNOWYOURROLE_WEB));
				displayObjects.push(userData.GetImage(GSOC_UserData_Girl_ImageKeys.KEY_CHOOSEYOURCAREER_BOX));
				
				if(displayObjects.length == 0)
				{
					TweenLite.delayedCall(.5, addCareerDisplayObjects);
				}
				
				Console.Log(displayObjects);
			}
			
			return displayObjects;
		}
		
		public override function DeInitialize():void
		{
			while(m_Popup_Buttons.length > 0)
			{
				var popupButton:MovieClip = m_Popup_Buttons.pop();
				popupButton.removeEventListener(PosterPopup.SAVE_BUTTON_PRESSED, eh_PopupSaved);
			}
			while(m_Border_Buttons.length > 0)
			{
				var borderButton:MovieClip = m_Border_Buttons.pop();
				borderButton.removeEventListener(InputController.DOWN, eh_BorderButtonClicked);
			}
			while(m_Pattern_Buttons.length > 0)
			{
				var patternButton:MovieClip = m_Pattern_Buttons.pop();
				patternButton.removeEventListener(InputController.DOWN, eh_PatternButtonClicked);
			}
			GSOC_ButtonToggle.ToggleButton(false, m_SubmitButton, this.activateNextGirlButton, this.deactivateNextGirlButton);
			
			if(m_ColorSelection)
			{
				m_ColorSelection.removeEventListener(m_ColorSelection.getColorChangeEventName(), eh_ColorChanged);
			}
		}
		
		private function addDisplayObject(obj:PosterObject):void
		{
			obj.Initialize();
			obj.addEventListener(InteractiveDisplayObject.TOUCH_END, eh_CheckObjectPosition);
			obj.addEventListener(InteractiveDisplayObject.TOUCH_START, eh_BringObjectToFront);
			
			m_Poster.addChild(obj);
			m_Border.parent.setChildIndex(m_Border, m_Border.parent.numChildren - 1); // keep the border on top!
			m_DisplayObjects.push(obj);
		}
		
		private function removeDisplayObject(obj:PosterObject):void
		{
			obj.DeInitialize();
			obj.removeEventListener(InteractiveDisplayObject.TOUCH_END, eh_CheckObjectPosition);
			obj.removeEventListener(InteractiveDisplayObject.TOUCH_START, eh_BringObjectToFront);
			
			m_Poster.removeChild(obj);
			m_DisplayObjects.removeAt(m_DisplayObjects.indexOf(obj));
		}
		
		private function activateNextGirlButton(e:Event = null):void
		{
			m_SubmitButton.addEventListener(InputController.DOWN, this.eh_SubmitButtonPressed);
		}
		
		private function deactivateNextGirlButton(e:Event = null):void
		{
			m_SubmitButton.removeEventListener(InputController.DOWN, this.eh_SubmitButtonPressed);
		}
		
		private function eh_SubmitButtonPressed(e:Event):void
		{
			if(e is MouseEvent || (e is TouchEvent && (e as TouchEvent).isPrimaryTouchPoint))
			{
				SoundHandler.PlaySound(GSOC_SFX.SOUND_CLICKS[SessionInformation_GirlsTablet.Age]);
				
				if(SessionInformation_GirlsTablet.IsRunningLocally)
				{
					StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_MAIN, StateController_StateIDs_Main.STATE_ATTRACT);
				}
				else
				{
					submitActivity();
				}
				
				GSOC_ButtonToggle.ToggleButton(false, m_SubmitButton, this.activateNextGirlButton, this.deactivateNextGirlButton);
			}
		}
		
		private function saveData():void
		{
			m_Poster.scaleX = 1.25;
			m_Poster.scaleY = 1.25;
			
			TweenLite.delayedCall(.5, savePoster);
		}
		
		private function savePoster():void
		{
			UploadManager.UploadDisplayObject(m_Poster, Keys_FileNames.KEY_MAKEAPOSTER_POSTER);
		}
		
		private function submitActivity():void
		{
			StateControllerManager.LeaveAllActiveStates();
			StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_MAIN, StateController_StateIDs_Main.STATE_SESSIONATTRACT);
			
			saveData();
			
			Document.TellServerActivityComplete();
		}
		
		private function eh_BringObjectToFront(e:Event):void
		{
			var obj:DisplayObject = e.target as DisplayObject;
			obj.parent.setChildIndex(obj, obj.parent.numChildren - 1);
			m_Border.parent.setChildIndex(m_Border, m_Border.parent.numChildren - 1); // keep the border on top!
		}
		
		private function eh_PopupSaved(e:Event):void
		{
			if(e.target is Icons_Popup)
			{
				displayIcon((e.target as Icons_Popup).Icon);
			}
			else
			{
				displayText((e.target as Typing_Popup).Text);
			}
		}
		
		private function displayIcon(icon:Poster_Icon):void
		{
			if(icon && icon.Icon)
			{
				var bitmap:MovieClip = Utils_Image.ConvertDisplayObjectToMovieClip(Utils_Image.ConvertParentToBitmap(icon.Icon), true);
				var iDisplayObj:PosterObject = new PosterObject(bitmap, true);
				
				iDisplayObj.x = m_Poster.width * .5;
				iDisplayObj.y = m_Poster.height * .5;
				
				TweenLite.fromTo(iDisplayObj.Object, .5, {scaleX: 15, scaleY: 15, alpha: .2}, {scaleY: 2, scaleX: 2, alpha: 1, ease: Back.easeOut});
				addDisplayObject(iDisplayObj);
			}
		}
		
		private function displayText(text:String):void
		{
			if(text)
			{
				var textField:TextField = new TextField();
				
				Utils_Text.EmbedText(textField, GSOC_Info.BodyFont, 250, GSOC_Info.ColorOne);
				textField.selectable = false;
				textField.multiline = true;
				textField.wordWrap = true;
				textField.width = 2000;
				textField.height = 1000;
				textField.text = text;
				Utils_Text.FitTextToTextfield(textField, 0, true);
				
				var iDisplayObj:PosterObject = new PosterObject(Utils_Image.ConvertDisplayObjectToMovieClip(Utils_Image.GetNonTransparentAreaOfDisplayObject(textField), true), true);
				
				iDisplayObj.Object.width = m_Poster.width * .5;
				iDisplayObj.Object.scaleY = iDisplayObj.Object.scaleX;
				iDisplayObj.y = m_Poster.height * .5;
				iDisplayObj.x = m_Poster.width * .5;
				
				TweenLite.fromTo(iDisplayObj.Object, .5, {scaleX: 5, scaleY: 5, alpha: .2}, {scaleY: .5, scaleX: .5, alpha: 1, ease: Back.easeOut});
				addDisplayObject(iDisplayObj);
			}
		}
		
		private function eh_CheckObjectPosition(e:Event):void
		{
			var obj:PosterObject = (e.target as PosterObject);
			var maxArea:Rectangle = new Rectangle(ms_BufferBetweenImages + m_Background.x, ms_BufferBetweenImages + m_Background.y, m_Background.width - (ms_BufferBetweenImages*2), m_Background.height - (ms_BufferBetweenImages*2));
			
			if(obj.Object.width > maxArea.width || obj.Object.height > maxArea.height)
			{
				if(obj.Object.width / maxArea.width > obj.Object.height / maxArea.height)
				{
					obj.Object.width = maxArea.width;
					obj.Object.scaleY = obj.Object.scaleX;
				}
				else
				{
					obj.Object.height = maxArea.height;
					obj.Object.scaleX = obj.Object.scaleY;
				}
			}
			
			var objRect:Rectangle= obj.Object.getBounds(m_Poster);
			var tempLocation:Number;
			
			if(objRect.x < maxArea.x)
			{
				tempLocation = maxArea.x + (obj.x - objRect.x);
				TweenLite.to(obj, .5, {x: tempLocation, ease:Back.easeOut});
			}
			if(objRect.y < maxArea.y)
			{
				tempLocation = maxArea.y + (obj.y - objRect.y);
				TweenLite.to(obj, .5, {y: tempLocation, ease:Back.easeOut});
			}
			if((objRect.x + obj.width) > (maxArea.x + maxArea.width))
			{
				tempLocation = (maxArea.x + maxArea.width) - (objRect.width + obj.getBounds(obj).x);
				TweenLite.to(obj, .5, {x: tempLocation, ease:Back.easeOut});
			}
			if((objRect.y + obj.height) > (maxArea.y + maxArea.height))
			{
				tempLocation = (maxArea.y + maxArea.height) - (objRect.height + obj.getBounds(obj).y);
				TweenLite.to(obj, .5, {y: tempLocation, ease:Back.easeOut});
			}
			
			if(obj.IsTrashable)
			{
				checkIfTrashed(obj);
			}
			
			GSOC_ButtonToggle.ToggleButton(true, m_SubmitButton, this.activateNextGirlButton, this.deactivateNextGirlButton);
		}
		
		private function checkIfTrashed(obj:PosterObject):void
		{
			var distToTrash:Number = Point.distance(new Point(CitrusFramework.frameworkStage.mouseX, CitrusFramework.frameworkStage.mouseY),
				new Point(m_TrashCan.x, m_TrashCan.y));
			if(distToTrash < ms_MinDistanceToTrash)
			{
				SoundHandler.PlaySound(GSOC_SFX.SOUND_TRASHCANS[SessionInformation_GirlsTablet.Age]);
				TweenLite.killTweensOf(obj);
				TweenLite.to(obj, .5, {alpha: 0, scaleX: 0, scaleY: 0, onComplete: removeDisplayObject, onCompleteParams: [obj]});
			}
		}
		
		private function eh_PopupButtonClicked(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_CLICKS[SessionInformation_GirlsTablet.Age]);
			if(e.target.name == "icons")
			{
				m_Popups[Poster_PopupIDs.ICONS_POPUP].DisplayPopUp();
			}
			else if(e.target.name == "type")
			{
				m_Popups[Poster_PopupIDs.TYPING_POPUP].DisplayPopUp();
			}
		}
		
		private function eh_BorderButtonClicked(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_SELECTBORDERS[SessionInformation_GirlsTablet.Age]);
			var borderButton:MovieClip = (e.target as MovieClip);
			m_Border.gotoAndStop(borderButton.currentFrame);
			
			GSOC_ButtonToggle.ToggleButton(true, m_SubmitButton, this.activateNextGirlButton, this.deactivateNextGirlButton);
			
			selectBorderButton(borderButton);
		}
		
		private function eh_PatternButtonClicked(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_SELECTPATTERNS[SessionInformation_GirlsTablet.Age]);
			var patternButton:MovieClip = (e.target as MovieClip);
			m_Pattern.gotoAndStop(patternButton.currentFrame);
			
			GSOC_ButtonToggle.ToggleButton(true, m_SubmitButton, this.activateNextGirlButton, this.deactivateNextGirlButton);
			
			selectPatternButton(patternButton);
		}
		
		private function selectPatternButton(button:MovieClip):void
		{
			for(var i:int = 0 ; i < m_Pattern_Buttons.length; i++)
			{
				var patternButton:MovieClip = m_Pattern_Buttons[i];
				if(patternButton != button) 
				{
					TweenMax.to(patternButton, .5, {glowFilter:{color: GSOC_Info.GlowColor, blurX:0, blurY:0, strength:0, alpha:1}});
				}
				else
				{
					TweenMax.to(patternButton, .5, {glowFilter:{color: GSOC_Info.GlowColor, blurX:50, blurY:50, strength:2, alpha:1}});
				}
			}
		}
		
		private function selectBorderButton(button:MovieClip):void
		{
			for(var i:int = 0 ; i < m_Border_Buttons.length; i++)
			{
				var borderButton:MovieClip = m_Border_Buttons[i];
				if(borderButton != button) 
				{
					TweenMax.to(borderButton, .5, {glowFilter:{color: GSOC_Info.GlowColor, blurX:0, blurY:0, strength:0, alpha:1}});
				}
				else
				{
					TweenMax.to(borderButton, .5, {glowFilter:{color: GSOC_Info.GlowColor, blurX:50, blurY:50, strength:2, alpha:1}});
				}
			}
		}
		
		protected function eh_ColorChanged(e:CourierEvent):void
		{
			GSOC_ButtonToggle.ToggleButton(true, m_SubmitButton, this.activateNextGirlButton, this.deactivateNextGirlButton);
			TweenLite.to(m_Pattern, .5, {colorTransform:{tint: e.payload[3], tintAmount: .5}});
		}
	}
}
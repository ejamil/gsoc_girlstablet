package com.bpi.gsoc.girlstablet.states.gsoc_11poster
{
	import com.bpi.components.poster.Poster_JC_Skin;
	import com.bpi.gsoc.framework.gsocui.ColorPicker;
	import com.bpi.gsoc.framework.gsocui.GSOC_Header;

	public class Poster_App_JC extends Poster_App
	{
		private var m_TypedSkin:Poster_JC_Skin;
		
		public function Poster_App_JC()
		{
			m_Skin = new Poster_JC_Skin();
			m_TypedSkin = (m_Skin as Poster_JC_Skin);
			m_GSOCHeader = new GSOC_Header(m_Skin.header.paginationArea);
			GSOCheader.addArrows(m_Skin.header.paginationArea.x, 0);
		}
		
		public override function Initialize():void
		{
			m_Poster = m_TypedSkin.poster;
			m_SubmitButton = m_TypedSkin.submit;
			m_Border = m_TypedSkin.poster.borders;
			m_Pattern = m_TypedSkin.poster.background;
			m_Background = m_TypedSkin.poster.maxArea;
			m_ColorSelection = new ColorPicker(m_TypedSkin.gradient, m_TypedSkin, "COLOR_CHANGED");
			
			m_Border_Buttons.push(m_TypedSkin.outline0);
			m_Border_Buttons.push(m_TypedSkin.outline1);
			m_Border_Buttons.push(m_TypedSkin.outline2);
			m_Border_Buttons.push(m_TypedSkin.outline3);
			m_Border_Buttons.push(m_TypedSkin.outline4);
			
			m_Pattern_Buttons.push(m_TypedSkin.pattern0);
			m_Pattern_Buttons.push(m_TypedSkin.pattern1);
			m_Pattern_Buttons.push(m_TypedSkin.pattern2);
			m_Pattern_Buttons.push(m_TypedSkin.pattern3);
			m_Pattern_Buttons.push(m_TypedSkin.pattern4);
			
			m_CanResize = true;
			m_CanRotate = false;
			
			super.Initialize();
		}
	}
}
package com.bpi.gsoc.girlstablet.states.end
{
	import com.bpi.citrusas.images.AnimationClip;
	import com.bpi.citrusas.utils.Utils_Text;
	import com.bpi.components.endscreen.DB_EndScreen_Skin;
	import com.bpi.components.endscreen.JC_EndScreen_Skin;
	import com.bpi.components.endscreen.SA_EndScreen_Skin;
	import com.bpi.components.endscreen.db_circle1;
	import com.bpi.components.endscreen.db_circle2;
	import com.bpi.components.endscreen.db_star1;
	import com.bpi.components.endscreen.db_star2;
	import com.bpi.components.endscreen.db_starMain;
	import com.bpi.components.endscreen.db_triangle1;
	import com.bpi.components.endscreen.db_triangle2;
	import com.bpi.components.endscreen.jc_circle1;
	import com.bpi.components.endscreen.jc_circle2;
	import com.bpi.components.endscreen.jc_star1;
	import com.bpi.components.endscreen.jc_star2;
	import com.bpi.components.endscreen.jc_starMain;
	import com.bpi.components.endscreen.jc_triangle1;
	import com.bpi.components.endscreen.jc_triangle2;
	import com.bpi.components.endscreen.sa_star1;
	import com.bpi.components.endscreen.sa_star2;
	import com.bpi.components.endscreen.sa_starMain;
	import com.bpi.components.endscreen.sa_triangle1;
	import com.bpi.components.endscreen.sa_triangle2;
	import com.bpi.components.exploreyourpassion.Particle_CircleDB;
	import com.bpi.components.exploreyourpassion.Particle_StarDB;
	import com.bpi.gsoc.framework.backgrounds.Background_All;
	import com.bpi.gsoc.framework.constants.AgeConstants;
	import com.bpi.gsoc.framework.constants.GSOC_Info;
	import com.bpi.gsoc.framework.constants.TrackConstants;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.gsocui.GSOC_Header;
	import com.greensock.TweenLite;
	import com.greensock.TweenMax;
	import com.greensock.easing.Back;
	import com.greensock.easing.Power2;
	import com.greensock.plugins.BlurFilterPlugin;
	import com.greensock.plugins.TransformAroundCenterPlugin;
	import com.greensock.plugins.TransformAroundPointPlugin;
	import com.greensock.plugins.TweenPlugin;
	
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.filters.DropShadowFilter;
	import flash.geom.Point;
	import flash.text.TextField;

	public class App_End extends Sprite
	{
		private static const ms_ExplosionStart_Delay:Number = 1;
		
		private static const ms_Circle_Num:int = 8;
		private static const ms_Circle_AnimationDuration:Number = .8;
		private static const ms_Circle_Distance:Number = 500;
		private static const ms_Circle_DistanceVariance:Number = 300;
		private static const ms_Circle_Scale:Number = .6;
		private static const ms_Circle_ScaleVariance:Number = .4;
		private static const ms_Circle_AngleVariance:Number = 20;
		
		private static const ms_Triangle_Num:int = 22;
		private static const ms_Triangle_AnimationDuration:Number = .8;
		private static const ms_Triangle_Distance:Number = 50;
		private static const ms_Triangle_DistanceVariance:Number = 50;
		private static const ms_Triangle_ScaleX:Number = .8;
		private static const ms_Triangle_ScaleXVariance:Number = .3;
		private static const ms_Triangle_ScaleY:Number = 1;
		private static const ms_Triangle_ScaleYVariance:Number = .1;
		private static const ms_Triangle_AngleVariance:Number = 18;
		
		private static const ms_Star_Num:int = 8;
		private static const ms_Star_AnimationDuration:Number = .8;
		private static const ms_Star_Distance:Number = 400;
		private static const ms_Star_DistanceVariance:Number = 300;
		private static const ms_Star_Scale:Number = .6;
		private static const ms_Star_ScaleVariance:Number = .4;
		private static const ms_Star_AngleVariance:Number = 20;
		
		private var m_ExplosionContainer:MovieClip;
		private var m_TriangleContainer:MovieClip;
		private var m_CircleContainer:MovieClip;
		private var m_StarContainer:MovieClip;
		private var m_BaseStar:Bitmap;
		private var m_UseSecondShape:Boolean;

		private var m_Background:AnimationClip;
		private var m_Textfield:TextField
		private var m_Skin:MovieClip;
		
		public function App_End()
		{
			m_Background = new AnimationClip(Background_All);
			m_TriangleContainer = new MovieClip();
			m_CircleContainer = new MovieClip();
			m_StarContainer = new MovieClip();
			
			TweenPlugin.activate([BlurFilterPlugin, TransformAroundCenterPlugin, TransformAroundPointPlugin]);
		}
		
		public function InitializeEventListeners():void
		{
		}
		
		public function Initialize():void
		{
			
		}
		
		public function DeInit():void
		{
			TweenMax.killAll();
			m_TriangleContainer = new MovieClip();
			m_CircleContainer = new MovieClip();
			m_StarContainer = new MovieClip();
			
			if(m_ExplosionContainer)
			{
				for each(var child:DisplayObject in m_ExplosionContainer)
				{
					m_ExplosionContainer.removeChild(child);
				}
			}
		}
		
		public function CleanUp():void
		{
		}
		
		public function Init():void
		{
			switch(SessionInformation_GirlsTablet.Age)
			{
				case AgeConstants.KEY_DAISYBROWNIE:
					m_Skin = new DB_EndScreen_Skin();
					break;
				case AgeConstants.KEY_JUNIORCADETTE:
					m_Skin = new JC_EndScreen_Skin();
					break;
				case AgeConstants.KEY_SENIORAMBASSADOR:
					m_Skin = new SA_EndScreen_Skin();
					break;
			}
			m_Skin.header.gotoAndStop(SessionInformation_GirlsTablet.Track + 1);
			
			m_Textfield = m_Skin.header.textArea;
			Utils_Text.EmbedText(m_Textfield, GSOC_Info.BodyFont, 72, GSOC_Info.ColorOne);
			m_Textfield.text = SessionInformation_GirlsTablet.Authentication_SessionID;
			Utils_Text.FitTextToTextfield(m_Textfield, 10, true);
			
			m_Background.Frame = SessionInformation_GirlsTablet.Age;
			
			var header:GSOC_Header = new GSOC_Header(m_Skin.paginationArea);
			var xPos:Number = m_Skin.paginationArea.x;
			if(SessionInformation_GirlsTablet.Track == TrackConstants.TAKE_ACTION)
			{
				// for some reason the take action header is larger than the Career header in JC/SA...
				// move the arrows to the right by an arbitary certain amount for JC/SA 
				if(SessionInformation_GirlsTablet.Age == AgeConstants.KEY_JUNIORCADETTE)
				{
					// move the arrows over to the right so they don't cover the text
					xPos += 150;
				}
				else if(SessionInformation_GirlsTablet.Age == AgeConstants.KEY_SENIORAMBASSADOR)
				{
					// move the arrows over to the right so they don't cover the text
					xPos += 175;
				}
			}
			header.addArrows(xPos, 0);
			
			this.addChild(m_Background);
			this.addChild(m_Skin);
			m_Skin.addChild(header);
			
			prepExplosionAnimation();
		}
		
		private function prepExplosionAnimation():void
		{
			m_ExplosionContainer = m_Skin.explosionContainer;
			m_ExplosionContainer.alpha = 0;
			m_ExplosionContainer.filters = [new DropShadowFilter(40, 30, 0x000000, .4, 20, 20)];
			
			for(i = 0; i < ms_Triangle_Num; i++)
			{
				var triangleContainer:Bitmap = triangleBitmap;
				m_TriangleContainer.addChild(triangleContainer);
			}
			m_ExplosionContainer.addChild(m_TriangleContainer);
			
			for(var i:int = 0; i < ms_Circle_Num; i++)
			{
				var circle:Bitmap = circleBitmap;
				circle.smoothing = true;
				m_CircleContainer.addChild(circle);
			}
			m_ExplosionContainer.addChild(m_CircleContainer);
			
			for(i = 0; i < ms_Star_Num; i++)
			{
				var star:Bitmap = starBitmap;
				star.smoothing = true;
				m_StarContainer.addChild(star);
			}
			m_ExplosionContainer.addChild(m_StarContainer);
			
			m_BaseStar = mainStar;
			m_BaseStar.scaleX = .8;
			m_BaseStar.scaleY = .8;
			m_BaseStar.x = m_ExplosionContainer.x + (-m_BaseStar.width * .5);
			m_BaseStar.y = m_ExplosionContainer.y + (-m_BaseStar.height * .5);
			m_BaseStar.alpha = 0;
			m_Skin.addChild(m_BaseStar);
			
			TweenLite.to(m_CircleContainer, int.MAX_VALUE*.1, {rotation: int.MAX_VALUE});
			TweenLite.to(m_TriangleContainer, int.MAX_VALUE*.2, {rotation: int.MAX_VALUE});
			TweenLite.to(m_StarContainer, int.MAX_VALUE*.25, {rotation: int.MAX_VALUE});
			TweenLite.delayedCall(ms_ExplosionStart_Delay, startExplosionAnimation);
		}
		
		private function startBaseStarGlow():void
		{
			TweenLite.to(m_BaseStar, 2, {glowFilter:{color:0xFFFFFF, blurX:100, blurY:100, strength:2, alpha:1}, transformAroundCenter:{scaleX: 1.3, scaleY: 1.3}, onComplete: endBaseStarGlow});
		}
		
		private function endBaseStarGlow():void
		{
			TweenLite.to(m_BaseStar, 2, {glowFilter:{color:0xFFFFFF, blurX:10, blurY:10, strength:.5, alpha:1}, transformAroundCenter:{scaleX: 1, scaleY: 1}, onComplete: startBaseStarGlow});
		}
		
		private function startExplosionAnimation():void
		{
			m_ExplosionContainer.alpha = 1;
			
			TweenLite.fromTo(m_BaseStar, 1, 
				{transformAroundCenter: {scaleX: 4, scaleY: 4, rotation: 90}},
				{transformAroundCenter: {scaleX: 1, scaleY: 1, rotation: 0}, alpha: 1, ease: Power2.easeInOut,
					onComplete: startBaseStarGlow});
			
			for(i = 0; i < ms_Triangle_Num; i++)
			{
				var triangle:Bitmap = m_TriangleContainer.getChildAt(i) as Bitmap;
				animateTriangle(triangle, i);
			}
			for(var i:int = 0; i < ms_Circle_Num; i++)
			{
				var circle:Bitmap = m_CircleContainer.getChildAt(i) as Bitmap;
				animateCircle(circle, i);
			}
			for(i = 0; i < ms_Star_Num; i++)
			{
				var star:Bitmap = m_StarContainer.getChildAt(i) as Bitmap;
				animateStar(star, i);
			}
		}
		
		private function animateTriangle(triangle:Bitmap, triangleNum:int):void
		{
			var randomScaleX:Number = Math.random()*ms_Triangle_ScaleXVariance - ms_Triangle_ScaleXVariance * .5;
			var randomScaleY:Number = Math.random()*ms_Star_ScaleVariance - ms_Star_ScaleVariance * .5;
			var randomDistance:Number = Math.random()*ms_Triangle_DistanceVariance - ms_Triangle_DistanceVariance * .5;
			var triangleRotation:Number = (360/ms_Triangle_Num) * triangleNum + Math.random()*ms_Triangle_AngleVariance + Math.cos(triangle.width * .5);
			var triangleWidth:Number = triangle.width;
			
			triangle.y = ms_Triangle_Distance + randomDistance;
			
			TweenLite.to(triangle, 0, {transformAroundPoint: {point: new Point(triangleWidth * .5, 0), 
				scaleX: ms_Triangle_ScaleX + randomScaleX,
				scaleY: ms_Triangle_ScaleY + randomScaleY,
				rotation: triangleRotation}});
			TweenLite.to(triangle, 0, {x: triangle.x - 130});
			TweenLite.fromTo(triangle, ms_Triangle_AnimationDuration, {alpha: 0, rotationY: 90}, {alpha: 1, rotationY: 0});
		}
		
		private function animateStar(star:Bitmap, starNum:int):void
		{
			var randomScale:Number = Math.random()*ms_Star_ScaleVariance - ms_Star_ScaleVariance * .5;
			var randomDistance:Number = Math.random()*ms_Star_DistanceVariance - ms_Star_DistanceVariance * .5;
			var starSize:Point = new Point(star.width, star.height);
			
			star.rotation = (360/ms_Star_Num) * starNum + Math.random()*ms_Star_AngleVariance;
			
			TweenLite.fromTo(m_StarContainer, ms_Star_AnimationDuration, {scaleX: .1, scaleY: .1}, {scaleX: 1, scaleY: 1, ease: Back.easeOut});
			TweenLite.fromTo(star, ms_Star_AnimationDuration, {x: 0, y: 0, alpha: 1}, {
				x: (ms_Star_Distance + randomDistance) * Math.cos(star.rotation*Math.PI/180),
				y: (ms_Star_Distance + randomDistance) * Math.sin(star.rotation*Math.PI/180),
				scaleX: ms_Star_Scale + randomScale,
				scaleY: ms_Star_Scale + randomScale,
				ease: Back.easeOut});
		}
		
		private function animateCircle(circle:Bitmap, circleNum:int):void
		{
			var randomScale:Number = Math.random()*ms_Circle_ScaleVariance - ms_Circle_ScaleVariance * .5;
			var randomDistance:Number = Math.random()*ms_Circle_DistanceVariance - ms_Circle_DistanceVariance * .5;
			
			circle.rotation = (360/ms_Circle_Num) * circleNum + Math.random()*ms_Circle_AngleVariance;
			
			TweenLite.fromTo(circle, ms_Circle_AnimationDuration, {x: 0, y: 0, height: 1, width: 1, alpha: 1}, {
				x: (ms_Circle_Distance + randomDistance) * Math.cos(circle.rotation*Math.PI/180),
				y: (ms_Circle_Distance + randomDistance) * Math.sin(circle.rotation*Math.PI/180),
				scaleX: randomScale + ms_Circle_Scale,
				scaleY: randomScale + ms_Circle_Scale,
				ease: Back.easeOut});
		}
		
		private function get mainStar():Bitmap
		{
			switch(SessionInformation_GirlsTablet.Age)
			{
				case AgeConstants.KEY_DAISYBROWNIE:
					return new Bitmap(new db_starMain());
					break;
				case AgeConstants.KEY_JUNIORCADETTE:
					return new Bitmap(new jc_starMain());
					break;
				case AgeConstants.KEY_SENIORAMBASSADOR:
					return new Bitmap(new sa_starMain());
					break;
				default:
					return new Bitmap(new Particle_StarDB());
			}
		}
		
		private function get starBitmap():Bitmap
		{
			m_UseSecondShape = !m_UseSecondShape;
			
			switch(SessionInformation_GirlsTablet.Age)
			{
				case AgeConstants.KEY_DAISYBROWNIE:
					if(m_UseSecondShape) return new Bitmap(new db_star1());
					else return new Bitmap(new db_star2());
					break;
				case AgeConstants.KEY_JUNIORCADETTE:
					if(m_UseSecondShape) return new Bitmap(new jc_star1());
					else return new Bitmap(new jc_star2());
					break;
				case AgeConstants.KEY_SENIORAMBASSADOR:
					if(m_UseSecondShape) return new Bitmap(new sa_star1());
					else return new Bitmap(new sa_star2());
					break;
				default:
					return new Bitmap(new Particle_StarDB());
			}
		}
		
		private function get circleBitmap():Bitmap
		{
			m_UseSecondShape = !m_UseSecondShape;
			
			switch(SessionInformation_GirlsTablet.Age)
			{
				case AgeConstants.KEY_DAISYBROWNIE:
					if(m_UseSecondShape) return new Bitmap(new db_circle1());
					else return new Bitmap(new db_circle2());
					break;
				case AgeConstants.KEY_JUNIORCADETTE:
					if(m_UseSecondShape) return new Bitmap(new jc_circle1());
					else return new Bitmap(new jc_circle2());
					break;
				case AgeConstants.KEY_SENIORAMBASSADOR:
					if(m_UseSecondShape) return new Bitmap(new jc_circle1());
					else return new Bitmap(new jc_circle2());
					break;
				default:
					return new Bitmap(new Particle_CircleDB());
			}
		}
		
		private function get triangleBitmap():Bitmap
		{
			m_UseSecondShape = !m_UseSecondShape;
			
			switch(SessionInformation_GirlsTablet.Age)
			{
				case AgeConstants.KEY_DAISYBROWNIE:
					if(m_UseSecondShape) return new Bitmap(new db_triangle1());
					else return new Bitmap(new db_triangle2());
					break;
				case AgeConstants.KEY_JUNIORCADETTE:
					if(m_UseSecondShape) return new Bitmap(new jc_triangle1());
					else return new Bitmap(new jc_triangle2());
					break;
				case AgeConstants.KEY_SENIORAMBASSADOR:
					if(m_UseSecondShape) return new Bitmap(new sa_triangle1());
					else return new Bitmap(new sa_triangle2());
					break;
				default:
					return new Bitmap(new sa_triangle2());
			}
		}
		
		public function Update():void
		{
			
		}
		
		public function QueuedUpdate():void
		{
			
		}
		
		public function Resume():void
		{
			
		}
		
		public function Pause():void
		{
			
		}
	}
}
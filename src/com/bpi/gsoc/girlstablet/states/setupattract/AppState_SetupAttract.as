package com.bpi.gsoc.girlstablet.states.setupattract
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.citrusas.citrus.framework.layers.LayerIDs;
	import com.bpi.citrusas.states.AppState;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_Main;

	public class AppState_SetupAttract extends AppState
	{
		private var m_AppSetupAttract:App_SetupAttract;
		
		public function AppState_SetupAttract()
		{
			super(StateController_StateIDs_Main.STATE_SETUPATTRACT);
			
			m_AppSetupAttract = new App_SetupAttract();
		}
		
		public override function Initialize():void
		{
			m_AppSetupAttract.Initialize();
			
			super.Initialize();
		}
		
		public override function CleanUp():void
		{
			m_AppSetupAttract.CleanUp();
			
			super.CleanUp();
		}
		
		public override function Update():void
		{
			m_AppSetupAttract.Update();
			
			super.Update();
		}
		
		protected override function Init():void
		{
			m_AppSetupAttract.Init();
			
			CitrusFramework.layerController.GetLayerByID(LayerIDs.CONTENT).addChild(m_AppSetupAttract);
			
			super.Init();
		}
		
		protected override function DeInit():void
		{
			m_AppSetupAttract.DeInit();
			
			CitrusFramework.layerController.GetLayerByID(LayerIDs.CONTENT).removeChild(m_AppSetupAttract);
			
			super.DeInit();
		}
		
		public override function QueuedUpdate():void
		{
			
		}
	}
}
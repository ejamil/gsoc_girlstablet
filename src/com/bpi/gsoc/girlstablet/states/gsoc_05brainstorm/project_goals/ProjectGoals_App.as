package com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.project_goals
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.citrusas.images.PlaceImage;
	import com.bpi.citrusas.input.inputcontroller.InputController;
	import com.bpi.citrusas.sound.SoundHandler;
	import com.bpi.citrusas.states.StateControllerManager;
	import com.bpi.citrusas.utils.Utils_Image;
	import com.bpi.citrusas.utils.Utils_Text;
	import com.bpi.gsoc.framework.constants.AgeColors;
	import com.bpi.gsoc.framework.constants.AgeConstants;
	import com.bpi.gsoc.framework.constants.ButtonPlacement;
	import com.bpi.gsoc.framework.constants.GSOC_Info;
	import com.bpi.gsoc.framework.constants.Keys_FileNames;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.data.userdata.GSOC_UserData_Team_DataKeys;
	import com.bpi.gsoc.framework.data.userdata.GSOC_UserData_Team_ImageKeys;
	import com.bpi.gsoc.framework.sfx.GSOC_SFX;
	import com.bpi.gsoc.framework.states.app.GSOC_AppSkin;
	import com.bpi.gsoc.girlstablet.global_data.events.Event_UploadDefaultData;
	import com.bpi.gsoc.girlstablet.global_data.upload.UploadManager;
	import com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.project_goals.classes.TextfieldManager;
	import com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.project_goals.classes.events.Event_TextfieldStatus;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_IDs;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_BrainStorm;
	import com.greensock.TweenLite;
	import com.greensock.easing.Power3;
	
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.text.TextField;

	public class ProjectGoals_App extends GSOC_AppSkin
	{
		public static const m_Dispatcher:IEventDispatcher = new EventDispatcher();;
		
		protected var m_TextfieldManager:TextfieldManager;
		protected var m_NextButton:MovieClip;
		protected var m_IssueCause:MovieClip;
		
		private var m_NextButtonYPos:Number;
		
		public function ProjectGoals_App()
		{
			m_TextfieldManager = new TextfieldManager();
		}
		
		public override function Initialize():void
		{
			super.Initialize();
			
			m_TextfieldManager.Initialize();
			
			m_Dispatcher.addEventListener(Event_TextfieldStatus.CHECK_TEXTFIELDS, eh_TextfieldsChanged);
		}
		
		public override function DeInitialize():void
		{
			m_TextfieldManager.DeInitialize();
			
			m_Dispatcher.removeEventListener(Event_TextfieldStatus.CHECK_TEXTFIELDS, eh_TextfieldsChanged);
			m_NextButton.removeEventListener(InputController.DOWN, eh_NextButtonPressed);
		}
		
		public function Setup():void
		{
			m_NextButtonYPos = m_NextButton.y;
			m_NextButton.y = m_NextButtonYPos + m_NextButton.height;
			m_NextButton.x = ButtonPlacement.NEXT_X;
			
			SessionInformation_GirlsTablet.SessionInformationEventDispatcher.dispatchEvent(new Event_UploadDefaultData(Event_UploadDefaultData.EVENT_UPLOAD_DEFAULT_MINDMAP_ISSUE));
			SessionInformation_GirlsTablet.SessionInformationEventDispatcher.dispatchEvent(new Event_UploadDefaultData(Event_UploadDefaultData.EVENT_UPLOAD_DEFAULT_MINDMAP_ROOTCAUSE));
			
			for(var i:int = 0 ; i < 3; i++)
			{
				if(SessionInformation_GirlsTablet.Age == AgeConstants.KEY_DAISYBROWNIE)
				{
					AgeColors
					Utils_Text.ConvertToEmbeddedFont(m_Skin["question_" + (i+1).toString()] as TextField, GSOC_Info.HeaderFont, false);
				}
				else
				{
					Utils_Text.ConvertToEmbeddedFont(m_Skin["question_" + (i+1).toString()] as TextField, GSOC_Info.BodyFont, false);
				}
			}
			
			var issueIcon:Sprite = SessionInformation_GirlsTablet.TeamInfo.Team_UserData.GetImage(GSOC_UserData_Team_ImageKeys.KEY_CHOOSEYOURISSUE_ISSUE);
			var causeIcon:Sprite = SessionInformation_GirlsTablet.TeamInfo.Team_UserData.GetImage(GSOC_UserData_Team_ImageKeys.KEY_MINDMAP_ROOTCAUSE);
			PlaceImage.ImageToExistingContainer(issueIcon, m_IssueCause.issue, false, true);
			PlaceImage.ImageToExistingContainer(causeIcon, m_IssueCause.cause, false, true);
			
			m_TextfieldManager.Init();
		}
		
		private function eh_TextfieldsChanged(e:Event_TextfieldStatus):void
		{
			if(e.m_IsComplete && !m_NextButton.hasEventListener(InputController.DOWN))
			{
				TweenLite.to(m_NextButton, .5, {y: m_NextButtonYPos, ease: Power3.easeInOut});
				m_NextButton.addEventListener(InputController.DOWN, eh_NextButtonPressed);
			}
			else if(!e.m_IsComplete)
			{
				TweenLite.to(m_NextButton, .5, {y: m_NextButtonYPos + m_NextButton.height, ease: Power3.easeInOut});
				m_NextButton.removeEventListener(InputController.DOWN, eh_NextButtonPressed);
			}
		}
		
		private function SubmitData():void
		{
			CitrusFramework.frameworkStage.focus = null;
			
			var tempSprite_AllAnswers:Sprite = new Sprite();
			var tempSprite_Answer3:Sprite = new Sprite();
			
			// note that a late change in graphics means that questions 2 and 3 were flipped.
			// so question_2 appears last in the list, and question_3 appears second.
			
			for(var i:int = 0 ; i < 3; i++)
			{
				m_Skin["answerbox_" + (i + 1).toString()].gotoAndStop(1);
				
				tempSprite_AllAnswers.addChild(m_Skin["question_" + (i + 1).toString()]);
				tempSprite_AllAnswers.addChild(m_Skin["answerbox_" + (i + 1).toString()]);
				
				if(i == 2)
				{
					var question:DisplayObject = m_Skin["question_" + (i + 1).toString()];
					var answer:DisplayObject = m_Skin["answerbox_" + (i + 1).toString()];
					var tempQuestion:Bitmap = Utils_Image.ConvertDisplayObjectToBitmap(question);
					var tempAnswer:Bitmap = Utils_Image.ConvertDisplayObjectToBitmap(answer);
					
					tempQuestion.x = (tempAnswer.width - tempQuestion.width)/2;
					tempQuestion.y = 0;
					
					tempAnswer.x = 0;
					tempAnswer.y = tempQuestion.height;
					
					tempSprite_Answer3.addChild(tempQuestion);
					tempSprite_Answer3.addChild(tempAnswer);
				}
			}
			
			var mc_AllAnswers:MovieClip = Utils_Image.ConvertDisplayObjectToMovieClip(tempSprite_AllAnswers);		
			var ms_Answer3:MovieClip = Utils_Image.ConvertDisplayObjectToMovieClip(tempSprite_Answer3);
			
			SessionInformation_GirlsTablet.TeamInfo.Team_UserData.SetData(GSOC_UserData_Team_DataKeys.KEY_PROJECT_IMPORTANT_ANSWER, m_Skin["answerbox_1"].textfield.text);
			SessionInformation_GirlsTablet.TeamInfo.Team_UserData.SetImage(GSOC_UserData_Team_ImageKeys.KEY_BRAINSTORM_PROJECTGOALS_BOARD, mc_AllAnswers);
			SessionInformation_GirlsTablet.TeamInfo.Team_UserData.SetImage(GSOC_UserData_Team_ImageKeys.KEY_BRAINSTORM_PROJECTGOALS_ANSWER3, tempSprite_Answer3);
			
			UploadManager.UploadDisplayObject(mc_AllAnswers, Keys_FileNames.KEY_BRAINSTORM_PROJECTGOALS_BOARD);
			UploadManager.UploadDisplayObject(tempSprite_Answer3, Keys_FileNames.KEY_BRAINSTORM_PROJECTGOALS_ANSWER3);
		}
		
		private function eh_NextButtonPressed(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_CLICKS[SessionInformation_GirlsTablet.Age]);
			SubmitData();
			StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_BRAINSTORM, StateController_StateIDs_BrainStorm.STATE_BRAINSTORM);
		}
		
		public static function get Dispatcher():IEventDispatcher
		{
			return m_Dispatcher;
		}
	}
}
package com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.stickynotes
{
	import com.bpi.citrusas.input.inputcontroller.InputController;
	import com.bpi.citrusas.ui.draggableobject.DraggableObject;
	import com.bpi.citrusas.ui.draggableobject.events.Event_DraggableObject;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TouchEvent;
	import flash.geom.Point;
	import flash.utils.getTimer;

	public class StickyNote extends Sprite
	{
		private static const CLICK_REGISTRATION_TIME:Number = 150;
		
		protected var m_Skin:DraggableObject;
		protected var m_ClickTimer:Number = 0;
		protected var m_PreviousPosition:Point;
		
		public function StickyNote()
		{
			
		}
		
		public function Initialize():void
		{
			m_Skin.draggableObject.addEventListener(InputController.DOWN, eh_StickyNote_Clicked);
			m_Skin.draggableObject.addEventListener(InputController.UP, eh_StickyNote_Clicked);
			
			m_Skin.addEventListener(Event_DraggableObject.DRAG_END, eh_StickyNoteMoved);
			m_Skin.AddEventListeners();
		}
		
		public function DeInitialize():void
		{
			m_Skin.draggableObject.removeEventListener(InputController.DOWN, eh_StickyNote_Clicked);
			m_Skin.draggableObject.removeEventListener(InputController.UP, eh_StickyNote_Clicked);
			
			m_Skin.removeEventListener(Event_DraggableObject.DRAG_END, eh_StickyNoteMoved);
			m_Skin.RemoveEventListeners();
		}
		
		public function eh_StickyNote_Clicked(e:Event):void
		{
			if(e.type == MouseEvent.MOUSE_DOWN || e.type == TouchEvent.TOUCH_BEGIN)
			{
				m_ClickTimer = getTimer();
				m_PreviousPosition = new Point(m_Skin.draggableObject.x, m_Skin.draggableObject.y);
			}
			else
			{
				if((getTimer() - m_ClickTimer) < CLICK_REGISTRATION_TIME && Point.distance(new Point(m_Skin.draggableObject.x, m_Skin.draggableObject.y), m_PreviousPosition) < 100)
				{
					StickyClicked(this);
				}
			}
		}
		
		protected function eh_StickyNoteMoved(e:Event_DraggableObject):void
		{
			
		}
		
		protected function StickyClicked(stickyNote:StickyNote):void
		{
			
		}
	}
}
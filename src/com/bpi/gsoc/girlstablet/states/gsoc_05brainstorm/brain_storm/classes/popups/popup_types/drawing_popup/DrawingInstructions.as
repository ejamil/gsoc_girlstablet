package com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.popups.popup_types.drawing_popup
{
	import com.bpi.citrusas.text.BasicTextField;
	import com.bpi.gsoc.framework.constants.AgeColors;
	import com.bpi.gsoc.framework.constants.AgeConstants;
	import com.bpi.gsoc.framework.constants.AgeFontLinkages;
	import com.bpi.gsoc.framework.constants.GSOC_Info;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.greensock.TweenLite;
	
	import flash.display.Sprite;
	import flash.text.TextFormatAlign;

	public class DrawingInstructions extends Sprite
	{
		private static const ms_DrawingInstructions:String = "Select a color and use your finger to draw here.";
		private static const ms_FontSize:int = 80;
		
		private var m_DrawingInstructions:BasicTextField;
		
		public function DrawingInstructions()
		{
			m_DrawingInstructions = new BasicTextField();
		}
		
		public function Initialize(width:Number, height:Number):void
		{
			if(SessionInformation_GirlsTablet.Age == AgeConstants.KEY_SENIORAMBASSADOR)
			{
				m_DrawingInstructions.EmbeddedFont = AgeFontLinkages.SA_BODYBOLD_FONT;
				m_DrawingInstructions.textColor = AgeColors.SA_ORANGE;
			}
			else 
			{
				m_DrawingInstructions.EmbeddedFont = GSOC_Info.BodyFont;
				m_DrawingInstructions.textColor = GSOC_Info.ColorTwo;
			}
			
			m_DrawingInstructions.width = width;
			m_DrawingInstructions.height = height;
			m_DrawingInstructions.x = -width * .5;
			m_DrawingInstructions.y = -height * .5;
			m_DrawingInstructions.FontSize = ms_FontSize;
			m_DrawingInstructions.text = ms_DrawingInstructions;
			m_DrawingInstructions.Alignment = TextFormatAlign.CENTER;
			m_DrawingInstructions.selectable = false;
			
			this.addChild(m_DrawingInstructions);
		}
		
		public function DisplayInstructions():void
		{
			this.visible = true;
			TweenLite.to(this, .25, {alpha: 1});
		}
		
		public function HideInstructions():void
		{
			TweenLite.to(this, .25, {alpha: 0, onComplete: removeInstructions});
		}
		
		public function HideInstructionsImmediately():void
		{
			this.removeInstructions();
		}
		
		private function removeInstructions():void
		{
			this.visible = false;
		}
	}
}
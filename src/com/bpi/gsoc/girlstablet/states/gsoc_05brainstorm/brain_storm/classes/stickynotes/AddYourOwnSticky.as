package com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.stickynotes
{
	import com.bpi.citrusas.ui.draggableobject.DraggableObject;
	import com.bpi.citrusas.ui.draggableobject.events.Event_DraggableObject;
	import com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.popups.PopupManager;
	import com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.popups.Popup_IDs;
	import com.greensock.TweenLite;
	import com.greensock.easing.Power3;
	
	import flash.display.MovieClip;

	public class AddYourOwnSticky extends StickyNote
	{
		private var m_CurrentSticky:BasicSticky;
		
		public function AddYourOwnSticky()
		{
		}
		
		public override function Initialize():void
		{
			
		}
		
		public override function DeInitialize():void
		{
		}
		
		public function SetupAssets(skin:MovieClip):void
		{
			m_Skin = new DraggableObject(skin);
			RePositionAddStickyButton();
			
			super.Initialize();
		}
		
		protected override function StickyClicked(stickyNote:StickyNote):void
		{
			var NewSticky:BasicSticky = StickyNoteManager.Instance.AvailableStickyNotes[0];
			NewSticky.CurrentPopupID = Popup_IDs.SELECTION_POPUP;
			PopupManager.Instance.DisplayPopup(NewSticky);
		}
		
		protected override function eh_StickyNoteMoved(e:Event_DraggableObject):void
		{
			StickyNoteManager.Instance.SwapStickyNotes(m_CurrentSticky, StickyNoteManager.Instance.GetStickyNoteByPosition(StickyNoteManager.Instance.GetClosestPosition(e.currentPosition)));
			TweenLite.to(m_Skin.draggableObject, .5, {x: m_CurrentSticky.CurrentPosition.x, y: m_CurrentSticky.CurrentPosition.y, alpha: 1, ease: Power3.easeInOut});
		}
		
		public function RePositionAddStickyButton():void
		{
			var availableStickyNotes:Vector.<BasicSticky> = StickyNoteManager.Instance.AvailableStickyNotes;
			if(availableStickyNotes.length == 0)
			{
				super.DeInitialize();
				TweenLite.to(m_Skin.draggableObject, .5, {alpha: 0, ease: Power3.easeOut, onComplete: removeStickyNote});
			}
			else
			{
				super.Initialize();
				
				m_Skin.draggableObject.visible = true;
				if(!m_CurrentSticky || m_CurrentSticky.IsActive)
				{
					m_CurrentSticky = availableStickyNotes[0];
				}
				TweenLite.to(m_Skin.draggableObject, .5, {x: m_CurrentSticky.CurrentPosition.x, y: m_CurrentSticky.CurrentPosition.y, alpha: 1, ease: Power3.easeInOut});
			}
		}
		
		private function removeStickyNote():void
		{
			m_Skin.draggableObject.visible = false;
		}
	}
}
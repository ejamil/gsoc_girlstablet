package com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.popups
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.citrusas.input.inputcontroller.InputController;
	import com.bpi.citrusas.ui.primitives.Primitive_Rectangle;
	import com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.stickynotes.BasicSticky;
	import com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.stickynotes.StickyNoteManager;
	import com.greensock.TweenLite;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.geom.Point;

	public class BrainStormPopup
	{
		private var m_CurrentStickyNote:BasicSticky;
		
		protected var m_CurrentPopupID:int;
		protected var m_ClearButton:MovieClip;
		protected var m_SaveButton:MovieClip;
		protected var m_CloseButton:MovieClip;
		
		protected var m_Skin:MovieClip;
		
		private var m_Background:Primitive_Rectangle;
		
		public function BrainStormPopup(skin:MovieClip)
		{
			m_Background = new Primitive_Rectangle(new Point(CitrusFramework.frameworkStageSize.x, CitrusFramework.frameworkStageSize.y), 0x000000);
			m_Background.alpha = .6;
			
			m_Skin = skin;
			m_CloseButton = skin.close;
			m_ClearButton = skin.clear;
			m_SaveButton = skin.save;
			
			m_Skin.visible = false;
			m_Background.visible = false;
		}

		public function Initialize():void
		{
			if(m_ClearButton) m_ClearButton.addEventListener(InputController.DOWN, eh_ClearButton_Pressed);
			if(m_SaveButton) m_SaveButton.addEventListener(InputController.DOWN, eh_SaveButton_Pressed);
			if(m_CloseButton) m_CloseButton.addEventListener(InputController.CLICK, eh_ClosePopup);
			
			CitrusFramework.contentLayer.addChild(m_Background);
			CitrusFramework.contentLayer.addChild(m_Skin);
		}
		
		public function DeInitialize():void
		{
			if(m_ClearButton) m_ClearButton.removeEventListener(InputController.DOWN, eh_ClearButton_Pressed);
			if(m_SaveButton) m_SaveButton.removeEventListener(InputController.DOWN, eh_SaveButton_Pressed);
			if(m_CloseButton) m_CloseButton.removeEventListener(InputController.CLICK, eh_ClosePopup);
			
			if(CitrusFramework.contentLayer.contains(m_Background)) CitrusFramework.contentLayer.removeChild(m_Background);
		}
		
		protected function eh_ClosePopup(e:Event):void
		{
			HidePopUp();
		}
		
		public function DisplayPopUp():void
		{
			m_Skin.visible = true;
			m_Background.visible = true;
			
			TweenLite.fromTo(m_Skin, .5, {scaleX: 1.3, scaleY: 1.3, alpha: 0}, {scaleX: 1, scaleY: 1, alpha: 1});
			TweenLite.fromTo(m_Background, .5, {alpha: 0}, {alpha: .6});
			
			if(m_SaveButton) m_SaveButton.mouseEnabled = true;
		}
		
		public function HidePopUp():void
		{
			if(m_SaveButton) m_SaveButton.mouseEnabled = false;
			TweenLite.to(m_Skin, .5, {scaleX: .4, scaleY: .4, alpha: 0, onComplete: removePopup});
			TweenLite.to(m_Background, .5, {alpha: 0, onComplete: removePopup});
		}
		
		private function removePopup():void
		{
			m_Skin.visible = false;
			m_Background.visible = false;
		}
		
		protected function eh_ClearButton_Pressed(e:Event):void
		{
			if(m_SaveButton) m_SaveButton.visible = false;
			StickyNoteManager.Instance.ClearStickyNote(this.CurrentStickyNote);
		}
		
		protected function eh_SaveButton_Pressed(e:Event):void
		{
			StickyNoteManager.Instance.SaveStickyNote(this.CurrentStickyNote, this);
			HidePopUp();
		}
		
		public function get CurrentStickyNote():BasicSticky
		{
			return m_CurrentStickyNote;
		}
		
		public function set CurrentStickyNote(sticky:BasicSticky):void
		{
			m_CurrentStickyNote = sticky;
		}
		
		public function get CurrentPopupID():int
		{
			return m_CurrentPopupID;
		}
	}
}
package com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.popups.popup_types.drawing_popup
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.citrusas.input.inputcontroller.InputController;
	import com.bpi.citrusas.utils.Utils_Image;
	
	import flash.display.Bitmap;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	import flash.events.TouchEvent;
	import flash.geom.Point;

	public class DrawingArea
	{
		private static const ms_DrawingSize:Number = 10;
		private static const ms_InstructionsOffset:Number = 200;
		
		public const m_EventDispatcher:EventDispatcher = new EventDispatcher();
		public const m_DrawEvent:String = "com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.popups.popup_types.drawing_popup.drawevent";
		
		private var m_Color:uint = 0x666666;
		
		private var m_Drawing:Shape;
		private var m_ExistingDrawing:Bitmap;
		
		private var m_Skin:MovieClip;
		private var m_DrawingInstructions:DrawingInstructions;
		
		private var m_MaxCoords:Point;
		private var m_MousePos:Point;
		
		private var m_ShouldResetOrigin:Boolean;
		
		public function DrawingArea(skin:MovieClip)
		{
			m_ExistingDrawing = new Bitmap();
			m_Drawing = new Shape();
			m_MaxCoords = new Point(skin.width*.5, skin.height*.5);
			m_DrawingInstructions = new DrawingInstructions();

			m_Skin = skin;
			
			m_Skin.addChild(m_DrawingInstructions);
			m_Skin.addChild(m_Drawing);
		}
		
		public function Initialize():void
		{
			m_DrawingInstructions.Initialize(m_Skin.width, m_Skin.height - ms_InstructionsOffset);
			m_DrawingInstructions.DisplayInstructions();
			
			if(m_Skin.container) m_Skin.container.visible = false;
			m_Skin.iconArea.visible = false;
			m_Skin.captionText.visible = false;
			
			m_Skin.addEventListener(InputController.DOWN, eh_MouseDown);
		}
		
		public function DeInitialize():void
		{
			if(m_Skin.container) m_Skin.container.visible = true;
			m_Skin.iconArea.visible = true;
			m_Skin.captionText.visible = true;
			m_Skin.removeEventListener(InputController.DOWN, eh_MouseDown);
			CitrusFramework.frameworkStage.removeEventListener(InputController.MOVE, eh_MouseMove);
			CitrusFramework.frameworkStage.removeEventListener(InputController.UP, eh_MouseUp);
		}
		
		public function Clear():void
		{
			m_Drawing.graphics.clear();
			if(m_Skin.contains(m_ExistingDrawing))
			{
				m_ExistingDrawing.bitmapData.dispose();
				m_Skin.removeChild(m_ExistingDrawing);
			}
		}
		
		private function eh_MouseDown(e:Event):void
		{
			m_DrawingInstructions.HideInstructions();
			
			m_ShouldResetOrigin = false;
			if(e is MouseEvent)
			{
				m_MousePos = m_Skin.globalToLocal(new Point((e as MouseEvent).stageX, (e as MouseEvent).stageY));
			}
			else
			{
				m_MousePos = m_Skin.globalToLocal(new Point((e as TouchEvent).stageX, (e as TouchEvent).stageY));
			}
			
			m_Drawing.graphics.moveTo(m_MousePos.x, m_MousePos.y);
			m_Drawing.graphics.lineStyle(ms_DrawingSize, m_Color);
			
			CitrusFramework.frameworkStage.addEventListener(InputController.MOVE, eh_MouseMove);
			CitrusFramework.frameworkStage.addEventListener(InputController.UP, eh_MouseUp);
		}
		
		private function eh_MouseMove(e:Event):void
		{
			var my:Number = m_Skin.mouseY;
			var mx:Number = m_Skin.mouseX;
			
			if(m_ShouldResetOrigin)
			{
				m_ShouldResetOrigin = false;
				m_Drawing.graphics.moveTo(mx, my);
			}
			
			if((mx > -m_MaxCoords.x) && (mx < m_MaxCoords.x) && (my > -m_MaxCoords.y) && (my < m_MaxCoords.y))
			{
				m_Drawing.graphics.lineTo(m_Drawing.mouseX, m_Drawing.mouseY);
			}
			else
			{
				m_ShouldResetOrigin = true;
			}
		}
		
		private function eh_MouseUp(e:Event):void
		{
			CitrusFramework.frameworkStage.removeEventListener(InputController.MOVE, eh_MouseMove);
			CitrusFramework.frameworkStage.removeEventListener(InputController.UP, eh_MouseUp);
			m_EventDispatcher.dispatchEvent(new Event(m_DrawEvent));
		}
		
		public function set Color(value:uint):void
		{
			m_Color = value;
		}
		
		public function get Drawing():Bitmap
		{
			return Utils_Image.ConvertDisplayObjectToBitmap(m_Skin);
		}
		
		public function set ExistingDrawing(drawing:Bitmap):void
		{
			Clear();
			
			m_ExistingDrawing = drawing;
			m_Skin.addChildAt(m_ExistingDrawing, 0);
			
			m_ExistingDrawing.scaleX = 1;
			m_ExistingDrawing.scaleY = 1;
			
			m_ExistingDrawing.x = -drawing.width * .5;
			m_ExistingDrawing.y = -drawing.height * .5;
		}
		
		public function HideInstructions(immediately:Boolean = false):void
		{
			if(immediately)
			{
				m_DrawingInstructions.HideInstructionsImmediately();
			}
			else
			{
				m_DrawingInstructions.HideInstructions();
			}
		}
	}
}
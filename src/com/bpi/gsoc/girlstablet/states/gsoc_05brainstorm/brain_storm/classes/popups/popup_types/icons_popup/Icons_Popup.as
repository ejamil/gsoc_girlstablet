package com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.popups.popup_types.icons_popup
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.citrusas.input.inputcontroller.InputController;
	import com.bpi.citrusas.sound.SoundHandler;
	import com.bpi.citrusas.ui.swipeablethings.SwipeableContent;
	import com.bpi.citrusas.ui.swipeablethings.SwipeableContent_NoVelocity;
	import com.bpi.citrusas.utils.Utils_Text;
	import com.bpi.gsoc.framework.constants.AgeColors;
	import com.bpi.gsoc.framework.constants.AgeConstants;
	import com.bpi.gsoc.framework.constants.AgeFontLinkages;
	import com.bpi.gsoc.framework.constants.GSOC_Info;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.sfx.GSOC_SFX;
	import com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.popups.BrainStormPopup;
	import com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.popups.Popup_IDs;
	import com.greensock.TweenLite;
	import com.greensock.easing.Power3;
	
	import flash.display.Bitmap;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TextEvent;
	import flash.events.TouchEvent;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.utils.getTimer;

	public class Icons_Popup extends BrainStormPopup
	{
		private static const CLICK_REGISTRATION_TIME:Number = 200;
		private static const MIN_DRAG_LENGTH:Number = 50;
		private static const DEFAULT_TEXT:String = "Add Caption Here";
		
		private var m_SkillIcons:Vector.<BrainStorm_Icon>;
		private var m_SwipeableContent:SwipeableContent_NoVelocity;
		private var m_CurrentIcon:BrainStorm_Icon;
		private var m_RightArrow:MovieClip;
		private var m_LeftArrow:MovieClip;
		
		private var m_Caption:MovieClip;
		private var m_TextField:TextField;
		
		private var m_ContentArea:MovieClip;
		private var m_ContentPosY:Point;
		
		private var m_ClickTimer:Number = 0;
		private var m_BackgroundClass:Class;
		
		private var m_MouseIconClickStartPosition:Point;
		
		public function Icons_Popup(skin:MovieClip)
		{
			m_SkillIcons = new Vector.<BrainStorm_Icon>();
			m_ContentArea = skin.contentArea;
			m_SwipeableContent = new SwipeableContent_NoVelocity(m_ContentArea, .5, 4);
			m_RightArrow = skin.RightArrow;
			m_LeftArrow = skin.LeftArrow;
			
			m_Caption = skin.addCaption;
			m_TextField = m_Caption.txt;
			m_TextField.maxChars = 25;
			
			if(SessionInformation_GirlsTablet.Age == AgeConstants.KEY_SENIORAMBASSADOR) Utils_Text.ConvertToEmbeddedFont(m_TextField, AgeFontLinkages.SA_BODYBOLD_FONT, false);
			else Utils_Text.ConvertToEmbeddedFont(m_TextField, GSOC_Info.BodyFont, false);
			
			m_ContentPosY = new Point(m_ContentArea.y, m_ContentArea.y + m_TextField.height);
			
			m_CurrentPopupID = Popup_IDs.ICONS_POPUP;
			super(skin);
		}
		
		public function AddSkillIcon(icon:Bitmap):void
		{
			icon.smoothing = true;
			var brainStormIcon:BrainStorm_Icon = new BrainStorm_Icon(icon, m_BackgroundClass);
			brainStormIcon.height = 260;
			brainStormIcon.scaleX = brainStormIcon.scaleY;
			m_SwipeableContent.AddItem(brainStormIcon, new Point(40,30));
			m_SkillIcons.push(brainStormIcon);
		}
		
		public override function DisplayPopUp():void
		{
			super.DisplayPopUp();
			
			reset();
			
			if(CurrentStickyNote.DisplayArea.CurrentIcon)
			{
				displayTextArea();
				
				m_CurrentIcon = CurrentStickyNote.DisplayArea.CurrentIcon;
				m_TextField.text = CurrentStickyNote.DisplayArea.CaptionText;
				if(m_SaveButton) m_SaveButton.visible = true;
			}
			else
			{
				if(m_SaveButton) m_SaveButton.visible = false;				
			}
			
			m_Skin.addEventListener(Event.ENTER_FRAME, eh_EnterFrame);
			
			m_RightArrow.addEventListener(InputController.DOWN, eh_ArrowPressed);
			m_LeftArrow.addEventListener(InputController.DOWN, eh_ArrowPressed);
			
			m_Skin.contentArea.addEventListener(InputController.DOWN, eh_Icon_Clicked);
			m_Skin.contentArea.addEventListener(InputController.UP, eh_Icon_Clicked);
			
			m_TextField.addEventListener(TextEvent.TEXT_INPUT, eh_TextfieldChanged);
			
			m_SwipeableContent.Initialize();
		}
		
		public override function HidePopUp():void
		{
			super.HidePopUp();
			
			m_Skin.removeEventListener(Event.ENTER_FRAME, eh_EnterFrame);
			
			m_RightArrow.removeEventListener(InputController.DOWN, eh_ArrowPressed);
			m_LeftArrow.removeEventListener(InputController.DOWN, eh_ArrowPressed);

			m_Skin.contentArea.removeEventListener(InputController.DOWN, eh_Icon_Clicked);
			m_Skin.contentArea.removeEventListener(InputController.UP, eh_Icon_Clicked);
			
			m_TextField.removeEventListener(TextEvent.TEXT_INPUT, eh_TextfieldChanged);
			
			m_SwipeableContent.DeInitialize();
		}
		
		private function eh_ArrowPressed(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_ARROWBUTTONS[SessionInformation_GirlsTablet.Age]);
			if(e.target.name == "RightArrow")
			{
				m_SwipeableContent.MoveTo(4);
			}
			else
			{
				m_SwipeableContent.MoveTo(-4);
				AgeColors
			}
		}
		
		public function eh_EnterFrame(e:Event):void
		{
			if(m_SwipeableContent.IsFarthestRightOrTop)
			{
				m_RightArrow.visible = false;
			}
			else
			{
				m_RightArrow.visible = true;
			}
			if(m_SwipeableContent.IsFarthestLeftOrBottom)
			{
				m_LeftArrow.visible = false;
			}
			else
			{
				m_LeftArrow.visible = true;
			}
		}
		
		public function eh_Icon_Clicked(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_CHOOSEICONS[SessionInformation_GirlsTablet.Age]);
			if(e.type == MouseEvent.MOUSE_DOWN || e.type == TouchEvent.TOUCH_BEGIN)
			{
				m_ClickTimer = getTimer();
				if(e is MouseEvent)
				{
					m_MouseIconClickStartPosition = new Point((e as MouseEvent).stageX, (e as MouseEvent).stageY);
				}
				else if(e is TouchEvent)
				{
					m_MouseIconClickStartPosition = new Point((e as TouchEvent).stageX, (e as TouchEvent).stageY);
				}
			}
			else
			{
				var endMousePoint:Point = new Point();
				if(e is MouseEvent)
				{
					endMousePoint = new Point((e as MouseEvent).stageX, (e as MouseEvent).stageY);
				}
				else if(e is TouchEvent)
				{
					endMousePoint = new Point((e as TouchEvent).stageX, (e as TouchEvent).stageY);
				}
				
				if((getTimer() - m_ClickTimer) < CLICK_REGISTRATION_TIME && e.target is BrainStorm_Icon)
				{
					deselectIcon(false);
					m_CurrentIcon = (e.target as BrainStorm_Icon);
					selectIcon();
				}
				else if(e.target is BrainStorm_Icon && Point.distance(m_MouseIconClickStartPosition, endMousePoint) < MIN_DRAG_LENGTH)
				{
					deselectIcon(false);
					m_CurrentIcon = (e.target as BrainStorm_Icon);
					selectIcon();
				}
			}
			if(m_SaveButton) m_SaveButton.visible = true;
		}	
		
		private function deselectIcon(animate:Boolean = true):void
		{
			if(animate) hideTextArea();
			
			if(m_CurrentIcon)
			{
				m_CurrentIcon.DeSelect();
			}
		}
		
		private function eh_TextfieldChanged(e:Event):void
		{
			if(m_TextField.text == DEFAULT_TEXT)
			{
				m_TextField.text = "";
			}
		}
		
		private function displayTextArea():void
		{
			m_TextField.visible = true;
			m_TextField.text = DEFAULT_TEXT;
			
			TweenLite.to(m_ContentArea, .5, {y: m_ContentPosY.x, ease:Power3.easeOut, onComplete: setTextfieldFocus});
			TweenLite.to(m_Caption, .5, {alpha: 1});
		}
		
		private function setTextfieldFocus():void
		{
			CitrusFramework.frameworkStage.focus = m_TextField;
		}
		
		private function hideTextArea():void
		{
			TweenLite.to(m_ContentArea, .5, {y: m_ContentPosY.y, ease:Power3.easeOut});
			TweenLite.to(m_Caption, .5, {alpha: 0, onComplete: removeTextfield});
			
			CitrusFramework.frameworkStage.focus = null;
		}
		
		private function removeTextfield():void
		{
			m_TextField.visible = false;
		}
		
		private function selectIcon():void
		{
			m_CurrentIcon.Select();
			displayTextArea();
		}
		
		private function reset():void
		{
			deselectIcon();
			m_TextField.text = "";
			m_CurrentIcon = null;
		}
		
		protected override function eh_ClearButton_Pressed(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_CLICKS[SessionInformation_GirlsTablet.Age]);
			reset();
			super.eh_ClearButton_Pressed(e);
		}
		
		protected override function eh_SaveButton_Pressed(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_SAVEBUTTONS[SessionInformation_GirlsTablet.Age]);
			super.eh_SaveButton_Pressed(e);
		}
		
		public function set Background(background:Class):void
		{
			m_BackgroundClass = background;
		}
		
		public function get Icon():BrainStorm_Icon
		{
			if(m_CurrentIcon)
			{
				return m_CurrentIcon;
			}
			return null;
		}
		
		public function get Text():String
		{
			if(m_TextField.text == DEFAULT_TEXT)
			{
				return "";
			}
			return m_TextField.text;
		}
	}
}
package com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.gsoc.framework.constants.AgeConstants;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.gsocui.keyboards.GSOC_Keyboards;
	import com.bpi.gsoc.framework.states.app.GSOC_AppState;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_BrainStorm;

	public class BrainStorm_State  extends GSOC_AppState
	{
		private var m_AppState:BrainStorm_App;
		
		public function BrainStorm_State()
		{
			super(StateController_StateIDs_BrainStorm.STATE_BRAINSTORM);
		}
		
		protected override function Init():void
		{
			switch(SessionInformation_GirlsTablet.Age)
			{
				case AgeConstants.KEY_DAISYBROWNIE:
					m_AppState = new BrainStorm_App_DB();
					break;
				case AgeConstants.KEY_JUNIORCADETTE:
					m_AppState = new BrainStorm_App_JC();
					break;
				case AgeConstants.KEY_SENIORAMBASSADOR:
					m_AppState = new BrainStorm_App_SA();
					break;
			}
			
			GSOC_Keyboards.SetKeyboardType(GSOC_Keyboards.KEYBOARDTYPE_REGULAR);
			CitrusFramework.contentLayer.addChild(m_AppState);
			
			m_AppState.Initialize();
			m_AppState.Init()
			m_AppState.Setup();
		}
		
		protected override function DeInit():void
		{	
			m_AppState.DeInitialize();
			CitrusFramework.contentLayer.removeChild(m_AppState);
			m_AppState = null;
			
			super.EndDeInit();
		}
	}
}
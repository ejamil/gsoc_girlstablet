package com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.popups.popup_types.tips_popup
{
	import com.bpi.citrusas.input.inputcontroller.InputController;
	import com.bpi.citrusas.sound.SoundHandler;
	import com.bpi.citrusas.ui.swipeablethings.SwipeableContent;
	import com.bpi.citrusas.utils.Utils_Text;
	import com.bpi.gsoc.framework.constants.AgeConstants;
	import com.bpi.gsoc.framework.constants.AgeFontLinkages;
	import com.bpi.gsoc.framework.constants.GSOC_Info;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.sfx.GSOC_SFX;
	import com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.popups.BrainStormPopup;
	import com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.popups.Popup_IDs;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.geom.Point;
	import flash.text.TextField;

	public class Tips_Popup extends BrainStormPopup
	{
		private var m_Tip_Texts:Vector.<String> = new <String>[
			"Tip 1: \nA simple search engine can be very helpful. Try to find credible sources, like news sites and online articles. Stay away from biased stories, like blogs.", 
			"Tip 2: \nLook up more information on your root cause. There may be other issues that have this same cause.",
			"Tip 3: \nFind out if  this is something your community has faced in the past, and what others may have done to help. Think about why these solutions may not have worked."];
		
		private var m_Textfields:Vector.<TextField>;
		
		private var m_ContentArea:MovieClip;
		private var m_SwipeableContent:SwipeableContent;
		
		public function Tips_Popup(skin:MovieClip)
		{
			m_CurrentPopupID = Popup_IDs.TIPS_POPUP;
			super(skin);
			
			m_ContentArea = m_Skin.contentArea;
			
			m_SwipeableContent = new SwipeableContent(m_ContentArea, .5, 1, 500);
			m_Textfields = new Vector.<TextField>();
			
			for(var i:int = 0 ; i < 3; i++)
			{
				var textfield:TextField = new TextField();
				
				if(SessionInformation_GirlsTablet.Age == AgeConstants.KEY_SENIORAMBASSADOR) Utils_Text.EmbedText(textfield, AgeFontLinkages.SA_BODYBOLD_FONT, 48, GSOC_Info.ColorOne);
				else Utils_Text.EmbedText(textfield, GSOC_Info.HeaderFont, 48, GSOC_Info.ColorOne);
				
				textfield.width = m_ContentArea.scrollRect.width - 50;
				textfield.height = m_ContentArea.scrollRect.height;
				textfield.text = m_Tip_Texts[i];
				textfield.multiline = true;
				textfield.wordWrap = true;
				textfield.selectable = false;
				Utils_Text.FitTextToTextfield(textfield, 4);
				
				m_SwipeableContent.AddItem(textfield, new Point(30,60));
			}
		}
		
		public override function DisplayPopUp():void
		{
			m_Skin.RightArrow.addEventListener(InputController.DOWN, eh_ArrowPressed);
			m_Skin.LeftArrow.addEventListener(InputController.DOWN, eh_ArrowPressed);
			m_SwipeableContent.Initialize();
			
			m_Skin.addEventListener(Event.ENTER_FRAME, eh_Update);
			
			super.DisplayPopUp();
		}
		
		public override function HidePopUp():void
		{
			m_Skin.RightArrow.removeEventListener(InputController.DOWN, eh_ArrowPressed);
			m_Skin.LeftArrow.removeEventListener(InputController.DOWN, eh_ArrowPressed);
			m_SwipeableContent.DeInitialize();
			
			m_Skin.removeEventListener(Event.ENTER_FRAME, eh_Update);
			
			super.HidePopUp();
		}
		
		private function eh_Update(e:Event):void
		{
			if(m_SwipeableContent.IsFarthestLeftOrBottom)
			{
				m_Skin.LeftArrow.visible = false;
			}
			else
			{
				m_Skin.LeftArrow.visible = true;
			}
			
			if(m_SwipeableContent.IsFarthestRightOrTop)
			{
				m_Skin.RightArrow.visible = false;
			}
			else
			{
				m_Skin.RightArrow.visible = true;
			}
			
		}
		
		private function eh_ArrowPressed(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_ARROWBUTTONS[SessionInformation_GirlsTablet.Age]);
			if(e.target.name == "RightArrow")
			{
				m_SwipeableContent.MoveTo(1);
			}
			else
			{
				m_SwipeableContent.MoveTo(-1);
			}
		}
		
		protected override function eh_ClearButton_Pressed(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_CLICKS[SessionInformation_GirlsTablet.Age]);
			super.eh_ClearButton_Pressed(e);
		}
		
		protected override function eh_SaveButton_Pressed(e:Event):void
		{	
			SoundHandler.PlaySound(GSOC_SFX.SOUND_SAVEBUTTONS[SessionInformation_GirlsTablet.Age]);
			super.eh_SaveButton_Pressed(e);
		}
	}
}
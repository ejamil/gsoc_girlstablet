package com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm
{
	import com.bpi.gsoc.components.brainstorm.BrainStorm_DB_Skin;
	import com.bpi.gsoc.components.brainstorm.IconBox_DB_Skin;
	import com.bpi.gsoc.components.global.binocularsSkill;
	import com.bpi.gsoc.components.global.booksSkill;
	import com.bpi.gsoc.components.global.clipboardSkill;
	import com.bpi.gsoc.components.global.compassSkill;
	import com.bpi.gsoc.components.global.computerSkill;
	import com.bpi.gsoc.components.global.magnifyingGlassSkill;
	import com.bpi.gsoc.components.global.microscopeSkill;
	import com.bpi.gsoc.components.global.plantSkill;
	import com.bpi.gsoc.components.global.speechSkill;
	import com.bpi.gsoc.components.global.thumbsupSkill;
	import com.bpi.gsoc.components.global.weatherSkill;
	import com.bpi.gsoc.components.global.writingSkill;
	import com.bpi.gsoc.framework.constants.GSOC_Info;
	import com.bpi.gsoc.framework.gsocui.GSOC_Header;
	import com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.popups.popup_types.drawing_popup.Drawing_Popup;
	import com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.popups.popup_types.icons_popup.Icons_Popup;
	import com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.popups.popup_types.selections_popup.BasicSelection_Popup;
	import com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.popups.popup_types.typing_popup.Typing_Popup;
	import com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.stickynotes.BasicSticky;
	
	import flash.display.Bitmap;

	public class BrainStorm_App_DB extends BrainStorm_App
	{
		private var m_TypedSkin:BrainStorm_DB_Skin;
		
		public function BrainStorm_App_DB()
		{
			m_Skin = new BrainStorm_DB_Skin();
			m_TypedSkin = (m_Skin as BrainStorm_DB_Skin);
		}
		
		public override function Initialize():void
		{
			var iconsPopup:Icons_Popup = new Icons_Popup(m_TypedSkin.Icons_Popup);
			m_PopupManager.AddPopup(new BasicSelection_Popup(m_TypedSkin.Selection_Popup));
			m_PopupManager.AddPopup(new Drawing_Popup(m_TypedSkin.Drawing_Popup));
			m_PopupManager.AddPopup(iconsPopup);
			m_PopupManager.AddPopup(new Typing_Popup(m_TypedSkin.Typing_Popup));
			
			m_SubmitButton = m_TypedSkin.submit;
			m_IssueCause = m_TypedSkin.issuecause;
			
			iconsPopup.Background = IconBox_DB_Skin;
			iconsPopup.AddSkillIcon(new Bitmap(new binocularsSkill()));
			iconsPopup.AddSkillIcon(new Bitmap(new booksSkill()));
			iconsPopup.AddSkillIcon(new Bitmap(new clipboardSkill()));
			iconsPopup.AddSkillIcon(new Bitmap(new compassSkill()));
			iconsPopup.AddSkillIcon(new Bitmap(new computerSkill()));
			iconsPopup.AddSkillIcon(new Bitmap(new magnifyingGlassSkill()));
			iconsPopup.AddSkillIcon(new Bitmap(new microscopeSkill()));
			iconsPopup.AddSkillIcon(new Bitmap(new plantSkill()));
			iconsPopup.AddSkillIcon(new Bitmap(new speechSkill()));
			iconsPopup.AddSkillIcon(new Bitmap(new weatherSkill()));
			iconsPopup.AddSkillIcon(new Bitmap(new writingSkill()));
			iconsPopup.AddSkillIcon(new Bitmap(new thumbsupSkill()));
			
			var displayColor:uint = GSOC_Info.ColorTwo;
			m_StickyNoteManager.AddStickyNote(new BasicSticky(m_TypedSkin.stickynote1, displayColor));
			m_StickyNoteManager.AddStickyNote(new BasicSticky(m_TypedSkin.stickynote2, displayColor));
			m_StickyNoteManager.AddStickyNote(new BasicSticky(m_TypedSkin.stickynote3, displayColor));
			m_StickyNoteManager.AddStickyNote(new BasicSticky(m_TypedSkin.stickynote4, displayColor));
			m_StickyNoteManager.AddStickyNote(new BasicSticky(m_TypedSkin.stickynote5, displayColor));
			m_StickyNoteManager.AddStickyNote(new BasicSticky(m_TypedSkin.stickynote6, displayColor));
			
			m_StickyNoteManager.SetupAssets(m_TypedSkin.addyourown, m_TypedSkin.garbagebin);
			
			super.Initialize();
			
			//this.addChild(new HeaderArrows(2000, -80));
			
			m_GSOCHeader = new GSOC_Header(m_Skin.header.paginationArea);
			//GSOCheader.addPagination(new Pagination_SA(), AgeFontLinkages.SA_HEADER_FONT);
			GSOCheader.addArrows(m_Skin.header.paginationArea.x, 0);
			this.addChild(m_GSOCHeader);
		}
	}
}
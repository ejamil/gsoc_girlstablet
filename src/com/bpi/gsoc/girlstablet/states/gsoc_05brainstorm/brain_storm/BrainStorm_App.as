package com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm
{
	import com.bpi.citrusas.images.PlaceImage;
	import com.bpi.citrusas.input.inputcontroller.InputController;
	import com.bpi.citrusas.sound.SoundHandler;
	import com.bpi.citrusas.states.StateControllerManager;
	import com.bpi.gsoc.framework.constants.ButtonPlacement;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.data.userdata.GSOC_UserData_Team_ImageKeys;
	import com.bpi.gsoc.framework.gsocui.GSOC_ButtonToggle;
	import com.bpi.gsoc.framework.gsocui.STEM_Popups;
	import com.bpi.gsoc.framework.gsocui.SkillMap.Events.Event_MapStatus;
	import com.bpi.gsoc.framework.sfx.GSOC_SFX;
	import com.bpi.gsoc.framework.states.app.GSOC_AppSkin;
	import com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.popups.PopupManager;
	import com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.stickynotes.StickyNoteManager;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_IDs;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_Main;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_ProjectPlanning;
	
	import flash.display.Bitmap;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;

	public class BrainStorm_App extends GSOC_AppSkin
	{
		private static const ms_Dispatcher:IEventDispatcher = new EventDispatcher();
		private static var ms_Instance:BrainStorm_App;
		
		protected var m_PopupManager:PopupManager;
		protected var m_StickyNoteManager:StickyNoteManager;
		
		protected var m_IssueCause:MovieClip;
		protected var m_SubmitButton:MovieClip;

		public function BrainStorm_App()
		{
			if(!ms_Instance) ms_Instance = this;
			m_PopupManager = new PopupManager();
			m_StickyNoteManager = new StickyNoteManager();
		}
		
		public override function Initialize():void
		{
			super.Initialize();

			m_PopupManager.Initialize();
			m_StickyNoteManager.Initialize();
			Dispatcher.addEventListener(Event_MapStatus.MAP_CHANGED, eh_BoardChanged);
			m_SubmitButton.removeEventListener(InputController.CLICK, this.eh_NextGirlButtonClicked);
		}
		
		public override function DeInitialize():void
		{
			m_PopupManager.DeInitialize();
			m_StickyNoteManager.DeInitialize();
			m_PopupManager = null;
			m_StickyNoteManager = null;
			ms_Instance = null;
			
			Dispatcher.removeEventListener(Event_MapStatus.MAP_CHANGED, eh_BoardChanged);
			m_SubmitButton.removeEventListener(InputController.CLICK, this.eh_NextGirlButtonClicked);
		}
		
		public function Setup():void
		{
			GSOC_ButtonToggle.ToggleButton(false, m_SubmitButton, this.activateNextGirlButton, this.deactivateNextGirlButton);
			m_SubmitButton.x = ButtonPlacement.SUBMIT_X;
			
			var causeIcon:Sprite = SessionInformation_GirlsTablet.TeamInfo.Team_UserData.GetImage(GSOC_UserData_Team_ImageKeys.KEY_MINDMAP_ROOTCAUSE);
			var issueIcon:Sprite = SessionInformation_GirlsTablet.TeamInfo.Team_UserData.GetImage(GSOC_UserData_Team_ImageKeys.KEY_CHOOSEYOURISSUE_ISSUE);
			
			PlaceImage.ImageToExistingContainer(issueIcon, m_IssueCause.issue, false, true);
			PlaceImage.ImageToExistingContainer(causeIcon, m_IssueCause.cause, false, true);
		}
			
		protected function eh_BoardChanged(e:Event_MapStatus):void
		{
			if(e.m_IsComplete)
			{
				STEM_Popups.DisplayPopup(STEM_Popups.BRAINSTORM);
				GSOC_ButtonToggle.ToggleButton(true, m_SubmitButton, this.activateNextGirlButton, this.deactivateNextGirlButton);
			}
			else
			{
				GSOC_ButtonToggle.ToggleButton(false, m_SubmitButton, this.activateNextGirlButton, this.deactivateNextGirlButton);
			}
		}
		
		private function activateNextGirlButton(e:Event = null):void
		{
			m_SubmitButton.addEventListener(InputController.CLICK, this.eh_NextGirlButtonClicked);
		}
		
		private function deactivateNextGirlButton(e:Event = null):void
		{
			m_SubmitButton.removeEventListener(InputController.CLICK, this.eh_NextGirlButtonClicked);
		}
		
		private function eh_NextGirlButtonClicked(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_CLICKS[SessionInformation_GirlsTablet.Age]);
			if(SessionInformation_GirlsTablet.IsRunningLocally)
			{
				StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_PROJECTPLANNING, StateController_StateIDs_ProjectPlanning.STATE_MAIN);
			}
			else
			{
				submitActivity();
			}
		}
		
		private function submitActivity():void
		{
			StateControllerManager.LeaveAllActiveStates();
			StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_MAIN, StateController_StateIDs_Main.STATE_SESSIONATTRACT);
			Document.TellServerActivityComplete();
		}
		
		public static function get Instance():BrainStorm_App
		{
			return ms_Instance;
		}
		
		public static function get Dispatcher():IEventDispatcher
		{
			return ms_Dispatcher;
		}
	}
}
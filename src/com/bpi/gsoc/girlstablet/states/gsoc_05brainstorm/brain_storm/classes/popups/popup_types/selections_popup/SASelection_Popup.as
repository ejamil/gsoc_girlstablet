package com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.popups.popup_types.selections_popup
{
	import com.bpi.citrusas.input.inputcontroller.InputController;
	import com.bpi.citrusas.sound.SoundHandler;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.sfx.GSOC_SFX;
	import com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.popups.PopupManager;
	import com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.popups.Popup_IDs;
	
	import flash.display.MovieClip;
	import flash.events.Event;

	public class  SASelection_Popup extends BasicSelection_Popup
	{
		private var m_TipsButton:MovieClip;
		
		public function SASelection_Popup(skin:MovieClip)
		{
			m_TipsButton = skin.tips;
			
			m_CurrentPopupID = Popup_IDs.SELECTION_POPUP;
			super(skin);
		}
		
		public override function DisplayPopUp():void
		{
			m_TipsButton.addEventListener(InputController.CLICK, eh_TipsButtonClicked);
			
			super.DisplayPopUp();
		}
		
		public override function HidePopUp():void
		{
			m_TipsButton.addEventListener(InputController.CLICK, eh_TipsButtonClicked);
			
			super.HidePopUp();
		}
		
		private function eh_TipsButtonClicked(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_CLICKS[SessionInformation_GirlsTablet.Age]);
			CurrentStickyNote.CurrentPopupID = Popup_IDs.TIPS_POPUP;
			PopupManager.Instance.DisplayPopup(CurrentStickyNote);
		}
	}
}
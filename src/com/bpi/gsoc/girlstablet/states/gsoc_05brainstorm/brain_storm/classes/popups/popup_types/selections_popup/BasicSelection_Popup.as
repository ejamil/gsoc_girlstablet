package com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.popups.popup_types.selections_popup
{
	import com.bpi.citrusas.input.inputcontroller.InputController;
	import com.bpi.citrusas.sound.SoundHandler;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.sfx.GSOC_SFX;
	import com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.popups.BrainStormPopup;
	import com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.popups.PopupManager;
	import com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.popups.Popup_IDs;
	
	import flash.display.MovieClip;
	import flash.events.Event;

	public class BasicSelection_Popup extends BrainStormPopup
	{
		private var m_TypeButton:MovieClip;
		private var m_DrawButton:MovieClip;
		private var m_IconsButton:MovieClip;
		
		public function BasicSelection_Popup(skin:MovieClip)
		{
			m_TypeButton = skin.type;
			m_DrawButton = skin.draw;
			m_IconsButton = skin.icons;
			
			m_CurrentPopupID = Popup_IDs.SELECTION_POPUP;
			super(skin);
		}
		
		public override function DisplayPopUp():void
		{
			initializeListeners();
			super.DisplayPopUp();
		}
		
		public override function HidePopUp():void
		{
			deInitializeListeners();
			super.HidePopUp();
		}
		
		private function initializeListeners():void
		{
			m_TypeButton.addEventListener(InputController.CLICK, eh_TypeButtonClicked);
			m_DrawButton.addEventListener(InputController.CLICK, eh_DrawButtonClicked);
			m_IconsButton.addEventListener(InputController.CLICK, eh_IconsButtonClicked);
		}
		
		private function deInitializeListeners():void
		{
			m_TypeButton.removeEventListener(InputController.CLICK, eh_TypeButtonClicked);
			m_DrawButton.removeEventListener(InputController.CLICK, eh_DrawButtonClicked);
			m_IconsButton.removeEventListener(InputController.CLICK, eh_IconsButtonClicked);
		}
		
		private function eh_TypeButtonClicked(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_CLICKS[SessionInformation_GirlsTablet.Age]);
			CurrentStickyNote.CurrentPopupID = Popup_IDs.TYPING_POPUP;
			PopupManager.Instance.DisplayPopup(CurrentStickyNote);
		}
		
		private function eh_DrawButtonClicked(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_CLICKS[SessionInformation_GirlsTablet.Age]);
			CurrentStickyNote.CurrentPopupID = Popup_IDs.DRAWING_POPUP;
			PopupManager.Instance.DisplayPopup(CurrentStickyNote);
		}
		
		private function eh_IconsButtonClicked(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_CHOOSEICONS[SessionInformation_GirlsTablet.Age]);
			CurrentStickyNote.CurrentPopupID = Popup_IDs.ICONS_POPUP;
			PopupManager.Instance.DisplayPopup(CurrentStickyNote);
		}
	}
}
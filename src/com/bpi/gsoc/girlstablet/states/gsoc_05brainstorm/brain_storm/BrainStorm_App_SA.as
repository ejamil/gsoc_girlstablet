package com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm
{
	import com.bpi.citrusas.input.inputcontroller.InputController;
	import com.bpi.citrusas.sound.SoundHandler;
	import com.bpi.citrusas.web.WebView;
	import com.bpi.gsoc.components.brainstorm.BrainStorm_SA_Skin;
	import com.bpi.gsoc.components.brainstorm.IconBox_SA_Skin;
	import com.bpi.gsoc.components.global.balanceSkill;
	import com.bpi.gsoc.components.global.booksSkill;
	import com.bpi.gsoc.components.global.broomSkill;
	import com.bpi.gsoc.components.global.computerSkill;
	import com.bpi.gsoc.components.global.drawingtoolSkill;
	import com.bpi.gsoc.components.global.earSkill;
	import com.bpi.gsoc.components.global.handshakeSkill;
	import com.bpi.gsoc.components.global.heartpulseSkill;
	import com.bpi.gsoc.components.global.lightbulbSkill;
	import com.bpi.gsoc.components.global.magnifyingGlassSkill;
	import com.bpi.gsoc.components.global.microphoneSkill;
	import com.bpi.gsoc.components.global.quotesSkill;
	import com.bpi.gsoc.framework.constants.AgeColors;
	import com.bpi.gsoc.framework.gsocui.GSOC_Header;
	import com.bpi.gsoc.framework.gsocui.GSOC_WebView;
	import com.bpi.gsoc.framework.sfx.GSOC_SFX;
	import com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.popups.popup_types.drawing_popup.Drawing_Popup;
	import com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.popups.popup_types.icons_popup.Icons_Popup;
	import com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.popups.popup_types.selections_popup.SASelection_Popup;
	import com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.popups.popup_types.tips_popup.Tips_Popup;
	import com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.popups.popup_types.typing_popup.Typing_Popup;
	import com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.stickynotes.BasicSticky;
	
	import flash.display.Bitmap;
	import flash.events.Event;

	public class BrainStorm_App_SA extends BrainStorm_App
	{
		private var m_TypedSkin:BrainStorm_SA_Skin;
		
		private var m_WebView:WebView;
		
		public function BrainStorm_App_SA()
		{
			m_Skin = new BrainStorm_SA_Skin();
			m_TypedSkin = (m_Skin as BrainStorm_SA_Skin);
			m_WebView = new GSOC_WebView();
		}
		
		public override function Initialize():void
		{
			m_SubmitButton = m_TypedSkin.submit;
			m_IssueCause = m_TypedSkin.issuecause;
			
			var iconsPopup:Icons_Popup = new Icons_Popup(m_TypedSkin.Icons_Popup);
			m_PopupManager.AddPopup(new SASelection_Popup(m_TypedSkin.Selection_Popup));
			m_PopupManager.AddPopup(new Drawing_Popup(m_TypedSkin.Draw_Popup));
			m_PopupManager.AddPopup(iconsPopup);
			m_PopupManager.AddPopup(new Typing_Popup(m_TypedSkin.Type_Popup));
			m_PopupManager.AddPopup(new Tips_Popup(m_TypedSkin.Tip_Popup));
			
			iconsPopup.Background = IconBox_SA_Skin;
			iconsPopup.AddSkillIcon(new Bitmap(new balanceSkill()));
			iconsPopup.AddSkillIcon(new Bitmap(new booksSkill()));
			iconsPopup.AddSkillIcon(new Bitmap(new broomSkill()));
			iconsPopup.AddSkillIcon(new Bitmap(new computerSkill()));
			iconsPopup.AddSkillIcon(new Bitmap(new drawingtoolSkill()));
			iconsPopup.AddSkillIcon(new Bitmap(new earSkill()));
			iconsPopup.AddSkillIcon(new Bitmap(new handshakeSkill()));
			iconsPopup.AddSkillIcon(new Bitmap(new heartpulseSkill()));
			iconsPopup.AddSkillIcon(new Bitmap(new lightbulbSkill()));
			iconsPopup.AddSkillIcon(new Bitmap(new magnifyingGlassSkill()));
			iconsPopup.AddSkillIcon(new Bitmap(new microphoneSkill()));
			iconsPopup.AddSkillIcon(new Bitmap(new quotesSkill()));
			
			var displayColor:uint = AgeColors.SA_PURPLE;
			m_StickyNoteManager.AddStickyNote(new BasicSticky(m_TypedSkin.stickynote1, displayColor));
			m_StickyNoteManager.AddStickyNote(new BasicSticky(m_TypedSkin.stickynote2, displayColor));
			m_StickyNoteManager.AddStickyNote(new BasicSticky(m_TypedSkin.stickynote3, displayColor));
			m_StickyNoteManager.AddStickyNote(new BasicSticky(m_TypedSkin.stickynote4, displayColor));
			m_StickyNoteManager.AddStickyNote(new BasicSticky(m_TypedSkin.stickynote5, displayColor));
			m_StickyNoteManager.AddStickyNote(new BasicSticky(m_TypedSkin.stickynote6, displayColor));
			
			m_StickyNoteManager.SetupAssets(m_TypedSkin.addyourown, m_TypedSkin.garbagebin);
			
			m_Skin.header.mouseEnabled = false;
			m_Skin.header.mouseChildren = false;
			
			m_Skin.launchweb.addEventListener(InputController.CLICK, launchWebWindow);
			
			super.Initialize();
			
			m_GSOCHeader = new GSOC_Header(m_Skin.header.paginationArea);

			GSOCheader.addArrows(m_Skin.header.paginationArea.x, 0);
			this.addChild(m_GSOCHeader);
		}
		
		public override function DeInitialize():void
		{
			m_WebView.DeInitialize();
			super.DeInitialize();
		}
		
		private function launchWebWindow(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_LAUNCHWEB);
			m_WebView.Initialize();
		}
	}
}
package com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.stickynotes
{
	import com.bpi.citrusas.ui.draggableobject.DraggableObject;
	import com.greensock.TweenMax;
	
	import flash.display.DisplayObject;
	import flash.events.Event;

	public class DraggableSticky extends DraggableObject
	{
		public function DraggableSticky(skin:DisplayObject)
		{
			super(skin);
		}
		
		protected override function eh_StartDrag(e:Event):void
		{
			TweenMax.killTweensOf(m_DraggableObject);
			super.eh_StartDrag(e);
		}
	}
}
package com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.popups.popup_types.typing_popup
{
	import com.adobe.utils.StringUtil;
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.citrusas.sound.SoundHandler;
	import com.bpi.citrusas.utils.Utils_Text;
	import com.bpi.gsoc.framework.constants.AgeConstants;
	import com.bpi.gsoc.framework.constants.AgeFontLinkages;
	import com.bpi.gsoc.framework.constants.GSOC_Info;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.sfx.GSOC_SFX;
	import com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.popups.BrainStormPopup;
	import com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.popups.Popup_IDs;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.text.TextField;

	public class Typing_Popup extends BrainStormPopup
	{
		public static const MAX_CHARACTER_LIMIT:int = 140;
		public static const DEFAULT_TEXT:String = "Start typing your message!";
		
		private var m_Textfield:TextField;
		
		public function Typing_Popup(skin:MovieClip)
		{
			m_Textfield = skin.textbox;
			if(SessionInformation_GirlsTablet.Age == AgeConstants.KEY_SENIORAMBASSADOR)
			{
				Utils_Text.EmbedText(m_Textfield, AgeFontLinkages.SA_BODYBOLD_FONT, 60, GSOC_Info.ColorTwo);
			}
			else 
			{
				Utils_Text.EmbedText(m_Textfield, GSOC_Info.BodyFont, 60, GSOC_Info.ColorTwo);
			}
			
			m_Textfield.multiline = true;
			m_Textfield.wordWrap = true;
			m_Textfield.maxChars = 160;
			
			m_CurrentPopupID = Popup_IDs.TYPING_POPUP;
			super(skin);
		}
		
		public override function DisplayPopUp():void
		{
			super.DisplayPopUp();
			
			m_Textfield.text = CurrentStickyNote.DisplayArea.CurrentText;
			
			CitrusFramework.frameworkStage.addEventListener(KeyboardEvent.KEY_DOWN, eh_KeyPressed);
			CitrusFramework.frameworkStage.addEventListener(KeyboardEvent.KEY_UP, eh_CheckIfEmpty);
			CitrusFramework.frameworkStage.focus = m_Textfield;
			
			if(m_Textfield.text.length > 0 && m_Textfield.text != DEFAULT_TEXT)
			{
				if(m_SaveButton) m_SaveButton.visible = true;
			}
			else
			{
				if(m_SaveButton) m_SaveButton.visible = false;				
			}
		}
		
		public override function HidePopUp():void
		{
			CitrusFramework.frameworkStage.removeEventListener(KeyboardEvent.KEY_DOWN, eh_KeyPressed);
			CitrusFramework.frameworkStage.removeEventListener(KeyboardEvent.KEY_UP, eh_CheckIfEmpty);
			CitrusFramework.frameworkStage.focus = null;
			
			m_Textfield.text = "";
			
			super.HidePopUp();
		}
		
		protected override function eh_ClearButton_Pressed(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_CLICKS[SessionInformation_GirlsTablet.Age]);
			m_Textfield.text = "";
			
			super.eh_ClearButton_Pressed(e);
		}
		
		protected override function eh_SaveButton_Pressed(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_SAVEBUTTONS[SessionInformation_GirlsTablet.Age]);
			checkText();
			super.eh_SaveButton_Pressed(e);
		}
		
		private function eh_KeyPressed(e:Event):void
		{
			checkText();	
		}
		
		private function checkText():void
		{
			if(m_Textfield.text == DEFAULT_TEXT)
			{
				m_Textfield.text = "";
			}
			this.checkEmpty();
		}
		
		private function eh_CheckIfEmpty(e:Event):void
		{
			this.checkEmpty();
		}
		
		private function checkEmpty():void
		{
			if(m_SaveButton)
			{
				m_SaveButton.visible = StringUtil.trim(m_Textfield.text);
			}
		}
		
		public function get Text():String
		{
			return m_Textfield.text;
		}
	}
}
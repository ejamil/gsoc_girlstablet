package com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.popups.popup_types.icons_popup
{
	import com.bpi.citrusas.images.PlaceImage;
	import com.bpi.gsoc.framework.constants.GSOC_Info;
	import com.greensock.TweenLite;
	import com.greensock.plugins.ColorTransformPlugin;
	import com.greensock.plugins.TweenPlugin;
	
	import flash.display.Bitmap;
	import flash.display.MovieClip;
	import flash.display.Sprite;

	public class BrainStorm_Icon extends Sprite
	{
		private var m_Background:MovieClip;
		private var m_Icon:Sprite;
		
		public function BrainStorm_Icon(icon:Bitmap, background:Class)
		{
			m_Icon = new Sprite();
			m_Background = new background();
			
			TweenPlugin.activate([ColorTransformPlugin]);
			TweenLite.to(m_Icon, 0, {colorTransform: {tint: GSOC_Info.ColorTwo}});
			
			m_Icon.addChild(icon);
			this.addChild(m_Background);
			PlaceImage.ImageToExistingContainer(m_Icon, m_Background.contentArea);
			
			DeSelect();
		}
		
		public function Select():void
		{
			m_Background.gotoAndStop(2);
		}
		
		public function DeSelect():void
		{
			m_Background.gotoAndStop(1);
		}
		
		public function get Icon():Sprite
		{
			return m_Icon;
		}
	}
}
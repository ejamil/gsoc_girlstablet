package com.bpi.gsoc.girlstablet.states.waitforactivation
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.citrusas.citrus.framework.layers.LayerIDs;
	import com.bpi.citrusas.states.AppState;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_Main;

	public class AppState_WaitForActivation extends AppState
	{
		private var m_App:App_WaitForActivation;
		
		public function AppState_WaitForActivation()
		{
			super(StateController_StateIDs_Main.STATE_WAITFORACTIVATION);
			
			m_App = new App_WaitForActivation();
		}
		
		public override function Initialize():void
		{
			m_App.Initialize();
			
			super.Initialize();
		}
		
		public override function CleanUp():void
		{
			m_App.CleanUp();
			
			super.CleanUp();
		}
		
		public override function Update():void
		{
			m_App.Update();
			
			super.Update();
		}
		
		protected override function Init():void
		{
			m_App.Init();
			
			CitrusFramework.layerController.GetLayerByID(LayerIDs.CONTENT).addChild(m_App);
			
			super.Init();
		}
		
		protected override function DeInit():void
		{
			m_App.DeInit();
			
			CitrusFramework.layerController.GetLayerByID(LayerIDs.CONTENT).removeChild(m_App);
			
			super.DeInit();
		}
		
		public override function QueuedUpdate():void
		{
			
		}
	}
}
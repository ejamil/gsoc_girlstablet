package com.bpi.gsoc.girlstablet.states.gsoc_02exploreyourpassion.multiple_questions
{
	import com.bpi.gsoc.components.exploreyourpassion.MultipleQuestions_DB_Skin;
	import com.bpi.gsoc.framework.constants.AgeFontLinkages;
	import com.bpi.gsoc.framework.embeddedcontent.EmbeddedFonts;
	import com.bpi.gsoc.framework.gsocui.GSOC_Header;

	public class MultipleQuestions_App_DB extends MultipleQuestions_App
	{
		public function MultipleQuestions_App_DB()
		{
			m_Skin = new MultipleQuestions_DB_Skin();
			m_GSOCHeader = new GSOC_Header(m_Skin.header.paginationArea);
			GSOCheader.addPagination();
			GSOCheader.addArrows(m_Skin.header.paginationArea.x, 0);
			
			m_HeaderFont = AgeFontLinkages.DB_HEADER_FONT;
			m_BodyFont = EmbeddedFonts.FontName_ZalderdashTTF;
			super.Initialize();
		}
	}
}
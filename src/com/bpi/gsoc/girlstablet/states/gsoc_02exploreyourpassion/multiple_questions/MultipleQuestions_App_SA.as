package com.bpi.gsoc.girlstablet.states.gsoc_02exploreyourpassion.multiple_questions
{
	import com.bpi.gsoc.components.exploreyourpassion.MultipleQuestions_SA_Skin;
	import com.bpi.gsoc.framework.constants.AgeFontLinkages;
	import com.bpi.gsoc.framework.gsocui.GSOC_Header;

	public class MultipleQuestions_App_SA extends MultipleQuestions_App
	{
		public function MultipleQuestions_App_SA()
		{
			m_Skin = new MultipleQuestions_SA_Skin();			
			m_GSOCHeader = new GSOC_Header(m_Skin.header.paginationArea);
			GSOCheader.addPagination();
			GSOCheader.addArrows(m_Skin.header.paginationArea.x, 0);
			
			m_HeaderFont = AgeFontLinkages.SA_HEADER_FONT;
			m_BodyFont = AgeFontLinkages.SA_BODY_FONT;
			super.Initialize();
		}
	}
}
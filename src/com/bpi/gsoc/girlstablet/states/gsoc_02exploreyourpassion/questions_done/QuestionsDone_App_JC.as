package com.bpi.gsoc.girlstablet.states.gsoc_02exploreyourpassion.questions_done
{
	import com.bpi.gsoc.components.exploreyourpassion.QuestionsDone_JC_Skin;
	import com.bpi.gsoc.framework.constants.AgeFontLinkages;

	public class QuestionsDone_App_JC extends QuestionsDone_App
	{
		public function QuestionsDone_App_JC()
		{
			m_Skin = new QuestionsDone_JC_Skin();
			//this.addChild(m_Skin);
			//m_Pagination = new Pagination(new Pagination_JC(), AgeFontLinkages.JC_HEADER_FONT);
			m_HeaderFont = AgeFontLinkages.JC_HEADER_FONT;
			m_BodyFont = AgeFontLinkages.JC_BODY_FONT;
			super.Initialize();
		}
	}
}
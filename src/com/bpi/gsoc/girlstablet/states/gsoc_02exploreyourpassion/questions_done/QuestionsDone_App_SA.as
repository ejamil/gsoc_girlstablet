package com.bpi.gsoc.girlstablet.states.gsoc_02exploreyourpassion.questions_done
{
	import com.bpi.gsoc.components.exploreyourpassion.QuestionsDone_SA_Skin;
	import com.bpi.gsoc.framework.constants.AgeFontLinkages;

	public class QuestionsDone_App_SA extends QuestionsDone_App
	{
		public function QuestionsDone_App_SA()
		{
			m_Skin = new QuestionsDone_SA_Skin();
			//this.addChild(m_Skin);
			//m_Pagination = new Pagination(new Pagination_SA(), AgeFontLinkages.SA_HEADER_FONT);
			m_HeaderFont = AgeFontLinkages.SA_HEADER_FONT;
			m_BodyFont = AgeFontLinkages.SA_BODY_FONT;
			super.Initialize();
		}
	}
}
package com.bpi.gsoc.girlstablet.states.gsoc_02exploreyourpassion.questions_done
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.citrusas.input.inputcontroller.InputController;
	import com.bpi.citrusas.sound.SoundHandler;
	import com.bpi.citrusas.states.StateControllerManager;
	import com.bpi.components.exploreyourpassion.Particle_CircleDB;
	import com.bpi.components.exploreyourpassion.Particle_CircleJC;
	import com.bpi.components.exploreyourpassion.Particle_CircleSA;
	import com.bpi.components.exploreyourpassion.Particle_StarDB;
	import com.bpi.components.exploreyourpassion.Particle_StarJC;
	import com.bpi.components.exploreyourpassion.Particle_StarSA;
	import com.bpi.components.exploreyourpassion.Particle_TriangleDB;
	import com.bpi.components.exploreyourpassion.Particle_TriangleJC;
	import com.bpi.components.exploreyourpassion.Particle_TriangleSA;
	import com.bpi.gsoc.framework.constants.AgeConstants;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.gsocui.keyboards.GSOC_Keyboards;
	import com.bpi.gsoc.framework.sfx.GSOC_SFX;
	import com.bpi.gsoc.framework.states.app.GSOC_AppState;
	import com.bpi.gsoc.girlstablet.global_data.FrameControlUtils;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_IDs;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_ChooseYourCareer;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_ExploreYourPassion;
	import com.greensock.TweenLite;
	import com.greensock.TweenMax;
	import com.greensock.easing.Back;
	import com.greensock.easing.Strong;
	
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.events.Event;
	import flash.filters.DropShadowFilter;
	import flash.geom.Point;

	public class QuestionsDone_State extends GSOC_AppState
	{
		private static const ms_ExplosionStart_Delay:Number = .5;
		
		private static const ms_Circle_Num:int = 10;
		private static const ms_Circle_AnimationDuration:Number = 1.2;
		private static const ms_Circle_Distance:Number = 550;
		private static const ms_Circle_DistanceVariance:Number = 350;
		private static const ms_Circle_Scale:Number = .4;
		private static const ms_Circle_ScaleVariance:Number = .4;
		private static const ms_Circle_AngleVariance:Number = 20;
		
		private static const ms_Triangle_Num:int = 22;
		private static const ms_Triangle_AnimationDuration:Number = 1.2;
		private static const ms_Triangle_Distance:Number = 100;
		private static const ms_Triangle_DistanceVariance:Number = 0;
		private static const ms_Triangle_ScaleX:Number = 2.2;
		private static const ms_Triangle_ScaleY:Number = 2.2;
		private static const ms_Triangle_ScaleXVariance:Number = .4;
		private static const ms_Triangle_ScaleYVariance:Number = .4;
		private static const ms_Triangle_AngleVariance:Number = 15;
		
		private static const ms_Star_Num:int = 8;
		private static const ms_Star_AnimationDuration:Number = 1.2;
		private static const ms_Star_Distance:Number = 550;
		private static const ms_Star_DistanceVariance:Number = 200;
		private static const ms_Star_Scale:Number = .4;
		private static const ms_Star_ScaleVariance:Number = .4;
		private static const ms_Star_AngleVariance:Number = 20;
		
		private var m_ExplosionContainer:MovieClip;
		private var m_TriangleContainer:MovieClip;
		private var m_CircleContainer:MovieClip;
		private var m_StarContainer:MovieClip;
		
		private var m_QuestionsDone_Group:Vector.<QuestionsDone_App>;
		private var m_AppState:QuestionsDone_App;
		
		public function QuestionsDone_State()
		{
			super(StateController_StateIDs_ExploreYourPassion.STATE_QUESTIONSDONE);
			
			m_TriangleContainer = new MovieClip();
			m_CircleContainer = new MovieClip();
			m_StarContainer = new MovieClip();
		}
		
		public override function Initialize():void
		{
		}
		
		protected override function Init():void
		{
			switch(SessionInformation_GirlsTablet.Age)
			{
				case 0:
					m_AppState = new QuestionsDone_App_DB();
					break;
				case 1:
					m_AppState = new QuestionsDone_App_JC();
					break;
				case 2:
					m_AppState = new QuestionsDone_App_SA();
					break;
				default:
					m_AppState = new QuestionsDone_App_DB();
					
			}
			
			m_AppState.Init();
			
			FrameControlUtils.StopAtFrameN(m_AppState);

			GSOC_Keyboards.SetKeyboardType(GSOC_Keyboards.KEYBOARDTYPE_UPPERCASE);
			CitrusFramework.contentLayer.addChild(m_AppState);
			
			setUpScreen(0);
			EndInit();
		}
		private function setUpScreen(startIndex:int):void
		{
			if (SessionInformation_GirlsTablet.TeamInfo.HasNextTeamMember())
			{
				m_AppState.skin.great_Job_Prompt.visible = false;
				m_AppState.skin.submitButton.visible = false;
				m_AppState.skin.next_Girl_Button.addEventListener(InputController.CLICK, nextGirl);
			}
			else
			{
				m_AppState.skin.doneText.visible = false;
				m_AppState.skin.next_Girl_Button.visible = false;
				m_AppState.skin.submitButton.visible = false;
				m_AppState.skin.great_Job_Prompt.alpha = 0;
				prepExplosionAnimation();
				
				if (SessionInformation_GirlsTablet.IsRunningLocally)
				{
					m_AppState.skin.submitButton.visible = true;
					m_AppState.skin.submitButton.addEventListener(InputController.CLICK, nextActivity);
				}
				else
				{
					nextActivity();
				}
			}
		}
		
		private function prepExplosionAnimation():void
		{
			m_ExplosionContainer = m_AppState.skin.great_Job_Prompt.explosionContainer;
			m_ExplosionContainer.alpha = 0;
			m_ExplosionContainer.filters = [new DropShadowFilter(40, 30, 0x000000, .4, 20, 20)];
			
			for(i = 0; i < ms_Triangle_Num; i++)
			{
				var triangleContainer:MovieClip = triangleMC;
				m_TriangleContainer.addChild(triangleContainer);
			}
			m_ExplosionContainer.addChild(m_TriangleContainer);
			
			for(var i:int = 0; i < ms_Circle_Num; i++)
			{
				var circle:Bitmap = circleBitmap;
				circle.smoothing = true;
				m_CircleContainer.addChild(circle);
			}
			m_ExplosionContainer.addChild(m_CircleContainer);
			
			for(i = 0; i < ms_Star_Num; i++)
			{
				var star:Bitmap = starBitmap;
				star.smoothing = true;
				m_StarContainer.addChild(star);
			}
			m_ExplosionContainer.addChild(m_StarContainer);
			
			TweenLite.to(m_CircleContainer, int.MAX_VALUE*.1, {rotation: int.MAX_VALUE});
			TweenLite.to(m_TriangleContainer, int.MAX_VALUE*.2, {rotation: int.MAX_VALUE});
			TweenLite.to(m_StarContainer, int.MAX_VALUE*.25, {rotation: int.MAX_VALUE});
			TweenLite.delayedCall(ms_ExplosionStart_Delay, startExplosionAnimation);
		}
		
		private function startExplosionAnimation():void
		{
			m_ExplosionContainer.alpha = 1;
			TweenLite.fromTo(m_AppState.skin.great_Job_Prompt, ms_Triangle_AnimationDuration, {scaleX: .1, scaleY: .1}, {scaleX: 1, scaleY: 1, alpha: 1, ease:Back.easeOut});
			
			for(i = 0; i < ms_Triangle_Num; i++)
			{
				var triangleContainer:MovieClip = m_TriangleContainer.getChildAt(i) as MovieClip;
				animateTriangle(triangleContainer, i);
			}
			for(var i:int = 0; i < ms_Circle_Num; i++)
			{
				var circle:Bitmap = m_CircleContainer.getChildAt(i) as Bitmap;
				animateCircle(circle, i);
			}
			for(i = 0; i < ms_Star_Num; i++)
			{
				var star:Bitmap = m_StarContainer.getChildAt(i) as Bitmap;
				animateStar(star, i);
			}
		}
		
		private function animateTriangle(triangleContainer:MovieClip, triangleNum:int):void
		{
			var triangle:Shape = triangleContainer.getChildAt(0) as Shape;
			
			var randomScaleX:Number = Math.random()*ms_Triangle_ScaleXVariance -  ms_Triangle_ScaleX * .5;
			var randomScaleY:Number = Math.random()*ms_Triangle_ScaleYVariance - ms_Triangle_ScaleY * .5;
			var randomDistance:Number = Math.random()*ms_Triangle_DistanceVariance - ms_Triangle_DistanceVariance * .5;
			
			triangleContainer.rotation = (360/ms_Triangle_Num) * triangleNum + Math.random()*ms_Triangle_AngleVariance;

			TweenLite.fromTo(triangle, ms_Triangle_AnimationDuration, {y: 0}, {y: ms_Triangle_Distance + randomDistance});
			TweenLite.fromTo(triangleContainer, ms_Triangle_AnimationDuration, {scaleX: .1, scaleY: .1, alpha: 1}, {
				scaleX: randomScaleX, scaleY: randomScaleY,
				ease: Back.easeOut.config(4)});
		}
		
		private function animateStar(star:Bitmap, starNum:int):void
		{
			var randomScale:Number = Math.random()*ms_Star_ScaleVariance - ms_Star_ScaleVariance * .5;
			var randomDistance:Number = Math.random()*ms_Star_DistanceVariance - ms_Star_DistanceVariance * .5;
			var starSize:Point = new Point(star.width, star.height);
			
			star.rotation = (360/ms_Star_Num) * starNum + Math.random()*ms_Star_AngleVariance;

			TweenLite.fromTo(m_StarContainer, ms_Star_AnimationDuration, {scaleX: .1, scaleY: .1}, {scaleX: 1, scaleY: 1, ease: Back.easeOut});
			TweenLite.fromTo(star, ms_Star_AnimationDuration, {x: 0, y: 0, alpha: 1}, {
				x: (ms_Star_Distance + randomDistance) * Math.cos(star.rotation*Math.PI/180),
				y: (ms_Star_Distance + randomDistance) * Math.sin(star.rotation*Math.PI/180),
				scaleX: ms_Star_Scale + randomScale,
				scaleY: ms_Star_Scale + randomScale,
				ease: Back.easeOut});
		}
		
		private function animateCircle(circle:Bitmap, circleNum:int):void
		{
			var randomScale:Number = Math.random()*ms_Circle_ScaleVariance - ms_Circle_ScaleVariance * .5;
			var randomDistance:Number = Math.random()*ms_Circle_DistanceVariance - ms_Circle_DistanceVariance * .5;
			
			circle.rotation = (360/ms_Circle_Num) * circleNum + Math.random()*ms_Circle_AngleVariance;
			
			TweenLite.fromTo(circle, ms_Circle_AnimationDuration, {x: 0, y: 0, height: 1, width: 1, alpha: 1}, {
				x: (ms_Circle_Distance + randomDistance) * Math.cos(circle.rotation*Math.PI/180),
				y: (ms_Circle_Distance + randomDistance) * Math.sin(circle.rotation*Math.PI/180),
				scaleX: randomScale + ms_Circle_Scale,
				scaleY: randomScale + ms_Circle_Scale,
				ease: Back.easeOut});
		}
		
		private function get starBitmap():Bitmap
		{
			switch(SessionInformation_GirlsTablet.Age)
			{
				case AgeConstants.KEY_DAISYBROWNIE:
					return new Bitmap(new Particle_StarDB());
					break;
				case AgeConstants.KEY_JUNIORCADETTE:
					return new Bitmap(new Particle_StarJC());
					break;
				case AgeConstants.KEY_SENIORAMBASSADOR:
					return new Bitmap(new Particle_StarSA());
					break;
				default:
					return new Bitmap(new Particle_StarDB());
			}
		}
		
		private function get circleBitmap():Bitmap
		{
			switch(SessionInformation_GirlsTablet.Age)
			{
				case AgeConstants.KEY_DAISYBROWNIE:
					return new Bitmap(new Particle_CircleDB());
					break;
				case AgeConstants.KEY_JUNIORCADETTE:
					return new Bitmap(new Particle_CircleJC());
					break;
				case AgeConstants.KEY_SENIORAMBASSADOR:
					return new Bitmap(new Particle_CircleSA());
					break;
				default:
					return new Bitmap(new Particle_CircleDB());
			}
		}
		
		private function get triangleMC():MovieClip
		{
			switch(SessionInformation_GirlsTablet.Age)
			{
				case AgeConstants.KEY_DAISYBROWNIE:
					return new Particle_TriangleDB();
					break;
				case AgeConstants.KEY_JUNIORCADETTE:
					return new Particle_TriangleJC();
					break;
				case AgeConstants.KEY_SENIORAMBASSADOR:
					return new Particle_TriangleSA();
					break;
				default:
					return new Particle_TriangleDB();
			}
		}
		
		private function nextGirl(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_CLICKS[SessionInformation_GirlsTablet.Age]);
			SessionInformation_GirlsTablet.TeamInfo.IncrementActiveIndex();
			StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_EXPLOREYOURPASSION, StateController_StateIDs_ExploreYourPassion.STATE_MULTIPLEQUESTIONS);
		}
		
		private function nextActivity(e:Event = null):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_CLICKS[SessionInformation_GirlsTablet.Age]);
			SessionInformation_GirlsTablet.TeamInfo.ResetActiveIndex();
			if (SessionInformation_GirlsTablet.IsRunningLocally)
			{
				StateControllerManager.LeaveActiveState(StateController_IDs.STATECONTROLLER_EXPLOREYOURPASSION, StateController_StateIDs_ExploreYourPassion.STATE_QUESTIONSDONE);
				StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_CHOOSEYOURCAREER, StateController_StateIDs_ChooseYourCareer.STATE_CAREERCHOICE);
			}
			else
			{
				submitActivity();
			}
		}
		
		private function submitActivity():void
		{
			StateControllerManager.LeaveAllActiveStates(StateController_IDs.STATECONTROLLER_EXPLOREYOURPASSION);
			Document.TellServerActivityComplete();
		}
		
		private function toggleButton(show:Boolean, mc:MovieClip, activateCallback:Function, deactivateCallback:Function):void
		{
			if(show)
			{
				TweenLite.to(mc, .5, {y:CitrusFramework.frameworkStageSize.y - mc.height, ease: Strong.easeOut, onComplete:activateCallback});
			}
			else
			{
				deactivateCallback();
				TweenLite.to(mc, .5, {y:CitrusFramework.frameworkStageSize.y, ease: Strong.easeOut});
			}
		}
		
		protected override function DeInit():void
		{
			TweenMax.killAll();
			m_TriangleContainer = new MovieClip();
			m_CircleContainer = new MovieClip();
			m_StarContainer = new MovieClip();

			if(m_ExplosionContainer)
			{
				for each(var child:DisplayObject in m_ExplosionContainer)
				{
					m_ExplosionContainer.removeChild(child);
				}
			}
			
			m_AppState.skin.next_Girl_Button.removeEventListener(InputController.CLICK, nextGirl);
			m_AppState.skin.submitButton.removeEventListener(InputController.CLICK, nextActivity);
			CitrusFramework.contentLayer.removeChild(m_AppState);
			EndDeInit();
		}
	}
}
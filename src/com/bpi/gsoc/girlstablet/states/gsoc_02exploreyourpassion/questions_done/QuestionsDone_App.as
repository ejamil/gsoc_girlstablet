package com.bpi.gsoc.girlstablet.states.gsoc_02exploreyourpassion.questions_done
{
	import com.bpi.gsoc.framework.states.app.GSOC_AppSkin;

	public class QuestionsDone_App extends GSOC_AppSkin
	{
		public function QuestionsDone_App()
		{
			super(true);
		}
	}
}
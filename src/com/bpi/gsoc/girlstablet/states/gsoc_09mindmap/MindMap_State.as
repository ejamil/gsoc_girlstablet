package com.bpi.gsoc.girlstablet.states.gsoc_09mindmap
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.gsoc.framework.constants.AgeConstants;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.data.userdata.GSOC_UserData_Team_ImageKeys;
	import com.bpi.gsoc.framework.gsocui.keyboards.GSOC_Keyboards;
	import com.bpi.gsoc.framework.states.app.GSOC_AppState;
	import com.bpi.gsoc.girlstablet.global_data.events.Event_UploadDefaultData;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_MindMap;

	public class MindMap_State extends GSOC_AppState
	{
		private var m_AppState:MindMap_App;
		
		public function MindMap_State()
		{
			super(StateController_StateIDs_MindMap.STATE_MINDMAP);
			
			initializeEventListeners();
		}
		
		private function initializeEventListeners():void
		{
			SessionInformation_GirlsTablet.SessionInformationEventDispatcher.addEventListener(Event_UploadDefaultData.EVENT_UPLOAD_DEFAULT_MINDMAP_ISSUE, eh_UploadDefaultIssue);
			SessionInformation_GirlsTablet.SessionInformationEventDispatcher.addEventListener(Event_UploadDefaultData.EVENT_UPLOAD_DEFAULT_MINDMAP_ROOTCAUSE, eh_UploadDefaultRootCause);
		}
		
		protected override function Init():void
		{
			init();
			
			CitrusFramework.contentLayer.addChild(m_AppState);
			GSOC_Keyboards.SetKeyboardType(GSOC_Keyboards.KEYBOARDTYPE_REGULAR);
		}
		
		protected override function DeInit():void
		{	
			CitrusFramework.contentLayer.removeChild(m_AppState);
			deInit();
			
			super.EndDeInit();
		}
		
		private function init():void
		{
			switch(SessionInformation_GirlsTablet.Age)
			{
				case AgeConstants.KEY_DAISYBROWNIE:
					m_AppState = new MindMap_App_DB();
					break;
				case AgeConstants.KEY_JUNIORCADETTE:
					m_AppState = new MindMap_App_JC();
					break;
				case AgeConstants.KEY_SENIORAMBASSADOR:
					m_AppState = new MindMap_App_SA();
					break;
			}
			
			m_AppState.Initialize();
			m_AppState.InitializeEventListeners();
		}
		
		private function deInit():void
		{
			m_AppState.DeInitialize();
			m_AppState = null;
		}
		
		private function eh_UploadDefaultIssue(evt:Event_UploadDefaultData):void
		{
			if(evt.E_ShouldForceUpload || !SessionInformation_GirlsTablet.TeamInfo.Team_UserData.GetImage(GSOC_UserData_Team_ImageKeys.KEY_CHOOSEYOURISSUE_ISSUE))
			{
				init();
				m_AppState.UploadDefaultIssue();
				deInit();
			}
		}
		
		private function eh_UploadDefaultRootCause(evt:Event_UploadDefaultData):void
		{
			if(evt.E_ShouldForceUpload || !SessionInformation_GirlsTablet.TeamInfo.Team_UserData.GetImage(GSOC_UserData_Team_ImageKeys.KEY_MINDMAP_ROOTCAUSE))
			{
				init();
				m_AppState.UploadDefaultRootCause();
				deInit();
			}
		}
	}
}
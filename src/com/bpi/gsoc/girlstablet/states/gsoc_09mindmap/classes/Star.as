package com.bpi.gsoc.girlstablet.states.gsoc_09mindmap.classes
{
	import com.bpi.citrusas.sound.SoundHandler;
	import com.bpi.citrusas.ui.draggableobject.DraggableObject;
	import com.bpi.citrusas.ui.draggableobject.events.Event_DraggableObject;
	import com.bpi.citrusas.utils.Utils_BPI;
	import com.bpi.components.knowyourrole.SkillShape_Skin;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.gsocui.SkillMap.SkillMap;
	import com.bpi.gsoc.framework.gsocui.SkillMap.Shapes.SkillShape;
	import com.bpi.gsoc.framework.sfx.GSOC_SFX;
	import com.greensock.TweenLite;
	import com.greensock.easing.Back;
	import com.greensock.easing.Power3;
	
	import flash.display.DisplayObjectContainer;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.geom.Point;
	import flash.geom.Rectangle;

	public class Star extends Sprite
	{
		public static const STAR_POSITIONS:Vector.<Point> = new <Point>[new Point(80, 80), new Point(120, 120), new Point(80, 80), new Point(80, 80)];
		
		private static var ms_Starting_Pos:Point;
		private static const SNAP_TO_RADIUS:int = 300;
		
		private static var m_Dispatcher:IEventDispatcher = new EventDispatcher();
		private var m_Skin:DraggableObject;
		
		private var m_SelectedShape:SkillShape;
		
		public function Star(skin:MovieClip)
		{
			m_Skin = new DraggableObject(skin);
			this.addChild(m_Skin.draggableObject);
			skin.visible = false;
			ms_Starting_Pos = new Point(skin.x, skin.y);
		}
		
		public function Intitialize():void
		{
			m_Skin.AddEventListeners();
			m_Skin.addEventListener(Event_DraggableObject.DRAG_END, eh_DragEnded);
			m_Skin.addEventListener(Event_DraggableObject.DRAG_START, eh_Began);
		}
		
		public function DeIntitialize():void
		{
			m_Skin.RemoveEventListeners();
			m_Skin.removeEventListener(Event_DraggableObject.DRAG_END, eh_DragEnded);
			m_Skin.addEventListener(Event_DraggableObject.DRAG_START, eh_Began);
		}
		
		public function Hide():void
		{
			TweenLite.to(m_Skin.draggableObject, .5, {alpha: 0, scaleX: 0, scaleY: 0, onComplete: removeStar, ease: Power3.easeInOut});
		}
		
		private function removeStar():void
		{
			m_Skin.draggableObject.visible = false;
		}
		
		public function Display():void
		{
			m_Skin.draggableObject.visible = true;
			TweenLite.fromTo(m_Skin.draggableObject, .5, {alpha: 0, scaleX: 3, scaleY: 3}, {alpha: 1, scaleX: 1, scaleY: 1, ease: Back.easeIn});
		}
		
		private function eh_Began(e:Event_DraggableObject):void
		{
			TweenLite.killTweensOf(m_Skin.draggableObject);
			TweenLite.to(m_Skin.draggableObject, .5, {scaleX: 1, scaleY: 1, ease: Back.easeOut});		
			
			Dispatcher.dispatchEvent(new Event_StarChanged(Event_StarChanged.STAR_CHANGED, false, null));
		}
		
		private function eh_DragEnded(e:Event_DraggableObject):void
		{
			TweenLite.killTweensOf(m_Skin.draggableObject);

			var skillShapes:Vector.<SkillShape> = SkillMap.Instance.SkillShapes;
			var draggedToSpace:Boolean = false;
			
			for(var i:int = 0 ; i < skillShapes.length; i++)
			{
				if(Point.distance(new Point(skillShapes[i].CurrentPosition.CurrentPosition.x, skillShapes[i].CurrentPosition.CurrentPosition.y - skillShapes[i].height), new Point(m_Skin.draggableObject.x + m_Skin.draggableObject.width * .5, m_Skin.draggableObject.y)) < SNAP_TO_RADIUS)
				{
					SoundHandler.PlaySound(GSOC_SFX.SOUND_STARDROPS[SessionInformation_GirlsTablet.Age]);
					m_SelectedShape = skillShapes[i];
					
					Dispatcher.dispatchEvent(new Event_StarChanged(Event_StarChanged.STAR_CHANGED, true, m_SelectedShape));
					
					var selectedShapePos:Rectangle = Utils_BPI.GetBoundsToStage(m_SelectedShape);
					
					if(m_SelectedShape.Shape == 2)
					{
						TweenLite.to(m_Skin.draggableObject, .5, {x: selectedShapePos.x + STAR_POSITIONS[m_SelectedShape.Shape-1].x, y: selectedShapePos.y + STAR_POSITIONS[m_SelectedShape.Shape-1].y, scaleX: .5, scaleY: .5});
					}
					else
					{
						TweenLite.to(m_Skin.draggableObject, .5, {x: selectedShapePos.x + STAR_POSITIONS[m_SelectedShape.Shape-1].x, y: selectedShapePos.y  + STAR_POSITIONS[m_SelectedShape.Shape-1].y, scaleX: .5, scaleY: .5});
					}
					draggedToSpace = true;
				}
			}	
			if(!draggedToSpace)
			{
				TweenLite.to(m_Skin.draggableObject, .5, {x: ms_Starting_Pos.x, y: ms_Starting_Pos.y});
			}
		}
		
		public function GenerateDefaultUserData():void
		{
			m_Skin.draggableObject.visible = true;
			if(SkillMap.Instance.SkillShapes.length > 0)
			{
				m_SelectedShape = SkillMap.Instance.SkillShapes[0];
			}
			else
			{
				m_SelectedShape = new SkillShape(new SkillShape_Skin());
				m_SelectedShape.Text = "Default Root Cause";
			}
			var selectedShapePos:Rectangle = Utils_BPI.GetBoundsToStage(m_SelectedShape);
			m_Skin.draggableObject.x = selectedShapePos.x + STAR_POSITIONS[m_SelectedShape.Shape-1].x;
			m_Skin.draggableObject.y = selectedShapePos.y + STAR_POSITIONS[m_SelectedShape.Shape-1].y;
			m_Skin.draggableObject.scaleX = m_Skin.draggableObject.scaleY = 0.5;
		}
		
		public function get SelectedShape():SkillShape
		{
			return m_SelectedShape;
		}
		
		public function get StarSkin():DisplayObjectContainer
		{
			return m_Skin.draggableObject as DisplayObjectContainer;
		}
		
		public static function get Dispatcher():IEventDispatcher
		{
			return m_Dispatcher;
		}
	}
}
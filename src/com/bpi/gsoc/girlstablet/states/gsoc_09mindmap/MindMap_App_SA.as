package com.bpi.gsoc.girlstablet.states.gsoc_09mindmap
{
	import com.bpi.citrusas.images.PlaceImage;
	import com.bpi.citrusas.input.inputcontroller.InputController;
	import com.bpi.citrusas.web.WebView;
	import com.bpi.components.knowyourrole.SkillShape_Skin;
	import com.bpi.gsoc.components.mindmap.MindMap_SA_Skin;
	import com.bpi.gsoc.framework.constants.AgeFontLinkages;
	import com.bpi.gsoc.framework.data.sessioninformation.Issue_Data;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.gsocui.GSOC_Header;
	import com.bpi.gsoc.framework.gsocui.GSOC_WebView;
	import com.bpi.gsoc.framework.gsocui.SkillMap.SkillMap_SA;
	import com.bpi.gsoc.framework.gsocui.SkillMap.Menus.CustomizeColorMenu;
	import com.bpi.gsoc.framework.gsocui.SkillMap.Menus.CustomizeShapeMenu;
	import com.bpi.gsoc.framework.gsocui.SkillMap.Popups.AddSkills_PopUp;
	import com.bpi.gsoc.framework.states.takeaction.Issue;
	import com.bpi.gsoc.framework.states.takeaction.IssueReference;
	import com.bpi.gsoc.girlstablet.global_data.FrameControlUtils;
	import com.bpi.gsoc.girlstablet.states.gsoc_09mindmap.classes.Star;
	import com.greensock.TweenMax;
	
	import flash.display.Bitmap;
	import flash.events.Event;

	public class MindMap_App_SA extends MindMap_App
	{
		private var m_Skin:MindMap_SA_Skin;
		private var m_WebView:WebView;
		
		public function MindMap_App_SA()
		{
			m_WebView = new GSOC_WebView();
		}
		
		public override function Initialize():void
		{
			m_Skin = new MindMap_SA_Skin();
			m_Header = m_Skin.Header;
			m_Star = new Star(m_Skin.star);
			m_Issues = m_Skin.issues;
			m_BodyFont = AgeFontLinkages.JC_BODY_FONT;
			m_HeaderFont = AgeFontLinkages.JC_HEADER_FONT;
			
			m_EditSkill_Popup = new AddSkills_PopUp(m_Skin.addSkillPopUp, SkillShape_Skin);
			
			m_CustomizeColor_Button = m_Skin.customizecolor;
			m_CustomizeShape_Button = m_Skin.customizedesign;
			m_CustomizeColor_Menu = new CustomizeColorMenu(m_Skin.customizeColorMenu);
			m_CustomizeShape_Menu = new CustomizeShapeMenu(m_Skin.customizeDesignMenu);
			
			m_ColorOne = 0xF37024;
			m_ColorTwo = 0xfaa61b;

			m_SkillMap = new SkillMap_SA(m_Skin.SkillMap);
			
			m_NextGirl_Button = m_Skin.next_Girl;
			
			FrameControlUtils.StopAtFrameN(m_Skin);
			this.addChild(m_Skin);
			
			m_Skin.launchweb.addEventListener(InputController.CLICK, launchWebWindow);
			
			super.Initialize();
		}
		
		public override function DeInitialize():void
		{
			m_WebView.DeInitialize();
			super.DeInitialize();
		}
		
		private function launchWebWindow(e:Event):void
		{
			m_WebView.Initialize();
		}
		
		protected override function SetupScreen():void
		{
			var issueData:Issue_Data = SessionInformation_GirlsTablet.TeamInfo.Team_Issue;
			var issue:Issue = IssueReference.GetIssueByKey(issueData.Key);
			m_Issues.textbox.text = issueData.Hash;
			
			var issueBitmap:Bitmap = issue.Icon;
			PlaceImage.ImageToExistingContainer(issueBitmap, m_Issues.imageContainer);
			issueBitmap.smoothing = true;
			TweenMax.to(issueBitmap, 0, {colorTransform: {tint: 0xffffff}});
			
			super.SetupScreen();
			
			m_GSOCHeader = new GSOC_Header(m_Skin.Header.paginationArea);
			GSOCheader.addArrows(m_Skin.Header.paginationArea.x, 0);
			this.addChild(m_GSOCHeader);
		}
	}
}
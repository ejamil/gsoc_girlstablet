package com.bpi.gsoc.girlstablet.states.gsoc_09mindmap
{
	import com.bpi.citrusas.images.PlaceImage;
	import com.bpi.citrusas.utils.Utils_Image;
	import com.bpi.gsoc.components.mindmap.MindMap_DB_Skin;
	import com.bpi.gsoc.framework.constants.AgeFontLinkages;
	import com.bpi.gsoc.framework.data.sessioninformation.Issue_Data;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.gsocui.GSOC_Header;
	import com.bpi.gsoc.framework.gsocui.SkillMap.SkillMap_DB;
	import com.bpi.gsoc.framework.gsocui.SkillMap.Menus.CustomizeColorMenu;
	import com.bpi.gsoc.framework.gsocui.SkillMap.Menus.CustomizeShapeMenu;
	import com.bpi.gsoc.framework.states.takeaction.Issue;
	import com.bpi.gsoc.framework.states.takeaction.IssueReference;
	import com.bpi.gsoc.framework.states.takeaction.RootCause;
	import com.bpi.gsoc.girlstablet.global_data.FrameControlUtils;
	import com.bpi.gsoc.girlstablet.states.gsoc_09mindmap.classes.Star;

	public class MindMap_App_DB extends MindMap_App
	{
		private var m_Skin:MindMap_DB_Skin;
		
		public function MindMap_App_DB()
		{
			
		}
		
		public override function Initialize():void
		{
			m_Skin = new MindMap_DB_Skin();
			m_Header = m_Skin.Header;
			m_Star = new Star(m_Skin.star);
			m_Issues = m_Skin.issues;
			
			m_BodyFont = AgeFontLinkages.DB_BODY_FONT;
			m_HeaderFont = AgeFontLinkages.DB_HEADER_FONT;
			
			m_CustomizeColor_Button = m_Skin.customizeColor;
			m_CustomizeShape_Button = m_Skin.customizeDesign;
			m_CustomizeColor_Menu = new CustomizeColorMenu(m_Skin.customize_color);
			m_CustomizeShape_Menu = new CustomizeShapeMenu(m_Skin.customize_shape);
			
			m_ColorOne = 0x713b18;
			m_ColorTwo = 0x17aae2;
			
			m_SkillMap = new SkillMap_DB(m_Skin.mindlMap);
			
			m_NextGirl_Button = m_Skin.next_Girl_Button;
			
			FrameControlUtils.StopAtFrameN(m_Skin);
			this.addChild(m_Skin);
			
			super.Initialize();
		}
		
		protected override function SetupScreen():void
		{
			var issueData:Issue_Data = SessionInformation_GirlsTablet.TeamInfo.Team_Issue;
			var issue:Issue = IssueReference.GetIssueByKey(issueData.Key);
			issue.BackgroundIndex = issueData.BackgroundIndex;
			issue.IconIndex = issueData.IconIndex;
			
			m_Issues.textbox.text = issueData.Text;

			PlaceImage.ImageToExistingContainer(issue.Icon, m_Issues.imageContainer, true, true, false);
			issue.Icon.x += 1; // rather than re-export all the graphics, I'm hacking this to move by 1 pixel
			
			super.SetupScreen();
			
			var rootCauses:Vector.<RootCause> = issue.RootCauses;
			for(var i:int = 0 ; i < m_SkillMap.SkillShapes.length; i++)
			{
				if(i < rootCauses.length)
				{
					m_SkillMap.SkillShapes[i].Icon = Utils_Image.ConvertDisplayObjectToMovieClip(rootCauses[i].Icon);
					m_SkillMap.SkillShapes[i].Text = rootCauses[i].RootCauseText;
				}
			}
			
			m_GSOCHeader = new GSOC_Header(m_Skin.Header.paginationArea);
			GSOCheader.addArrows(m_Skin.Header.paginationArea.x, 0);
			this.addChild(m_GSOCHeader);
		}
	}
}
package com.bpi.gsoc.girlstablet.states.gsoc_09mindmap
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.citrusas.input.inputcontroller.InputController;
	import com.bpi.citrusas.sound.SoundHandler;
	import com.bpi.citrusas.states.StateControllerManager;
	import com.bpi.citrusas.ui.primitives.Primitive_Rectangle;
	import com.bpi.citrusas.utils.Utils_BPI;
	import com.bpi.citrusas.utils.Utils_Image;
	import com.bpi.citrusas.utils.Utils_Text;
	import com.bpi.gsoc.framework.constants.AgeConstants;
	import com.bpi.gsoc.framework.constants.AgeFontLinkages;
	import com.bpi.gsoc.framework.constants.ButtonPlacement;
	import com.bpi.gsoc.framework.constants.GSOC_Info;
	import com.bpi.gsoc.framework.constants.Keys_FileNames;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.data.userdata.GSOC_UserData_Team_ImageKeys;
	import com.bpi.gsoc.framework.gsocui.GSOC_ButtonToggle;
	import com.bpi.gsoc.framework.gsocui.STEM_Popups;
	import com.bpi.gsoc.framework.gsocui.SkillMap.SkillMap;
	import com.bpi.gsoc.framework.gsocui.SkillMap.Events.Event_AddYourOwnSelected;
	import com.bpi.gsoc.framework.gsocui.SkillMap.Events.Event_MapStatus;
	import com.bpi.gsoc.framework.gsocui.SkillMap.Menus.CustomizeColorMenu;
	import com.bpi.gsoc.framework.gsocui.SkillMap.Menus.CustomizeShapeMenu;
	import com.bpi.gsoc.framework.gsocui.SkillMap.Popups.AddSkills_PopUp;
	import com.bpi.gsoc.framework.gsocui.SkillMap.Shapes.SkillShape;
	import com.bpi.gsoc.framework.sfx.GSOC_SFX;
	import com.bpi.gsoc.framework.states.app.GSOC_AppSkin;
	import com.bpi.gsoc.girlstablet.global_data.upload.UploadManager;
	import com.bpi.gsoc.girlstablet.states.gsoc_09mindmap.classes.Event_StarChanged;
	import com.bpi.gsoc.girlstablet.states.gsoc_09mindmap.classes.Star;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_IDs;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_CommunityResources;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_Main;
	import com.greensock.TimelineMax;
	import com.greensock.TweenLite;
	import com.greensock.easing.Back;
	import com.greensock.easing.Bounce;
	import com.greensock.easing.Power3;
	
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.filters.DropShadowFilter;
	import flash.filters.GlowFilter;
	import flash.geom.Point;
	import flash.geom.Rectangle;

	public class MindMap_App extends GSOC_AppSkin
	{
		private static const m_Dispatcher:IEventDispatcher = new EventDispatcher();
		private static var m_Instance:MindMap_App;
		
		protected var m_Header:MovieClip;
		protected var m_Issues:MovieClip;
		protected var m_Star:Star;
		
		protected var m_CustomizeColor_Button:MovieClip;
		protected var m_CustomizeShape_Button:MovieClip;
		protected var m_CustomizeColor_Menu:CustomizeColorMenu;
		protected var m_CustomizeShape_Menu:CustomizeShapeMenu;
		private var m_CustomizeButtonY:Number;
		
		protected var m_SkillMap:SkillMap;
		protected var m_AvatarHolder:MovieClip;
		
		protected var m_NextGirl_Button:MovieClip;
		protected var m_NextGirlPosY:Number;
		
		protected var m_EditSkill_Popup:AddSkills_PopUp;
		
		protected var m_ColorOne:uint;
		protected var m_ColorTwo:uint;
		
		private var m_Background:Primitive_Rectangle;
		private var m_Timeline:TimelineMax;
		
		private var m_DragLayer:Sprite;
		
		public function MindMap_App()
		{
			if(Instance == null)
			{
				m_Instance = this;
			}
			else
			{
				throw new Error("You can only have one instance of choiceWeb_App");
			}
			
			m_Background = new Primitive_Rectangle(new Point(CitrusFramework.frameworkStageSize.x, CitrusFramework.frameworkStageSize.y), 0x000000);
			m_Background.alpha = .6;
		}
		
		public override function Initialize():void
		{
			super.Initialize();
			
			m_DragLayer = new Sprite();
			
			m_CustomizeColor_Menu.Initialize();
			m_CustomizeShape_Menu.Initialize();
			m_SkillMap.Initialize(m_DragLayer);
			m_Star.Intitialize();

			this.addChild(m_Background);
			this.addChild(m_SkillMap);
			this.addChild(m_Issues);
			this.addChild(m_NextGirl_Button);
			this.addChild(m_DragLayer);
			this.addChild(m_Star);
			this.addChild(m_CustomizeColor_Menu);
			this.addChild(m_CustomizeShape_Menu);
			this.addChild(m_Header);
			this.addChild(m_CustomizeColor_Button);
			this.addChild(m_CustomizeShape_Button);
			
			SetupScreen();
		}
		
		public function InitializeEventListeners():void
		{			
			SkillMap.Dispatcher.addEventListener(Event_MapStatus.MAP_CHANGED, eh_MapChanged);
			SkillMap.Dispatcher.addEventListener(Event_AddYourOwnSelected.ADDYOUROWN_SELECTED, eh_AddYourOwnSelected);
			
			m_CustomizeColor_Button.addEventListener(InputController.CLICK, eh_CustomizeColorButtonClicked);
			m_CustomizeShape_Button.addEventListener(InputController.CLICK, eh_CustomizeShapeButtonClicked);

			m_CustomizeColor_Menu.InitializeEventListeners();
			m_CustomizeShape_Menu.InitializeEventListeners();
			m_SkillMap.InitializeEventListeners();
		}
		
		public override function DeInitialize():void
		{
			this.removeChild(m_CustomizeColor_Menu);
			this.removeChild(m_CustomizeShape_Menu);
			this.removeChild(m_Header);
			this.removeChild(m_CustomizeColor_Button);
			this.removeChild(m_CustomizeShape_Button);
			this.removeChild(m_SkillMap);
			this.removeChild(m_Issues);
			this.removeChild(m_Star);
			this.removeChild(m_NextGirl_Button);
			this.removeChild(m_DragLayer);
			this.removeChild(m_Background);
			
			m_CustomizeColor_Menu.DeInitialize();
			m_CustomizeShape_Menu.DeInitialize();
			m_SkillMap.DeInitialize();
			m_Star.DeIntitialize();
			
			if(m_EditSkill_Popup)
			{
				m_EditSkill_Popup.DeInitialize();
				this.removeChild(m_EditSkill_Popup);
			}
			
			Star.Dispatcher.removeEventListener(Event_StarChanged.STAR_CHANGED, eh_StarChanged);
			SkillMap.Dispatcher.removeEventListener(Event_AddYourOwnSelected.ADDYOUROWN_SELECTED, eh_AddYourOwnSelected);
			SkillMap.Dispatcher.removeEventListener(Event_MapStatus.MAP_CHANGED, eh_MapChanged);
			removeButtonListeners();
			
			for(var i:int = 0; i < m_Issues.imageContainer.numChildren; i++)
			{
				m_Issues.imageContainer.removeChildAt(0);
			}
			
			m_Instance = null;
		}
		
		protected function SetupScreen():void
		{
			m_Header.mouseEnabled = false;
			
			m_NextGirlPosY = m_NextGirl_Button.y;
			m_NextGirl_Button.x = ButtonPlacement.NEXT_X;
			m_CustomizeButtonY = m_CustomizeShape_Button.y;
			
			GSOC_ButtonToggle.ToggleButton(false, m_NextGirl_Button, this.activateNextGirlButton, this.deactivateNextGirlButton); 

			m_Star.Hide();
			m_Background.visible = false;

			m_Issues.gotoAndStop(SessionInformation_GirlsTablet.TeamInfo.Team_Issue.BackgroundIndex);
			
			var dropShadow:DropShadowFilter = new DropShadowFilter(5, 45, 0x000000, .5);
			m_Issues.textbox.filters = [dropShadow];
			
			if(m_EditSkill_Popup)
			{
				m_EditSkill_Popup.Initialize()
				this.addChild(m_EditSkill_Popup);
			}
			
			m_Timeline = new TimelineMax();
			m_Timeline.to([m_CustomizeShape_Button, m_CustomizeColor_Button], 3, {
				rotationx: 0, rotationY: 0, scaleX: 1, scaleY: 1, y: m_CustomizeButtonY, ease: Power3.easeOut});
			m_Timeline.to([m_CustomizeShape_Button, m_CustomizeColor_Button], .5, {
				rotationX: 10, rotationY: 10, scaleX: 1.1, scaleY: 1.1, y: m_CustomizeButtonY - 20, ease: Back.easeIn});
			m_Timeline.to([m_CustomizeShape_Button, m_CustomizeColor_Button], .5, {
				rotationX: 0, rotationY: 0, scaleX: 1, scaleY: 1, y: m_CustomizeButtonY, ease: Bounce.easeOut, onComplete: repeatAnimation});
			
			if(SessionInformation_GirlsTablet.Age == AgeConstants.KEY_SENIORAMBASSADOR)
			{
				Utils_Text.ConvertToEmbeddedFont(m_Issues.textbox, AgeFontLinkages.SA_BODYBOLD_FONT, true);
				m_Issues.imageContainer.filters = [new GlowFilter(0xffffff, .3, 30, 30)];
			}
			else
			{
				Utils_Text.ConvertToEmbeddedFont(m_Issues.textbox, GSOC_Info.BodyFont, true);
			}
			m_Issues.textbox.multiline = true;
			m_Issues.textbox.wordWrap = true;
			Utils_Text.FitTextToTextfield(m_Issues.textbox, 4, true);
			
			SessionInformation_GirlsTablet.TeamInfo.Team_UserData.SetImage(GSOC_UserData_Team_ImageKeys.KEY_MINDMAP_WEB, Utils_Image.ConvertDisplayObjectToMovieClip(m_Issues));
			UploadManager.UploadDisplayObject(m_Issues, Keys_FileNames.KEY_MINDMAP_WEB);
		}
		
		private function repeatAnimation():void
		{
			m_Timeline.play(0);
		}
		
		protected function eh_MapChanged(e:Event_MapStatus):void
		{
			STEM_Popups.DisplayPopup(STEM_Popups.ROOT_CAUSE);
			
			if(e.m_IsComplete)
			{
				GSOC_ButtonToggle.ToggleButton(true, m_NextGirl_Button, this.activateNextGirlButton, this.deactivateNextGirlButton);
			}
			else
			{
				GSOC_ButtonToggle.ToggleButton(false, m_NextGirl_Button, this.activateNextGirlButton, this.deactivateNextGirlButton);
			}
		}
		
		private function activateNextGirlButton(e:Event = null):void
		{
			m_NextGirl_Button.addEventListener(InputController.CLICK, this.eh_NextGirlButtonClicked);
		}
		
		private function deactivateNextGirlButton(e:Event = null):void
		{
			m_NextGirl_Button.removeEventListener(InputController.CLICK, this.eh_NextGirlButtonClicked);
		}
		
		private function activateSubmitButton(e:Event = null):void
		{
			m_NextGirl_Button.addEventListener(InputController.CLICK, this.eh_SubmitButtonClicked);
		}
		
		private function deactivateSubmitButton(e:Event = null):void
		{
			m_NextGirl_Button.removeEventListener(InputController.CLICK, this.eh_SubmitButtonClicked);
		}
		
		private function removeButtonListeners():void
		{
			m_CustomizeColor_Button.removeEventListener(InputController.CLICK, eh_CustomizeColorButtonClicked);
			m_CustomizeShape_Button.removeEventListener(InputController.CLICK, eh_CustomizeShapeButtonClicked);
			m_NextGirl_Button.removeEventListener(InputController.CLICK, eh_NextGirlButtonClicked);
			m_NextGirl_Button.removeEventListener(InputController.CLICK, eh_SubmitButtonClicked);
		}
		
		private function eh_NextGirlButtonClicked(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_CLICKS[SessionInformation_GirlsTablet.Age]);
			m_SkillMap.RemoveUnusedShapes();
			m_SkillMap.RemoveEventListeners();
			removeButtonListeners();
			GSOC_ButtonToggle.ToggleButton(false, m_NextGirl_Button, this.activateNextGirlButton, this.deactivateNextGirlButton);
			SkillMap.Dispatcher.removeEventListener(Event_MapStatus.MAP_CHANGED, eh_MapChanged);
			m_Background.visible = true;
			TweenLite.to(m_Background, .5, {alpha: .6, onComplete: function():void{m_NextGirl_Button.gotoAndStop(2);}});
			TweenLite.to(m_CustomizeColor_Button, .5, {alpha: 0});
			TweenLite.to(m_CustomizeShape_Button, .5, {alpha: 0});
			m_Header.gotoAndStop(2);
			m_CustomizeColor_Menu.HideMenu();
			m_CustomizeShape_Menu.HideMenu();
			
			m_Star.Display();
			Star.Dispatcher.addEventListener(Event_StarChanged.STAR_CHANGED, eh_StarChanged);
		}
		
		private function eh_StarChanged(e:Event_StarChanged):void
		{
			if(e.e_StarPlaced)
			{
				var bm:Bitmap = Utils_Image.ConvertDisplayObjectToBitmap(e.e_SkillShape);
				SessionInformation_GirlsTablet.TeamInfo.Team_Issue.RootCause_Text = e.e_SkillShape.Text;
				GSOC_ButtonToggle.ToggleButton(true, m_NextGirl_Button, this.activateSubmitButton, this.deactivateSubmitButton);
			}
			else
			{
				GSOC_ButtonToggle.ToggleButton(false, m_NextGirl_Button, this.activateSubmitButton, this.deactivateSubmitButton);
			}
		}
		
		private function submitData_SkillMap():void
		{
			m_SkillMap.addChild(m_DragLayer);
			var tempSprite:Sprite = new Sprite();
			var tempSkillMap:Bitmap = Utils_Image.ConvertDisplayObjectToBitmap(m_SkillMap, 0, 0, 0, 30);
			
			var prevScale:Number = m_Issues.scaleX;
			m_Issues.scaleX = 1;
			m_Issues.scaleY = 1;
			
			var tempIssue1:Bitmap = Utils_Image.ConvertDisplayObjectToBitmap(m_Issues);
			var tempIssue2:Bitmap = Utils_Image.ConvertDisplayObjectToBitmap(m_Issues);
			
			tempIssue1.scaleX = prevScale;
			tempIssue1.scaleY = prevScale;
			m_Issues.scaleX = prevScale;
			m_Issues.scaleY = prevScale;
			
			switch(SessionInformation_GirlsTablet.Age)
			{
				case AgeConstants.KEY_DAISYBROWNIE:
					tempIssue1.x =  500 * (2-prevScale);
					tempIssue1.y =  280 * (2-prevScale);
					break;
				case AgeConstants.KEY_JUNIORCADETTE:
					tempIssue1.x =  525 * (2-prevScale);
					tempIssue1.y =  280 * (2-prevScale);
					break;
				case AgeConstants.KEY_SENIORAMBASSADOR:
					tempIssue1.x =  1010 * (2-prevScale);
					tempIssue1.y =  300 * (2-prevScale);
					break;
			}

			tempSprite.addChild(tempSkillMap);
			tempSprite.addChild(tempIssue1);

			SessionInformation_GirlsTablet.TeamInfo.Team_UserData.SetImage(GSOC_UserData_Team_ImageKeys.KEY_MINDMAP_WEB, tempSprite);
			UploadManager.UploadDisplayObject(tempSprite, Keys_FileNames.KEY_MINDMAP_WEB);
			
			this.addChild(m_DragLayer);
			this.addChild(m_Star);
		}
		
		private function uploadRootCause():void
		{
			var selectedShape:SkillShape = m_Star.SelectedShape;
			if(selectedShape)
			{
				var star:DisplayObject = m_Star.StarSkin;
				var skillShapeBounds:Rectangle = Utils_BPI.GetBoundsToStage(selectedShape.Skin);
				selectedShape.Skin.addChild(star);
				
				star.x = Star.STAR_POSITIONS[selectedShape.Shape - 1].x - skillShapeBounds.width * .5;
				star.y = Star.STAR_POSITIONS[selectedShape.Shape - 1].y - skillShapeBounds.height * .5;
				
				var rootCause:MovieClip = Utils_Image.ConvertDisplayObjectToMovieClip(Utils_Image.ConvertDisplayObjectToBitmap(selectedShape, 0, 0, 50, 50));
				SessionInformation_GirlsTablet.TeamInfo.Team_UserData.SetImage(GSOC_UserData_Team_ImageKeys.KEY_MINDMAP_ROOTCAUSE, rootCause);
				UploadManager.UploadDisplayObject(rootCause, Keys_FileNames.KEY_MINDMAP_ROOTCAUSE);
			}
		}
		
		private function uploadIssue():void
		{
			var issueSprite:Sprite = Utils_Image.ConvertDisplayObjectToMovieClip(m_Issues);
			SessionInformation_GirlsTablet.TeamInfo.Team_UserData.SetImage(GSOC_UserData_Team_ImageKeys.KEY_CHOOSEYOURISSUE_ISSUE, issueSprite);
			UploadManager.UploadDisplayObject(issueSprite, Keys_FileNames.KEY_CHOOSEYOURISSUE_ISSUE);
		}
		
		private function eh_SubmitButtonClicked(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_CLICKS[SessionInformation_GirlsTablet.Age]);
			uploadRootCause();
			submitData_SkillMap();
			
			SessionInformation_GirlsTablet.UploadCurrentInformation();
			
			if(SessionInformation_GirlsTablet.IsRunningLocally)
			{
				SessionInformation_GirlsTablet.TeamInfo.ResetActiveIndex();
				StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_COMMUNITYRESOURCES, StateController_StateIDs_CommunityResources.STATE_COMMUNITYRESOURCES);
			}
			else
			{
				submitActivity();
			}
		}
		
		private function submitActivity():void
		{
			StateControllerManager.LeaveAllActiveStates();
			StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_MAIN, StateController_StateIDs_Main.STATE_SESSIONATTRACT);
			Document.TellServerActivityComplete();
		}
		
		private function eh_CustomizeColorButtonClicked(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_COLORDESIGNDROPDOWNS[SessionInformation_GirlsTablet.Age]);
			m_CustomizeColor_Menu.ToggleDisplay();
		}
		
		private function eh_CustomizeShapeButtonClicked(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_COLORDESIGNDROPDOWNS[SessionInformation_GirlsTablet.Age]);
			m_CustomizeShape_Menu.ToggleDisplay();
		}
		
		private function eh_AddYourOwnSelected(e:Event_AddYourOwnSelected):void
		{
			m_EditSkill_Popup.MapPos = e.m_MapPos;
			m_EditSkill_Popup.DisplayPopUp();
		}
		
		public function UploadDefaultIssue():void
		{
			uploadIssue();
		}
		
		public function UploadDefaultRootCause():void
		{
			m_Star.GenerateDefaultUserData();
			uploadRootCause();
		}
		
		public function get BodyFont():String
		{
			return m_BodyFont;
		}
		
		public function get HeaderFont():String
		{
			return m_HeaderFont;
		}
		
		public function get ColorOne():uint
		{
			return m_ColorOne;
		}
		
		public function get ColorTwo():uint
		{
			return m_ColorTwo;
		}
		
		public static function get Instance():MindMap_App
		{
			return m_Instance;
		}
	}
}
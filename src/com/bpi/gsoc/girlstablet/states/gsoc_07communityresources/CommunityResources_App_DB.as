package com.bpi.gsoc.girlstablet.states.gsoc_07communityresources
{
	import com.bpi.gsoc.components.communityrescources.ResourceSelection_DB_Skin;
	import com.bpi.gsoc.framework.constants.AgeFontLinkages;
	import com.bpi.gsoc.framework.embeddedcontent.EmbeddedFonts;
	import com.bpi.gsoc.framework.gsocui.GSOC_Header;

	public class CommunityResources_App_DB extends CommunityResources_App
	{
		public function CommunityResources_App_DB()
		{
			m_Skin = new ResourceSelection_DB_Skin();
			m_HeaderFont = AgeFontLinkages.DB_HEADER_FONT;
			m_BodyFont = EmbeddedFonts.FontName_ZalderdashTTF;
			m_GSOCHeader = new GSOC_Header(m_Skin.Header.paginationArea);
			GSOCheader.addArrows(m_Skin.Header.paginationArea.x, 0);
			super.Initialize();
		}
	}
}
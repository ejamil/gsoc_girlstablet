package com.bpi.gsoc.girlstablet.states.gsoc_07communityresources
{
	import com.bpi.gsoc.framework.states.app.GSOC_AppSkin;

	public class CommunityResources_App extends GSOC_AppSkin
	{
		public function CommunityResources_App()
		{
			super(true);
		}
	}
}
package com.bpi.gsoc.girlstablet.states.gsoc_07communityresources.classes
{
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;

	public class FontUtils_CommunityResources
	{
		public static function ReplaceWithEmbeddedFont(tf:TextField, newFont:String):void
		{
			var format:TextFormat = new TextFormat(newFont, tf.defaultTextFormat.size, tf.defaultTextFormat.color, false);
			
			tf.defaultTextFormat = format;
			tf.embedFonts = true;
			tf.setTextFormat(format);
			tf.wordWrap = false;
			tf.autoSize = TextFieldAutoSize.CENTER;
		}
	}
}
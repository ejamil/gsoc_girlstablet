package com.bpi.gsoc.girlstablet.states.gsoc_07communityresources.classes
{
	import com.bpi.citrusas.utils.Utils_Text;
	import com.bpi.gsoc.girlstablet.states.gsoc_01pictureyourself.name_selection.classes.TextField_NameEntry;
	
	import flash.display.DisplayObjectContainer;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.text.TextFormat;

	public class TextField_AddYourOwnResource extends Sprite
	{
		public static const ms_DefaultText:String = "Add Your Own";
		
		public var asset:MovieClip;
		private var originalPosition:Point;
		private var originalFontSize:Object;
		public var primaryColor:uint;
		public var secondaryColor:uint;
		public var theFont:String;
		
		public static const TEXT_ENTERED:String = "TEXT_ENTERED";
		public static var TextEntryDispatcher:EventDispatcher = new EventDispatcher();
		
		public function TextField_AddYourOwnResource(skin:MovieClip, context:DisplayObjectContainer, font:String, pc:uint, sc:uint)
		{			
			this.addChild(skin);
			context.addChild(this);
			primaryColor = pc;
			secondaryColor = sc;
			theFont = font;
			
			this.asset = skin;
			
			this.originalPosition = new Point();
			this.originalPosition.y = this.asset.txt.y;
			this.originalPosition.x = this.asset.txt.x;
			
			this.originalFontSize = (this.asset.txt as TextField).defaultTextFormat.size;
			
			var tf:TextFormat = new TextFormat(font, (this.asset.txt as TextField).defaultTextFormat.size, (this.asset.txt as TextField).defaultTextFormat.color, false);
			
			var textField:TextField = (this.asset.txt as TextField);
			textField.defaultTextFormat = tf;
			textField.embedFonts = true;
			textField.setTextFormat(tf);
			textField.maxChars = 32;
			//(this.asset.txt as TextField).autoSize = TextFieldAutoSize.CENTER;
			
			(this.asset.txt as TextField).addEventListener(Event.CHANGE, eh_ToUpper, false, 0, true);
		}
		
		public function changeColor(i:int):void
		{	
			var tf:TextFormat;
			
			switch (i)
			{
				case 0:
					tf = new TextFormat(theFont, (this.asset.txt as TextField).defaultTextFormat.size, primaryColor, false);
					break;
				case 1:
					tf = new TextFormat(theFont, (this.asset.txt as TextField).defaultTextFormat.size, secondaryColor, false);
					break;
			}
			
			(this.asset.txt as TextField).defaultTextFormat = tf;
			(this.asset.txt as TextField).embedFonts = true;
			(this.asset.txt as TextField).setTextFormat(tf);
			//(this.asset.txt as TextField).autoSize = TextFieldAutoSize.CENTER;
		}
		
		public function resetText():void
		{
			if (this.asset.txt.text == "")
			{
				this.asset.txt.text = ms_DefaultText;
			}
		}
		
		public function clearField():void
		{
			if (this.asset.txt.text == ms_DefaultText)
			{
				this.asset.txt.text = "";
				this.asset.txt.scaleX = this.asset.txt.scaleY = 1;
			}	
		}
		
		public function receiveChar(e:KeyboardEvent):void
		{
			var textfield:TextField = this.asset.txt;
			
			if(textfield.text == ms_DefaultText)
			{
				textfield.text = "";
			}
			//this.asset.txt.text = (this.asset.txt.text as String);
		}
		
		public function caps():void
		{
			var textfield:TextField = this.asset.txt;
			textfield.text = (this.asset.txt.text as String);
		}
		
		public function backspace():void
		{
			var txt:String = this.asset.txt.text;
			txt = txt.substr(0, txt.length - 1);
			
			this.asset.txt.text = txt;
			this.asset.txt.scaleX = 1;
			this.asset.txt.scaleY = 1;
			this.scaleText();
		}
		
		private function scaleText():void
		{
			while((this.asset.txt as TextField).textWidth * this.asset.txt.scaleX > 720)
			{
				this.asset.txt.scaleX -= 0.01;
				this.asset.txt.scaleY = this.asset.txt.scaleX;
			}
			
			if(this.asset.txt.scaleX < 1)
			{
				this.asset.txt.x = this.originalPosition.x + ((1 - this.asset.txt.scaleX)/0.03);
				this.asset.txt.y = this.originalPosition.y + ((1 - this.asset.txt.scaleX)/0.03);
			}
		}
		
		public function getTextFieldLength():Number
		{
			return this.asset.txt.text.length;
		}
		
		public function getText():String
		{
			return this.asset.txt.text;
		}
		
		public function isShown():Boolean
		{
			return this.asset.visible;
		}
		
		public function hide():void
		{
			this.asset.txt.text = "";
			this.asset.visible = false;
		}
		
		public function show():void
		{
			this.asset.visible = true;
		}
		
		public function activate(e:Event = null):void
		{	
			this.asset.gotoAndStop(2);
		}
		
		public function deactivate():void
		{
			this.asset.gotoAndStop(1);
		}
		
		public function enableMouseClick():void
		{
			this.addEventListener(MouseEvent.CLICK, this.activate);
		}
		
		public function disableMouseClick():void
		{
			this.removeEventListener(MouseEvent.CLICK, this.activate);
		}
		
		private function eh_ToUpper(evt:Event):void
		{
			var text:String = evt.target.text;
			if (text.length > 20)
			{
				text = text.substr(0, text.length - 1);
			}
			this.refreshTitleText();
			this.centerTitleText();
			
			this.dispatchEvent(new Event(TextField_NameEntry.TEXT_ENTERED));
		}
		
		private function refreshTitleText():void
		{
			this.asset.txt.y = this.originalPosition.y;
			var tf:TextFormat = this.asset.txt.getTextFormat();
			tf.size = this.originalFontSize;
			this.asset.txt.setTextFormat(tf);
			this.asset.txt.defaultTextFormat = tf;
		}
		
		private function centerTitleText():void
		{
			Utils_Text.FitTextToTextfield(this.asset.txt, 5);
			this.asset.txt.y = this.originalPosition.y + Math.round((this.asset.txt.height - this.asset.txt.textHeight) / 2); 
		}
		
		public function GetTextField():TextField
		{
			return this.asset.txt;
		}
	}
}
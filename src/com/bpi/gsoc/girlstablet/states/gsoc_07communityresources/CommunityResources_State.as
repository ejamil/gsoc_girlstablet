package com.bpi.gsoc.girlstablet.states.gsoc_07communityresources
{
	import com.adobe.utils.StringUtil;
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.citrusas.input.inputcontroller.InputController;
	import com.bpi.citrusas.sound.SoundHandler;
	import com.bpi.citrusas.states.StateControllerManager;
	import com.bpi.citrusas.utils.Utils_Image;
	import com.bpi.gsoc.framework.constants.AgeConstants;
	import com.bpi.gsoc.framework.constants.Keys_FileNames;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.data.userdata.GSOC_UserData_Team_DataKeys;
	import com.bpi.gsoc.framework.data.userdata.GSOC_UserData_Team_ImageKeys;
	import com.bpi.gsoc.framework.gsocui.keyboards.GSOC_Keyboards;
	import com.bpi.gsoc.framework.sfx.GSOC_SFX;
	import com.bpi.gsoc.framework.states.app.GSOC_AppState;
	import com.bpi.gsoc.girlstablet.global_data.events.Event_UploadDefaultData;
	import com.bpi.gsoc.girlstablet.global_data.upload.UploadManager;
	import com.bpi.gsoc.girlstablet.states.gsoc_07communityresources.classes.BuildingTextLookup;
	import com.bpi.gsoc.girlstablet.states.gsoc_07communityresources.classes.TextField_AddYourOwnResource;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_IDs;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_BrainStorm;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_CommunityResources;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_Main;
	import com.greensock.TweenLite;
	import com.greensock.TweenMax;
	import com.greensock.easing.Bounce;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.events.TouchEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.ui.Keyboard;

	public class CommunityResources_State  extends GSOC_AppState
	{
		private var m_AppState:CommunityResources_App;
		private var numAnswers:int = 3;
		private var numSelected:int = 0;
		private var submitY:int;
		private var buildingSelections:Vector.<String>;
		private var addYourOwnFields:Vector.<TextField_AddYourOwnResource>;
		private var _activeTextField:int;
		private var m_Buildings:Vector.<MovieClip>;
		private var m_BuildingsY:Vector.<Number>;
		private var m_InfoTargets:Vector.<MovieClip>;
		private var m_Callouts:Vector.<MovieClip>;
		private var m_SelectedTextField:TextField_AddYourOwnResource;
		
		public function CommunityResources_State()
		{
			super(StateController_StateIDs_CommunityResources.STATE_COMMUNITYRESOURCES);
		}
		
		protected override function Init():void
		{		
			m_SelectedTextField = null;
			
			GSOC_Keyboards.SetKeyboardType(GSOC_Keyboards.KEYBOARDTYPE_REGULAR);
			this.addYourOwnFields = new Vector.<TextField_AddYourOwnResource>();
			this._activeTextField = -1;
			
			buildingSelections = new Vector.<String>();
			m_Buildings = new Vector.<MovieClip>();
			m_BuildingsY = new Vector.<Number>();
			m_InfoTargets = new Vector.<MovieClip>();
			m_Callouts = new Vector.<MovieClip>();
			var ageLevelTextFormat:TextFormat;
			var resetCallback:Function;
			
			SessionInformation_GirlsTablet.SessionInformationEventDispatcher.dispatchEvent(new Event_UploadDefaultData(Event_UploadDefaultData.EVENT_UPLOAD_DEFAULT_MINDMAP_ISSUE));
			
			switch(SessionInformation_GirlsTablet.Age)
			{
				case AgeConstants.KEY_DAISYBROWNIE:
					m_AppState = new CommunityResources_App_DB();
					m_AppState.Init();
					numAnswers = 3;
					m_AppState.skin.theHitArea.addEventListener(InputController.CLICK, resetPopups);
					m_AppState.skin.theMap.map.addEventListener(InputController.CLICK, resetPopups);
					
					m_BuildingsY.push(m_AppState.skin.theMap.building_communitycenter.y,
						m_AppState.skin.theMap.building_home.y,
						m_AppState.skin.theMap.building_library.y,
						m_AppState.skin.theMap.building_girlscoutmeeting.y,
						m_AppState.skin.theMap.building_classroom.y,
						m_AppState.skin.theMap.building_townhall.y,
						m_AppState.skin.theMap.building_newspaper.y);
					
					m_Buildings.push(m_AppState.skin.theMap.building_communitycenter,
						m_AppState.skin.theMap.building_home,
						m_AppState.skin.theMap.building_library,
						m_AppState.skin.theMap.building_girlscoutmeeting,
						m_AppState.skin.theMap.building_classroom,
						m_AppState.skin.theMap.building_townhall,
						m_AppState.skin.theMap.building_newspaper);
					
						
					m_InfoTargets.push(m_AppState.skin.theMap.I_communitycenter,
						m_AppState.skin.theMap.I_home,
						m_AppState.skin.theMap.I_library,
						m_AppState.skin.theMap.I_girlscoutmeeting,
						m_AppState.skin.theMap.I_classroom,
						m_AppState.skin.theMap.I_townhall,
						m_AppState.skin.theMap.I_newspaper);
					
					m_Callouts.push(m_AppState.skin.theMap.callout_communitycenter,
						m_AppState.skin.theMap.callout_home,
						m_AppState.skin.theMap.callout_classroom,
						m_AppState.skin.theMap.callout_townhall,
						m_AppState.skin.theMap.callout_library,
						m_AppState.skin.theMap.callout_girlscoutmeeting,
						m_AppState.skin.theMap.callout_newspaper);
					
					ageLevelTextFormat = new TextFormat(m_AppState.headerFont, (m_AppState.skin.theMap.callout_communitycenter.txt as TextField).defaultTextFormat.size, (m_AppState.skin.theMap.callout_communitycenter.txt as TextField).defaultTextFormat.color, false);
					resetCallback = resetPopups;
					
					initCallOut(m_AppState.skin.theMap.callout_communitycenter.txt, "Find people in the community that would like to help.");
					initCallOut(m_AppState.skin.theMap.callout_home.txt, "Get some help from your parents, siblings, and extended family.");
					initCallOut(m_AppState.skin.theMap.callout_classroom.txt, "Ask your teachers and classmates for advice or to volunteer.");
					initCallOut(m_AppState.skin.theMap.callout_townhall.txt, "Make a presentation at a town meeting.");
					initCallOut(m_AppState.skin.theMap.callout_library.txt, "Look for books or articles to research your issue.");
					initCallOut(m_AppState.skin.theMap.callout_girlscoutmeeting.txt, "Present your ideas to other Girl Scouts and see if they’d like to help.");
					initCallOut(m_AppState.skin.theMap.callout_newspaper.txt, "Talk to reporters about writing an article on your issue.");
					
					TweenLite.delayedCall(1, centerPopupTextVertically, null, true);
						
					break;
				case AgeConstants.KEY_JUNIORCADETTE:
					m_AppState = new CommunityResources_App_JC();
					m_AppState.Init();
					numAnswers = 4;
					m_AppState.skin.theHitArea.addEventListener(InputController.CLICK, resetPopups);
					m_AppState.skin.theMap.map.addEventListener(InputController.CLICK, resetPopups);
					
					
					m_Buildings.push(m_AppState.skin.theMap.building_publiclibrary,
						m_AppState.skin.theMap.building_recdept,
						m_AppState.skin.theMap.building_classroom,
						m_AppState.skin.theMap.building_pta,
						m_AppState.skin.theMap.building_newsstation,
						m_AppState.skin.theMap.building_home,
						m_AppState.skin.theMap.building_foodbank,
						m_AppState.skin.theMap.building_girlscoutleader,
						m_AppState.skin.theMap.building_cityhall,
						m_AppState.skin.theMap.building_grocerystore,
						m_AppState.skin.theMap.building_addyourown1,
						m_AppState.skin.theMap.building_addyourown2);
					
					m_BuildingsY.push(m_AppState.skin.theMap.building_publiclibrary.y,
						m_AppState.skin.theMap.building_recdept.y,
						m_AppState.skin.theMap.building_classroom.y,
						m_AppState.skin.theMap.building_pta.y,
						m_AppState.skin.theMap.building_newsstation.y,
						m_AppState.skin.theMap.building_home.y,
						m_AppState.skin.theMap.building_foodbank.y,
						m_AppState.skin.theMap.building_girlscoutleader.y,
						m_AppState.skin.theMap.building_cityhall.y,
						m_AppState.skin.theMap.building_grocerystore.y,
						m_AppState.skin.theMap.building_addyourown1.y,
						m_AppState.skin.theMap.building_addyourown2.y);
				
					m_InfoTargets.push(m_AppState.skin.theMap.I_publiclibrary,
						m_AppState.skin.theMap.I_recdept,
						m_AppState.skin.theMap.I_classroom,
						m_AppState.skin.theMap.I_pta,
						m_AppState.skin.theMap.I_newsstation,
						m_AppState.skin.theMap.I_home,
						m_AppState.skin.theMap.I_foodbank,
						m_AppState.skin.theMap.I_girlscoutleader,
						m_AppState.skin.theMap.I_cityhall,
						m_AppState.skin.theMap.I_grocerystore);

					m_Callouts.push(m_AppState.skin.theMap.callout_publiclibrary,
						m_AppState.skin.theMap.callout_recdept,
						m_AppState.skin.theMap.callout_classroom,
						m_AppState.skin.theMap.callout_pta,
						m_AppState.skin.theMap.callout_newsstation,
						m_AppState.skin.theMap.callout_home,
						m_AppState.skin.theMap.callout_foodbank,
						m_AppState.skin.theMap.callout_girlscoutleader,
						m_AppState.skin.theMap.callout_grocerystore,
						m_AppState.skin.theMap.callout_cityhall);
						
					ageLevelTextFormat = new TextFormat(m_AppState.bodyFont, (m_AppState.skin.theMap.callout_publiclibrary.txt as TextField).defaultTextFormat.size, (m_AppState.skin.theMap.callout_publiclibrary.txt as TextField).defaultTextFormat.color, false);
					resetCallback = resetPopups;
					
					initCallOut(m_AppState.skin.theMap.callout_publiclibrary.txt, "Research your issue by finding books, films, and internet resources on subjects related to your issue.");
					initCallOut(m_AppState.skin.theMap.callout_recdept.txt, "Here you could promote interest among community members, and look for volunteers.");
					initCallOut(m_AppState.skin.theMap.callout_classroom.txt, "Your peers, teachers, and even the principal may be willing to help create change for your issue.");
					initCallOut(m_AppState.skin.theMap.callout_pta.txt, "The Parent Teacher Association holds monthly meetings where you could present your issue and ask their advice.");
					initCallOut(m_AppState.skin.theMap.callout_newsstation.txt, "Compare your research with what journalists know about your issue, and promote it on the local news.");
					initCallOut(m_AppState.skin.theMap.callout_home.txt, "It’s right under your nose! You can get ideas and help from your parents and other family members.");
					initCallOut(m_AppState.skin.theMap.callout_foodbank.txt, "Talking to the volunteers and families at the food bank could help you collect information on your issue.");
					initCallOut(m_AppState.skin.theMap.callout_girlscoutleader.txt, "Present your ideas to your leader and see what advice he/she may have.");
					initCallOut(m_AppState.skin.theMap.callout_grocerystore.txt, "This is a great spot for free advertising! You could hang up a poster about your issue on the store’s bulletin board.");
					initCallOut(m_AppState.skin.theMap.callout_cityhall.txt, "Try attending a local meeting to present your issue to the mayor and other officials.");
					
					TweenLite.delayedCall(1, centerPopupTextVertically, null, true);
					
					
					this.addYourOwnFields.push(
						new TextField_AddYourOwnResource(m_AppState.skin.theMap.JC_addyourownbox_1, m_AppState.skin.theMap, m_AppState.bodyFont, 0xDF2131, 0x0CB1A5),
						new TextField_AddYourOwnResource(m_AppState.skin.theMap.JC_addyourownbox_2, m_AppState.skin.theMap, m_AppState.bodyFont, 0xDF2131, 0x0CB1A5)
					);
					m_AppState.skin.theMap.JC_addyourownbox_1.gotoAndStop(1);
					m_AppState.skin.theMap.JC_addyourownbox_2.gotoAndStop(1);
					
					CitrusFramework.frameworkStage.addEventListener(KeyboardEvent.KEY_DOWN, this.receiveChar);

					break;
				case AgeConstants.KEY_SENIORAMBASSADOR:
					m_AppState = new CommunityResources_App_SA();
					m_AppState.Init();
					numAnswers = 5;
					m_AppState.skin.theHitArea.addEventListener(InputController.CLICK, resetPopups);
					m_AppState.skin.theMap.map.addEventListener(InputController.CLICK, resetPopups);
					
					m_Buildings.push(m_AppState.skin.theMap.building_countyofficebuilding,
						m_AppState.skin.theMap.building_academiclibrary,
						m_AppState.skin.theMap.building_publicaccesstv,
						m_AppState.skin.theMap.building_girlscoutcommunity,
						m_AppState.skin.theMap.building_civiccenter,
						m_AppState.skin.theMap.building_bank,
						m_AppState.skin.theMap.building_boardofeducation,
						m_AppState.skin.theMap.building_addyourown1,
						m_AppState.skin.theMap.building_addyourown2,
						m_AppState.skin.theMap.building_addyourown3,
						m_AppState.skin.theMap.building_addyourown4);
					
					m_BuildingsY.push(m_AppState.skin.theMap.building_countyofficebuilding.y,
						m_AppState.skin.theMap.building_academiclibrary.y,
						m_AppState.skin.theMap.building_publicaccesstv.y,
						m_AppState.skin.theMap.building_girlscoutcommunity.y,
						m_AppState.skin.theMap.building_civiccenter.y,
						m_AppState.skin.theMap.building_bank.y,
						m_AppState.skin.theMap.building_boardofeducation.y,
						m_AppState.skin.theMap.building_addyourown1.y,
						m_AppState.skin.theMap.building_addyourown2.y,
						m_AppState.skin.theMap.building_addyourown3.y,
						m_AppState.skin.theMap.building_addyourown4.y);
						
					m_InfoTargets.push(m_AppState.skin.theMap.I_countyofficebuilding,
						m_AppState.skin.theMap.I_academiclibrary,
						m_AppState.skin.theMap.I_publicaccesstv,
						m_AppState.skin.theMap.I_girlscoutcommunity,
						m_AppState.skin.theMap.I_civiccenter,
						m_AppState.skin.theMap.I_bank,
						m_AppState.skin.theMap.I_boardofeducation);

					m_Callouts.push(m_AppState.skin.theMap.callout_countyofficebuilding,
						m_AppState.skin.theMap.callout_academiclibrary,
						m_AppState.skin.theMap.callout_publicaccesstv,
						m_AppState.skin.theMap.callout_girlscoutcommunity,
						m_AppState.skin.theMap.callout_civiccenter,
						m_AppState.skin.theMap.callout_bank,
						m_AppState.skin.theMap.callout_boardofeducation);
						
					ageLevelTextFormat = new TextFormat(m_AppState.bodyFont, (m_AppState.skin.theMap.callout_countyofficebuilding.txt as TextField).defaultTextFormat.size, (m_AppState.skin.theMap.callout_countyofficebuilding.txt as TextField).defaultTextFormat.color, false);
					resetCallback = resetPopups;
					
					initCallOut(m_AppState.skin.theMap.callout_countyofficebuilding.txt, "The county legislator works here, and helps set laws and policies within your county. He/she could help make change for your issue.");
					initCallOut(m_AppState.skin.theMap.callout_academiclibrary.txt, "This is a great space for researching your issue, since university libraries have more detailed information on things like science and law.");
					initCallOut(m_AppState.skin.theMap.callout_publicaccesstv.txt, "Here anyone in the general public can create a local TV program. You could use the space to make a commercial, promoting your project.");
					initCallOut(m_AppState.skin.theMap.callout_girlscoutcommunity.txt, "Create a presentation to show other Girl Scouts and leaders your issue & ideas. They can offer valuable feedback on your project.");
					initCallOut(m_AppState.skin.theMap.callout_civiccenter.txt, "This is the town’s business district, which usually has spaces for recreation, conventions, and meetings. You could seek lots of volunteers here.");
					initCallOut(m_AppState.skin.theMap.callout_bank.txt, "Financial planners at the bank are great resources for providing guidance on setting and following your budget; a crucial part of the project.");
					initCallOut(m_AppState.skin.theMap.callout_boardofeducation.txt, "The school board and superintendent help create educational policies. You could present your issue at a committee meeting to see how they could help.");
					
					TweenLite.delayedCall(1, centerPopupTextVertically, null, true);
					
					this.addYourOwnFields.push(
						new TextField_AddYourOwnResource(m_AppState.skin.theMap.SA_addyourownbox_1, m_AppState.skin.theMap, m_AppState.bodyFont, 0xFAA61B, 0x6BCBD9),
						new TextField_AddYourOwnResource(m_AppState.skin.theMap.SA_addyourownbox_2, m_AppState.skin.theMap, m_AppState.bodyFont, 0xFAA61B, 0x6BCBD9),
						new TextField_AddYourOwnResource(m_AppState.skin.theMap.SA_addyourownbox_3, m_AppState.skin.theMap, m_AppState.bodyFont, 0xFAA61B, 0x6BCBD9),
						new TextField_AddYourOwnResource(m_AppState.skin.theMap.SA_addyourownbox_4, m_AppState.skin.theMap, m_AppState.bodyFont, 0xFAA61B, 0x6BCBD9)
					);
					m_AppState.skin.theMap.SA_addyourownbox_1.gotoAndStop(1);
					m_AppState.skin.theMap.SA_addyourownbox_2.gotoAndStop(1);
					m_AppState.skin.theMap.SA_addyourownbox_3.gotoAndStop(1);
					m_AppState.skin.theMap.SA_addyourownbox_4.gotoAndStop(1);
					
					CitrusFramework.frameworkStage.addEventListener(KeyboardEvent.KEY_DOWN, this.receiveChar);

					break;
			}
			for each (var field:TextField_AddYourOwnResource in this.addYourOwnFields) 
			{
				field.addEventListener(InputController.DOWN, this.setActiveTextFieldByMouseClick);
				field.addEventListener(InputController.UP, this.setActiveTextFieldByMouseClickUp);
				field.addEventListener(TextField_AddYourOwnResource.TEXT_ENTERED, this.selectBuildingIfNeeded);
			}
			for(var index:int = 0; index < this.addYourOwnFields.length; index++)
			{
				this.addYourOwnFields[index].asset.txt.tabIndex = index;
			}
			CitrusFramework.frameworkStage.addEventListener(InputController.UP, this.keepBuildingsSelectedCorrect);
			
			for each (var building:MovieClip in m_Buildings)
			{
				building.addEventListener(InputController.CLICK, this.buildingPressed);
				building.gotoAndStop(1);
			}
			
			for each (var infoTarget:MovieClip in m_InfoTargets)
			{
				infoTarget.addEventListener(InputController.CLICK, this.infoPressed);
			}
			
			for each (var callout:MovieClip in m_Callouts)
			{
				(callout.txt as TextField).defaultTextFormat = ageLevelTextFormat;
				(callout.txt as TextField).embedFonts = true;
				(callout.txt as TextField).setTextFormat(ageLevelTextFormat);
				callout.addEventListener(InputController.CLICK, resetCallback);
			}
			
			submitY = m_AppState.skin.submit_Button.y;
			m_AppState.skin.submit_Button.y = submitY + m_AppState.skin.submit_Button.height;
			
			CitrusFramework.contentLayer.addChild(m_AppState);
			m_AppState.skin.setChildIndex(m_AppState.skin.submit_Button, m_AppState.skin.numChildren - 1);
			
			chooseBuilding();
			
			EndInit();
		}
		
		private function initCallOut(textField:TextField, text:String):void
		{
			textField.text = text;
			textField.selectable = false;
		}
		
		private function centerPopupTextVertically():void
		{
			for each(var callout:MovieClip in m_Callouts)
			{
				callout.visible = true;
				callout.txt.y = callout.txt.y + callout.txt.height / 2 - callout.txt.textHeight / 2;
			}
			this.resetPopups();
		}
		
		private function chooseBuilding():void
		{
			var aBuilding:int = Math.floor(Math.random() * m_Buildings.length);
			TweenMax.to(m_Buildings[aBuilding], .2, {y:m_Buildings[aBuilding].y - 50, onComplete:chooseAgain, onCompleteParams:[aBuilding]});
		}
		
		private function chooseAgain(aBuilding:int):void
		{
			TweenMax.to(m_Buildings[aBuilding], 1, {y:m_BuildingsY[aBuilding], ease:Bounce.easeOut, onComplete:chooseBuilding});
		}
		
		private function resetPopups(e:Event = null):void
		{
			for each (var callout:MovieClip in m_Callouts)
			{
				callout.visible = false;
			}
			
			for each (var infoTarget:MovieClip in m_InfoTargets)
			{
				infoTarget.removeEventListener(InputController.CLICK, infoPressed);
				infoTarget.addEventListener(InputController.CLICK, infoPressed);
				infoTarget.removeEventListener(InputController.CLICK, infoPressedAgain);
			}
		}
		
		private function buildingPressed(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_SELECTRESOURCES[SessionInformation_GirlsTablet.Age]);
			var nameString:String = "";
			if(e is MouseEvent)
			{
				var mEvent:MouseEvent = (e as MouseEvent);
				nameString = mEvent.target.name;
			}
			
			if(e is TouchEvent)
			{
				var tEvent:TouchEvent = (e as TouchEvent);
				nameString = tEvent.target.name;
			}
			
			selectBuilding(nameString);
		}
		
		private function selectBuilding(nameString:String):void
		{
			if (numSelected >= numAnswers) return;
			if(buildingSelections.indexOf(nameString) == -1) 
			{
				buildingSelections.push(nameString);
			}
			
			switch (SessionInformation_GirlsTablet.Age)
			{
				case AgeConstants.KEY_DAISYBROWNIE:
					if (numSelected < numAnswers)
					{
						resetPopups();
						switch(nameString)
						{
							case "building_newspaper":
								if (m_AppState.skin.theMap.building_newspaper.currentFrame < 2)
								{
									numSelected++;
								}
								m_AppState.skin.theMap.building_newspaper.gotoAndStop(2);
								m_AppState.skin.theMap.building_newspaper.removeEventListener(InputController.CLICK, buildingPressed);
								m_AppState.skin.theMap.building_newspaper.addEventListener(InputController.CLICK, buildingPressedAgain);
								
								break;
							case "building_home":
								if (m_AppState.skin.theMap.building_home.currentFrame < 2)
								{
									numSelected++;
								}
								m_AppState.skin.theMap.building_home.gotoAndStop(2);
								m_AppState.skin.theMap.building_home.removeEventListener(InputController.CLICK, buildingPressed);
								m_AppState.skin.theMap.building_home.addEventListener(InputController.CLICK, buildingPressedAgain);
								break;
							case "building_communitycenter":
								if (m_AppState.skin.theMap.building_communitycenter.currentFrame < 2)
								{
									numSelected++;
								}
								m_AppState.skin.theMap.building_communitycenter.gotoAndStop(2);
								m_AppState.skin.theMap.building_communitycenter.removeEventListener(InputController.CLICK, buildingPressed);
								m_AppState.skin.theMap.building_communitycenter.addEventListener(InputController.CLICK, buildingPressedAgain);
								break;
							case "building_classroom":
								if (m_AppState.skin.theMap.building_classroom.currentFrame < 2)
								{
									numSelected++;
								}
								m_AppState.skin.theMap.building_classroom.gotoAndStop(2);
								m_AppState.skin.theMap.building_classroom.removeEventListener(InputController.CLICK, buildingPressed);
								m_AppState.skin.theMap.building_classroom.addEventListener(InputController.CLICK, buildingPressedAgain);
								break;
							case "building_townhall":
								if (m_AppState.skin.theMap.building_townhall.currentFrame < 2)
								{
									numSelected++;
								}
								m_AppState.skin.theMap.building_townhall.gotoAndStop(2);
								m_AppState.skin.theMap.building_townhall.removeEventListener(InputController.CLICK, buildingPressed);
								m_AppState.skin.theMap.building_townhall.addEventListener(InputController.CLICK, buildingPressedAgain);
								break;
							case "building_library":
								if (m_AppState.skin.theMap.building_library.currentFrame < 2)
								{
									numSelected++;
								}
								m_AppState.skin.theMap.building_library.gotoAndStop(2);
								m_AppState.skin.theMap.building_library.removeEventListener(InputController.CLICK, buildingPressed);
								m_AppState.skin.theMap.building_library.addEventListener(InputController.CLICK, buildingPressedAgain);
								break;
							case "building_girlscoutmeeting":
								if (m_AppState.skin.theMap.building_girlscoutmeeting.currentFrame < 2)
								{
									numSelected++;
								}
								m_AppState.skin.theMap.building_girlscoutmeeting.gotoAndStop(2);
								m_AppState.skin.theMap.building_girlscoutmeeting.removeEventListener(InputController.CLICK, buildingPressed);
								m_AppState.skin.theMap.building_girlscoutmeeting.addEventListener(InputController.CLICK, buildingPressedAgain);
								break;
						}
					}
					break;
				case AgeConstants.KEY_JUNIORCADETTE:
					if (numSelected < numAnswers)
					{
						resetPopups();
						switch (nameString)
						{
							case "building_publiclibrary":
								if (m_AppState.skin.theMap.building_publiclibrary.currentFrame < 2)
								{
									numSelected++;
								}
								m_AppState.skin.theMap.building_publiclibrary.gotoAndStop(2);
								m_AppState.skin.theMap.building_publiclibrary.removeEventListener(InputController.CLICK, buildingPressed);
								m_AppState.skin.theMap.building_publiclibrary.addEventListener(InputController.CLICK, buildingPressedAgain);
								break;
							case "building_recdept":
								if (m_AppState.skin.theMap.building_recdept.currentFrame < 2)
								{
									numSelected++;
								}
								m_AppState.skin.theMap.building_recdept.gotoAndStop(2);
								m_AppState.skin.theMap.building_recdept.removeEventListener(InputController.CLICK, buildingPressed);
								m_AppState.skin.theMap.building_recdept.addEventListener(InputController.CLICK, buildingPressedAgain);
								break;
							case "building_classroom":
								if (m_AppState.skin.theMap.building_classroom.currentFrame < 2)
								{
									numSelected++;
								}
								m_AppState.skin.theMap.building_classroom.gotoAndStop(2);
								m_AppState.skin.theMap.building_classroom.removeEventListener(InputController.CLICK, buildingPressed);
								m_AppState.skin.theMap.building_classroom.addEventListener(InputController.CLICK, buildingPressedAgain);
								break;
							case "building_pta":
								if (m_AppState.skin.theMap.building_pta.currentFrame < 2)
								{
									numSelected++;
								}
								m_AppState.skin.theMap.building_pta.gotoAndStop(2);
								m_AppState.skin.theMap.building_pta.removeEventListener(InputController.CLICK, buildingPressed);
								m_AppState.skin.theMap.building_pta.addEventListener(InputController.CLICK, buildingPressedAgain);
								break;
							case "building_newsstation":
								if (m_AppState.skin.theMap.building_newsstation.currentFrame < 2)
								{
									numSelected++;
								}
								m_AppState.skin.theMap.building_newsstation.gotoAndStop(2);
								m_AppState.skin.theMap.building_newsstation.removeEventListener(InputController.CLICK, buildingPressed);
								m_AppState.skin.theMap.building_newsstation.addEventListener(InputController.CLICK, buildingPressedAgain);
								break;
							case "building_home":
								if (m_AppState.skin.theMap.building_home.currentFrame < 2)
								{
									numSelected++;
								}
								m_AppState.skin.theMap.building_home.gotoAndStop(2);
								m_AppState.skin.theMap.building_home.removeEventListener(InputController.CLICK, buildingPressed);
								m_AppState.skin.theMap.building_home.addEventListener(InputController.CLICK, buildingPressedAgain);
								break;
							case "building_foodbank":
								if (m_AppState.skin.theMap.building_foodbank.currentFrame < 2)
								{
									numSelected++;
								}
								m_AppState.skin.theMap.building_foodbank.gotoAndStop(2);
								m_AppState.skin.theMap.building_foodbank.removeEventListener(InputController.CLICK, buildingPressed);
								m_AppState.skin.theMap.building_foodbank.addEventListener(InputController.CLICK, buildingPressedAgain);
								break;
							case "building_girlscoutleader":
								if (m_AppState.skin.theMap.building_girlscoutleader.currentFrame < 2)
								{
									numSelected++;
								}
								m_AppState.skin.theMap.building_girlscoutleader.gotoAndStop(2);
								m_AppState.skin.theMap.building_girlscoutleader.removeEventListener(InputController.CLICK, buildingPressed);
								m_AppState.skin.theMap.building_girlscoutleader.addEventListener(InputController.CLICK, buildingPressedAgain);
								break;
							case "building_cityhall":
								if (m_AppState.skin.theMap.building_cityhall.currentFrame < 2)
								{
									numSelected++;
								}
								m_AppState.skin.theMap.building_cityhall.gotoAndStop(2);
								m_AppState.skin.theMap.building_cityhall.removeEventListener(InputController.CLICK, buildingPressed);
								m_AppState.skin.theMap.building_cityhall.addEventListener(InputController.CLICK, buildingPressedAgain);
								break;
							case "building_grocerystore":
								if (m_AppState.skin.theMap.building_grocerystore.currentFrame < 2)
								{
									numSelected++;
								}
								m_AppState.skin.theMap.building_grocerystore.gotoAndStop(2);
								m_AppState.skin.theMap.building_grocerystore.removeEventListener(InputController.CLICK, buildingPressed);
								m_AppState.skin.theMap.building_grocerystore.addEventListener(InputController.CLICK, buildingPressedAgain);
								break;
							case "building_addyourown1":
								if(this.addYourOwnFields[0].asset.txt.text.length > 0 && this.addYourOwnFields[0].asset.txt.text != TextField_AddYourOwnResource.ms_DefaultText)
								{
									// there is text in the field and it is not the default text... select the building
									if (m_AppState.skin.theMap.building_addyourown1.currentFrame < 2)
									{
										numSelected++;
									}
									m_AppState.skin.theMap.building_addyourown1.gotoAndStop(2);
									m_AppState.skin.theMap.building_addyourown1.removeEventListener(InputController.CLICK, buildingPressed);
									m_AppState.skin.theMap.building_addyourown1.addEventListener(InputController.CLICK, buildingPressedAgain);
									this.addYourOwnFields[0].changeColor(1);
								}
								else
								{
									// there is no txt in the field or its the default text, don't select the building but select the text field
									buildingSelections.pop();
								}
								this.activateTextField(this.addYourOwnFields[0]);
								break;
							case "building_addyourown2":
								if(this.addYourOwnFields[1].asset.txt.text.length > 0 && this.addYourOwnFields[1].asset.txt.text != TextField_AddYourOwnResource.ms_DefaultText)
								{
									// there is text in the field and it is not the default text... select the building
									if (m_AppState.skin.theMap.building_addyourown2.currentFrame < 2)
									{
										numSelected++;
									}
									m_AppState.skin.theMap.building_addyourown2.gotoAndStop(2);
									m_AppState.skin.theMap.building_addyourown2.removeEventListener(InputController.CLICK, buildingPressed);
									m_AppState.skin.theMap.building_addyourown2.addEventListener(InputController.CLICK, buildingPressedAgain);
									this.addYourOwnFields[1].changeColor(1);
								}
								else
								{
									// there is no txt in the field or its the default text, don't select the building but select the text field
									buildingSelections.pop();
								}
								this.activateTextField(this.addYourOwnFields[1]);
								break;
						}
					}
					break;
				case AgeConstants.KEY_SENIORAMBASSADOR:
					if (numSelected < numAnswers)
					{
						resetPopups();
						switch (nameString)
						{
							case "building_countyofficebuilding":
								if (m_AppState.skin.theMap.building_countyofficebuilding.currentFrame < 2)
								{
									numSelected++;
								}
								m_AppState.skin.theMap.building_countyofficebuilding.gotoAndStop(2);
								m_AppState.skin.theMap.building_countyofficebuilding.removeEventListener(InputController.CLICK, buildingPressed);
								m_AppState.skin.theMap.building_countyofficebuilding.addEventListener(InputController.CLICK, buildingPressedAgain);
								break;
							case "building_academiclibrary":
								if (m_AppState.skin.theMap.building_academiclibrary.currentFrame < 2)
								{
									numSelected++;
								}
								m_AppState.skin.theMap.building_academiclibrary.gotoAndStop(2);
								m_AppState.skin.theMap.building_academiclibrary.removeEventListener(InputController.CLICK, buildingPressed);
								m_AppState.skin.theMap.building_academiclibrary.addEventListener(InputController.CLICK, buildingPressedAgain);
								break;
							case "building_publicaccesstv":
								if (m_AppState.skin.theMap.building_publicaccesstv.currentFrame < 2)
								{
									numSelected++;
								}
								m_AppState.skin.theMap.building_publicaccesstv.gotoAndStop(2);
								m_AppState.skin.theMap.building_publicaccesstv.removeEventListener(InputController.CLICK, buildingPressed);
								m_AppState.skin.theMap.building_publicaccesstv.addEventListener(InputController.CLICK, buildingPressedAgain);
								break;
							case "building_girlscoutcommunity":
								if (m_AppState.skin.theMap.building_girlscoutcommunity.currentFrame < 2)
								{
									numSelected++;
								}
								m_AppState.skin.theMap.building_girlscoutcommunity.gotoAndStop(2);
								m_AppState.skin.theMap.building_girlscoutcommunity.removeEventListener(InputController.CLICK, buildingPressed);
								m_AppState.skin.theMap.building_girlscoutcommunity.addEventListener(InputController.CLICK, buildingPressedAgain);
								break;
							case "building_civiccenter":
								if (m_AppState.skin.theMap.building_civiccenter.currentFrame < 2)
								{
									numSelected++;
								}
								m_AppState.skin.theMap.building_civiccenter.gotoAndStop(2);
								m_AppState.skin.theMap.building_civiccenter.removeEventListener(InputController.CLICK, buildingPressed);
								m_AppState.skin.theMap.building_civiccenter.addEventListener(InputController.CLICK, buildingPressedAgain);
								break;
							case "building_bank":
								if (m_AppState.skin.theMap.building_bank.currentFrame < 2)
								{
									numSelected++;
								}
								m_AppState.skin.theMap.building_bank.gotoAndStop(2);
								m_AppState.skin.theMap.building_bank.removeEventListener(InputController.CLICK, buildingPressed);
								m_AppState.skin.theMap.building_bank.addEventListener(InputController.CLICK, buildingPressedAgain);
								break;
							case "building_boardofeducation":
								if (m_AppState.skin.theMap.building_boardofeducation.currentFrame < 2)
								{
									numSelected++;
								}
								m_AppState.skin.theMap.building_boardofeducation.gotoAndStop(2);
								m_AppState.skin.theMap.building_boardofeducation.removeEventListener(InputController.CLICK, buildingPressed);
								m_AppState.skin.theMap.building_boardofeducation.addEventListener(InputController.CLICK, buildingPressedAgain);
								break;
							case "building_addyourown1":
								if(StringUtil.trim(this.addYourOwnFields[0].asset.txt.text) && this.addYourOwnFields[0].asset.txt.text.length > 0 && this.addYourOwnFields[0].asset.txt.text != TextField_AddYourOwnResource.ms_DefaultText)
								{
									// there is text in the field and it is not the default text... select the building
									if (m_AppState.skin.theMap.building_addyourown1.currentFrame < 2)
									{
										numSelected++;
									}
									m_AppState.skin.theMap.building_addyourown1.gotoAndStop(2);
									m_AppState.skin.theMap.building_addyourown1.removeEventListener(InputController.CLICK, buildingPressed);
									m_AppState.skin.theMap.building_addyourown1.addEventListener(InputController.CLICK, buildingPressedAgain);
									this.addYourOwnFields[0].changeColor(1);
								}
								else
								{
									// there is no txt in the field or its the default text, don't select the building but select the text field
									buildingSelections.pop();
								}
								this.activateTextField(this.addYourOwnFields[0]);
								break;
							case "building_addyourown2":
								if(StringUtil.trim(this.addYourOwnFields[1].asset.txt.text) && this.addYourOwnFields[1].asset.txt.text.length > 0 && this.addYourOwnFields[1].asset.txt.text != TextField_AddYourOwnResource.ms_DefaultText)
								{
									// there is text in the field and it is not the default text... select the building
									if (m_AppState.skin.theMap.building_addyourown2.currentFrame < 2)
									{
										numSelected++;
									}
									m_AppState.skin.theMap.building_addyourown2.gotoAndStop(2);
									m_AppState.skin.theMap.building_addyourown2.removeEventListener(InputController.CLICK, buildingPressed);
									m_AppState.skin.theMap.building_addyourown2.addEventListener(InputController.CLICK, buildingPressedAgain);
									this.addYourOwnFields[1].changeColor(1);
								}
								else
								{
									// there is no txt in the field or its the default text, don't select the building but select the text field
									buildingSelections.pop();
								}
								this.activateTextField(this.addYourOwnFields[1]);
								break;
							case "building_addyourown3":
								if(StringUtil.trim(this.addYourOwnFields[2].asset.txt.text) && this.addYourOwnFields[2].asset.txt.text.length > 0 && this.addYourOwnFields[2].asset.txt.text != TextField_AddYourOwnResource.ms_DefaultText)
								{
									// there is text in the field and it is not the default text... select the building
									if (m_AppState.skin.theMap.building_addyourown3.currentFrame < 2)
									{
										numSelected++;
									}
									m_AppState.skin.theMap.building_addyourown3.gotoAndStop(2);
									m_AppState.skin.theMap.building_addyourown3.removeEventListener(InputController.CLICK, buildingPressed);
									m_AppState.skin.theMap.building_addyourown3.addEventListener(InputController.CLICK, buildingPressedAgain);
									this.addYourOwnFields[2].changeColor(1);
								}
								else
								{
									// there is no txt in the field or its the default text, don't select the building but select the text field
									buildingSelections.pop();
								}
								this.activateTextField(this.addYourOwnFields[2]);
								break;
							case "building_addyourown4":
								if(StringUtil.trim(this.addYourOwnFields[3].asset.txt.text) && this.addYourOwnFields[3].asset.txt.text.length > 0 && this.addYourOwnFields[3].asset.txt.text != TextField_AddYourOwnResource.ms_DefaultText)
								{
									// there is text in the field and it is not the default text... select the building
									if (m_AppState.skin.theMap.building_addyourown4.currentFrame < 2)
									{
										numSelected++;
									}
									m_AppState.skin.theMap.building_addyourown4.gotoAndStop(2);
									m_AppState.skin.theMap.building_addyourown4.removeEventListener(InputController.CLICK, buildingPressed);
									m_AppState.skin.theMap.building_addyourown4.addEventListener(InputController.CLICK, buildingPressedAgain);
									this.addYourOwnFields[3].changeColor(1);
								}
								else
								{
									// there is no txt in the field or its the default text, don't select the building but select the text field
									buildingSelections.pop();
								}
								this.activateTextField(this.addYourOwnFields[3]);
								break;
						}
					}
					break;
			}
			
			if (numSelected == numAnswers)
			{
				TweenMax.to(m_AppState.skin.submit_Button, .5, {y:submitY});
				m_AppState.skin.submit_Button.addEventListener(InputController.CLICK, submitData);
			}
		}

		
		private function buildingPressedAgain(e:Event):void
		{	
			SoundHandler.PlaySound(GSOC_SFX.SOUND_SELECTRESOURCES[SessionInformation_GirlsTablet.Age]);
			var nameString:String = "";
			if(e is MouseEvent)
			{
				var mEvent:MouseEvent = (e as MouseEvent);
				nameString = mEvent.target.name;
			}
			
			if(e is TouchEvent)
			{
				var tEvent:TouchEvent = (e as TouchEvent);
				nameString = tEvent.target.name;
			}
			numSelected--;
			
			unSelectBuilding(nameString);
		}
		
		private function unSelectBuilding(nameString:String):void
		{
			switch (SessionInformation_GirlsTablet.Age)
			{
				case AgeConstants.KEY_DAISYBROWNIE:
					resetPopups();
					switch(nameString)
					{
						case "building_newspaper":
							m_AppState.skin.theMap.building_newspaper.gotoAndStop(1);
							m_AppState.skin.theMap.building_newspaper.addEventListener(InputController.CLICK, buildingPressed);
							m_AppState.skin.theMap.building_newspaper.removeEventListener(InputController.CLICK, buildingPressedAgain);
							break;
						case "building_home":
							m_AppState.skin.theMap.building_home.gotoAndStop(1);
							m_AppState.skin.theMap.building_home.addEventListener(InputController.CLICK, buildingPressed);
							m_AppState.skin.theMap.building_home.removeEventListener(InputController.CLICK, buildingPressedAgain);
							break;
						case "building_communitycenter":
							m_AppState.skin.theMap.building_communitycenter.gotoAndStop(1);
							m_AppState.skin.theMap.building_communitycenter.addEventListener(InputController.CLICK, buildingPressed);
							m_AppState.skin.theMap.building_communitycenter.removeEventListener(InputController.CLICK, buildingPressedAgain);
							break;
						case "building_classroom":
							m_AppState.skin.theMap.building_classroom.gotoAndStop(1);
							m_AppState.skin.theMap.building_classroom.addEventListener(InputController.CLICK, buildingPressed);
							m_AppState.skin.theMap.building_classroom.removeEventListener(InputController.CLICK, buildingPressedAgain);
							break;
						case "building_townhall":
							m_AppState.skin.theMap.building_townhall.gotoAndStop(1);
							m_AppState.skin.theMap.building_townhall.addEventListener(InputController.CLICK, buildingPressed);
							m_AppState.skin.theMap.building_townhall.removeEventListener(InputController.CLICK, buildingPressedAgain);
							break;
						case "building_library":
							m_AppState.skin.theMap.building_library.gotoAndStop(1);
							m_AppState.skin.theMap.building_library.addEventListener(InputController.CLICK, buildingPressed);
							m_AppState.skin.theMap.building_library.removeEventListener(InputController.CLICK, buildingPressedAgain);
							break;
						case "building_girlscoutmeeting":
							m_AppState.skin.theMap.building_girlscoutmeeting.gotoAndStop(1);
							m_AppState.skin.theMap.building_girlscoutmeeting.addEventListener(InputController.CLICK, buildingPressed);
							m_AppState.skin.theMap.building_girlscoutmeeting.removeEventListener(InputController.CLICK, buildingPressedAgain);
							break;
					}
					break;
				case AgeConstants.KEY_JUNIORCADETTE:
					resetPopups();
					switch (nameString)
					{
						case "building_publiclibrary":
							m_AppState.skin.theMap.building_publiclibrary.gotoAndStop(1);
							m_AppState.skin.theMap.building_publiclibrary.addEventListener(InputController.CLICK, buildingPressed);
							m_AppState.skin.theMap.building_publiclibrary.removeEventListener(InputController.CLICK, buildingPressedAgain);
							break;
						case "building_recdept":
							m_AppState.skin.theMap.building_recdept.gotoAndStop(1);
							m_AppState.skin.theMap.building_recdept.addEventListener(InputController.CLICK, buildingPressed);
							m_AppState.skin.theMap.building_recdept.removeEventListener(InputController.CLICK, buildingPressedAgain);
							break;
						case "building_classroom":
							m_AppState.skin.theMap.building_classroom.gotoAndStop(1);
							m_AppState.skin.theMap.building_classroom.addEventListener(InputController.CLICK, buildingPressed);
							m_AppState.skin.theMap.building_classroom.removeEventListener(InputController.CLICK, buildingPressedAgain);
							break;
						case "building_pta":
							m_AppState.skin.theMap.building_pta.gotoAndStop(1);
							m_AppState.skin.theMap.building_pta.addEventListener(InputController.CLICK, buildingPressed);
							m_AppState.skin.theMap.building_pta.removeEventListener(InputController.CLICK, buildingPressedAgain);
							break;
						case "building_newsstation":
							m_AppState.skin.theMap.building_newsstation.gotoAndStop(1);
							m_AppState.skin.theMap.building_newsstation.addEventListener(InputController.CLICK, buildingPressed);
							m_AppState.skin.theMap.building_newsstation.removeEventListener(InputController.CLICK, buildingPressedAgain);
							break;
						case "building_home":
							m_AppState.skin.theMap.building_home.gotoAndStop(1);
							m_AppState.skin.theMap.building_home.addEventListener(InputController.CLICK, buildingPressed);
							m_AppState.skin.theMap.building_home.removeEventListener(InputController.CLICK, buildingPressedAgain);
							break;
						case "building_foodbank":
							m_AppState.skin.theMap.building_foodbank.gotoAndStop(1);
							m_AppState.skin.theMap.building_foodbank.addEventListener(InputController.CLICK, buildingPressed);
							m_AppState.skin.theMap.building_foodbank.removeEventListener(InputController.CLICK, buildingPressedAgain);
							break;
						case "building_girlscoutleader":
							m_AppState.skin.theMap.building_girlscoutleader.gotoAndStop(1);
							m_AppState.skin.theMap.building_girlscoutleader.addEventListener(InputController.CLICK, buildingPressed);
							m_AppState.skin.theMap.building_girlscoutleader.removeEventListener(InputController.CLICK, buildingPressedAgain);
							break;
						case "building_cityhall":
							m_AppState.skin.theMap.building_cityhall.gotoAndStop(1);
							m_AppState.skin.theMap.building_cityhall.addEventListener(InputController.CLICK, buildingPressed);
							m_AppState.skin.theMap.building_cityhall.removeEventListener(InputController.CLICK, buildingPressedAgain);
							break;
						case "building_grocerystore":
							m_AppState.skin.theMap.building_grocerystore.gotoAndStop(1);
							m_AppState.skin.theMap.building_grocerystore.addEventListener(InputController.CLICK, buildingPressed);
							m_AppState.skin.theMap.building_grocerystore.removeEventListener(InputController.CLICK, buildingPressedAgain);
							break;
						case "building_addyourown1":
							m_AppState.skin.theMap.building_addyourown1.gotoAndStop(1);
							m_AppState.skin.theMap.building_addyourown1.addEventListener(InputController.CLICK, buildingPressed);
							m_AppState.skin.theMap.building_addyourown1.removeEventListener(InputController.CLICK, buildingPressedAgain);
							this.addYourOwnFields[0].changeColor(0);
							break;
						case "building_addyourown2":
							m_AppState.skin.theMap.building_addyourown2.gotoAndStop(1);
							m_AppState.skin.theMap.building_addyourown2.addEventListener(InputController.CLICK, buildingPressed);
							m_AppState.skin.theMap.building_addyourown2.removeEventListener(InputController.CLICK, buildingPressedAgain);
							this.addYourOwnFields[1].changeColor(0);
							break;
					}
					break;
				case AgeConstants.KEY_SENIORAMBASSADOR:
					resetPopups();
					switch (nameString)
					{
						case "building_countyofficebuilding":
							m_AppState.skin.theMap.building_countyofficebuilding.gotoAndStop(1);
							m_AppState.skin.theMap.building_countyofficebuilding.addEventListener(InputController.CLICK, buildingPressed);
							m_AppState.skin.theMap.building_countyofficebuilding.removeEventListener(InputController.CLICK, buildingPressedAgain);
							break;
						case "building_academiclibrary":
							m_AppState.skin.theMap.building_academiclibrary.gotoAndStop(1);
							m_AppState.skin.theMap.building_academiclibrary.addEventListener(InputController.CLICK, buildingPressed);
							m_AppState.skin.theMap.building_academiclibrary.removeEventListener(InputController.CLICK, buildingPressedAgain);
							break;
						case "building_publicaccesstv":
							m_AppState.skin.theMap.building_publicaccesstv.gotoAndStop(1);
							m_AppState.skin.theMap.building_publicaccesstv.addEventListener(InputController.CLICK, buildingPressed);
							m_AppState.skin.theMap.building_publicaccesstv.removeEventListener(InputController.CLICK, buildingPressedAgain);
							break;
						case "building_girlscoutcommunity":
							m_AppState.skin.theMap.building_girlscoutcommunity.gotoAndStop(1);
							m_AppState.skin.theMap.building_girlscoutcommunity.addEventListener(InputController.CLICK, buildingPressed);
							m_AppState.skin.theMap.building_girlscoutcommunity.removeEventListener(InputController.CLICK, buildingPressedAgain);
							break;
						case "building_civiccenter":
							m_AppState.skin.theMap.building_civiccenter.gotoAndStop(1);
							m_AppState.skin.theMap.building_civiccenter.addEventListener(InputController.CLICK, buildingPressed);
							m_AppState.skin.theMap.building_civiccenter.removeEventListener(InputController.CLICK, buildingPressedAgain);
							break;
						case "building_bank":
							m_AppState.skin.theMap.building_bank.gotoAndStop(1);
							m_AppState.skin.theMap.building_bank.addEventListener(InputController.CLICK, buildingPressed);
							m_AppState.skin.theMap.building_bank.removeEventListener(InputController.CLICK, buildingPressedAgain);
							break;
						case "building_boardofeducation":
							m_AppState.skin.theMap.building_boardofeducation.gotoAndStop(1);
							m_AppState.skin.theMap.building_boardofeducation.addEventListener(InputController.CLICK, buildingPressed);
							m_AppState.skin.theMap.building_boardofeducation.removeEventListener(InputController.CLICK, buildingPressedAgain);
							break;
						case "building_addyourown1":
							m_AppState.skin.theMap.building_addyourown1.gotoAndStop(1);
							m_AppState.skin.theMap.building_addyourown1.addEventListener(InputController.CLICK, buildingPressed);
							m_AppState.skin.theMap.building_addyourown1.removeEventListener(InputController.CLICK, buildingPressedAgain);
							this.addYourOwnFields[0].changeColor(0);
							break;
						case "building_addyourown2":
							m_AppState.skin.theMap.building_addyourown2.gotoAndStop(1);
							m_AppState.skin.theMap.building_addyourown2.addEventListener(InputController.CLICK, buildingPressed);
							m_AppState.skin.theMap.building_addyourown2.removeEventListener(InputController.CLICK, buildingPressedAgain);
							this.addYourOwnFields[1].changeColor(0);
							break;
						case "building_addyourown3":
							m_AppState.skin.theMap.building_addyourown3.gotoAndStop(1);
							m_AppState.skin.theMap.building_addyourown3.addEventListener(InputController.CLICK, buildingPressed);
							m_AppState.skin.theMap.building_addyourown3.removeEventListener(InputController.CLICK, buildingPressedAgain);
							this.addYourOwnFields[2].changeColor(0);
							break;
						case "building_addyourown4":
							m_AppState.skin.theMap.building_addyourown4.gotoAndStop(1);
							m_AppState.skin.theMap.building_addyourown4.addEventListener(InputController.CLICK, buildingPressed);
							m_AppState.skin.theMap.building_addyourown4.removeEventListener(InputController.CLICK, buildingPressedAgain);
							this.addYourOwnFields[3].changeColor(0);
							break;
					}
					break;
				
			}
			TweenMax.to(m_AppState.skin.submit_Button, .5, {y:submitY + m_AppState.skin.submit_Button.height});
			
			buildingSelections.removeAt(buildingSelections.indexOf(nameString));
			
			m_AppState.skin.submit_Button.removeEventListener(InputController.CLICK, submitData);
		}
		
		private function infoPressedAgain(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_INFOBUTTONS[SessionInformation_GirlsTablet.Age]);
			var nameString:String = "";
			if(e is MouseEvent)
			{
				var mEvent:MouseEvent = (e as MouseEvent);
				nameString = mEvent.target.name;
			}
			
			if(e is TouchEvent)
			{
				var tEvent:TouchEvent = (e as TouchEvent);
				nameString = tEvent.target.name;
			}
			
			resetPopups();
			e.target.addEventListener(InputController.CLICK, infoPressed);
			e.target.removeEventListener(InputController.CLICK, infoPressedAgain);	
		}
		
		private function infoPressed(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_INFOBUTTONS[SessionInformation_GirlsTablet.Age]);
			var nameString:String = "";
			if(e is MouseEvent)
			{
				var mEvent:MouseEvent = (e as MouseEvent);
				nameString = mEvent.target.name;
			}
			
			if(e is TouchEvent)
			{
				var tEvent:TouchEvent = (e as TouchEvent);
				nameString = tEvent.target.name;
			}
			
			switch (SessionInformation_GirlsTablet.Age)
			{
				case AgeConstants.KEY_DAISYBROWNIE:
					switch(nameString)
					{
						case "I_newspaper":
							resetPopups();
							m_AppState.skin.theMap.callout_newspaper.visible = true;
							m_AppState.skin.theMap.setChildIndex(m_AppState.skin.theMap.callout_newspaper, m_AppState.skin.theMap.numChildren - 1);
							break;
						case "I_home":
							resetPopups();
							m_AppState.skin.theMap.callout_home.visible = true;
							m_AppState.skin.theMap.setChildIndex(m_AppState.skin.theMap.callout_home, m_AppState.skin.theMap.numChildren - 1);
							break;
						case "I_communitycenter":
							resetPopups();
							m_AppState.skin.theMap.callout_communitycenter.visible = true;
							m_AppState.skin.theMap.setChildIndex(m_AppState.skin.theMap.callout_communitycenter, m_AppState.skin.theMap.numChildren - 1);
							break;
						case "I_classroom":
							resetPopups();
							m_AppState.skin.theMap.callout_classroom.visible = true;
							m_AppState.skin.theMap.setChildIndex(m_AppState.skin.theMap.callout_classroom, m_AppState.skin.theMap.numChildren - 1);
							break;
						case "I_townhall":
							resetPopups();
							m_AppState.skin.theMap.callout_townhall.visible = true;
							m_AppState.skin.theMap.setChildIndex(m_AppState.skin.theMap.callout_townhall, m_AppState.skin.theMap.numChildren - 1);
							break;
						case "I_library":
							resetPopups();
							m_AppState.skin.theMap.callout_library.visible = true;
							m_AppState.skin.theMap.setChildIndex(m_AppState.skin.theMap.callout_library, m_AppState.skin.theMap.numChildren - 1);
							break;
						case "I_girlscoutmeeting":
							resetPopups();
							m_AppState.skin.theMap.callout_girlscoutmeeting.visible = true;
							m_AppState.skin.theMap.setChildIndex(m_AppState.skin.theMap.callout_girlscoutmeeting, m_AppState.skin.theMap.numChildren - 1);
							break;
					}
					break;
				case AgeConstants.KEY_JUNIORCADETTE:
					switch (nameString)
					{
						case "I_publiclibrary":
							resetPopups();
							m_AppState.skin.theMap.callout_publiclibrary.visible = true;
							m_AppState.skin.theMap.setChildIndex(m_AppState.skin.theMap.callout_publiclibrary, m_AppState.skin.theMap.numChildren - 1);
							break;
						case "I_recdept":
							resetPopups();
							m_AppState.skin.theMap.callout_recdept.visible = true;
							m_AppState.skin.theMap.setChildIndex(m_AppState.skin.theMap.callout_recdept, m_AppState.skin.theMap.numChildren - 1);
							break;
						case "I_classroom":
							resetPopups();
							m_AppState.skin.theMap.callout_classroom.visible = true;
							m_AppState.skin.theMap.setChildIndex(m_AppState.skin.theMap.callout_classroom, m_AppState.skin.theMap.numChildren - 1);
							break;
						case "I_pta":
							resetPopups();
							m_AppState.skin.theMap.callout_pta.visible = true;
							m_AppState.skin.theMap.setChildIndex(m_AppState.skin.theMap.callout_pta, m_AppState.skin.theMap.numChildren - 1);
							break;
						case "I_newsstation":
							resetPopups();
							m_AppState.skin.theMap.callout_newsstation.visible = true;
							m_AppState.skin.theMap.setChildIndex(m_AppState.skin.theMap.callout_newsstation, m_AppState.skin.theMap.numChildren - 1);
							break;
						case "I_home":
							resetPopups();
							m_AppState.skin.theMap.callout_home.visible = true;
							m_AppState.skin.theMap.setChildIndex(m_AppState.skin.theMap.callout_home, m_AppState.skin.theMap.numChildren - 1);
							break;
						case "I_foodbank":
							resetPopups();
							m_AppState.skin.theMap.callout_foodbank.visible = true;
							m_AppState.skin.theMap.setChildIndex(m_AppState.skin.theMap.callout_foodbank, m_AppState.skin.theMap.numChildren - 1);
							break;
						case "I_girlscoutleader":
							resetPopups();
							m_AppState.skin.theMap.callout_girlscoutleader.visible = true;
							m_AppState.skin.theMap.setChildIndex(m_AppState.skin.theMap.callout_girlscoutleader, m_AppState.skin.theMap.numChildren - 1);
							break;
						case "I_cityhall":
							resetPopups();
							m_AppState.skin.theMap.callout_cityhall.visible = true;
							m_AppState.skin.theMap.setChildIndex(m_AppState.skin.theMap.callout_cityhall, m_AppState.skin.theMap.numChildren - 1);
							break;
						case "I_grocerystore":
							resetPopups();
							m_AppState.skin.theMap.callout_grocerystore.visible = true;
							m_AppState.skin.theMap.setChildIndex(m_AppState.skin.theMap.callout_grocerystore, m_AppState.skin.theMap.numChildren - 1);
							break;
					}
					break;
				case AgeConstants.KEY_SENIORAMBASSADOR:
					switch (nameString)
					{
						case "I_countyofficebuilding":
							resetPopups();
							m_AppState.skin.theMap.callout_countyofficebuilding.visible = true;
							m_AppState.skin.theMap.setChildIndex(m_AppState.skin.theMap.callout_countyofficebuilding, m_AppState.skin.theMap.numChildren - 1);
							break;
						case "I_academiclibrary":
							resetPopups();
							m_AppState.skin.theMap.callout_academiclibrary.visible = true;
							m_AppState.skin.theMap.setChildIndex(m_AppState.skin.theMap.callout_academiclibrary, m_AppState.skin.theMap.numChildren - 1);
							break;
						case "I_publicaccesstv":
							resetPopups();
							m_AppState.skin.theMap.callout_publicaccesstv.visible = true;
							m_AppState.skin.theMap.setChildIndex(m_AppState.skin.theMap.callout_publicaccesstv, m_AppState.skin.theMap.numChildren - 1);
							break;
						case "I_girlscoutcommunity":
							resetPopups();
							m_AppState.skin.theMap.callout_girlscoutcommunity.visible = true;
							m_AppState.skin.theMap.setChildIndex(m_AppState.skin.theMap.callout_girlscoutcommunity, m_AppState.skin.theMap.numChildren - 1);
							break;
						case "I_civiccenter":
							resetPopups();
							m_AppState.skin.theMap.callout_civiccenter.visible = true;
							m_AppState.skin.theMap.setChildIndex(m_AppState.skin.theMap.callout_civiccenter, m_AppState.skin.theMap.numChildren - 1);
							break;
						case "I_bank":
							resetPopups();
							m_AppState.skin.theMap.callout_bank.visible = true;
							m_AppState.skin.theMap.setChildIndex(m_AppState.skin.theMap.callout_bank, m_AppState.skin.theMap.numChildren - 1);
							break;
						case "I_boardofeducation":
							resetPopups();
							m_AppState.skin.theMap.callout_boardofeducation.visible = true;
							m_AppState.skin.theMap.setChildIndex(m_AppState.skin.theMap.callout_boardofeducation, m_AppState.skin.theMap.numChildren - 1);
							break;
					}
					break;
			}
			e.target.removeEventListener(InputController.CLICK, infoPressed);
			e.target.addEventListener(InputController.CLICK, infoPressedAgain);
		}
			
		private function setActiveTextFieldByMouseClick(e:Event = null):void
		{
			var selectedTextfield:TextField_AddYourOwnResource = (e.currentTarget as TextField_AddYourOwnResource);
			this.activateTextField(selectedTextfield, true);
		}
		
		private function activateTextField(textfield:TextField_AddYourOwnResource, select:Boolean = false, setFocus:Boolean = true):void
		{
			m_SelectedTextField = textfield;
			textfield.clearField();
			
			this._activeTextField = this.addYourOwnFields.indexOf(textfield);
			if(select)
			{
				switch(this.addYourOwnFields.indexOf(textfield))
				{
					case 0:
						selectBuilding("building_addyourown1");
						break;
					case 1:
						selectBuilding("building_addyourown2");
						break;
					case 2:
						selectBuilding("building_addyourown3");
						break;
					case 3:
						selectBuilding("building_addyourown4");
						break;
				}
			}
			this.activateSingleTextField(this._activeTextField);	
			this.addYourOwnFields[this._activeTextField].clearField();
			for (var i:int = 0; i < this.addYourOwnFields.length; i++)
			{
				if (i != this._activeTextField)
				{
					this.addYourOwnFields[i].resetText();
				}
			}
			
			if(setFocus)
			{
				CitrusFramework.frameworkStage.focus = textfield.asset.txt;
			}
		}
		
		private function setActiveTextFieldByMouseClickUp(e:Event = null):void
		{
			var selectedTextfield:TextField_AddYourOwnResource = (e.currentTarget as TextField_AddYourOwnResource);
			this.activateTextField(selectedTextfield, false);
		}
		
		private function keepBuildingsSelectedCorrect(e:Event = null):void
		{
			for (var i:int = 0; i < this.addYourOwnFields.length; i++)
			{
				if(this.addYourOwnFields[i].asset.txt.text.length == 0 && this.addYourOwnFields[i].asset.txt.text != TextField_AddYourOwnResource.ms_DefaultText)
				{
					// may be used to re-add the "add your own" text but doesn't work, so if they want that they this needs some tweaking
					//this.addYourOwnFields[i].resetText();
				}
			}
		}
		
		private function activateSingleTextField(fieldNum:int):void
		{
			for each (var field:TextField_AddYourOwnResource in this.addYourOwnFields) 
			{
				field.deactivate();
			}
			this.addYourOwnFields[fieldNum].activate();
		}
		
		private function receiveChar(e:KeyboardEvent):void
		{
			if(this._activeTextField >= 0)
			{
				switch (e.charCode)
				{
					case Keyboard.TAB:
						this._activeTextField++;
						if(this._activeTextField >= this.addYourOwnFields.length)
						{
							this._activeTextField = 0;
						}
						this.activateTextField(this.addYourOwnFields[this._activeTextField], false, false);
						return;
					case Keyboard.ESCAPE:
						return;
				}
				
				this.addYourOwnFields[this._activeTextField].receiveChar(e);
				//this.addYourOwnFields[this._activeTextField].caps();
			}
		}
		
		private function selectBuildingIfNeeded(e:Event):void
		{
			if(this._activeTextField >= 0)
			{
				var addYourOwnFieldText:String = this.addYourOwnFields[this._activeTextField].asset.txt.text;
				if(StringUtil.trim(addYourOwnFieldText) && addYourOwnFieldText != TextField_AddYourOwnResource.ms_DefaultText)
				{
					// there is text in the textfield and it is not the default text... select the building for the love of god
					switch(this._activeTextField)
					{
						case 0:
							selectBuilding("building_addyourown1");
							break;
						case 1:
							selectBuilding("building_addyourown2");
							break;
						case 2:
							selectBuilding("building_addyourown3");
							break;
						case 3:
							selectBuilding("building_addyourown4");
							break;
					}
				}
				else
				{
					var buildingName:String = "";
					switch(this._activeTextField)
					{
						case 0:
							buildingName = "building_addyourown1";
							break;
						case 1:
							buildingName = "building_addyourown2";
							break;
						case 2:
							buildingName = "building_addyourown3";
							break;
						case 3:
							buildingName = "building_addyourown4";
							break;
					}
					
					if(buildingSelections.indexOf(buildingName) != -1)
					{
						numSelected--;
						unSelectBuilding(buildingName);
					}
				}
			}
		}
		
		private function submitData(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_CLICKS[SessionInformation_GirlsTablet.Age]);
			
			resetPopups();
			
			var tempSprite:Sprite = new Sprite();
			m_AppState.skin.theMap.x = 0;
			m_AppState.skin.theMap.y = 0;
			var bd:BitmapData = new BitmapData(m_AppState.skin.theMap.width, m_AppState.skin.theMap.height, true, 0x00ffffff);
			bd.draw(m_AppState.skin.theMap);
			var bm:Bitmap = new Bitmap(bd);
			tempSprite.addChild(bm);
			var buildingLabels:Vector.<String> = new Vector.<String>();
			
			SessionInformation_GirlsTablet.TeamInfo.Team_UserData.SetImage(GSOC_UserData_Team_ImageKeys.KEY_COMMUNITYRESOURCES_MAP, tempSprite);
			UploadManager.UploadDisplayObject(tempSprite, Keys_FileNames.KEY_COMMUNITYRESOURCES_MAP);
			
			for (var i:int = 0; i < buildingSelections.length; i++)
			{
				var buildSprite:Sprite = new Sprite();
				var bdB:BitmapData;
				var bmB:Bitmap;

				switch (SessionInformation_GirlsTablet.Age)
				{
					case AgeConstants.KEY_DAISYBROWNIE:
						switch(buildingSelections[i])
							{
								case "building_newspaper":
									m_AppState.skin.theMap.building_newspaper.gotoAndStop(1);
									buildSprite.addChild(m_AppState.skin.theMap.building_newspaper);
									m_AppState.skin.theMap.building_newspaper.x = 0;
									m_AppState.skin.theMap.building_newspaper.y = 0;
									break;
								case "building_home":
									m_AppState.skin.theMap.building_home.gotoAndStop(1);
									buildSprite.addChild(m_AppState.skin.theMap.building_home);
									m_AppState.skin.theMap.building_home.x = 0;
									m_AppState.skin.theMap.building_home.y = 0;
									break;
								case "building_communitycenter":
									m_AppState.skin.theMap.building_communitycenter.gotoAndStop(1);
									buildSprite.addChild(m_AppState.skin.theMap.building_communitycenter);
									m_AppState.skin.theMap.building_communitycenter.x = 0;
									m_AppState.skin.theMap.building_communitycenter.y = 0;
									break;
								case "building_classroom":
									m_AppState.skin.theMap.building_classroom.gotoAndStop(1);
									buildSprite.addChild(m_AppState.skin.theMap.building_classroom);
									m_AppState.skin.theMap.building_classroom.x = 0;
									m_AppState.skin.theMap.building_classroom.y = 0;
									break;
								case "building_townhall":
									m_AppState.skin.theMap.building_townhall.gotoAndStop(1);
									buildSprite.addChild(m_AppState.skin.theMap.building_townhall);
									m_AppState.skin.theMap.building_townhall.x = 0;
									m_AppState.skin.theMap.building_townhall.y = 0;
									break;
								case "building_library":
									m_AppState.skin.theMap.building_library.gotoAndStop(1);
									buildSprite.addChild(m_AppState.skin.theMap.building_library);
									m_AppState.skin.theMap.building_library.x = 0;
									m_AppState.skin.theMap.building_library.y = 0;
									break;
								case "building_girlscoutmeeting":
									m_AppState.skin.theMap.building_girlscoutmeeting.gotoAndStop(1);
									buildSprite.addChild(m_AppState.skin.theMap.building_girlscoutmeeting);
									m_AppState.skin.theMap.building_girlscoutmeeting.x = 0;
									m_AppState.skin.theMap.building_girlscoutmeeting.y = 0;
									break;
							}
						buildingLabels.push(BuildingTextLookup.GetHumanReadableNameFromInstanceName(buildingSelections[i]));
						break;
					case AgeConstants.KEY_JUNIORCADETTE:
						switch (buildingSelections[i])
							{
								case "building_publiclibrary":
									buildingLabels.push(BuildingTextLookup.GetHumanReadableNameFromInstanceName(buildingSelections[i]));
									m_AppState.skin.theMap.building_publiclibrary.gotoAndStop(1);
									buildSprite.addChild(m_AppState.skin.theMap.building_publiclibrary);
									m_AppState.skin.theMap.building_publiclibrary.x = 0;
									m_AppState.skin.theMap.building_publiclibrary.y = 0;
									break;
								case "building_recdept":
									buildingLabels.push(BuildingTextLookup.GetHumanReadableNameFromInstanceName(buildingSelections[i]));
									m_AppState.skin.theMap.building_recdept.gotoAndStop(1);
									buildSprite.addChild(m_AppState.skin.theMap.building_recdept);
									m_AppState.skin.theMap.building_recdept.x = 0;
									m_AppState.skin.theMap.building_recdept.y = 0;
									break;
								case "building_classroom":
									buildingLabels.push(BuildingTextLookup.GetHumanReadableNameFromInstanceName(buildingSelections[i]));
									m_AppState.skin.theMap.building_classroom.gotoAndStop(1);
									buildSprite.addChild(m_AppState.skin.theMap.building_classroom);
									m_AppState.skin.theMap.building_classroom.x = 0;
									m_AppState.skin.theMap.building_classroom.y = 0;
									break;
								case "building_pta":
									buildingLabels.push(BuildingTextLookup.GetHumanReadableNameFromInstanceName(buildingSelections[i]));
									m_AppState.skin.theMap.building_pta.gotoAndStop(1);
									buildSprite.addChild(m_AppState.skin.theMap.building_pta);
									m_AppState.skin.theMap.building_pta.x = 0;
									m_AppState.skin.theMap.building_pta.y = 0;
									break;
								case "building_newsstation":
									buildingLabels.push(BuildingTextLookup.GetHumanReadableNameFromInstanceName(buildingSelections[i]));
									m_AppState.skin.theMap.building_newsstation.gotoAndStop(1);
									buildSprite.addChild(m_AppState.skin.theMap.building_newsstation);
									m_AppState.skin.theMap.building_newsstation.x = 0;
									m_AppState.skin.theMap.building_newsstation.y = 0;
									break;
								case "building_home":
									buildingLabels.push(BuildingTextLookup.GetHumanReadableNameFromInstanceName(buildingSelections[i]));
									m_AppState.skin.theMap.building_home.gotoAndStop(1);
									buildSprite.addChild(m_AppState.skin.theMap.building_home);
									m_AppState.skin.theMap.building_home.x = 0;
									m_AppState.skin.theMap.building_home.y = 0;
									break;
								case "building_foodbank":
									buildingLabels.push(BuildingTextLookup.GetHumanReadableNameFromInstanceName(buildingSelections[i]));
									m_AppState.skin.theMap.building_foodbank.gotoAndStop(1);
									buildSprite.addChild(m_AppState.skin.theMap.building_foodbank);
									m_AppState.skin.theMap.building_foodbank.x = 0;
									m_AppState.skin.theMap.building_foodbank.y = 0;
									break;
								case "building_girlscoutleader":
									buildingLabels.push(BuildingTextLookup.GetHumanReadableNameFromInstanceName(buildingSelections[i]));
									m_AppState.skin.theMap.building_girlscoutleader.gotoAndStop(1);
									buildSprite.addChild(m_AppState.skin.theMap.building_girlscoutleader);
									m_AppState.skin.theMap.building_girlscoutleader.x = 0;
									m_AppState.skin.theMap.building_girlscoutleader.y = 0;
									break;
								case "building_cityhall":
									buildingLabels.push(BuildingTextLookup.GetHumanReadableNameFromInstanceName(buildingSelections[i]));
									m_AppState.skin.theMap.building_cityhall.gotoAndStop(1);
									buildSprite.addChild(m_AppState.skin.theMap.building_cityhall);
									m_AppState.skin.theMap.building_cityhall.x = 0;
									m_AppState.skin.theMap.building_cityhall.y = 0;
									break;
								case "building_grocerystore":
									buildingLabels.push(BuildingTextLookup.GetHumanReadableNameFromInstanceName(buildingSelections[i]));
									m_AppState.skin.theMap.building_grocerystore.gotoAndStop(1);
									buildSprite.addChild(m_AppState.skin.theMap.building_grocerystore);
									m_AppState.skin.theMap.building_grocerystore.x = 0;
									m_AppState.skin.theMap.building_grocerystore.y = 0;
									break;
								case "building_addyourown1":
									buildingLabels.push(this.addYourOwnFields[0].getText());
									m_AppState.skin.theMap.building_addyourown1.gotoAndStop(1);
									buildSprite.addChild(m_AppState.skin.theMap.building_addyourown1);
									this.addYourOwnFields[0].changeColor(0);
									m_AppState.skin.theMap.building_addyourown1.x = 0;
									m_AppState.skin.theMap.building_addyourown1.y = 0;
									
									positionTextfieldToBuilding(this.addYourOwnFields[0], buildSprite);
									buildSprite.addChild(this.addYourOwnFields[0].GetTextField());
									break;
								case "building_addyourown2":
									buildingLabels.push(this.addYourOwnFields[1].getText());
									m_AppState.skin.theMap.building_addyourown2.gotoAndStop(1);
									buildSprite.addChild(m_AppState.skin.theMap.building_addyourown2);
									this.addYourOwnFields[1].changeColor(0);
									m_AppState.skin.theMap.building_addyourown2.x = 0;
									m_AppState.skin.theMap.building_addyourown2.y = 0;
									
									positionTextfieldToBuilding(this.addYourOwnFields[1], buildSprite);
									buildSprite.addChild(this.addYourOwnFields[1].GetTextField());
									break;
							}
						break;
					case AgeConstants.KEY_SENIORAMBASSADOR:
						switch (buildingSelections[i])
							{
								case "building_countyofficebuilding":
									buildingLabels.push(BuildingTextLookup.GetHumanReadableNameFromInstanceName(buildingSelections[i]));
									m_AppState.skin.theMap.building_countyofficebuilding.gotoAndStop(1);
									buildSprite.addChild(m_AppState.skin.theMap.building_countyofficebuilding);
									m_AppState.skin.theMap.building_countyofficebuilding.x = 0;
									m_AppState.skin.theMap.building_countyofficebuilding.y = 0;
									break;
								case "building_academiclibrary":
									buildingLabels.push(BuildingTextLookup.GetHumanReadableNameFromInstanceName(buildingSelections[i]));
									m_AppState.skin.theMap.building_academiclibrary.gotoAndStop(1);
									buildSprite.addChild(m_AppState.skin.theMap.building_academiclibrary);
									m_AppState.skin.theMap.building_academiclibrary.x = 0;
									m_AppState.skin.theMap.building_academiclibrary.y = 0;
									break;
								case "building_publicaccesstv":
									buildingLabels.push(BuildingTextLookup.GetHumanReadableNameFromInstanceName(buildingSelections[i]));
									m_AppState.skin.theMap.building_publicaccesstv.gotoAndStop(1);
									buildSprite.addChild(m_AppState.skin.theMap.building_publicaccesstv);
									m_AppState.skin.theMap.building_publicaccesstv.x = 0;
									m_AppState.skin.theMap.building_publicaccesstv.y = 0;
									break;
								case "building_girlscoutcommunity":
									buildingLabels.push(BuildingTextLookup.GetHumanReadableNameFromInstanceName(buildingSelections[i]));
									m_AppState.skin.theMap.building_girlscoutcommunity.gotoAndStop(1);
									buildSprite.addChild(m_AppState.skin.theMap.building_girlscoutcommunity);
									m_AppState.skin.theMap.building_girlscoutcommunity.x = 0;
									m_AppState.skin.theMap.building_girlscoutcommunity.y = 0;
									break;
								case "building_civiccenter":
									buildingLabels.push(BuildingTextLookup.GetHumanReadableNameFromInstanceName(buildingSelections[i]));
									m_AppState.skin.theMap.building_civiccenter.gotoAndStop(1);
									buildSprite.addChild(m_AppState.skin.theMap.building_civiccenter);
									m_AppState.skin.theMap.building_civiccenter.x = 0;
									m_AppState.skin.theMap.building_civiccenter.y = 0;
									break;
								case "building_bank":
									buildingLabels.push(BuildingTextLookup.GetHumanReadableNameFromInstanceName(buildingSelections[i]));
									m_AppState.skin.theMap.building_bank.gotoAndStop(1);
									buildSprite.addChild(m_AppState.skin.theMap.building_bank);
									m_AppState.skin.theMap.building_bank.x = 0;
									m_AppState.skin.theMap.building_bank.y = 0;
									break;
								case "building_boardofeducation":
									buildingLabels.push(BuildingTextLookup.GetHumanReadableNameFromInstanceName(buildingSelections[i]));
									m_AppState.skin.theMap.building_boardofeducation.gotoAndStop(1);
									buildSprite.addChild(m_AppState.skin.theMap.building_boardofeducation);
									m_AppState.skin.theMap.building_boardofeducation.x = 0;
									m_AppState.skin.theMap.building_boardofeducation.y = 0;
									break;
								case "building_addyourown1":
									buildingLabels.push(this.addYourOwnFields[0].getText());
									m_AppState.skin.theMap.building_addyourown1.gotoAndStop(1);
									buildSprite.addChild(m_AppState.skin.theMap.building_addyourown1);
									this.addYourOwnFields[0].changeColor(0);
									m_AppState.skin.theMap.building_addyourown1.x = 0;
									m_AppState.skin.theMap.building_addyourown1.y = 0;
									
									positionTextfieldToBuilding(this.addYourOwnFields[0], buildSprite);
									buildSprite.addChild(this.addYourOwnFields[0].GetTextField());
									break;
								case "building_addyourown2":
									buildingLabels.push(this.addYourOwnFields[1].getText());
									m_AppState.skin.theMap.building_addyourown2.gotoAndStop(1);
									buildSprite.addChild(m_AppState.skin.theMap.building_addyourown2);
									this.addYourOwnFields[1].changeColor(0);
									m_AppState.skin.theMap.building_addyourown2.x = 0;
									m_AppState.skin.theMap.building_addyourown2.y = 0;
									
									positionTextfieldToBuilding(this.addYourOwnFields[1], buildSprite);
									buildSprite.addChild(this.addYourOwnFields[1].GetTextField());
									break;
								case "building_addyourown3":
									buildingLabels.push(this.addYourOwnFields[2].getText());
									m_AppState.skin.theMap.building_addyourown3.gotoAndStop(1);
									buildSprite.addChild(m_AppState.skin.theMap.building_addyourown3);
									this.addYourOwnFields[2].changeColor(0);
									m_AppState.skin.theMap.building_addyourown3.x = 0;
									m_AppState.skin.theMap.building_addyourown3.y = 0;
									
									positionTextfieldToBuilding(this.addYourOwnFields[2], buildSprite);
									buildSprite.addChild(this.addYourOwnFields[2].GetTextField());
									break;
								case "building_addyourown4":
									buildingLabels.push(this.addYourOwnFields[3].getText());
									m_AppState.skin.theMap.building_addyourown4.gotoAndStop(1);
									buildSprite.addChild(m_AppState.skin.theMap.building_addyourown4);
									this.addYourOwnFields[3].changeColor(0);
									m_AppState.skin.theMap.building_addyourown4.x = 0;
									m_AppState.skin.theMap.building_addyourown4.y = 0;
									
									positionTextfieldToBuilding(this.addYourOwnFields[3], buildSprite);
									buildSprite.addChild(this.addYourOwnFields[3].GetTextField());
									break;
							}
						break;
					}
				
				var tempBuildSprite:MovieClip = Utils_Image.ConvertDisplayObjectToMovieClip(Utils_Image.ConvertDisplayObjectToBitmap(buildSprite, 0, 0, 10, 10));
		
				switch (i)
				{
					case 0:
						SessionInformation_GirlsTablet.TeamInfo.Team_UserData.SetImage(GSOC_UserData_Team_ImageKeys.KEY_COMMUNITYRESOURCES_BUILDING_01, tempBuildSprite);
						UploadManager.UploadDisplayObject(tempBuildSprite, Keys_FileNames.KEY_COMMUNITYRESOURCES_BUILDING_01);
						break;
					case 1:
						SessionInformation_GirlsTablet.TeamInfo.Team_UserData.SetImage(GSOC_UserData_Team_ImageKeys.KEY_COMMUNITYRESOURCES_BUILDING_02, tempBuildSprite);
						UploadManager.UploadDisplayObject(tempBuildSprite, Keys_FileNames.KEY_COMMUNITYRESOURCES_BUILDING_02);
						break;
					case 2:
						SessionInformation_GirlsTablet.TeamInfo.Team_UserData.SetImage(GSOC_UserData_Team_ImageKeys.KEY_COMMUNITYRESOURCES_BUILDING_03, tempBuildSprite);
						UploadManager.UploadDisplayObject(tempBuildSprite, Keys_FileNames.KEY_COMMUNITYRESOURCES_BUILDING_03);
						break;
					case 3:
						SessionInformation_GirlsTablet.TeamInfo.Team_UserData.SetImage(GSOC_UserData_Team_ImageKeys.KEY_COMMUNITYRESOURCES_BUILDING_04, tempBuildSprite);
						UploadManager.UploadDisplayObject(tempBuildSprite, Keys_FileNames.KEY_COMMUNITYRESOURCES_BUILDING_04);
						break;
					case 4:
						SessionInformation_GirlsTablet.TeamInfo.Team_UserData.SetImage(GSOC_UserData_Team_ImageKeys.KEY_COMMUNITYRESOURCES_BUILDING_05, tempBuildSprite);
						UploadManager.UploadDisplayObject(tempBuildSprite, Keys_FileNames.KEY_COMMUNITYRESOURCES_BUILDING_05);
						break;
				}	
			}
			SessionInformation_GirlsTablet.TeamInfo.Team_UserData.SetData(GSOC_UserData_Team_DataKeys.KEY_COMMUNITYRESOURCES_BUILDING_LIST, buildingLabels);
			
			SessionInformation_GirlsTablet.UploadCurrentInformation();
			
			SessionInformation_GirlsTablet.TeamInfo.ResetActiveIndex();
			if (SessionInformation_GirlsTablet.IsRunningLocally)
			{
				StateControllerManager.LeaveActiveState(StateController_IDs.STATECONTROLLER_COMMUNITYRESOURCES, StateController_StateIDs_CommunityResources.STATE_COMMUNITYRESOURCES);
				StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_BRAINSTORM, StateController_StateIDs_BrainStorm.STATE_PROJECTGOALS);
			}
			else
			{
				submitActivity();
			}
		}
		
		private function positionTextfieldToBuilding(textfieldContainer:TextField_AddYourOwnResource, building:DisplayObject):void
		{
			var textfield:DisplayObject = textfieldContainer.GetTextField();
			
			textfield.x = (building.width * .5) - (textfield.width * .5);
			textfield.y = building.height + textfield.height * .5;
		}
		
		private function submitActivity():void
		{
			StateControllerManager.LeaveAllActiveStates();
			StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_MAIN, StateController_StateIDs_Main.STATE_SESSIONATTRACT);
			Document.TellServerActivityComplete();
		}
		
		protected override function DeInit():void
		{	
			for (var i:int = 0; i < m_Buildings.length; i++)
			{
				TweenMax.killTweensOf(m_Buildings[i]);
				m_Buildings[i].y = m_BuildingsY[i];
			}
			for each (var field:TextField_AddYourOwnResource in this.addYourOwnFields)
			{
				field.removeEventListener(InputController.DOWN, this.setActiveTextFieldByMouseClick);
				field.removeEventListener(InputController.UP, this.setActiveTextFieldByMouseClickUp);
				field.removeEventListener(TextField_AddYourOwnResource.TEXT_ENTERED, this.selectBuildingIfNeeded);
			}
			CitrusFramework.frameworkStage.removeEventListener(InputController.UP, this.keepBuildingsSelectedCorrect);
			
			for each (var building:MovieClip in m_Buildings)
			{
				building.removeEventListener(InputController.CLICK, this.buildingPressed);
			}
			
			m_AppState.DeInitialize();
			m_AppState.skin.theHitArea.removeEventListener(InputController.CLICK, resetPopups);
			m_AppState.skin.theMap.map.removeEventListener(InputController.CLICK, resetPopups);
			numSelected = 0;
			CitrusFramework.contentLayer.removeChild(m_AppState);
			m_AppState = null;
			
			super.EndDeInit();
		}
	}
}
package com.bpi.gsoc.girlstablet.states.gsoc_03chooseyourcareer.career_choice
{
	import com.bpi.gsoc.components.chooseyourcareer.CareerChoice_Screen1_DB_Skin;
	import com.bpi.gsoc.framework.constants.AgeColors;
	import com.bpi.gsoc.framework.constants.AgeFontLinkages;
	import com.bpi.gsoc.framework.gsocui.GSOC_Header;

	public class CareerChoice_App_DB extends CareerChoice_App
	{
		public function CareerChoice_App_DB()
		{
			m_Skin = new CareerChoice_Screen1_DB_Skin();
			m_Glow = AgeColors.DB_GLOW;
			m_HeaderFont = AgeFontLinkages.DB_HEADER_FONT;
			m_BodyFont = AgeFontLinkages.DB_BODY_FONT;
			
			m_GSOCHeader = new GSOC_Header(m_Skin.header.paginationArea);
			GSOCheader.addPagination();
			GSOCheader.addArrows(m_Skin.header.paginationArea.x, 0);
			super.Initialize();
		}
	}
}
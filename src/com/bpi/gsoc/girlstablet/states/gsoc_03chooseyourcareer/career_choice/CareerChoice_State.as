package com.bpi.gsoc.girlstablet.states.gsoc_03chooseyourcareer.career_choice
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.citrusas.input.inputcontroller.InputController;
	import com.bpi.citrusas.sound.SoundHandler;
	import com.bpi.citrusas.states.StateControllerManager;
	import com.bpi.citrusas.utils.Utils_Image;
	import com.bpi.gsoc.framework.avatar.GSOC_Avatar_Builder;
	import com.bpi.gsoc.framework.avatar.GSOC_Avatar_Skin;
	import com.bpi.gsoc.framework.career.Career;
	import com.bpi.gsoc.framework.career.CareerReference;
	import com.bpi.gsoc.framework.career.algorithm.CareerDataUtils;
	import com.bpi.gsoc.framework.career.algorithm.CareerGenerator;
	import com.bpi.gsoc.framework.career.group.CareerGroupReference;
	import com.bpi.gsoc.framework.career.keys.CareerKeys_DB;
	import com.bpi.gsoc.framework.career.keys.CareerKeys_JC;
	import com.bpi.gsoc.framework.career.keys.CareerKeys_SA;
	import com.bpi.gsoc.framework.constants.AgeConstants;
	import com.bpi.gsoc.framework.constants.Keys_FileNames;
	import com.bpi.gsoc.framework.data.sessioninformation.GirlInformation;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.data.userdata.GSOC_UserData;
	import com.bpi.gsoc.framework.data.userdata.GSOC_UserData_Girl_DataKeys;
	import com.bpi.gsoc.framework.data.userdata.GSOC_UserData_Girl_ImageKeys;
	import com.bpi.gsoc.framework.gsocui.GSOC_ButtonToggle;
	import com.bpi.gsoc.framework.sfx.GSOC_SFX;
	import com.bpi.gsoc.framework.states.app.GSOC_AppState;
	import com.bpi.gsoc.girlstablet.global_data.FrameControlUtils;
	import com.bpi.gsoc.girlstablet.global_data.upload.UploadManager;
	import com.bpi.gsoc.girlstablet.states.gsoc_03chooseyourcareer.career_choice.classes.CareerBubble;
	import com.bpi.gsoc.girlstablet.states.gsoc_03chooseyourcareer.career_choice.classes.CareerChoiceBlock;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_IDs;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_ChooseYourCareer;
	
	import flash.display.DisplayObjectContainer;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.filters.GlowFilter;

	public class CareerChoice_State extends GSOC_AppState
	{
		private var m_AppState:CareerChoice_App;
		private var _selectionBoxes:Vector.<CareerChoiceBlock>;
		private var _currentlySelectedCareer:Career;
		private var _currentlySelectedCareerBox:MovieClip;
		
		public function CareerChoice_State()
		{
			super(StateController_StateIDs_ChooseYourCareer.STATE_CAREERCHOICE);
		}
		
		public override function Initialize():void
		{
			this._selectionBoxes = new Vector.<CareerChoiceBlock>();
		}
		
		protected override function Init():void
		{
			switch(SessionInformation_GirlsTablet.Age)
			{
				case AgeConstants.KEY_DAISYBROWNIE:
					m_AppState = new CareerChoice_App_DB();
					break;
				case AgeConstants.KEY_JUNIORCADETTE:
					m_AppState = new CareerChoice_App_JC();
					break;
				case AgeConstants.KEY_SENIORAMBASSADOR:
					m_AppState = new CareerChoice_App_SA();
					break;
				default:
					m_AppState = new CareerChoice_App_DB();			
			}
			
			m_AppState.Init();
			FrameControlUtils.StopAtFrameN(m_AppState);
			
			if(CareerDataUtils.DataAvailable)
			{
				this.setUpForNextGirl();
			}
			else
			{
				CareerDataUtils.CareerDataLoader.addEventListener(CareerDataUtils.CAREER_DATA_LOADED, this.setUpForNextGirl);
				CareerDataUtils.LoadCareerMatrix(SessionInformation_GirlsTablet.Age, SessionInformation_GirlsTablet.Journey);
			}
			
			m_AppState.GSOCheader.pagination.setNumMembers(SessionInformation_GirlsTablet.TeamInfo.GetTeamMemberCount());
			m_AppState.GSOCheader.pagination.goToIndex(SessionInformation_GirlsTablet.TeamInfo.GetActiveIndex());
			m_AppState.GSOCheader.pagination.setActiveName(SessionInformation_GirlsTablet.TeamInfo.GetActiveGirlInformation().Name);
			
			EndInit();
		}
	
		protected override function DeInit():void
		{
			this.disableCareerDisplays();
			this._selectionBoxes = new Vector.<CareerChoiceBlock>();
			this._currentlySelectedCareer = null;
			CitrusFramework.contentLayer.removeChild(m_AppState);
			
			m_Skin = null;
			EndDeInit();
		}
	
		private function setUpForNextGirl(e:Event = null):void
		{
			m_AppState.GSOCheader.pagination.goToIndex(SessionInformation_GirlsTablet.TeamInfo.GetActiveIndex());
			m_AppState.GSOCheader.pagination.setActiveName(SessionInformation_GirlsTablet.TeamInfo.GetActiveGirlInformation().Name);
			CareerDataUtils.CareerDataLoader.removeEventListener(CareerDataUtils.CAREER_DATA_LOADED, this.setUpForNextGirl);
			
			var headerFont:String = m_AppState.headerFont;
			
			this._selectionBoxes.push(new CareerChoiceBlock(m_AppState.skin.job_Box_Left, headerFont, m_AppState.bodyFont), 
				new CareerChoiceBlock(m_AppState.skin.job_Box_Center, m_AppState.headerFont, m_AppState.bodyFont),
				new CareerChoiceBlock(m_AppState.skin.job_Box_Right, m_AppState.headerFont, m_AppState.bodyFont));
			
			CareerGenerator.CalculateRecommendedCareers(SessionInformation_GirlsTablet.TeamInfo.GetActiveGirlInformation().GirlUserData.GetData(GSOC_UserData_Girl_DataKeys.KEY_ANSWER_ARRAY) as Array);
			
			this.fillCareerBoxes();
			
			CitrusFramework.contentLayer.addChild(m_AppState);
			
			setUpScreen();
		}
		
		private function fillCareerBoxes():void
		{
			for (var i:int = 0; i < this._selectionBoxes.length; i++)
			{
				this._selectionBoxes[i].linkedCareer = this.getRankedCareerByIndex(i);
				this._selectionBoxes[i].addEventListener(CareerChoiceBlock.CAREER_BLOCK_SELECTED, this.eh_getLinkedCareer);
				this.fillInAvatar(this._selectionBoxes[i]);
				this.fillBackground(this._selectionBoxes[i]);	
			}
		}
		
		private function fillBackground(box:CareerChoiceBlock):void
		{
			box.setBackground(CareerGroupReference.GetBackgroundForCareer(box.linkedCareer.title));
		}
		
		private function getRankedCareerByIndex(index:int):Career
		{
			return this.getCareerByName(CareerGenerator.GetRankedCareers()[index]);	
		}
		
		private function getCareerByName(key:String):Career
		{
			var career:Career;
			switch(SessionInformation_GirlsTablet.Age)
			{
				case AgeConstants.KEY_DAISYBROWNIE:
					career = CareerReference.GetDBCareer(CareerKeys_DB[key]);
					break;
				case AgeConstants.KEY_JUNIORCADETTE:
					career = CareerReference.GetJCCareer(CareerKeys_JC[key]);
					break;
				case AgeConstants.KEY_SENIORAMBASSADOR:
					career = CareerReference.GetSACareer(CareerKeys_SA[key]);
					break;
				default:
					career = CareerReference.GetDBCareer(CareerKeys_DB[key]);
					break;
			}
			
			if(!career)
			{
				throw new Error("The career title you passed was not recognized: " + key);
			}
			
			return career;
		}
		
		private function enableCareerDisplays():void
		{
			for each(var careerBox:CareerChoiceBlock in this._selectionBoxes)
			{
				careerBox.Init(this.eh_careerSelected);
			}
		}
		
		private function disableMouseChildren(mc:DisplayObjectContainer):void
		{
			if(!mc || mc.numChildren == 0)
			{
				return;
			}
		
			for(var i:int = 0; i < mc.numChildren; i++)
			{
				this.disableMouseChildren(mc.getChildAt(i) as DisplayObjectContainer);
			}
		}
		
		private function disableCareerDisplays():void
		{
			for each(var careerBox:CareerChoiceBlock in this._selectionBoxes)
			{
				careerBox.DeInit(this.eh_careerSelected);
			}
		}
		
		private function eh_careerSelected(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_CHOOSECAREERS[SessionInformation_GirlsTablet.Age]);
			if(SessionInformation_GirlsTablet.TeamInfo.HasNextTeamMember())
			{
				GSOC_ButtonToggle.ToggleButton(true, m_AppState.skin.next_Girl_Button, this.enableNextButton, this.disableNextButton);
				GSOC_ButtonToggle.ToggleButton(false, m_AppState.skin.submitButton, this.enableSubmitButton, this.disableSubmitButton);
			}
			else
			{
				GSOC_ButtonToggle.ToggleButton(false, m_AppState.skin.next_Girl_Button, this.enableNextButton, this.disableNextButton);
				GSOC_ButtonToggle.ToggleButton(true, m_AppState.skin.submitButton, this.enableSubmitButton, this.disableSubmitButton);
			}
			
			for each(var box:CareerChoiceBlock in this._selectionBoxes)
			{
				box.asset.filters = [];
			}
			if (e.target as MovieClip) 
			{
				(e.target as MovieClip).filters = [new GlowFilter(this.m_AppState.m_Glow, 0.7, 200, 200, 3)]; 
				_currentlySelectedCareerBox = (e.target as MovieClip);
			}
		}
		
		private function eh_getLinkedCareer(e:Event):void
		{
			this._currentlySelectedCareer = (e.target as CareerChoiceBlock).linkedCareer;
		}
		
		private function fillInAvatar(box: CareerChoiceBlock):void
		{
			var girlUserData:GSOC_UserData = SessionInformation_GirlsTablet.TeamInfo.GetActiveGirlInformation().GirlUserData;
			var avatar:GSOC_Avatar_Skin = GSOC_Avatar_Builder.BuildAvatar(false, girlUserData.GetImage(GSOC_UserData_Girl_ImageKeys.KEY_PICTUREYOURSELF_GIRL_PHOTO), int(girlUserData.GetData(GSOC_UserData_Girl_DataKeys.KEY_HAIR_STYLE)), 
				uint(girlUserData.GetData(GSOC_UserData_Girl_DataKeys.KEY_SKIN_COLOR)),uint(girlUserData.GetData(GSOC_UserData_Girl_DataKeys.KEY_HAIR_COLOR)), CareerGroupReference.GetOutfitForCareer(box.linkedCareer.title));
			Career;
			box.avatarMarker = avatar;
		}
		
		private function moveOn(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_CLICKS[SessionInformation_GirlsTablet.Age]);
			CareerGenerator.addChosenCareer(this._currentlySelectedCareer.title.toLocaleUpperCase().split( " " ).join( "" ));
			
			var girlInformation:GirlInformation = SessionInformation_GirlsTablet.TeamInfo.GetActiveGirlInformation();
			girlInformation.GirlUserData.SetData(GSOC_UserData_Girl_DataKeys.KEY_CAREER_TITLE, this._currentlySelectedCareer.title);
			girlInformation.GirlUserData.SetData(GSOC_UserData_Girl_DataKeys.KEY_CAREER_PICKED, true);
			this._currentlySelectedCareerBox.filters = [];
			var careerBox:Sprite = Utils_Image.CopySprite(this._currentlySelectedCareerBox);
			var careerBubble:Sprite = getCareerBubble(girlInformation.GirlUserData);
			girlInformation.GirlUserData.SetImage(GSOC_UserData_Girl_ImageKeys.KEY_CHOOSEYOURCAREER_BOX, careerBox);
			girlInformation.GirlUserData.SetImage(GSOC_UserData_Girl_ImageKeys.KEY_CHOOSEYOURCAREER_BUBBLE, careerBubble);
			SessionInformation_GirlsTablet.UploadCurrentInformation();
			UploadManager.UploadDisplayObject(careerBox, Keys_FileNames.KEY_CHOOSEYOURCAREER_BOX, girlInformation.UID);
			UploadManager.UploadDisplayObject(careerBubble, Keys_FileNames.KEY_CHOOSEYOURCAREER_BUBBLE, girlInformation.UID);
			
			if(SessionInformation_GirlsTablet.TeamInfo.HasNextTeamMember())
			{
				SessionInformation_GirlsTablet.TeamInfo.IncrementActiveIndex();
				StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_CHOOSEYOURCAREER, StateController_StateIDs_ChooseYourCareer.STATE_CAREERCHOICE);
			}
			else
			{
				CareerGenerator.ClearChosenCareers();
				SessionInformation_GirlsTablet.TeamInfo.ResetActiveIndex();
				StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_CHOOSEYOURCAREER, StateController_StateIDs_ChooseYourCareer.STATE_CAREERDONE);
				Document.TellServerActivityComplete();
			}
		}
		
		private function getCareerBubble(girlUserData:GSOC_UserData):Sprite
		{
			var careerBubble:CareerBubble = new CareerBubble();
			careerBubble.Initialize(girlUserData);
			var sprite:Sprite = new Sprite();
			sprite.addChild(careerBubble.GetBitmap());
			return sprite;
		}
		
		private function setUpScreen():void
		{
			this.enableCareerDisplays();
			m_AppState.skin.next_Girl_Button.addEventListener(InputController.CLICK, this.moveOn);
			GSOC_ButtonToggle.ToggleButton(false, m_AppState.skin.next_Girl_Button, this.enableNextButton, this.disableNextButton);
			GSOC_ButtonToggle.ToggleButton(false, m_AppState.skin.submitButton, this.enableSubmitButton, this.disableSubmitButton);
		}
		
		private function enableNextButton(e:Event = null):void
		{
			m_AppState.skin.next_Girl_Button.addEventListener(InputController.CLICK, this.moveOn);
		}
		
		private function disableNextButton(e:Event = null):void
		{
			m_AppState.skin.next_Girl_Button.removeEventListener(InputController.CLICK, this.moveOn);
		}
		
		private function enableSubmitButton(e:Event = null):void
		{
			m_AppState.skin.submitButton.addEventListener(InputController.CLICK, this.moveOn);
		}
		
		private function disableSubmitButton(e:Event = null):void
		{
			m_AppState.skin.submitButton.removeEventListener(InputController.CLICK, this.moveOn);
		}
	}
}
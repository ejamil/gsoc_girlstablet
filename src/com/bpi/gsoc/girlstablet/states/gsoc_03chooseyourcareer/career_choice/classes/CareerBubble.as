package com.bpi.gsoc.girlstablet.states.gsoc_03chooseyourcareer.career_choice.classes
{
	import com.bpi.gsoc.framework.avatar.GSOC_Avatar_Builder;
	import com.bpi.gsoc.framework.avatar.GSOC_Avatar_Skin;
	import com.bpi.gsoc.framework.career.group.CareerGroupReference;
	import com.bpi.gsoc.framework.constants.GSOC_Info;
	import com.bpi.gsoc.framework.data.userdata.GSOC_UserData;
	import com.bpi.gsoc.framework.data.userdata.GSOC_UserData_Girl_DataKeys;
	import com.bpi.gsoc.girlstablet.chooseyourcareer.CareerMask;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;

	public class CareerBubble extends Sprite
	{
		private static var ms_Avatar_BorderThickness:Number = 15;
		private static var ms_Avatar_Offset:Point = new Point(20, 128);
		private static var ms_Avatar_Scale:Number = 0.5;
		
		private var m_Image_Background:MovieClip;
		private var m_Image_Avatar:GSOC_Avatar_Skin;
		private var m_Image_Mask:MovieClip;
		private var m_Image_Border:Shape;
		
		private var m_GirlUserData:GSOC_UserData;
		private var m_CareerTitle:String;
		
		public function CareerBubble()
		{
			
		}
		
		public function Initialize(girlUserData:GSOC_UserData):void
		{
			m_GirlUserData = girlUserData;
			m_CareerTitle = m_GirlUserData.GetData(GSOC_UserData_Girl_DataKeys.KEY_CAREER_TITLE) as String;
			
			
			m_Image_Mask = new CareerMask();
			m_Image_Border = new Shape();
			
			this.addChild(m_Image_Mask);
			
			generateCareerAvatar();
		}
		
		private function generateCareerAvatar():void
		{
			createBackground();
			createAvatar();
			createBorder();
		}
		
		private function createBackground():void
		{
			if(m_Image_Background && this.contains(m_Image_Background))
			{
				this.removeChild(m_Image_Background);
			}
			
			m_Image_Background = CareerGroupReference.GetBackgroundForCareer(m_CareerTitle);
			
			this.addChild(m_Image_Background);
		}
		
		private function createAvatar():void
		{
			if(m_Image_Avatar && this.contains(m_Image_Avatar))
			{
				this.removeChild(m_Image_Avatar);
			}
			
			m_Image_Avatar = GSOC_Avatar_Builder.BuildAvatarFromUserData(false, m_GirlUserData);
			
			m_Image_Avatar.scaleX = ms_Avatar_Scale;
			m_Image_Avatar.scaleY = ms_Avatar_Scale;
			m_Image_Avatar.x += ms_Avatar_Offset.x;
			m_Image_Avatar.y += ms_Avatar_Offset.y;
			m_Image_Avatar.mask = m_Image_Mask;
			
			this.addChild(m_Image_Avatar);
		}
		
		private function createBorder():void
		{
			var borderWidth:Number = m_Image_Mask.width + ms_Avatar_BorderThickness * .5;
			var borderHeight:Number = m_Image_Mask.height + ms_Avatar_BorderThickness * .5;
			
			m_Image_Border.graphics.lineStyle(ms_Avatar_BorderThickness, GSOC_Info.ColorTwo);
			m_Image_Border.graphics.drawEllipse(0, 0, borderWidth, borderHeight);
			
			this.addChild(m_Image_Border);
		}
		
		public function GetBitmap():Bitmap
		{
			var bitmapData:BitmapData = new BitmapData(m_Image_Border.width, m_Image_Border.height, true, 0x00000000);
			var bounds:Rectangle = this.getBounds(this);
			var mtx:Matrix = new Matrix(1, 0, 0, 1, -bounds.x, -bounds.y);
			bitmapData.draw(this, mtx);
			var bitmap:Bitmap = new Bitmap(bitmapData);
			return bitmap;
		}
	}
}
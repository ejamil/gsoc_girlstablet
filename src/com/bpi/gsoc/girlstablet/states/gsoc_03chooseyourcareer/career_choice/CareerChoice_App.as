package com.bpi.gsoc.girlstablet.states.gsoc_03chooseyourcareer.career_choice
{
	import com.bpi.gsoc.framework.states.app.GSOC_AppSkin;

	public class CareerChoice_App extends GSOC_AppSkin
	{
		protected var _m_Glow:uint;
		
		public function CareerChoice_App()
		{
			super(true);
		}

		public function get m_Glow():uint
		{
			return _m_Glow;
		}
		
		public function set m_Glow(value:uint):void
		{
			_m_Glow = value;
		}
	}
}
package com.bpi.gsoc.girlstablet.states.gsoc_03chooseyourcareer.career_done
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.citrusas.input.inputcontroller.InputController;
	import com.bpi.citrusas.sound.SoundHandler;
	import com.bpi.citrusas.states.StateControllerManager;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.gsocui.GSOC_ButtonToggle;
	import com.bpi.gsoc.framework.sfx.GSOC_SFX;
	import com.bpi.gsoc.framework.states.app.GSOC_AppState;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_IDs;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_ChooseYourCareer;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_KnowYourRole;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_Main;
	
	import flash.display.MovieClip;
	import flash.events.Event;

	public class CareerDone_State extends GSOC_AppState
	{
		private var m_AppState:CareerDone_App;
		
		public function CareerDone_State() 
		{
			super(StateController_StateIDs_ChooseYourCareer.STATE_CAREERDONE);
		}
		
		protected override function Init():void
		{
			switch(SessionInformation_GirlsTablet.Age)
			{
				case 0:
					m_AppState = new CareerDone_App_DB();
					break;
				case 1:
					m_AppState = new CareerDone_App_JC();
					break;
				case 2:
					m_AppState = new CareerDone_App_SA();
					break;
				default:
					m_AppState = new CareerDone_App_DB();
					
			}
			
			m_AppState.Init();
			CitrusFramework.contentLayer.addChild(m_AppState);
			
			if(SessionInformation_GirlsTablet.IsRunningLocally)
			{
				GSOC_ButtonToggle.ToggleButton(true, m_AppState.skin.submitButton, this.enableSubmitButton, null);
			}
			else
			{
				GSOC_ButtonToggle.ToggleButton(false, m_AppState.skin.submitButton, null, null);
			}
			
			EndInit();
		}
		
		protected override function DeInit():void
		{
			CitrusFramework.contentLayer.removeChild(m_AppState);
			m_Skin = null;
			EndDeInit();
			
		}
		
		private function enableSubmitButton(e:Event = null):void
		{
			(m_AppState.skin.submitButton as MovieClip).addEventListener(InputController.CLICK, this.moveOn);
		}
		
		private function moveOn(e:Event  = null):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_CLICKS[SessionInformation_GirlsTablet.Age]);
			if(SessionInformation_GirlsTablet.IsRunningLocally)
			{
				StateControllerManager.LeaveActiveState(StateController_IDs.STATECONTROLLER_CHOOSEYOURCAREER, StateController_StateIDs_ChooseYourCareer.STATE_CAREERDONE);
				StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_KNOWYOURROLE, StateController_StateIDs_KnowYourRole.STATE_CHOICEWEB);
			}
			else
			{
				submitActivity();
			}
		}
		
		private function submitActivity():void
		{
			StateControllerManager.LeaveAllActiveStates();
			StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_MAIN, StateController_StateIDs_Main.STATE_SESSIONATTRACT);
			Document.TellServerActivityComplete();
		}
	}
}
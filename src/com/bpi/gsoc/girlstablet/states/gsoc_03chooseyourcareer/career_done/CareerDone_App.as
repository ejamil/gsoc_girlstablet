package com.bpi.gsoc.girlstablet.states.gsoc_03chooseyourcareer.career_done
{
	import com.bpi.gsoc.framework.states.app.GSOC_AppSkin;
	
	public class CareerDone_App extends GSOC_AppSkin
	{
		public function CareerDone_App()
		{
			super(false);
		}
	}
}
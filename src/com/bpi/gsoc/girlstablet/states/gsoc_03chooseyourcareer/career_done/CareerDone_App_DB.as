package com.bpi.gsoc.girlstablet.states.gsoc_03chooseyourcareer.career_done
{
	import com.bpi.gsoc.components.chooseyourcareer.CareerChoice_Screen3_DB_Skin;

	public class CareerDone_App_DB extends CareerDone_App
	{
		public function CareerDone_App_DB()
		{
			m_Skin = new CareerChoice_Screen3_DB_Skin();
			
			super.Initialize();
		}
	}
}
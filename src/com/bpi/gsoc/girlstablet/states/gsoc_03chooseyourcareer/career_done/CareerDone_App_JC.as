package com.bpi.gsoc.girlstablet.states.gsoc_03chooseyourcareer.career_done
{
	import com.bpi.gsoc.components.chooseyourcareer.CareerChoice_Screen3_JC_Skin;

	public class CareerDone_App_JC extends CareerDone_App
	{
		public function CareerDone_App_JC()
		{
			m_Skin = new CareerChoice_Screen3_JC_Skin();

			super.Initialize();
		}
	}
}
package com.bpi.gsoc.girlstablet.states.attract
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.citrusas.citrus.framework.layers.LayerIDs;
	import com.bpi.citrusas.input.singleinput.Input;
	import com.bpi.citrusas.states.AppState;
	import com.bpi.citrusas.states.StateControllerManager;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_IDs;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_Main;
	
	import flash.ui.Keyboard;

	public class AppState_Attract extends AppState
	{
		private var m_AppAttract:App_Attract;
		
		public function AppState_Attract()
		{
			super(StateController_StateIDs_Main.STATE_ATTRACT);
			
			m_AppAttract = new App_Attract();
		}
		
		public override function Initialize():void
		{
			m_AppAttract.Initialize();
			
			super.Initialize();
		}
		
		public override function CleanUp():void
		{
			m_AppAttract.CleanUp();
			
			super.CleanUp();
		}
		
		public override function Update():void
		{
			checkMoveToConnectionField();
			
			m_AppAttract.Update();
			
			super.Update();
		}
		
		protected override function Init():void
		{
			m_AppAttract.Init();
			
			CitrusFramework.layerController.GetLayerByID(LayerIDs.CONTENT).addChild(m_AppAttract);
			
			super.Init();
		}
		
		protected override function DeInit():void
		{
			m_AppAttract.DeInit();
			
			CitrusFramework.layerController.GetLayerByID(LayerIDs.CONTENT).removeChild(m_AppAttract);
			
			super.DeInit();
		}
		
		public override function QueuedUpdate():void
		{
			
		}
		
		private function checkMoveToConnectionField():void
		{
			if(Input.IsKeyPressed(Keyboard.N) && Input.IsKeyDown(Keyboard.CONTROL))
			{
				StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_MAIN, StateController_StateIDs_Main.STATE_NETWORKCONNECTION, true);
			}
		}
	}
}
package com.bpi.gsoc.girlstablet.states.waitforstart
{
	import com.bpi.citrusas.images.AnimationClip;
	import com.bpi.gsoc.components.attractvideos.WaitForStart_Attract;
	import com.bpi.gsoc.components.girlstablet.general.WaitForStart_Background;
	import com.bpi.gsoc.framework.states.general.servercommunication.states.update.content.ProgressCircle;
	
	import flash.display.Sprite;
	import flash.geom.Point;

	public class App_WaitForStart extends Sprite
	{
		private static const ms_SpinnerPosition:Point = new Point(1370, 950);
		
		private var m_Spinner:ProgressCircle;
		private var m_Background:AnimationClip;
		private var m_Attract:AnimationClip;
		
		public function App_WaitForStart()
		{
			m_Background = new AnimationClip(WaitForStart_Background);
			m_Spinner = new ProgressCircle();
			m_Attract = new AnimationClip(WaitForStart_Attract);
		}
		
		public function InitializeEventListeners():void
		{
		}
		
		public function Initialize():void
		{	
			m_Spinner.Initialize();
			
			this.addChild(m_Background);
			this.addChild(m_Attract);
			this.addChild(m_Spinner);
		}
		
		public function CleanUp():void
		{
		}
		
		public function Init():void
		{
			m_Attract.Play();
			m_Spinner.SetPosition(ms_SpinnerPosition);
		}
		
		public function DeInit():void
		{
			m_Attract.Stop();
		}
		
		public function Update():void
		{
			m_Spinner.Update();
		}
		
		public function QueuedUpdate():void
		{
		}
		
		public function Resume():void
		{
		}
		
		public function Pause():void
		{
			
		}
	}
}
package com.bpi.gsoc.girlstablet.states.waitforstart
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.citrusas.citrus.framework.layers.LayerIDs;
	import com.bpi.citrusas.states.AppState;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_Main;

	public class AppState_WaitForStart extends AppState
	{
		private var m_AppWaitForStart:App_WaitForStart;
		
		public function AppState_WaitForStart()
		{
			super(StateController_StateIDs_Main.STATE_WAITFORSTART);
			
			m_AppWaitForStart = new App_WaitForStart();
		}
		
		public override function Initialize():void
		{
			m_AppWaitForStart.Initialize();
			
			super.Initialize();
		}
		
		public override function CleanUp():void
		{
			m_AppWaitForStart.CleanUp();
			
			super.CleanUp();
		}
		
		public override function Update():void
		{
			m_AppWaitForStart.Update();
			
			super.Update();
		}
		
		protected override function Init():void
		{
			m_AppWaitForStart.Init();
			
			CitrusFramework.layerController.GetLayerByID(LayerIDs.CONTENT).addChild(m_AppWaitForStart);
			
			super.Init();
		}
		
		protected override function DeInit():void
		{
			m_AppWaitForStart.DeInit();
			
			CitrusFramework.layerController.GetLayerByID(LayerIDs.CONTENT).removeChild(m_AppWaitForStart);
			
			super.DeInit();
		}
		
		public override function QueuedUpdate():void
		{
			
		}
	}
}
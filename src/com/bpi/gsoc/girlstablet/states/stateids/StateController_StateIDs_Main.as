package com.bpi.gsoc.girlstablet.states.stateids
{
	public class StateController_StateIDs_Main
	{
		private static var ENUM_VALUE:int = 0;
		public static const STATE_CMS:int = ENUM_VALUE++;
		public static const STATE_ATTRACT:int = ENUM_VALUE++;
		public static const STATE_NETWORKCONNECTION:int = ENUM_VALUE++;
		public static const STATE_SETUPATTRACT:int = ENUM_VALUE++;
		public static const STATE_SESSIONATTRACT:int = ENUM_VALUE++;
		public static const STATE_WAITFORSTART:int = ENUM_VALUE++;
		public static const STATE_END:int = ENUM_VALUE++;
		public static const STATE_WAITFORACTIVATION:int = ENUM_VALUE++;
	}
}
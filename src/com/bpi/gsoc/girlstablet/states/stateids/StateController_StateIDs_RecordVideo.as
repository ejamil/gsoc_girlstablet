package com.bpi.gsoc.girlstablet.states.stateids
{
	public class StateController_StateIDs_RecordVideo
	{
		public static const STATE_ASSIGNROLES:int = 0;
		public static const STATE_WRITESCRIPT:int = 1;
		public static const STATE_WARMUP:int = 2;
		public static const STATE_RECORDVIDEO:int = 3;
		public static const STATE_REVIEWVIDEO:int = 4;
		public static const STATE_EDITVIDEO:int = 5;
		public static const STATE_PLAYBACKVIDEO:int = 6;
	}
}
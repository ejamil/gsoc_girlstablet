package com.bpi.gsoc.girlstablet.states.stateids
{
	public class StateController_StateIDs_PictureYourself
	{
		public static const STATE_NAMESELECTION:int = 0;
		public static const STATE_CUSTOMIZECHARACTER:int = 1;
		public static const STATE_TAKEPICTURE:int = 2;
		public static const STATE_LOADING:int = 4;
	}
}
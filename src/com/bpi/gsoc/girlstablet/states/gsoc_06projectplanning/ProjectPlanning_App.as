package com.bpi.gsoc.girlstablet.states.gsoc_06projectplanning
{
	import com.bpi.citrusas.states.StateControllerManager;
	import com.bpi.citrusas.utils.Utils_Image;
	import com.bpi.gsoc.framework.constants.Keys_FileNames;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.data.userdata.GSOC_UserData_Team_ImageKeys;
	import com.bpi.gsoc.framework.states.app.GSOC_AppSkin;
	import com.bpi.gsoc.girlstablet.global_data.upload.UploadManager;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_IDs;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_Main;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_ProjectPlanning;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_RecordVideo;
	
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;

	public class ProjectPlanning_App extends GSOC_AppSkin
	{
		protected var indexOfProblemPuzzlePiece:int;
		
		public function ProjectPlanning_App()
		{
			super(false);
		}
		
		public override function Init():void
		{
			super.Init();
		}
		
		protected function SubmitButtonHandler(event:Event):void
		{
			var tempSprite:Sprite = new Sprite();
			m_Skin.header.visible = false;
			m_Skin.submit_Button.visible = false;
			m_GSOCHeader.visible = false;
			tempSprite.addChild(m_Skin);
			
			var bitmap:Bitmap = Utils_Image.GetNonTransparentAreaOfDisplayObject(tempSprite);
			var finalSprite:Sprite = new Sprite();
			finalSprite.addChild(bitmap);
			
			SessionInformation_GirlsTablet.TeamInfo.Team_UserData.SetImage(GSOC_UserData_Team_ImageKeys.KEY_PROJECTPLANNING_PUZZLE, finalSprite);
			SessionInformation_GirlsTablet.UploadCurrentInformation();
			UploadManager.UploadDisplayObject(finalSprite, Keys_FileNames.KEY_PROJECTPLANNING_PUZZLE);
			
			if(SessionInformation_GirlsTablet.IsRunningLocally)
			{
				StateControllerManager.LeaveActiveState(StateController_IDs.STATECONTROLLER_PROJECTPLANNING, StateController_StateIDs_ProjectPlanning.STATE_MAIN);
				StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_RECORDVIDEO, StateController_StateIDs_RecordVideo.STATE_ASSIGNROLES);
			}
			else
			{
				submitActivity();
			}
		}
		
		protected function submitActivity():void
		{
			StateControllerManager.LeaveAllActiveStates();
			StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_MAIN, StateController_StateIDs_Main.STATE_SESSIONATTRACT);
			
			saveNotes();
			
			Document.TellServerActivityComplete();
		}
		
		private function saveNotes():void
		{
			var notes:Sprite = this.m_Skin.notes;
			notes.x = 0;
			notes.y = 0;
			
			UploadManager.UploadDisplayObject(notes, Keys_FileNames.KEY_PROJECTPLANNING_NOTES);
		}
	}
}
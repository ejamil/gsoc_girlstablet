package com.bpi.gsoc.girlstablet.states.gsoc_06projectplanning
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.citrusas.input.inputcontroller.InputController;
	import com.bpi.citrusas.ui.draggableobject.DraggableObject;
	import com.bpi.citrusas.ui.draggableobject.events.Event_DraggableObject;
	import com.bpi.citrusas.utils.Utils_Text;
	import com.bpi.gsoc.components.projectplanning.ProjectPlanning_Screen_DB_Skin;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.embeddedcontent.EmbeddedFonts;
	import com.bpi.gsoc.framework.gsocui.GSOC_Header;
	import com.bpi.gsoc.framework.gsocui.STEM_Popups;
	import com.bpi.gsoc.girlstablet.states.gsoc_06projectplanning.calloutwindow.CalloutWindow;
	import com.bpi.gsoc.girlstablet.states.gsoc_06projectplanning.calloutwindow.Event_CalloutWindow;
	import com.bpi.gsoc.girlstablet.states.gsoc_06projectplanning.calloutwindow.OptionField;
	import com.bpi.gsoc.girlstablet.states.gsoc_06projectplanning.noteswindow.Event_NotesWindow;
	import com.bpi.gsoc.girlstablet.states.gsoc_06projectplanning.noteswindow.NotesWindow;
	import com.bpi.gsoc.girlstablet.states.gsoc_06projectplanning.puzzlepieces.GroupOfPuzzlePieces;
	import com.bpi.gsoc.girlstablet.states.gsoc_06projectplanning.puzzlepieces.PlacedPuzzlePiece;
	import com.bpi.gsoc.girlstablet.states.gsoc_06projectplanning.puzzlepieces.PopupPuzzlePiece;
	import com.greensock.TweenLite;
	
	import flash.display.MovieClip;
	import flash.filters.DropShadowFilter;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.text.TextFormat;

	public class ProjectPlanning_App_DB extends ProjectPlanning_App
	{
		private var db_Skin:ProjectPlanning_Screen_DB_Skin;
		private var db_CalloutWindow:CalloutWindow;
		private var puzzleTextFormat:TextFormat;
		private var puzzleTextFormatFrame2:TextFormat;
		
		private var puzzlePieces:Vector.<GroupOfPuzzlePieces>;
		private var currentEmptyPuzzleIndex:int;
		private var emptyPuzzlePlaces:Vector.<MovieClip>;
		private var activePuzzleGroupIndex:int;
		
		private var placedPieces:Vector.<PlacedPuzzlePiece>;
		
		private var notesDraggableObject:DraggableObject;
		private var notesWindow:NotesWindow;
		private var notesVisibleWidth:Number;
		
		private var popupPieces:Vector.<PopupPuzzlePiece>;
		
		private const PUZZLE_TEXTS:Vector.<String> = new <String>["Find Supplies", "Make Issue Posters", "Ask Friends to Help", "Find Place to Meet"];
		private const FIND_SUPPLIES_OPTIONS:Vector.<String> = new <String>["Use Cookie Money to Buy Supplies", "Collect Supplies from Home"];
		
		public function ProjectPlanning_App_DB()
		{
			super();
			
			this.m_Skin = new ProjectPlanning_Screen_DB_Skin();
			
			this.db_Skin = this.m_Skin as ProjectPlanning_Screen_DB_Skin;
			this.db_Skin.prefilledpieces.mouseEnabled = false;
			
			m_GSOCHeader = new GSOC_Header(m_Skin.header.paginationArea);
			GSOCheader.addArrows(m_Skin.header.paginationArea.x, 0);
		}
		
		public override function Initialize():void
		{
			super.Initialize();
			
			this.db_CalloutWindow = new CalloutWindow(this.db_Skin.calloutbox, new OptionField(this.db_Skin.calloutbox.optionbox1, this.db_Skin.calloutbox.optiontext1), new OptionField(this.db_Skin.calloutbox.optionbox2, this.db_Skin.calloutbox.optiontext2));
			this.db_CalloutWindow.InitializeFonts(new TextFormat(EmbeddedFonts.FontName_PostinoTTF, 36, 0x723b18), new TextFormat(EmbeddedFonts.FontName_PostinoTTF, 48, 0x723b18), new TextFormat(EmbeddedFonts.FontName_PostinoTTF, 32, 0xffffff));
			this.db_CalloutWindow.InitializeOptionTexts(this.FIND_SUPPLIES_OPTIONS);
			this.puzzleTextFormat = new TextFormat(EmbeddedFonts.FontName_PostinoTTF, 35, 0xffc413);
			this.puzzleTextFormatFrame2 = new TextFormat(EmbeddedFonts.FontName_PostinoTTF, 35, 0xffffff);
			
			this.puzzlePieces = new Vector.<GroupOfPuzzlePieces>();
			this.puzzlePieces.push(new GroupOfPuzzlePieces(this.db_Skin.leftAddEnd0, this.db_Skin.middleOutAddConnector0, this.db_Skin.middleInAddConnector0, this.db_Skin.middleOutAddConnector4, this.db_Skin.rightAddEnd0));
			this.puzzlePieces.push(new GroupOfPuzzlePieces(this.db_Skin.leftAddEnd1, this.db_Skin.middleOutAddConnector1, this.db_Skin.middleInAddConnector1, this.db_Skin.middleOutAddConnector5, this.db_Skin.rightAddEnd1));
			this.puzzlePieces.push(new GroupOfPuzzlePieces(this.db_Skin.leftAddEnd2, this.db_Skin.middleOutAddConnector2, this.db_Skin.middleInAddConnector2, this.db_Skin.middleOutAddConnector6, this.db_Skin.rightAddEnd2));
			this.puzzlePieces.push(new GroupOfPuzzlePieces(this.db_Skin.leftAddEnd3, this.db_Skin.middleOutAddConnector3, this.db_Skin.middleInAddConnector3, this.db_Skin.middleOutAddConnector7, this.db_Skin.rightAddEnd3));
			this.puzzlePieces[0].Initialize(this.PUZZLE_TEXTS[0], this.FindPlaceToMeetCallback);
			this.puzzlePieces[1].Initialize(this.PUZZLE_TEXTS[1]);
			this.puzzlePieces[2].Initialize(this.PUZZLE_TEXTS[2]);
			this.puzzlePieces[3].Initialize(this.PUZZLE_TEXTS[3]);
			
			this.emptyPuzzlePlaces = new Vector.<MovieClip>();
			this.emptyPuzzlePlaces.push(this.db_Skin.blank_end1);
			this.emptyPuzzlePlaces.push(this.db_Skin.blank_endconnector1);
			this.emptyPuzzlePlaces.push(this.db_Skin.blank_middleconnector);
			this.emptyPuzzlePlaces.push(this.db_Skin.blank_endconnector2);
			this.emptyPuzzlePlaces.push(this.db_Skin.blank_end2);
			
			this.popupPieces = new Vector.<PopupPuzzlePiece>();
			this.popupPieces.push(new PopupPuzzlePiece(this.db_Skin.popupPiece0));
			this.popupPieces.push(new PopupPuzzlePiece(this.db_Skin.popupPiece1));
			this.popupPieces.push(new PopupPuzzlePiece(this.db_Skin.popupPiece2));
			this.popupPieces.push(new PopupPuzzlePiece(this.db_Skin.popupPiece3));
		
			for each(var piece:PopupPuzzlePiece in this.popupPieces)
			{
				piece.puzzlePieceGraphic.visible = false;
			}
		
			this.currentEmptyPuzzleIndex = 0;
			this.activePuzzleGroupIndex = -1;
			this.placedPieces = new Vector.<PlacedPuzzlePiece>();
			
			this.notesDraggableObject = new DraggableObject(this.db_Skin.notes, this.db_Skin.notes.notesHitBox);
			this.notesWindow = new NotesWindow(this.db_Skin.notes, new TextFormat(EmbeddedFonts.FontName_PostinoTTF, 36, 0x993092), SessionInformation_GirlsTablet.Age);
			this.notesVisibleWidth = this.db_Skin.notes.edgeMarker.x;
			this.notesWindow.AddEventListeners();
			this.notesWindow.addEventListener(Event_NotesWindow.CLOSE, this.CloseNotesWindowHandler);
			this.notesWindow.addEventListener(Event_NotesWindow.SAVE, this.SaveNotesWindowHandler);
	
			
			this.SetUpChildIndexes();
		}
		
		public override function Init():void
		{
			super.Init();
			
			this.AddGlobalWindows();
			
			this.ToggleSubmitButton(false);
			
			this.InitPuzzlePieces();
			
			this.ToggleNoteButton(true);
			
			this.notesWindow.Init();
		}
		
		public override function DeInit():void
		{
			this.RemoveGlobalWindows();
		}
		
		private function InitPuzzlePieces():void
		{
			this.currentEmptyPuzzleIndex = 0;
			this.indexOfProblemPuzzlePiece = 2;
			this.activePuzzleGroupIndex = -1;
			
			for(var i:int = 0; i < this.puzzlePieces.length; i++)
			{
				this.puzzlePieces[i].SetVisible(true);
				this.puzzlePieces[i].AddDraggableEventListeners(this.PuzzleDragStartHandler, this.PuzzleDragEndHandler);
				this.puzzlePieces[i].SetPuzzlePiece(this.currentEmptyPuzzleIndex);
			}
			
			this.SetTextFormatToPuzzlePieceTextfield(this.db_Skin.leftAddEnd0.text, this.puzzleTextFormat, true);
			this.SetTextFormatToPuzzlePieceTextfield(this.db_Skin.leftAddEnd1.text, this.puzzleTextFormat, true);
			this.SetTextFormatToPuzzlePieceTextfield(this.db_Skin.leftAddEnd2.text, this.puzzleTextFormat, true);
			this.SetTextFormatToPuzzlePieceTextfield(this.db_Skin.leftAddEnd3.text, this.puzzleTextFormat, true);
			
			this.SetTextFormatToPuzzlePieceTextfield(this.db_Skin.middleOutAddConnector0.text, this.puzzleTextFormat, true);
			this.SetTextFormatToPuzzlePieceTextfield(this.db_Skin.middleOutAddConnector1.text, this.puzzleTextFormat, true);
			this.SetTextFormatToPuzzlePieceTextfield(this.db_Skin.middleOutAddConnector2.text, this.puzzleTextFormat, true);
			this.SetTextFormatToPuzzlePieceTextfield(this.db_Skin.middleOutAddConnector3.text, this.puzzleTextFormat, true);
			
			this.SetTextFormatToPuzzlePieceTextfield(this.db_Skin.middleInAddConnector0.text, this.puzzleTextFormat, true);
			this.SetTextFormatToPuzzlePieceTextfield(this.db_Skin.middleInAddConnector1.text, this.puzzleTextFormat, true);
			this.SetTextFormatToPuzzlePieceTextfield(this.db_Skin.middleInAddConnector2.text, this.puzzleTextFormat, true);
			this.SetTextFormatToPuzzlePieceTextfield(this.db_Skin.middleInAddConnector3.text, this.puzzleTextFormat, true);
			
			this.SetTextFormatToPuzzlePieceTextfield(this.db_Skin.middleOutAddConnector4.text, this.puzzleTextFormat, true);
			this.SetTextFormatToPuzzlePieceTextfield(this.db_Skin.middleOutAddConnector5.text, this.puzzleTextFormat, true);
			this.SetTextFormatToPuzzlePieceTextfield(this.db_Skin.middleOutAddConnector6.text, this.puzzleTextFormat, true);
			this.SetTextFormatToPuzzlePieceTextfield(this.db_Skin.middleOutAddConnector7.text, this.puzzleTextFormat, true);
			
			this.SetTextFormatToPuzzlePieceTextfield(this.db_Skin.rightAddEnd0.text, this.puzzleTextFormat, true);
			this.SetTextFormatToPuzzlePieceTextfield(this.db_Skin.rightAddEnd1.text, this.puzzleTextFormat, true);
			this.SetTextFormatToPuzzlePieceTextfield(this.db_Skin.rightAddEnd2.text, this.puzzleTextFormat, true);
			this.SetTextFormatToPuzzlePieceTextfield(this.db_Skin.rightAddEnd3.text, this.puzzleTextFormat, true);
			
			this.SetUpChildIndexes();
		}
		
		private function SetTextFormatToPuzzlePieceTextfield(textField:TextField, textFormat:TextFormat, addDropShadow:Boolean):void
		{
			textField.defaultTextFormat = textFormat;
			textField.setTextFormat(textFormat);
			
			if(addDropShadow)
			{
				var dropShadow:DropShadowFilter = new DropShadowFilter(2, 45, 0, 0.6, 6, 6);
				textField.filters = [ dropShadow ];
			}
			else
			{
				textField.filters = [];
			}
			
			Utils_Text.FitTextToTextfield(textField, 4, true);
		}
		
		private function PuzzleDragStartHandler(e:Event_DraggableObject, currentPuzzleGroup:GroupOfPuzzlePieces):void
		{
			this.ToggleSubmitButton(false);
			
			// if a piece is currently picked up in addition to this one
			if(this.activePuzzleGroupIndex != -1) 
			{
				// dont' drop the current piece if it's the "correct" active one
				if(! (this.puzzlePieces.indexOf(currentPuzzleGroup) == this.activePuzzleGroupIndex))
				{
					// but do drop it if it's the second one to be picked up
					currentPuzzleGroup.ForceDropActivePuzzlePieces();
				}
			}
			else
			{
				// otherwise we can just set the active index
				this.activePuzzleGroupIndex = this.puzzlePieces.indexOf(currentPuzzleGroup);
			}	
		}
		
		private function PuzzleDragEndHandler(event:Event_DraggableObject, groupOfPuzzlePieces:GroupOfPuzzlePieces):void
		{
			this.activePuzzleGroupIndex = -1;
			// check to see if the dragged puzzle piece is on top of the empty puzzle piece
			if(!groupOfPuzzlePieces.placeholder)
			{
				//groupOfPuzzlePieces.DisplayTextFieldSize();
				if((event.target as DraggableObject).draggableObject.hitTestObject(this.emptyPuzzlePlaces[this.currentEmptyPuzzleIndex]))
				{
					groupOfPuzzlePieces.SetFrame(2);
					groupOfPuzzlePieces.placeholder = this.emptyPuzzlePlaces[this.currentEmptyPuzzleIndex];
					this.SetTextFormatToPuzzlePieceTextfield(groupOfPuzzlePieces.puzzlePieces[groupOfPuzzlePieces.activePiece].text, this.puzzleTextFormatFrame2, true);
		
					// need to tween from the current position to the below position
					(event.target as DraggableObject).draggableObject.x = this.emptyPuzzlePlaces[this.currentEmptyPuzzleIndex].x;
					(event.target as DraggableObject).draggableObject.y = this.emptyPuzzlePlaces[this.currentEmptyPuzzleIndex].y;
					
					// if the placed puzzle piece is a middle connector, put it on top so that the text doesn't get cut off or overlapped
					
					for (var i:int = 0; i < this.puzzlePieces.length; i++)
					{
						if (this.puzzlePieces[i].puzzlePieces[this.puzzlePieces[i].activePiece].name.indexOf("middleInAddConnector") != -1)
						{
							//this.db_Skin.addChildAt(this.puzzlePieces[i].puzzlePieces[this.puzzlePieces[i].activePiece], this.db_Skin.numChildren); // place it on top
						}
					}
					
					this.placedPieces.push(new PlacedPuzzlePiece(groupOfPuzzlePieces));
					
					if(groupOfPuzzlePieces.optionCallback)
					{
						// remove drag listeners until option has been selected
						for(i = 0; i < this.puzzlePieces.length; i++)
						{
							this.puzzlePieces[i].RemoveDraggableEventListeners();
						}
						groupOfPuzzlePieces.optionCallback();
					}
					else
					{
						STEM_Popups.DisplayPopup(STEM_Popups.PROJECT_PLANNING);
						this.currentEmptyPuzzleIndex++;
						if(this.currentEmptyPuzzleIndex == groupOfPuzzlePieces.puzzlePieces.length)
						{
							// show submit button
							this.ToggleSubmitButton(true);
						}
						else
						{
							for(i = 0; i < this.puzzlePieces.length; i++)
							{
								this.puzzlePieces[i].SetPuzzlePiece(this.currentEmptyPuzzleIndex);
							}
						}
						
						if(this.placedPieces.length > 1)
						{
							this.placedPieces[this.placedPieces.length-2].puzzlePieces.RemoveDraggableEventListeners();
						}
					}
				}
				else
				{
					// snap it back to its original location (add tween)
					(event.target as DraggableObject).draggableObject.x = (event.target as DraggableObject).originalPosition.x;
					(event.target as DraggableObject).draggableObject.y = (event.target as DraggableObject).originalPosition.y;
				}
			}
			else
			{
				if((event.target as DraggableObject).draggableObject.hitTestObject(groupOfPuzzlePieces.placeholder))
				{
					if(this.currentEmptyPuzzleIndex == groupOfPuzzlePieces.puzzlePieces.length)
					{
						this.ToggleSubmitButton(true);
					}
					
					// snap it back to its placed location
					(event.target as DraggableObject).draggableObject.x = groupOfPuzzlePieces.placeholder.x;
					(event.target as DraggableObject).draggableObject.y = groupOfPuzzlePieces.placeholder.y;
				}
				else
				{
					// the puzzle piece was in a placed position and was dragged out of it, send it back to it's original location and reset it so it can be placed again
					(event.target as DraggableObject).draggableObject.x = (event.target as DraggableObject).originalPosition.x;
					(event.target as DraggableObject).draggableObject.y = (event.target as DraggableObject).originalPosition.y;
					
					this.currentEmptyPuzzleIndex--;
					
					var pop:PlacedPuzzlePiece = this.placedPieces.pop();
					if(pop.linkedToNextPiece)
					{
						// need to remove the next puzzle piece from the child
						pop.puzzlePieces.puzzlePieces[pop.puzzlePieces.activePiece].removeChild(pop.puzzlePieces.puzzlePieces[pop.puzzlePieces.activePiece].getChildByName(pop.linkedPieceName));
						this.currentEmptyPuzzleIndex--;
					}
					
					groupOfPuzzlePieces.placeholder = null;
					groupOfPuzzlePieces.SetFrame(1);
					this.SetTextFormatToPuzzlePieceTextfield(groupOfPuzzlePieces.puzzlePieces[groupOfPuzzlePieces.activePiece].text, this.puzzleTextFormat, true);
					Utils_Text.FitTextToTextfield(groupOfPuzzlePieces.puzzlePieces[groupOfPuzzlePieces.activePiece].text, 0, true);
					
					for(i = 0; i < this.puzzlePieces.length; i++)
					{
						this.puzzlePieces[i].SetPuzzlePiece(this.currentEmptyPuzzleIndex);
					}
					
					if(this.placedPieces.length > 0)
					{
						this.placedPieces[this.placedPieces.length-1].puzzlePieces.AddDraggableEventListeners(this.PuzzleDragStartHandler, this.PuzzleDragEndHandler);
					}
				}
			}
			
			var effectiveIndex:int = -1;
			for (i = 0; i < this.placedPieces.length; i ++)
			{
				effectiveIndex++;
				// check if group has linked piece
				if(this.placedPieces[i].linkedToNextPiece)
				{
					effectiveIndex++;
				}
				
				if(effectiveIndex == this.indexOfProblemPuzzlePiece) 
				{
					this.db_Skin.addChildAt(this.placedPieces[i].puzzlePieces.puzzlePieces[this.placedPieces[i].puzzlePieces.activePiece], this.db_Skin.numChildren);
				}
			}
		}
		
		private function FindPlaceToMeetCallback():void
		{
			this.db_CalloutWindow.visible = true;
			this.db_CalloutWindow.PlaceWindowOnTop();
			
			var middleCurrentEmptyPieceX:int = this.emptyPuzzlePlaces[this.currentEmptyPuzzleIndex].x + (this.emptyPuzzlePlaces[this.currentEmptyPuzzleIndex].width/2) - 30; // include 30 for shadow...
			var middleCurrentEmptyPieceY:int = this.emptyPuzzlePlaces[this.currentEmptyPuzzleIndex].y + (this.emptyPuzzlePlaces[this.currentEmptyPuzzleIndex].height/2);
			
			this.db_CalloutWindow.SetPosition(new Point(middleCurrentEmptyPieceX, middleCurrentEmptyPieceY));
			
			this.db_CalloutWindow.AddEventListeners();
			this.db_CalloutWindow.addEventListener(Event_CalloutWindow.OPTION_SELECTED, this.OptionSelectedHandler);
		}
		
		private function OptionSelectedHandler(event:Event_CalloutWindow):void
		{
			event.target.removeEventListener(Event_CalloutWindow.OPTION_SELECTED, this.OptionSelectedHandler);
			
			var popupPiece:PopupPuzzlePiece = this.popupPieces[this.currentEmptyPuzzleIndex];
			var mc:MovieClip = popupPiece.puzzlePieceGraphic;
			mc.visible = true;
			mc.gotoAndStop(2);
			this.SetTextFormatToPuzzlePieceTextfield(mc.text, this.puzzleTextFormatFrame2, true);
			mc.text.text = event.optionText;
			
			mc.y = CitrusFramework.frameworkStageSize.y; // move it off the stage so the text doesn't flicker from the wrong position
			var parent:ProjectPlanning_App_DB = this; // because for some reason 'this' is not recognized inside the anonymous function
			TweenLite.delayedCall(1, 
				function():void 
					{
						parent.DelayedOptionSelectedHandler(popupPiece);
					},
				null, true);
		}
		
		/**
		 * The dynamic text in the popup piece has to be centered vertically, but because of Flash, the text height
		 * isn't accurate until the piece is visible for one frame. So we set the text in OptionSelectedHandler, 
		 * delay one frame and then continue the option selected logic.
		 */
		private function DelayedOptionSelectedHandler(popupPiece:PopupPuzzlePiece):void
		{
			var mc:MovieClip = popupPiece.puzzlePieceGraphic;
			Utils_Text.FitTextToTextfield(mc.text, 0, true);
			mc.text.y = popupPiece.origTextPosition.y + mc.text.height / 2 - mc.text.textHeight / 2;
			
			
			this.currentEmptyPuzzleIndex++;
			
			mc.x = (this.emptyPuzzlePlaces[this.currentEmptyPuzzleIndex].x - this.emptyPuzzlePlaces[this.currentEmptyPuzzleIndex-1].x);
			mc.y = (this.emptyPuzzlePlaces[this.currentEmptyPuzzleIndex].y - this.emptyPuzzlePlaces[this.currentEmptyPuzzleIndex-1].y);
			// if the placed puzzle piece is a middle connector, put it on top so that the text doesn't get cut off or overlapped
			if(mc.name == "popupPiece1")
			{
				this.db_Skin.addChildAt(this.placedPieces[this.placedPieces.length-1].puzzlePieces.puzzlePieces[this.placedPieces[this.placedPieces.length-1].puzzlePieces.activePiece], this.db_Skin.numChildren-2);
				this.placedPieces[this.placedPieces.length-1].puzzlePieces.puzzlePieces[this.placedPieces[this.placedPieces.length-1].puzzlePieces.activePiece].addChild(mc);
			}
			else this.placedPieces[this.placedPieces.length-1].puzzlePieces.puzzlePieces[this.placedPieces[this.placedPieces.length-1].puzzlePieces.activePiece].addChildAt(mc, 0);
			this.placedPieces[this.placedPieces.length-1].linkedToNextPiece = true;
			this.placedPieces[this.placedPieces.length-1].linkedPieceName = mc.name;
			
			this.currentEmptyPuzzleIndex++;
			if(this.currentEmptyPuzzleIndex == this.puzzlePieces[0].puzzlePieces.length)
			{
				// show submit button
				this.ToggleSubmitButton(true);
				
				// make sure the puzzle piece has the drag listeners on it
				this.placedPieces[this.placedPieces.length-1].puzzlePieces.AddDraggableEventListeners(this.PuzzleDragStartHandler, this.PuzzleDragEndHandler);
			}
			else
			{
				this.ToggleSubmitButton(false);; // hide the submit button
				
				for(var i:int = 0; i < this.puzzlePieces.length; i++)
				{
					this.puzzlePieces[i].SetPuzzlePiece(this.currentEmptyPuzzleIndex);
					if(!this.puzzlePieces[i].placeholder)
					{
						this.puzzlePieces[i].AddDraggableEventListeners(this.PuzzleDragStartHandler, this.PuzzleDragEndHandler);
					}
				}
				if(this.placedPieces.length > 0)
				{
					this.placedPieces[this.placedPieces.length-1].puzzlePieces.AddDraggableEventListeners(this.PuzzleDragStartHandler, this.PuzzleDragEndHandler);
				}
			}
			
			this.db_CalloutWindow.visible = false;
			this.db_CalloutWindow.RemoveEventListeners();
		}
		
		private function ToggleSubmitButton(toggle:Boolean):void
		{
			if(toggle)
			{
				
				// (TODO add tween) // See utility for toggling buttons: GSOC_ButtonToggle.ToggleButton(...);
				this.db_Skin.submit_Button.visible = true;
				this.db_Skin.submit_Button.addEventListener(InputController.CLICK, this.SubmitButtonHandler);
			}
			else
			{
				// (TODO add tween)
				this.db_Skin.submit_Button.visible = false;
				this.db_Skin.submit_Button.removeEventListener(InputController.CLICK, this.SubmitButtonHandler);
			}
		}
		
		private function ToggleNoteButton(toggle:Boolean):void
		{
			if(toggle)
			{
				this.notesDraggableObject.AddEventListeners();
				this.notesDraggableObject.addEventListener(Event_DraggableObject.DRAG_UPDATE, this.NoteDragUpdateHandler);
				this.notesDraggableObject.addEventListener(Event_DraggableObject.DRAG_END, this.NoteDragEndHandler);
			}
			else
			{
				this.notesDraggableObject.RemoveEventListeners();
				this.notesDraggableObject.removeEventListener(Event_DraggableObject.DRAG_UPDATE, this.NoteDragUpdateHandler);
				this.notesDraggableObject.removeEventListener(Event_DraggableObject.DRAG_END, this.NoteDragEndHandler);
			}
		}
		
		private function NoteDragUpdateHandler(event:Event_DraggableObject):void
		{
			this.notesDraggableObject.draggableObject.y = this.notesDraggableObject.originalPosition.y;
	
			if(this.notesDraggableObject.draggableObject.x < (CitrusFramework.frameworkStageSize.x - this.notesVisibleWidth))
			{
				// don't allow the notes window to go out further than it's full size
				this.notesDraggableObject.draggableObject.x = (CitrusFramework.frameworkStageSize.x - this.notesVisibleWidth);
			}
			else if(this.notesDraggableObject.draggableObject.x > this.notesDraggableObject.originalPosition.x)
			{
				// don't let the window go past its original position outside (prevents it from going off screen)
				this.notesDraggableObject.draggableObject.x = this.notesDraggableObject.originalPosition.x;
			}
		}
		
		private function NoteDragEndHandler(event:Event_DraggableObject):void
		{
			// check to see how far the object was dragged
			if(Math.abs(this.notesDraggableObject.dragStartPosition.x - event.currentPosition.x) > 25)
			{
				// we moved it more than x pixels, consider this a drag, not a click
				if(this.notesDraggableObject.draggableObject.x < (CitrusFramework.frameworkStageSize.x - (this.notesVisibleWidth)/2)) 
				{
					// if the window is more than halfway out, snap it to its fully out position (TODO add tween)
					this.notesDraggableObject.draggableObject.x = (CitrusFramework.frameworkStageSize.x - this.notesVisibleWidth);
				}
				else
				{
					// the window is less than halfway out, snap it back into its closed position (TODO add tween)
					this.notesDraggableObject.draggableObject.x = this.notesDraggableObject.originalPosition.x;
				}
			}
			else
			{
				// we didn't move it very far, consider it a click
				if(this.notesDraggableObject.dragStartPosition.x == this.notesDraggableObject.originalPosition.x)
				{
					// the notes window is currently closed, so open it!
					this.notesDraggableObject.draggableObject.x = (CitrusFramework.frameworkStageSize.x - this.notesVisibleWidth);
				}
				else
				{
					// the notes window is open, so close it!
					this.notesDraggableObject.draggableObject.x = this.notesDraggableObject.originalPosition.x;
				}
			}
		}
		
		private function SetUpChildIndexes():void
		{
			// the 4 puzzle pieces should be at the top
			for(var piece:int = 0; piece < this.puzzlePieces.length; piece++)
			{
				for(var inner:int = 0; inner < this.puzzlePieces[piece].puzzlePieces.length; inner++)
				{
					this.db_Skin.addChild(this.puzzlePieces[piece].puzzlePieces[inner]);
				}
			}
		}
		
		private function AddGlobalWindows():void
		{
			// put notes in the global layer
			CitrusFramework.globalLayer.addChild(this.notesDraggableObject.draggableObject);
			CitrusFramework.globalLayer.addChild(this.db_CalloutWindow.calloutWindow);
		}
		
		private function RemoveGlobalWindows():void
		{
			CitrusFramework.globalLayer.removeChild(this.notesDraggableObject.draggableObject);
			CitrusFramework.globalLayer.removeChild(this.db_CalloutWindow.calloutWindow);
		}
		
		private function CloseNotesWindowHandler(event:Event_NotesWindow):void
		{
			this.notesDraggableObject.draggableObject.x = this.notesDraggableObject.originalPosition.x;
		}
		
		private function SaveNotesWindowHandler(event:Event_NotesWindow):void
		{
			this.notesDraggableObject.draggableObject.x = this.notesDraggableObject.originalPosition.x;
		}
	}
}
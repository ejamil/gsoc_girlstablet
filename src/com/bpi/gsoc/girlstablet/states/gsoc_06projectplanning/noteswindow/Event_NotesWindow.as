package com.bpi.gsoc.girlstablet.states.gsoc_06projectplanning.noteswindow
{
	import flash.events.Event;
	
	public class Event_NotesWindow extends Event
	{
		public static const CLOSE:String = "com.bpi.gsoc.girlstablet.states.gsoc_06projectplanning.noteswindow.CLOSE";
		public static const SAVE:String = "com.bpi.gsoc.girlstablet.states.gsoc_06projectplanning.noteswindow.SAVE";
		public static const TOGGLE:String = "com.bpi.gsoc.girlstablet.states.gsoc_06projectplanning.noteswindow.TOGGLE";
		
		public function Event_NotesWindow(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
	}
}
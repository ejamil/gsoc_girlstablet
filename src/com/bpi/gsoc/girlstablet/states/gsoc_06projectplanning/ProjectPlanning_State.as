package com.bpi.gsoc.girlstablet.states.gsoc_06projectplanning
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.gsoc.framework.constants.AgeConstants;
	import com.bpi.gsoc.framework.constants.GSOC_Info;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.gsocui.keyboards.GSOC_Keyboards;
	import com.bpi.gsoc.framework.states.app.GSOC_AppState;
	import com.bpi.gsoc.girlstablet.global_data.FrameControlUtils;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_ProjectPlanning;

	public class ProjectPlanning_State extends GSOC_AppState
	{
		private var m_AppState:ProjectPlanning_App;
		
		public function ProjectPlanning_State()
		{
			super(StateController_StateIDs_ProjectPlanning.STATE_MAIN);
		}
		
		public override function Initialize():void
		{
			GSOC_Info.AgeGroup = SessionInformation_GirlsTablet.Age;
		}
		
		protected override function Init():void
		{
			switch(SessionInformation_GirlsTablet.Age)
			{
				case AgeConstants.KEY_DAISYBROWNIE:
					m_AppState = new ProjectPlanning_App_DB();
					break;
				case AgeConstants.KEY_JUNIORCADETTE:
					m_AppState = new ProjectPlanning_App_JC();
					break;
				case AgeConstants.KEY_SENIORAMBASSADOR:
					m_AppState = new ProjectPlanning_App_SA();
					break;
				default:
					m_AppState = new ProjectPlanning_App_DB();
			}
			
			m_AppState.Initialize();
			
			m_AppState.Init();
			
			GSOC_Keyboards.SetKeyboardType(GSOC_Keyboards.KEYBOARDTYPE_REGULAR);
			FrameControlUtils.StopAtFrameN(m_AppState);
			
			CitrusFramework.contentLayer.addChild(m_AppState);
			
			setUpScreen();
			
			EndInit();
		}
		
		private function setUpScreen():void
		{
			
		}
		
		protected override function DeInit():void
		{
			m_AppState.DeInit();
			
			CitrusFramework.contentLayer.removeChild(m_AppState);
			
			EndDeInit();
		}
	}
}
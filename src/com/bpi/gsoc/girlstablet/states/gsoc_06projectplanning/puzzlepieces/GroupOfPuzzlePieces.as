package com.bpi.gsoc.girlstablet.states.gsoc_06projectplanning.puzzlepieces
{
	import com.bpi.citrusas.sound.SoundHandler;
	import com.bpi.citrusas.ui.draggableobject.DraggableObject;
	import com.bpi.citrusas.ui.draggableobject.events.Event_DraggableObject;
	import com.bpi.citrusas.utils.Utils_Text;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.sfx.GSOC_SFX;
	import com.greensock.TweenLite;
	
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.geom.Point;
	
	public class GroupOfPuzzlePieces
	{
		private var _puzzlePieces:Vector.<MovieClip>;
		private var _draggableObjects:Vector.<DraggableObject>;
		private var dragEndCallback:Function;
		private var dragStartCallback:Function;
		private var activeFrame:int;
		private var isVisible:Boolean;
		private var _optionCallback:Function;
		private var _placeholder:DisplayObject;
		private var _subPieceBeingDragged:Boolean;
		
		public function GroupOfPuzzlePieces(... puzzleMovieClips)
		{
			super();
			
			this._puzzlePieces = new Vector.<MovieClip>();
			this._draggableObjects = new Vector.<DraggableObject>();
			
			for(var i:int = 0; i < puzzleMovieClips.length; i++)
			{
				this._puzzlePieces.push(puzzleMovieClips[i]);
				(puzzleMovieClips[i] as MovieClip).mouseChildren = false;
				this._draggableObjects.push(new DraggableObject(puzzleMovieClips[i]));
			}
			this.dragEndCallback = null;
			this.dragStartCallback = null;
			this.activeFrame = 0;
			this.isVisible = true;
			this._subPieceBeingDragged = false;
			
			this._placeholder = null;
			
			this.SetPuzzlePiece(this.activeFrame);
		}
		
		public function Initialize(puzzleText:String, optionCallback:Function = null):void
		{
			for(var i:int = 0; i < this._puzzlePieces.length; i++)
			{
				this._puzzlePieces[i].visible = true;
				this._puzzlePieces[i].text.text = puzzleText;
			}
			
			// delay one frame for the piece to be rendered to the stage so we can actually get the accurate text height 
			TweenLite.delayedCall(1, DisplayTextFieldSize, null, true);

			this._optionCallback = optionCallback;
		}
		
		public function DisplayTextFieldSize():void
		{
			for(var i:int = 0; i < this._puzzlePieces.length; i++)
			{
				Utils_Text.FitTextToTextfield(this._puzzlePieces[i].text, 4, true);
				this._puzzlePieces[i].text.y = this._puzzlePieces[i].text.y + this._puzzlePieces[i].text.height / 2 - this._puzzlePieces[i].text.textHeight / 2;
			}
		}
		
		public function get puzzlePieces():Vector.<MovieClip>
		{
			return this._puzzlePieces;
		}
		
		public function get optionCallback():Function
		{
			return this._optionCallback;
		}
		
		public function get activePiece():int
		{
			return this.activeFrame;
		}
		
		public function get currentPuzzlePieceClass():Class
		{
			return Object(this._puzzlePieces[this.activeFrame]).constructor;
		}
		
		public function get nextPuzzlePieceClass():Class
		{
			if(this.activeFrame < this._puzzlePieces.length - 1)
			{
				return Object(this._puzzlePieces[this.activeFrame + 1]).constructor;
			}
			return Object(this._puzzlePieces[this.activeFrame]).constructor;
		}
		
		public function get visible():Boolean
		{
			return this.isVisible;
		}
		
		public function set placeholder(placeholder:DisplayObject):void
		{
			this._placeholder = placeholder;
		}
		
		public function get placeholder():DisplayObject
		{
			return this._placeholder;
		}
		
		public function SetVisible(isVisible:Boolean):void
		{
			this.isVisible = isVisible;
			for(var i:int = 0; i < this._puzzlePieces.length; i++)
			{
				this._puzzlePieces[i].visible = isVisible;
				Utils_Text.FitTextToTextfield(this._puzzlePieces[i].text, 0, true);
			}
		}
		
		public function SetFrame(frameIndex:int):void
		{
			if(!this._placeholder)
			{
				// only set frame if the piece isn't placed yet
				for(var i:int = 0; i < this._puzzlePieces.length; i++)
				{
					this._puzzlePieces[i].gotoAndStop(frameIndex);
				}
			}
		}
		
		public function SetPuzzlePiece(puzzlePieceIndex:int):void
		{
			if(!this._placeholder)
			{
				// only set piece if the piece isn't placed yet
				this.activeFrame = puzzlePieceIndex;
				for(var i:int = 0; i < this._puzzlePieces.length; i++)
				{
					this._puzzlePieces[i].visible = false;
				}
				if(this.isVisible)
				{
					this._puzzlePieces[this.activeFrame].visible = true;
					Utils_Text.FitTextToTextfield(this._puzzlePieces[this.activeFrame].text, 0, true);
				}
			}
		}
		
		public function SetPuzzlePiecePosition(position:Point):void
		{
			for(var i:int = 0; i < this._puzzlePieces.length; i++)
			{
				this._puzzlePieces[i].x = position.x;
				this._puzzlePieces[i].y = position.y;
			}
		}
		
		public function ResetPiecesToOriginalPositions():void
		{
			for(var i:int = 0; i < this._draggableObjects.length; i++)
			{
				this._draggableObjects[i].draggableObject.x = this._draggableObjects[i].originalPosition.x;
				this._draggableObjects[i].draggableObject.y = this._draggableObjects[i].originalPosition.y;
			}
		}
		
		public function AddDraggableEventListeners(dragStartCallback:Function, dragEndCallback:Function):void
		{
			this.dragEndCallback = dragEndCallback;
			this.dragStartCallback = dragStartCallback;
			
			for(var i:int = 0; i < this._draggableObjects.length; i++)
			{
				this._draggableObjects[i].AddEventListeners();
				
				if(this.dragStartCallback)
				{
					this._draggableObjects[i].addEventListener(Event_DraggableObject.DRAG_START, this.DragStartHandler);
				}
				
				if(this.dragEndCallback)
				{
					this._draggableObjects[i].addEventListener(Event_DraggableObject.DRAG_END, this.DragEndHandler);
				}
			}
		}
		
		public function RemoveDraggableEventListeners():void
		{
			for(var i:int = 0; i < this._draggableObjects.length; i++)
			{
				this._draggableObjects[i].RemoveEventListeners();
				
				if(this.dragStartCallback)
				{
					this._draggableObjects[i].removeEventListener(Event_DraggableObject.DRAG_START, this.DragStartHandler);
				}
				
				if(this.dragEndCallback)
				{
					this._draggableObjects[i].removeEventListener(Event_DraggableObject.DRAG_END, this.DragEndHandler);
				}
				
			}
			this.dragEndCallback = null;
		}
		
		private function DragStartHandler(event:Event_DraggableObject):void
		{
			this.dragStartCallback(event, this);
		}
		
		private function DragEndHandler(event:Event_DraggableObject):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_MOVEPUZZLEPIECES[SessionInformation_GirlsTablet.Age]);
			this.dragEndCallback(event, this);
		}
		
		public function get puzzlePiecesBeingDragged():Vector.<DraggableObject>
		{
			var puzzlePiecesInAction:Vector.<DraggableObject> = new Vector.<DraggableObject>();
			for (var i:int = 0; i < this._draggableObjects.length; i++)
			{
				if(this._draggableObjects[i].isBeingDragged) puzzlePiecesInAction.push(this._draggableObjects[i]);
			}
			
			if(puzzlePiecesInAction.length == 0)
			{
				return null; 
			}
			else
			{
				return puzzlePiecesInAction;
			}	
		}
		
		public function ForceDropActivePuzzlePieces():void
		{
			for(var i:int = 0; i < this._draggableObjects.length; i++)
			{
				if(this._draggableObjects[i].isBeingDragged)
				{
					this._draggableObjects[i].ForceEndDrag();
				}
			}	
		}
	}
}
package com.bpi.gsoc.girlstablet.states.gsoc_06projectplanning.puzzlepieces
{
	public class PlacedPuzzlePiece
	{
		private var _puzzlePieces:GroupOfPuzzlePieces;
		private var _linkedToNextPiece:Boolean;
		private var _linkedPieceName:String;
		
		public function PlacedPuzzlePiece(puzzlePieces:GroupOfPuzzlePieces)
		{
			this._puzzlePieces = puzzlePieces;
			this._linkedToNextPiece = false;
		}
		
		public function get puzzlePieces():GroupOfPuzzlePieces
		{
			return this._puzzlePieces;
		}
		
		public function set linkedToNextPiece(linked:Boolean):void
		{
			this._linkedToNextPiece = linked;
		}
		
		public function get linkedToNextPiece():Boolean
		{
			return this._linkedToNextPiece;
		}
		
		public function set linkedPieceName(name:String):void
		{
			this._linkedPieceName = name;
		}
		
		public function get linkedPieceName():String
		{
			return this._linkedPieceName;
		}
	}
}
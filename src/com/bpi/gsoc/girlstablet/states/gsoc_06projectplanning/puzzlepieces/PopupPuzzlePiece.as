package com.bpi.gsoc.girlstablet.states.gsoc_06projectplanning.puzzlepieces
{
	import flash.display.MovieClip;
	import flash.geom.Point;

	public class PopupPuzzlePiece
	{
		private var _graphic:MovieClip;
		private var _origTextPosition:Point;
		
		public function PopupPuzzlePiece(puzzlePieceImage:MovieClip)
		{
			this._graphic = puzzlePieceImage;
			this._origTextPosition = new Point(this._graphic.text.x, this._graphic.text.y);
		}
		
		public function get puzzlePieceGraphic():MovieClip
		{
			return this._graphic;
		}
		
		public function get origTextPosition():Point
		{
			return this._origTextPosition;	
		}
	}
}
package com.bpi.gsoc.girlstablet.states.gsoc_06projectplanning.calloutwindow
{
	import com.adobe.utils.StringUtil;
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.citrusas.input.inputcontroller.InputController;
	import com.bpi.citrusas.sound.SoundHandler;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.sfx.GSOC_SFX;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	public class AddYourOwnCalloutWindow extends CalloutWindow
	{
		private static const MAX_CHARACTERS_IN_CREATE_YOUR_OWN:int = 64;
		
		public function AddYourOwnCalloutWindow(calloutWindow:MovieClip, ... optionFields)
		{
			super(calloutWindow);
			this.InitializeOptionFields.apply(this, optionFields);
			this.skin.save.visible = false;
		}
		
		public override function AddEventListeners():void
		{
			super.AddEventListeners();
			
			this.skin.close.addEventListener(InputController.CLICK, this.CloseButtonHandler);
			this.skin.clear.addEventListener(InputController.CLICK, this.ClearButtonHandler);
			this.skin.save.addEventListener(InputController.CLICK, this.SaveButtonHandler);
			
			this.skin.addyourown.addEventListener(InputController.DOWN, this.AddYourOwnTextHandler);
			this.skin.addyourown.addEventListener(InputController.UP, this.eh_FocusTextField);
		}
		
		public override function RemoveEventListeners():void
		{
			super.RemoveEventListeners();
			
			this.skin.close.removeEventListener(InputController.CLICK, this.CloseButtonHandler);
			this.skin.clear.removeEventListener(InputController.CLICK, this.ClearButtonHandler);
			this.skin.save.removeEventListener(InputController.CLICK, this.SaveButtonHandler);
			
			this.skin.addyourown.removeEventListener(InputController.DOWN, this.AddYourOwnTextHandler);
			this.skin.addyourown.removeEventListener(InputController.UP, this.eh_FocusTextField);
		}
		
		public function InitializeAddYourOwnFont(addYourOwnFormat:TextFormat):void
		{
			(this.skin.addyourown as TextField).defaultTextFormat = addYourOwnFormat;
			(this.skin.addyourown as TextField).setTextFormat(addYourOwnFormat);
			
			for each(var opt:OptionField in this.optionFields)
			{
				opt.Refresh();
			}
		}
		
		private function eh_FocusTextField(e:Event):void
		{
			CitrusFramework.frameworkStage.focus = this.skin.addyourown;
			this.AddYourOwnTextHandler(e);
		}
		
		private function CloseButtonHandler(event:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_CLOSEBUTTONS[SessionInformation_GirlsTablet.Age]);
			this.dispatchEvent(new Event_CalloutWindow(Event_CalloutWindow.CLOSE_WINDOW, ""));
			this.skin.addyourown.removeEventListener(Event.CHANGE, textChanged);
		}
		
		private function ClearButtonHandler(event:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_CLICKS[SessionInformation_GirlsTablet.Age]);
			this.skin.addyourown.text = "";
			this.skin.save.visible = false;
			this.skin.addyourown.stage.focus = this.skin.addyourown;	
			if(!(this.skin.addyourown as TextField).hasEventListener(Event.CHANGE))
			{
				this.skin.addyourown.addEventListener(Event.CHANGE, textChanged, false, 0, true);
			}
		}
		
		private function SaveButtonHandler(event:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_SAVEBUTTONS[SessionInformation_GirlsTablet.Age]);
			this.dispatchEvent(new Event_CalloutWindow(Event_CalloutWindow.OPTION_SELECTED, this.skin.addyourown.text));
			this.skin.addyourown.removeEventListener(Event.CHANGE, textChanged);
		}
		
		private function AddYourOwnTextHandler(event:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_CLICKS[SessionInformation_GirlsTablet.Age]);
			this.skin.addyourown.removeEventListener(InputController.DOWN, this.AddYourOwnTextHandler);
			this.skin.addyourown.addEventListener(Event.CHANGE, textChanged, false, 0, true);
			this.skin.save.visible = false;
			this.skin.addyourown.text = "";
		}
		
		private function textChanged(e:Event):void
		{
			if(this.skin.addyourown.text.length >= MAX_CHARACTERS_IN_CREATE_YOUR_OWN)
			{
				var text:String = this.skin.addyourown.text;
				text = text.substr(0, MAX_CHARACTERS_IN_CREATE_YOUR_OWN);
				this.skin.addyourown.text = text;
			}
			if (StringUtil.trim(this.skin.addyourown.text) && this.skin.addyourown.text != "Add Your Own")
			{
				this.skin.save.visible = true;
			}
			else
			{
				this.skin.save.visible = false;
			}
		}
	}
}
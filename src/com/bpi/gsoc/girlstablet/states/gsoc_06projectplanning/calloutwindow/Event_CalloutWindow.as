package com.bpi.gsoc.girlstablet.states.gsoc_06projectplanning.calloutwindow
{
	import flash.events.Event;
	
	public class Event_CalloutWindow extends Event
	{
		public static const OPTION_SELECTED:String = "com.bpi.gsoc.girlstablet.states.gsoc_06projectplanning.calloutwindow.OPTION_SELECTED";
		public static const CLOSE_WINDOW:String = "com.bpi.gsoc.girlstablet.states.gsoc_06projectplanning.calloutwindow.CLOSE_WINDOW";
		
		private var _optionText:String;
		public function get optionText():String
		{
			return this._optionText;
		}
		
		public function Event_CalloutWindow(type:String, optionText:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			
			this._optionText = optionText;
		}
	}
}
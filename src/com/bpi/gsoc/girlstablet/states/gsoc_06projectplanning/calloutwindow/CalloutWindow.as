package com.bpi.gsoc.girlstablet.states.gsoc_06projectplanning.calloutwindow
{
	import com.bpi.citrusas.input.inputcontroller.InputController;
	import com.bpi.citrusas.sound.SoundHandler;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.sfx.GSOC_SFX;
	import com.bpi.gsoc.girlstablet.global_data.FrameControlUtils;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;

	public class CalloutWindow extends EventDispatcher
	{
		protected var skin:MovieClip;
		protected var optionFields:Vector.<OptionField>;
		
		public function CalloutWindow(calloutWindow:MovieClip, ... optionFields)
		{
			this.skin = calloutWindow;
			this.optionFields = new Vector.<OptionField>();
			this.InitializeOptionFields.apply(this, optionFields);
			
			this.visible = false;
			
			FrameControlUtils.StopAtFrameN(calloutWindow);
		}
		
		public function InitializeFonts(explainationFormat:TextFormat, questionFormat:TextFormat, optionFormat:TextFormat):void
		{
			optionFormat.align = TextFormatAlign.CENTER; // all these text fields should be center aligned
			
			(this.skin.explainationText as TextField).defaultTextFormat = explainationFormat;
			(this.skin.explainationText as TextField).setTextFormat(explainationFormat);
			
			(this.skin.questionText as TextField).defaultTextFormat = questionFormat;
			(this.skin.questionText as TextField).setTextFormat(questionFormat);
			
			for(var i:int = 0; i < this.optionFields.length; i++)
			{
				this.optionFields[i].textField.multiline = false;
				this.optionFields[i].textField.defaultTextFormat = optionFormat;
				this.optionFields[i].textField.setTextFormat(optionFormat);
			}
		}
		
		public function InitializeOptionTexts(optionTexts:Vector.<String>):void
		{
			// optionsTexts.length and optionFields.length must be equal
			if(this.optionFields.length == optionTexts.length)
			{
				for(var i:int = 0; i < this.optionFields.length; i++)
				{
					this.optionFields[i].SetText(optionTexts[i]);
				}
			}
		}
		
		public function PlaceWindowOnTop():void
		{
			if(this.skin.parent)
			{
				this.skin.parent.addChild(this.skin);
			}
		}
		
		public function SetPosition(tipPosition:Point):void
		{
			this.skin.x = tipPosition.x;
			this.skin.y = tipPosition.y;
		}
		
		public function AddEventListeners():void
		{
			for(var i:int = 0; i < this.optionFields.length; i++)
			{
				this.optionFields[i].textField.addEventListener(InputController.CLICK, this.OptionClickHandler);
			}
		}
		
		public function RemoveEventListeners():void
		{
			for(var i:int = 0; i < this.optionFields.length; i++)
			{
				this.optionFields[i].textField.removeEventListener(InputController.CLICK, this.OptionClickHandler);
			}
		}
		
		public function set visible(isVisible:Boolean):void
		{
			this.skin.visible = isVisible;
		}
		
		public function get calloutWindow():MovieClip
		{
			return this.skin;
		}
		
		protected final function InitializeOptionFields(... optionFields):void
		{
			for(var i:int = 0; i < optionFields.length; i++)
			{
				this.optionFields.push(optionFields[i]);
			}
		}
		
		protected function OptionClickHandler(event:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_DECISIONCHOICES[SessionInformation_GirlsTablet.Age]);
			this.dispatchEvent(new Event_CalloutWindow(Event_CalloutWindow.OPTION_SELECTED, event.target.text));
		}
	}
}
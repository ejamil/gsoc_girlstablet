package com.bpi.gsoc.girlstablet.states.gsoc_06projectplanning
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.citrusas.input.inputcontroller.InputController;
	import com.bpi.citrusas.ui.draggableobject.DraggableObject;
	import com.bpi.citrusas.ui.draggableobject.events.Event_DraggableObject;
	import com.bpi.citrusas.utils.Utils_Text;
	import com.bpi.gsoc.components.projectplanning.ProjectPlanning_Screen_SA_Skin;
	import com.bpi.gsoc.framework.constants.AgeColors;
	import com.bpi.gsoc.framework.constants.AgeFontLinkages;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.embeddedcontent.EmbeddedFonts;
	import com.bpi.gsoc.framework.gsocui.GSOC_Header;
	import com.bpi.gsoc.framework.gsocui.STEM_Popups;
	import com.bpi.gsoc.girlstablet.states.gsoc_06projectplanning.calloutwindow.AddYourOwnCalloutWindow;
	import com.bpi.gsoc.girlstablet.states.gsoc_06projectplanning.calloutwindow.CalloutWindow;
	import com.bpi.gsoc.girlstablet.states.gsoc_06projectplanning.calloutwindow.Event_CalloutWindow;
	import com.bpi.gsoc.girlstablet.states.gsoc_06projectplanning.calloutwindow.OptionField;
	import com.bpi.gsoc.girlstablet.states.gsoc_06projectplanning.noteswindow.Event_NotesWindow;
	import com.bpi.gsoc.girlstablet.states.gsoc_06projectplanning.noteswindow.NotesWindow;
	import com.bpi.gsoc.girlstablet.states.gsoc_06projectplanning.puzzlepieces.GroupOfPuzzlePieces;
	import com.bpi.gsoc.girlstablet.states.gsoc_06projectplanning.puzzlepieces.PlacedPuzzlePiece;
	import com.bpi.gsoc.girlstablet.states.gsoc_06projectplanning.puzzlepieces.PopupPuzzlePiece;
	import com.greensock.TweenLite;
	
	import flash.display.MovieClip;
	import flash.filters.DropShadowFilter;
	import flash.text.TextField;
	import flash.text.TextFormat;

	public class ProjectPlanning_App_SA extends ProjectPlanning_App
	{
		private var sa_Skin:ProjectPlanning_Screen_SA_Skin;
		
		private var findWorkspace:AddYourOwnCalloutWindow;
		private var makeASchedule:AddYourOwnCalloutWindow;
		private var lookForVolunteers:AddYourOwnCalloutWindow;
		private var budgetAfterSupplies:CalloutWindow;
		
		private var puzzleTextFormat:TextFormat;
		
		private var puzzlePieces:Vector.<GroupOfPuzzlePieces>;
		private var currentEmptyPuzzleIndex:int;
		private var emptyPuzzlePlaces:Vector.<MovieClip>;
		private var activePuzzleGroupIndex:int;
		
		private var placedPieces:Vector.<PlacedPuzzlePiece>;
		
		private var notesDraggableObject:DraggableObject;
		private var notesWindow:NotesWindow;
		private var notesVisibleWidth:Number;
		
		private var popupPieces:Vector.<PopupPuzzlePiece>;
		
		private const PUZZLE_TEXTS:Vector.<String> = new <String>["Find Workspace", "Set Deadlines", "Set A Budget", "Build Project Team", "Obtain Supplies"];
		private const LOOK_FOR_VOLUNTEERS_OPTIONS:Vector.<String> = new <String>["Setup A Lunchtime Booth At School To Gain Interest", "Have A Meeting At The Civic Center And Invite People From The Community"];
		private const FIND_WORKSPACE_OPTIONS:Vector.<String> = new <String>["Make A Meeting Room In Your House", "Rent Space At An Office Building"];
		private const MAKE_A_SCHEDULE_OPTIONS:Vector.<String> = new <String>["Change Your Schedule To Add More Time To Your Overall Project", "Tweak Parts Of Your Project To Cut Down On Time"];
		private const BUDGET_AFTER_SUPPLIES_OPTIONS:Vector.<String> = new <String>["Swap Steps", "Keep As Is"];
		
		public function ProjectPlanning_App_SA()
		{
			super();
			
			this.m_Skin = new ProjectPlanning_Screen_SA_Skin();
			
			this.sa_Skin = this.m_Skin as ProjectPlanning_Screen_SA_Skin;
			
			m_GSOCHeader = new GSOC_Header(m_Skin.header.paginationArea);
			GSOCheader.addArrows(m_Skin.header.paginationArea.x, 0);
		}
		
		public override function Initialize():void
		{
			super.Initialize();
			this.findWorkspace = new AddYourOwnCalloutWindow(this.sa_Skin.callout1, new OptionField(this.sa_Skin.callout1.optionbox1,this.sa_Skin.callout1.optiontext1),
				new OptionField(this.sa_Skin.callout1.optionbox2, this.sa_Skin.callout1.optiontext2));
			this.findWorkspace.InitializeFonts(new TextFormat(EmbeddedFonts.FontName_HelveticaBoldTTF, 48, 0xffffff, true), new TextFormat(EmbeddedFonts.FontName_HelveticaBoldTTF, 60, 0xffffff, true), new TextFormat(EmbeddedFonts.FontName_HelveticaBoldTTF, 44, 0xffffff, true));
			this.findWorkspace.InitializeOptionTexts(this.FIND_WORKSPACE_OPTIONS);
			this.findWorkspace.InitializeAddYourOwnFont(new TextFormat(AgeFontLinkages.SA_BODY_FONT, 48, 0x0CB1A5, true));
			
			this.makeASchedule = new AddYourOwnCalloutWindow(this.sa_Skin.callout2, new OptionField(this.sa_Skin.callout2.optionbox1, this.sa_Skin.callout2.optiontext1),
				new OptionField(this.sa_Skin.callout2.optionbox2, this.sa_Skin.callout2.optiontext2));
			this.makeASchedule.InitializeFonts(new TextFormat(EmbeddedFonts.FontName_HelveticaBoldTTF, 48, 0xffffff, true), new TextFormat(EmbeddedFonts.FontName_HelveticaBoldTTF, 60, 0xffffff, true), new TextFormat(EmbeddedFonts.FontName_HelveticaBoldTTF, 44, 0xffffff, true));
			this.makeASchedule.InitializeOptionTexts(this.MAKE_A_SCHEDULE_OPTIONS);
			this.makeASchedule.InitializeAddYourOwnFont(new TextFormat(AgeFontLinkages.SA_BODY_FONT, 48, 0x0CB1A5, true));
			
			this.lookForVolunteers = new AddYourOwnCalloutWindow(this.sa_Skin.callout3, new OptionField(this.sa_Skin.callout3.optionbox1, this.sa_Skin.callout3.optiontext1),
				new OptionField(this.sa_Skin.callout3.optionbox2, this.sa_Skin.callout3.optiontext2));
			this.lookForVolunteers.InitializeFonts(new TextFormat(EmbeddedFonts.FontName_HelveticaBoldTTF, 48, 0xffffff, true), new TextFormat(EmbeddedFonts.FontName_HelveticaBoldTTF, 60, 0xffffff, true), new TextFormat(EmbeddedFonts.FontName_HelveticaBoldTTF, 44, 0xffffff, true));
			this.lookForVolunteers.InitializeOptionTexts(this.LOOK_FOR_VOLUNTEERS_OPTIONS);
			this.lookForVolunteers.InitializeAddYourOwnFont(new TextFormat(AgeFontLinkages.SA_BODY_FONT, 48, 0x0CB1A5, true));
			
			this.budgetAfterSupplies = new CalloutWindow(this.sa_Skin.callout4, new OptionField(this.sa_Skin.callout4.optionbox1, this.sa_Skin.callout4.optiontext1),
				new OptionField(this.sa_Skin.callout4.optionbox2, this.sa_Skin.callout4.optiontext2));
			this.budgetAfterSupplies.InitializeFonts(new TextFormat(EmbeddedFonts.FontName_HelveticaBoldTTF, 48, 0xffffff, true), new TextFormat(EmbeddedFonts.FontName_HelveticaBoldTTF, 60, 0xffffff, true), new TextFormat(EmbeddedFonts.FontName_HelveticaBoldTTF, 44, 0xffffff, true));
			this.budgetAfterSupplies.InitializeOptionTexts(this.BUDGET_AFTER_SUPPLIES_OPTIONS);
			
			this.puzzleTextFormat = new TextFormat(EmbeddedFonts.FontName_HelveticaBoldTTF, 42, 0xffffff, true);
			
			this.puzzlePieces = new Vector.<GroupOfPuzzlePieces>();
			this.puzzlePieces.push(new GroupOfPuzzlePieces(this.sa_Skin.addend1_1, this.sa_Skin.middleInAddConnector0, this.sa_Skin.addend2_1, this.sa_Skin.addend3_1, this.sa_Skin.addend4_1, this.sa_Skin.addend5_1, this.sa_Skin.addconnector2_1, this.sa_Skin.addend6_1));
			this.puzzlePieces.push(new GroupOfPuzzlePieces(this.sa_Skin.addend1_2, this.sa_Skin.middleInAddConnector1, this.sa_Skin.addend2_2, this.sa_Skin.addend3_2, this.sa_Skin.addend4_2, this.sa_Skin.addend5_2, this.sa_Skin.addconnector2_2, this.sa_Skin.addend6_2));
			this.puzzlePieces.push(new GroupOfPuzzlePieces(this.sa_Skin.addend1_3, this.sa_Skin.middleInAddConnector2, this.sa_Skin.addend2_3, this.sa_Skin.addend3_3, this.sa_Skin.addend4_3, this.sa_Skin.addend5_3, this.sa_Skin.addconnector2_3, this.sa_Skin.addend6_3));
			this.puzzlePieces.push(new GroupOfPuzzlePieces(this.sa_Skin.addend1_4, this.sa_Skin.middleInAddConnector3, this.sa_Skin.addend2_4, this.sa_Skin.addend3_4, this.sa_Skin.addend4_4, this.sa_Skin.addend5_4, this.sa_Skin.addconnector2_4, this.sa_Skin.addend6_4));
			this.puzzlePieces.push(new GroupOfPuzzlePieces(this.sa_Skin.addend1_5, this.sa_Skin.middleInAddConnector4, this.sa_Skin.addend2_5, this.sa_Skin.addend3_5, this.sa_Skin.addend4_5, this.sa_Skin.addend5_5, this.sa_Skin.addconnector2_5, this.sa_Skin.addend6_5));
			this.puzzlePieces[0].Initialize(this.PUZZLE_TEXTS[0], this.FindWorkspaceCallback);
			this.puzzlePieces[1].Initialize(this.PUZZLE_TEXTS[1], this.MakeAScheduleCallback);
			this.puzzlePieces[2].Initialize(this.PUZZLE_TEXTS[2], this.BudgetAfterSuppliesCallback);
			this.puzzlePieces[3].Initialize(this.PUZZLE_TEXTS[3], this.LookForVolunteersCallback);
			this.puzzlePieces[4].Initialize(this.PUZZLE_TEXTS[4]);
			
			this.emptyPuzzlePlaces = new Vector.<MovieClip>();
			this.emptyPuzzlePlaces.push(this.sa_Skin.blankend1);
			this.emptyPuzzlePlaces.push(this.sa_Skin.blankconnector1);
			this.emptyPuzzlePlaces.push(this.sa_Skin.blankend2);
			this.emptyPuzzlePlaces.push(this.sa_Skin.blankend3);
			this.emptyPuzzlePlaces.push(this.sa_Skin.blankend4);
			this.emptyPuzzlePlaces.push(this.sa_Skin.blankend5);
			this.emptyPuzzlePlaces.push(this.sa_Skin.blankconnector2);
			this.emptyPuzzlePlaces.push(this.sa_Skin.blankend6);
			
			this.popupPieces = new Vector.<PopupPuzzlePiece>();
			this.popupPieces.push(new PopupPuzzlePiece(this.sa_Skin.popupPiece0));
			this.popupPieces.push(new PopupPuzzlePiece(this.sa_Skin.popupPiece1));
			this.popupPieces.push(new PopupPuzzlePiece(this.sa_Skin.popupPiece2));
			this.popupPieces.push(new PopupPuzzlePiece(this.sa_Skin.popupPiece3));
			this.popupPieces.push(new PopupPuzzlePiece(this.sa_Skin.popupPiece4));
			this.popupPieces.push(new PopupPuzzlePiece(this.sa_Skin.popupPiece5));
			this.popupPieces.push(new PopupPuzzlePiece(this.sa_Skin.popupPiece6));
			
			for(var i:int = 0; i < this.popupPieces.length; i++)
			{
				this.popupPieces[i].puzzlePieceGraphic.visible = false;
			}
			
			this.currentEmptyPuzzleIndex = 0;
			this.activePuzzleGroupIndex = -1;
			this.placedPieces = new Vector.<PlacedPuzzlePiece>();
			
			this.notesDraggableObject = new DraggableObject(this.sa_Skin.notes, this.sa_Skin.notes.notesHitBox);
			this.notesWindow = new NotesWindow(this.sa_Skin.notes, new TextFormat(AgeFontLinkages.SA_BODY_FONT, 48, AgeColors.SA_YELLOW, true), SessionInformation_GirlsTablet.Age);
			this.notesVisibleWidth = this.sa_Skin.notes.edgeMarker.x;
			this.notesWindow.AddEventListeners();
			this.notesWindow.addEventListener(Event_NotesWindow.CLOSE, this.CloseNotesWindowHandler);
			this.notesWindow.addEventListener(Event_NotesWindow.SAVE, this.SaveNotesWindowHandler);
			this.SetUpChildIndexes();
		}
		
		
//		private function eh_ReportOnActivePiece(e:Event):void
//		{
//			trace("The active piece is currently: " + this.activePuzzleGroupIndex);
//		}
		public override function Init():void
		{
			super.Init();
			
			this.AddGlobalWindows();
			
			this.ToggleSubmitButton(false);
			
			this.InitPuzzlePieces();
			
			this.ToggleNoteButton(true);
			
			this.notesWindow.Init();
		}
		
		public override function DeInit():void
		{
			this.RemoveGlobalWindows();
		}
		
		private function InitPuzzlePieces():void
		{
			this.currentEmptyPuzzleIndex = 0;
			this.indexOfProblemPuzzlePiece = 1;
			this.activePuzzleGroupIndex = -1;
			
			for(var i:int = 0; i < this.puzzlePieces.length; i++)
			{
				this.puzzlePieces[i].SetVisible(true);
				this.puzzlePieces[i].AddDraggableEventListeners(this.PuzzleDragStartHandler, this.PuzzleDragEndHandler);
				this.puzzlePieces[i].SetPuzzlePiece(this.currentEmptyPuzzleIndex);
			}
			
			this.SetTextFormatToPuzzlePieceTextfield(this.sa_Skin.addend1_1.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.sa_Skin.addend1_2.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.sa_Skin.addend1_3.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.sa_Skin.addend1_4.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.sa_Skin.addend1_5.text, this.puzzleTextFormat);
			
			this.SetTextFormatToPuzzlePieceTextfield(this.sa_Skin.addend2_1.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.sa_Skin.addend2_2.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.sa_Skin.addend2_3.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.sa_Skin.addend2_4.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.sa_Skin.addend2_5.text, this.puzzleTextFormat);
			
			this.SetTextFormatToPuzzlePieceTextfield(this.sa_Skin.middleInAddConnector0.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.sa_Skin.middleInAddConnector1.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.sa_Skin.middleInAddConnector2.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.sa_Skin.middleInAddConnector3.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.sa_Skin.middleInAddConnector4.text, this.puzzleTextFormat);
			
			this.SetTextFormatToPuzzlePieceTextfield(this.sa_Skin.addconnector2_1.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.sa_Skin.addconnector2_2.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.sa_Skin.addconnector2_3.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.sa_Skin.addconnector2_4.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.sa_Skin.addconnector2_5.text, this.puzzleTextFormat);
			
			this.SetTextFormatToPuzzlePieceTextfield(this.sa_Skin.addend3_1.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.sa_Skin.addend3_2.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.sa_Skin.addend3_3.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.sa_Skin.addend3_4.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.sa_Skin.addend3_5.text, this.puzzleTextFormat);
			
			this.SetTextFormatToPuzzlePieceTextfield(this.sa_Skin.addend4_1.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.sa_Skin.addend4_2.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.sa_Skin.addend4_3.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.sa_Skin.addend4_4.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.sa_Skin.addend4_5.text, this.puzzleTextFormat);
			
			this.SetTextFormatToPuzzlePieceTextfield(this.sa_Skin.addend5_1.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.sa_Skin.addend5_2.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.sa_Skin.addend5_3.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.sa_Skin.addend5_4.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.sa_Skin.addend5_5.text, this.puzzleTextFormat);
			
			this.SetTextFormatToPuzzlePieceTextfield(this.sa_Skin.addend6_1.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.sa_Skin.addend6_2.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.sa_Skin.addend6_3.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.sa_Skin.addend6_4.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.sa_Skin.addend6_5.text, this.puzzleTextFormat);
			
			this.SetUpChildIndexes();
		}
		
		private function SetTextFormatToPuzzlePieceTextfield(textField:TextField, textFormat:TextFormat, addDropShadow:Boolean = true):void
		{
			textField.defaultTextFormat = textFormat;
			textField.setTextFormat(textFormat);
			
			if(addDropShadow)
			{
				var dropShadow:DropShadowFilter = new DropShadowFilter(2, 45, 0, 0.6, 6, 6);
				textField.filters = [ dropShadow ];
			}
			else
			{
				textField.filters = [];
			}
		}
		
		private function PuzzleDragStartHandler(e:Event_DraggableObject, currentPuzzleGroup:GroupOfPuzzlePieces):void
		{
			this.ToggleSubmitButton(false);
			
			// if a piece is currently picked up in addition to this one
			if(this.activePuzzleGroupIndex != -1) 
			{
				// dont' drop the current piece if it's the "correct" active one
				if(! (this.puzzlePieces.indexOf(currentPuzzleGroup) == this.activePuzzleGroupIndex))
				{
					// but do drop it if it's the second one to be picked up
					currentPuzzleGroup.ForceDropActivePuzzlePieces();
				}
			}
			else
			{
				// otherwise we can just set the active index
				this.activePuzzleGroupIndex = this.puzzlePieces.indexOf(currentPuzzleGroup);
			}
			
		}
	
		private function PuzzleDragEndHandler(event:Event_DraggableObject, puzzlePieces:GroupOfPuzzlePieces):void
		{
			this.activePuzzleGroupIndex = -1;
			
			if(!puzzlePieces.placeholder)
			{
				// check to see if the dragged puzzle piece is on top of the empty puzzle piece
				if((event.target as DraggableObject).draggableObject.hitTestObject(this.emptyPuzzlePlaces[this.currentEmptyPuzzleIndex]))
				{
					puzzlePieces.SetFrame(2);
					puzzlePieces.placeholder = this.emptyPuzzlePlaces[this.currentEmptyPuzzleIndex];
					
					// TODO need to tween from the current position to the below position
					(event.target as DraggableObject).draggableObject.x = this.emptyPuzzlePlaces[this.currentEmptyPuzzleIndex].x;
					(event.target as DraggableObject).draggableObject.y = this.emptyPuzzlePlaces[this.currentEmptyPuzzleIndex].y;
					
					// if the placed puzzle piece is a middle connector, put it on top so that the text doesn't get cut off or overlapped
					if(puzzlePieces.puzzlePieces[puzzlePieces.activePiece].name.indexOf("middleInAddConnector") != -1)
					{
						this.sa_Skin.addChildAt(puzzlePieces.puzzlePieces[puzzlePieces.activePiece], this.sa_Skin.numChildren); // place it on top
					}
					
					this.placedPieces.push(new PlacedPuzzlePiece(puzzlePieces));
					
					if(puzzlePieces.optionCallback)
					{
						// remove drag listeners until option has been selected
						for(var i:int = 0; i < this.puzzlePieces.length; i++)
						{
							this.puzzlePieces[i].RemoveDraggableEventListeners();
						}
						puzzlePieces.optionCallback();
					}
					else
					{
						STEM_Popups.DisplayPopup(STEM_Popups.PROJECT_PLANNING);
						this.currentEmptyPuzzleIndex++;
						if(this.currentEmptyPuzzleIndex == puzzlePieces.puzzlePieces.length)
						{
							// show submit button
							this.ToggleSubmitButton(true);
						}
						else
						{
							for(i = 0; i < this.puzzlePieces.length; i++)
							{
								this.puzzlePieces[i].SetPuzzlePiece(this.currentEmptyPuzzleIndex);
							}
						}
						
						if(this.placedPieces.length > 1)
						{
							this.placedPieces[this.placedPieces.length-2].puzzlePieces.RemoveDraggableEventListeners();
						}
					}
				}
				else
				{
					// snap it back to its original location (add tween)
					(event.target as DraggableObject).draggableObject.x = (event.target as DraggableObject).originalPosition.x;
					(event.target as DraggableObject).draggableObject.y = (event.target as DraggableObject).originalPosition.y;
				}
			}
			else
			{
				if((event.target as DraggableObject).draggableObject.hitTestObject(puzzlePieces.placeholder))
				{
					if(this.currentEmptyPuzzleIndex == puzzlePieces.puzzlePieces.length)
					{
						this.ToggleSubmitButton(true);
					}
					
					// snap it back to its placed location
					(event.target as DraggableObject).draggableObject.x = puzzlePieces.placeholder.x;
					(event.target as DraggableObject).draggableObject.y = puzzlePieces.placeholder.y;
				}
				else
				{
					// TODO add tween the puzzle piece was in a placed position and was dragged out of it, send it back to it's original location and reset it so it can be placed again
					(event.target as DraggableObject).draggableObject.x = (event.target as DraggableObject).originalPosition.x;
					(event.target as DraggableObject).draggableObject.y = (event.target as DraggableObject).originalPosition.y;
					
					// when the tween is done, move the hidden pieces back to their original places just to make sure everything is in the right place
					puzzlePieces.ResetPiecesToOriginalPositions();
					
					this.currentEmptyPuzzleIndex--;
					
					var pop:PlacedPuzzlePiece = this.placedPieces.pop();
					if(pop.linkedToNextPiece)
					{
						// need to remove the next puzzle piece from the child
						pop.puzzlePieces.puzzlePieces[pop.puzzlePieces.activePiece].removeChild(pop.puzzlePieces.puzzlePieces[pop.puzzlePieces.activePiece].getChildByName(pop.linkedPieceName));
						this.currentEmptyPuzzleIndex--;
					}
					
					puzzlePieces.placeholder = null;
					puzzlePieces.SetFrame(1);
					//this.SetTextFormatToPuzzlePieceTextfield(puzzlePieces.puzzlePieces[puzzlePieces.activePiece].text, this.puzzleTextFormat);
					
					for(i = 0; i < this.puzzlePieces.length; i++)
					{
						this.puzzlePieces[i].SetPuzzlePiece(this.currentEmptyPuzzleIndex);
					}
					
					if(this.placedPieces.length > 0)
					{
						this.placedPieces[this.placedPieces.length-1].puzzlePieces.AddDraggableEventListeners(this.PuzzleDragStartHandler, this.PuzzleDragEndHandler);
					}
				}
			}
			
			//bubble problem puzzle piece to the top so text doesn't get overlapped
			var effectiveIndex:int = -1;
			for (i = 0; i < this.placedPieces.length; i ++)
			{
				effectiveIndex++;
				if(this.placedPieces[i].linkedToNextPiece)
				{
					effectiveIndex++;
				}
				
				if(effectiveIndex == this.indexOfProblemPuzzlePiece) 
				{
					this.sa_Skin.addChildAt(this.placedPieces[i].puzzlePieces.puzzlePieces[this.placedPieces[i].puzzlePieces.activePiece], this.sa_Skin.numChildren);
				}
			}
		}
		
		private function FindWorkspaceCallback():void
		{
			this.findWorkspace.visible = true;
			this.findWorkspace.PlaceWindowOnTop();
			
			this.findWorkspace.AddEventListeners();
			this.findWorkspace.addEventListener(Event_CalloutWindow.OPTION_SELECTED, this.OptionSelectedHandler);
			this.findWorkspace.addEventListener(Event_CalloutWindow.CLOSE_WINDOW, this.OptionWindowClosedHandler);
		}
		
		private function LookForVolunteersCallback():void
		{
			this.lookForVolunteers.visible = true;
			this.lookForVolunteers.PlaceWindowOnTop();
			
			this.lookForVolunteers.AddEventListeners();
			this.lookForVolunteers.addEventListener(Event_CalloutWindow.OPTION_SELECTED, this.OptionSelectedHandler);
			this.lookForVolunteers.addEventListener(Event_CalloutWindow.CLOSE_WINDOW, this.OptionWindowClosedHandler);
		}
		
		private function MakeAScheduleCallback():void
		{
			this.makeASchedule.visible = true;
			this.makeASchedule.PlaceWindowOnTop();
			
			this.makeASchedule.AddEventListeners();
			this.makeASchedule.addEventListener(Event_CalloutWindow.OPTION_SELECTED, this.OptionSelectedHandler);
			this.makeASchedule.addEventListener(Event_CalloutWindow.CLOSE_WINDOW, this.OptionWindowClosedHandler);
		}
		
		private function BudgetAfterSuppliesCallback():void
		{
			// if budget is placed after supplies... show the window, otherwise allow them to continue
			var indexOfBudget:int = -1;
			var indexOfSupplies:int = -1;
			for(var i:int = 0; i < this.placedPieces.length; i++)
			{
				// find the "set a budget" (piece at index 2 in puzzlePiece vector) puzzle piece and the "obtain supplies" puzzle piece (piece at index 4 in puzzlePiece vector)
				if(this.placedPieces[i].puzzlePieces == this.puzzlePieces[2])
				{
					indexOfBudget = i;
				}
				else if(this.placedPieces[i].puzzlePieces == this.puzzlePieces[4])
				{
					indexOfSupplies = i;
				}
			}
			if(indexOfBudget != -1 && indexOfSupplies != -1 && indexOfBudget > indexOfSupplies)
			{
				this.budgetAfterSupplies.visible = true;
				this.budgetAfterSupplies.PlaceWindowOnTop();
				
				this.budgetAfterSupplies.AddEventListeners();
				this.budgetAfterSupplies.addEventListener(Event_CalloutWindow.OPTION_SELECTED, this.BudgetAfterSuppliesSelectedHandler);
			}
			else
			{
				this.currentEmptyPuzzleIndex++;
				
				if(this.currentEmptyPuzzleIndex == this.puzzlePieces[0].puzzlePieces.length)
				{
					// show submit button
					this.ToggleSubmitButton(true);
					
					// make sure the puzzle piece has the drag listeners on it
					this.placedPieces[this.placedPieces.length-1].puzzlePieces.AddDraggableEventListeners(this.PuzzleDragStartHandler, this.PuzzleDragEndHandler);
				}
				else
				{
					this.ToggleSubmitButton(false);; // hide the submit button
					
					for(i = 0; i < this.puzzlePieces.length; i++)
					{
						this.puzzlePieces[i].SetPuzzlePiece(this.currentEmptyPuzzleIndex);
						if(!this.puzzlePieces[i].placeholder)
						{
							this.puzzlePieces[i].AddDraggableEventListeners(this.PuzzleDragStartHandler, this.PuzzleDragEndHandler);
						}
					}
					if(this.placedPieces.length > 0)
					{
						this.placedPieces[this.placedPieces.length-1].puzzlePieces.AddDraggableEventListeners(this.PuzzleDragStartHandler, this.PuzzleDragEndHandler);
					}
				}
			}
		}
		
		private function BudgetAfterSuppliesSelectedHandler(event:Event_CalloutWindow):void
		{
			event.target.removeEventListener(Event_CalloutWindow.OPTION_SELECTED, this.OptionSelectedHandler);
			event.target.visible = false;
			event.target.RemoveEventListeners();
			
			if(event.optionText == this.BUDGET_AFTER_SUPPLIES_OPTIONS[0])
			{
				// swap puzzle pieces
				// find the budget and supplies placements on the board
				var indexOfBudget:int = -1;
				var indexOfSupplies:int = -1;
				var numOfLinkedPiecesBeforeBudget:int = 0; // linked pieces (the pieces with a popup window) are only stored as 1 piece, so to get the right indexes we need to add the number of linked pieces that appear before the budget piece
				var numOfLinkedPiecesBeforeSupplies:int = 0; // linked pieces (the pieces with a popup window) are only stored as 1 piece, so to get the right indexes we need to add the number of linked pieces that appear before the budget piece
				for(var i:int = 0; i < this.placedPieces.length; i++)
				{
					if(this.placedPieces[i].linkedToNextPiece)
					{
						// only add linked pieces if we haven't found the budget/supplies pieces yet
						if(indexOfBudget == -1)
						{
							numOfLinkedPiecesBeforeBudget++;
						}
						if(indexOfSupplies == -1)
						{
							numOfLinkedPiecesBeforeSupplies++;
						}
					}
					// find the "set a budget" (piece at index 2 in puzzlePiece vector) puzzle piece and the "obtain supplies" puzzle piece (piece at index 4 in puzzlePiece vector)
					if(this.placedPieces[i].puzzlePieces == this.puzzlePieces[2])
					{
						indexOfBudget = i;
					}
					else if(this.placedPieces[i].puzzlePieces == this.puzzlePieces[4])
					{
						indexOfSupplies = i;
					}
				}
				
				var budgetPlacedPiece:PlacedPuzzlePiece = this.placedPieces[indexOfBudget];
				var suppliesPlacedPiece:PlacedPuzzlePiece = this.placedPieces[indexOfSupplies];
				
				// swap the pieces TODO add tweens and whatnot
				this.placedPieces[indexOfBudget] = suppliesPlacedPiece;
				this.placedPieces[indexOfBudget].puzzlePieces.placeholder = null; // we have to null it out so that we can switch the puzzle piece
				this.placedPieces[indexOfBudget].puzzlePieces.SetPuzzlePiece(indexOfBudget + numOfLinkedPiecesBeforeBudget);
				this.placedPieces[indexOfBudget].puzzlePieces.placeholder = this.emptyPuzzlePlaces[indexOfBudget + numOfLinkedPiecesBeforeBudget];
				this.placedPieces[indexOfBudget].puzzlePieces.puzzlePieces[indexOfBudget + numOfLinkedPiecesBeforeBudget].x = this.emptyPuzzlePlaces[indexOfBudget + numOfLinkedPiecesBeforeBudget].x;
				this.placedPieces[indexOfBudget].puzzlePieces.puzzlePieces[indexOfBudget + numOfLinkedPiecesBeforeBudget].y = this.emptyPuzzlePlaces[indexOfBudget + numOfLinkedPiecesBeforeBudget].y;
				
				this.placedPieces[indexOfSupplies] = budgetPlacedPiece;
				this.placedPieces[indexOfSupplies].puzzlePieces.placeholder = null; // we have to null it out so that we can switch the puzzle piece
				this.placedPieces[indexOfSupplies].puzzlePieces.SetPuzzlePiece(indexOfSupplies + numOfLinkedPiecesBeforeSupplies);
				this.placedPieces[indexOfSupplies].puzzlePieces.placeholder = this.emptyPuzzlePlaces[indexOfSupplies + numOfLinkedPiecesBeforeSupplies];
				this.placedPieces[indexOfSupplies].puzzlePieces.puzzlePieces[indexOfSupplies + numOfLinkedPiecesBeforeSupplies].x = this.emptyPuzzlePlaces[indexOfSupplies + numOfLinkedPiecesBeforeSupplies].x;
				this.placedPieces[indexOfSupplies].puzzlePieces.puzzlePieces[indexOfSupplies + numOfLinkedPiecesBeforeSupplies].y = this.emptyPuzzlePlaces[indexOfSupplies + numOfLinkedPiecesBeforeSupplies].y;
			}
			else
			{
				// don't do anything
				trace("Don't swap puzzle pieces.");
			}
			
			this.currentEmptyPuzzleIndex++;
			
			if(this.currentEmptyPuzzleIndex == this.puzzlePieces[0].puzzlePieces.length)
			{
				// show submit button
				this.ToggleSubmitButton(true);
				
				// make sure the puzzle piece has the drag listeners on it
				this.placedPieces[this.placedPieces.length-1].puzzlePieces.AddDraggableEventListeners(this.PuzzleDragStartHandler, this.PuzzleDragEndHandler);
			}
			else
			{
				this.ToggleSubmitButton(false);; // hide the submit button
				
				for(i = 0; i < this.puzzlePieces.length; i++)
				{
					this.puzzlePieces[i].SetPuzzlePiece(this.currentEmptyPuzzleIndex);
					if(!this.puzzlePieces[i].placeholder)
					{
						this.puzzlePieces[i].AddDraggableEventListeners(this.PuzzleDragStartHandler, this.PuzzleDragEndHandler);
					}
				}
				if(this.placedPieces.length > 0)
				{
					this.placedPieces[this.placedPieces.length-1].puzzlePieces.AddDraggableEventListeners(this.PuzzleDragStartHandler, this.PuzzleDragEndHandler);
				}
			}
		}
		
		private function OptionSelectedHandler(event:Event_CalloutWindow):void
		{
			event.target.removeEventListener(Event_CalloutWindow.OPTION_SELECTED, this.OptionSelectedHandler);
			
			var popupPiece:PopupPuzzlePiece = this.popupPieces[this.currentEmptyPuzzleIndex];
			var mc:MovieClip = popupPiece.puzzlePieceGraphic;
			mc.visible = true;
			mc.gotoAndStop(2);
			this.SetTextFormatToPuzzlePieceTextfield(mc.text, this.puzzleTextFormat);
			mc.text.text = event.optionText;
			
			var parent:ProjectPlanning_App_SA = this; // because for some reason 'this' is not recognized inside the anonymous function
			TweenLite.delayedCall(1, 
				function():void 
				{
					parent.DelayedOptionSelectedHandler(popupPiece);
				},
				null, true);
			
			
			event.target.visible = false;
			event.target.RemoveEventListeners();
		}
		
		/**
		 * The dynamic text in the popup piece has to be centered vertically, but because of Flash, the text height
		 * isn't accurate until the piece is visible for one frame. So we set the text in OptionSelectedHandler, 
		 * delay one frame and then continue the option selected logic.
		 */
		private function DelayedOptionSelectedHandler(popupPiece:PopupPuzzlePiece):void
		{
			var mc:MovieClip = popupPiece.puzzlePieceGraphic;
			Utils_Text.FitTextToTextfield(mc.text, 4, true);
			mc.text.y = popupPiece.origTextPosition.y + mc.text.height / 2 - mc.text.textHeight / 2;
			
			this.currentEmptyPuzzleIndex++;
			
			mc.x = (this.emptyPuzzlePlaces[this.currentEmptyPuzzleIndex].x - this.emptyPuzzlePlaces[this.currentEmptyPuzzleIndex-1].x);
			mc.y = (this.emptyPuzzlePlaces[this.currentEmptyPuzzleIndex].y - this.emptyPuzzlePlaces[this.currentEmptyPuzzleIndex-1].y);
			// if the placed puzzle piece is a middle connector, put it on top so that the text doesn't get cut off or overlapped
			if(mc.name == "popupPiece0")
			{
				this.sa_Skin.addChildAt(this.placedPieces[this.placedPieces.length-1].puzzlePieces.puzzlePieces[this.placedPieces[this.placedPieces.length-1].puzzlePieces.activePiece], this.sa_Skin.numChildren-2);
				this.placedPieces[this.placedPieces.length-1].puzzlePieces.puzzlePieces[this.placedPieces[this.placedPieces.length-1].puzzlePieces.activePiece].addChild(mc);
			}
			else this.placedPieces[this.placedPieces.length-1].puzzlePieces.puzzlePieces[this.placedPieces[this.placedPieces.length-1].puzzlePieces.activePiece].addChildAt(mc, 0);
			this.placedPieces[this.placedPieces.length-1].linkedToNextPiece = true;
			this.placedPieces[this.placedPieces.length-1].linkedPieceName = mc.name;
			
			this.currentEmptyPuzzleIndex++;
			if(this.currentEmptyPuzzleIndex == this.puzzlePieces[0].puzzlePieces.length)
			{
				// show submit button
				this.ToggleSubmitButton(true);
				
				// make sure the puzzle piece has the drag listeners on it
				this.placedPieces[this.placedPieces.length-1].puzzlePieces.AddDraggableEventListeners(this.PuzzleDragStartHandler, this.PuzzleDragEndHandler);
			}
			else
			{
				this.ToggleSubmitButton(false);; // hide the submit button
				
				for(var i:int = 0; i < this.puzzlePieces.length; i++)
				{
					this.puzzlePieces[i].SetPuzzlePiece(this.currentEmptyPuzzleIndex);
					if(!this.puzzlePieces[i].placeholder)
					{
						this.puzzlePieces[i].AddDraggableEventListeners(this.PuzzleDragStartHandler, this.PuzzleDragEndHandler);
					}
				}
				if(this.placedPieces.length > 0)
				{
					this.placedPieces[this.placedPieces.length-1].puzzlePieces.AddDraggableEventListeners(this.PuzzleDragStartHandler, this.PuzzleDragEndHandler);
				}
			}
			
		}
		
		private function OptionWindowClosedHandler(event:Event_CalloutWindow):void
		{
			this.placedPieces[this.placedPieces.length-1].puzzlePieces.ResetPiecesToOriginalPositions();
			
			this.placedPieces[this.placedPieces.length-1].puzzlePieces.placeholder = null;
			this.placedPieces[this.placedPieces.length-1].puzzlePieces.SetFrame(1);
			
			for(var i:int = 0; i < this.puzzlePieces.length; i++)
			{
				this.puzzlePieces[i].SetPuzzlePiece(this.currentEmptyPuzzleIndex);
				if(!this.puzzlePieces[i].placeholder)
				{
					this.puzzlePieces[i].AddDraggableEventListeners(this.PuzzleDragStartHandler, this.PuzzleDragEndHandler);
				}
			}
			
			this.placedPieces.pop();
			
			if(this.placedPieces.length > 0)
			{
				this.placedPieces[this.placedPieces.length-1].puzzlePieces.AddDraggableEventListeners(this.PuzzleDragStartHandler, this.PuzzleDragEndHandler);
			}
			
			(event.target as AddYourOwnCalloutWindow).RemoveEventListeners();
			(event.target as AddYourOwnCalloutWindow).removeEventListener(Event_CalloutWindow.OPTION_SELECTED, this.OptionSelectedHandler);
			(event.target as AddYourOwnCalloutWindow).removeEventListener(Event_CalloutWindow.CLOSE_WINDOW, this.OptionWindowClosedHandler);
			(event.target as AddYourOwnCalloutWindow).visible = false;
		}
		
		private function ToggleSubmitButton(toggle:Boolean):void
		{
			if(toggle)
			{
				// (TODO add tween)
				this.sa_Skin.submit_Button.visible = true;
				this.sa_Skin.submit_Button.addEventListener(InputController.CLICK, this.SubmitButtonHandler);
			}
			else
			{
				// (TODO add tween)
				this.sa_Skin.submit_Button.visible = false;
				this.sa_Skin.submit_Button.removeEventListener(InputController.CLICK, this.SubmitButtonHandler);
			}
		}
		
		private function ToggleNoteButton(toggle:Boolean):void
		{
			if(toggle)
			{
				this.notesDraggableObject.AddEventListeners();
				this.notesDraggableObject.addEventListener(Event_DraggableObject.DRAG_UPDATE, this.NoteDragUpdateHandler);
				this.notesDraggableObject.addEventListener(Event_DraggableObject.DRAG_END, this.NoteDragEndHandler);
			}
			else
			{
				this.notesDraggableObject.RemoveEventListeners();
				this.notesDraggableObject.removeEventListener(Event_DraggableObject.DRAG_UPDATE, this.NoteDragUpdateHandler);
				this.notesDraggableObject.removeEventListener(Event_DraggableObject.DRAG_END, this.NoteDragEndHandler);
			}
		}
		
		private function NoteDragUpdateHandler(event:Event_DraggableObject):void
		{
			this.notesDraggableObject.draggableObject.y = this.notesDraggableObject.originalPosition.y;
			
			if(this.notesDraggableObject.draggableObject.x < (CitrusFramework.frameworkStageSize.x - this.notesVisibleWidth)) // we remove 82 from the width because of shadows
			{
				// don't allow the notes window to go out further than it's full size
				this.notesDraggableObject.draggableObject.x = (CitrusFramework.frameworkStageSize.x - this.notesVisibleWidth);
			}
			else if(this.notesDraggableObject.draggableObject.x > this.notesDraggableObject.originalPosition.x)
			{
				// don't let the window go past its original position outside (prevents it from going off screen)
				this.notesDraggableObject.draggableObject.x = this.notesDraggableObject.originalPosition.x;
			}
		}
		
		private function NoteDragEndHandler(event:Event_DraggableObject):void
		{
			// check to see how far the object was dragged
			if(Math.abs(this.notesDraggableObject.dragStartPosition.x - event.currentPosition.x) > 25)
			{
				// we moved it more than x pixels, consider this a drag, not a click
				if(this.notesDraggableObject.draggableObject.x < (CitrusFramework.frameworkStageSize.x - (this.notesVisibleWidth)/2)) 
				{
					// if the window is more than halfway out, snap it to its fully out position (TODO add tween)
					this.notesDraggableObject.draggableObject.x = (CitrusFramework.frameworkStageSize.x - this.notesVisibleWidth);
				}
				else
				{
					// the window is less than halfway out, snap it back into its closed position (TODO add tween)
					this.notesDraggableObject.draggableObject.x = this.notesDraggableObject.originalPosition.x;
				}
			}
			else
			{
				// we didn't move it very far, consider it a click
				if(this.notesDraggableObject.dragStartPosition.x == this.notesDraggableObject.originalPosition.x)
				{
					// the notes window is currently closed, so open it!
					this.notesDraggableObject.draggableObject.x = (CitrusFramework.frameworkStageSize.x - this.notesVisibleWidth);
				}
				else
				{
					// the notes window is open, so close it!
					this.notesDraggableObject.draggableObject.x = this.notesDraggableObject.originalPosition.x;
				}
			}
		}

		
		private function SetUpChildIndexes():void
		{
			// the 4 puzzle pieces should be at the top
			for(var piece:int = 0; piece < this.puzzlePieces.length; piece++)
			{
				for(var inner:int = 0; inner < this.puzzlePieces[piece].puzzlePieces.length; inner++)
				{
					this.sa_Skin.addChild(this.puzzlePieces[piece].puzzlePieces[inner]);
				}
			}
		}
		
		private function AddGlobalWindows():void
		{
			// put notes in the global layer
			CitrusFramework.globalLayer.addChild(this.notesDraggableObject.draggableObject);
			CitrusFramework.globalLayer.addChild(this.findWorkspace.calloutWindow);
			CitrusFramework.globalLayer.addChild(this.lookForVolunteers.calloutWindow);
			CitrusFramework.globalLayer.addChild(this.makeASchedule.calloutWindow);
			CitrusFramework.globalLayer.addChild(this.budgetAfterSupplies.calloutWindow);
		}
		
		private function RemoveGlobalWindows():void
		{
			CitrusFramework.globalLayer.removeChild(this.notesDraggableObject.draggableObject);
			CitrusFramework.globalLayer.removeChild(this.findWorkspace.calloutWindow);
			CitrusFramework.globalLayer.removeChild(this.lookForVolunteers.calloutWindow);
			CitrusFramework.globalLayer.removeChild(this.makeASchedule.calloutWindow);
			CitrusFramework.globalLayer.removeChild(this.budgetAfterSupplies.calloutWindow);
		}
		
		private function CloseNotesWindowHandler(event:Event_NotesWindow):void
		{
			this.notesDraggableObject.draggableObject.x = this.notesDraggableObject.originalPosition.x;
		}
		
		private function SaveNotesWindowHandler(event:Event_NotesWindow):void
		{
			this.notesDraggableObject.draggableObject.x = this.notesDraggableObject.originalPosition.x;
		}
	}
}
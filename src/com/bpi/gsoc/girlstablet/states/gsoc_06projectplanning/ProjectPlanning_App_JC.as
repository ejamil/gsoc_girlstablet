package com.bpi.gsoc.girlstablet.states.gsoc_06projectplanning
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.citrusas.input.inputcontroller.InputController;
	import com.bpi.citrusas.ui.draggableobject.DraggableObject;
	import com.bpi.citrusas.ui.draggableobject.events.Event_DraggableObject;
	import com.bpi.citrusas.utils.Utils_Text;
	import com.bpi.gsoc.components.projectplanning.ProjectPlanning_Screen_JC_Skin;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.embeddedcontent.EmbeddedFonts;
	import com.bpi.gsoc.framework.gsocui.GSOC_Header;
	import com.bpi.gsoc.framework.gsocui.STEM_Popups;
	import com.bpi.gsoc.girlstablet.states.gsoc_06projectplanning.calloutwindow.AddYourOwnCalloutWindow;
	import com.bpi.gsoc.girlstablet.states.gsoc_06projectplanning.calloutwindow.Event_CalloutWindow;
	import com.bpi.gsoc.girlstablet.states.gsoc_06projectplanning.calloutwindow.OptionField;
	import com.bpi.gsoc.girlstablet.states.gsoc_06projectplanning.noteswindow.Event_NotesWindow;
	import com.bpi.gsoc.girlstablet.states.gsoc_06projectplanning.noteswindow.NotesWindow;
	import com.bpi.gsoc.girlstablet.states.gsoc_06projectplanning.puzzlepieces.GroupOfPuzzlePieces;
	import com.bpi.gsoc.girlstablet.states.gsoc_06projectplanning.puzzlepieces.PlacedPuzzlePiece;
	import com.bpi.gsoc.girlstablet.states.gsoc_06projectplanning.puzzlepieces.PopupPuzzlePiece;
	import com.greensock.TweenLite;
	
	import flash.display.MovieClip;
	import flash.filters.DropShadowFilter;
	import flash.text.TextField;
	import flash.text.TextFormat;

	public class ProjectPlanning_App_JC extends ProjectPlanning_App
	{
		private var jc_Skin:ProjectPlanning_Screen_JC_Skin;
		
		private var lookForVolunteers:AddYourOwnCalloutWindow;
		private var createBudget:AddYourOwnCalloutWindow;
		private var findSupplies:AddYourOwnCalloutWindow;
		
		private var puzzleTextFormat:TextFormat;
		
		private var puzzlePieces:Vector.<GroupOfPuzzlePieces>;
		private var currentEmptyPuzzleIndex:int;
		private var emptyPuzzlePlaces:Vector.<MovieClip>;
		private var activePuzzleGroupIndex:int;
		
		private var placedPieces:Vector.<PlacedPuzzlePiece>;
		
		private var notesDraggableObject:DraggableObject;
		private var notesWindow:NotesWindow;
		private var notesVisibleWidth:Number;
		
		private var popupPieces:Vector.<PopupPuzzlePiece>;
		
		private const PUZZLE_TEXTS:Vector.<String> = new <String>["Find Workspace", "Set Deadlines", "Create A Budget", "Build Project Team", "Find Supplies"];
		private const LOOK_FOR_VOLUNTEERS_OPTIONS:Vector.<String> = new <String>["Ask Girls In My Troop To Help", "Hang Up Posters At School To Get People Interested"];
		private const CREATE_A_BUDGET_OPTIONS:Vector.<String> = new <String>["Hire Designer And Adjust Budget", "Ask Art Students To Design Posters For Free"];
		private const FIND_SUPPLIES_OPTIONS:Vector.<String> = new <String>["Contact Local Businesses To See If You Can Borrow Supplies", "Order Supplies Online Using Discounts And Coupon Codes"];
		
		public function ProjectPlanning_App_JC()
		{
			super();
			
			this.m_Skin = new ProjectPlanning_Screen_JC_Skin();
			
			this.jc_Skin = this.m_Skin as ProjectPlanning_Screen_JC_Skin;
			
			m_GSOCHeader = new GSOC_Header(m_Skin.header.paginationArea);
			GSOCheader.addArrows(m_Skin.header.paginationArea.x, 0);
		}
		
		public override function Initialize():void
		{
			super.Initialize();
			
			this.lookForVolunteers = new AddYourOwnCalloutWindow(this.jc_Skin.callout1, new OptionField(this.jc_Skin.callout1.optionbox1,this.jc_Skin.callout1.optiontext1),
				new OptionField(this.jc_Skin.callout1.optionbox2, this.jc_Skin.callout1.optiontext2));
			this.lookForVolunteers.InitializeFonts(new TextFormat(EmbeddedFonts.FontName_TwCenMTTTF, 48, 0xffffff, true), new TextFormat(EmbeddedFonts.FontName_TwCenMTTTF, 60, 0xffffff, true), new TextFormat(EmbeddedFonts.FontName_TwCenMTTTF, 48, 0xffffff, true));
			this.lookForVolunteers.InitializeOptionTexts(this.LOOK_FOR_VOLUNTEERS_OPTIONS);
			this.lookForVolunteers.InitializeAddYourOwnFont(new TextFormat(EmbeddedFonts.FontName_TwCenMTTTF, 48, 0x0CB1A5, true));
			
			this.createBudget = new AddYourOwnCalloutWindow(this.jc_Skin.callout2, new OptionField(this.jc_Skin.callout2.optionbox1, this.jc_Skin.callout2.optiontext1),
				new OptionField(this.jc_Skin.callout2.optionbox2, this.jc_Skin.callout2.optiontext2));
			this.createBudget.InitializeFonts(new TextFormat(EmbeddedFonts.FontName_TwCenMTTTF, 48, 0xffffff, true), new TextFormat(EmbeddedFonts.FontName_TwCenMTTTF, 60, 0xffffff, true), new TextFormat(EmbeddedFonts.FontName_TwCenMTTTF, 48, 0xffffff, true));
			this.createBudget.InitializeOptionTexts(this.CREATE_A_BUDGET_OPTIONS);
			this.createBudget.InitializeAddYourOwnFont(new TextFormat(EmbeddedFonts.FontName_TwCenMTTTF, 48, 0x0CB1A5, true));
			
			this.findSupplies = new AddYourOwnCalloutWindow(this.jc_Skin.callout3, new OptionField(this.jc_Skin.callout3.optionbox1, this.jc_Skin.callout3.optiontext1),
				new OptionField(this.jc_Skin.callout3.optionbox2, this.jc_Skin.callout3.optiontext2));
			this.findSupplies.InitializeFonts(new TextFormat(EmbeddedFonts.FontName_TwCenMTTTF, 48, 0xffffff, true), new TextFormat(EmbeddedFonts.FontName_TwCenMTTTF, 60, 0xffffff, true), new TextFormat(EmbeddedFonts.FontName_TwCenMTTTF, 48, 0xffffff, true));
			this.findSupplies.InitializeOptionTexts(this.FIND_SUPPLIES_OPTIONS);
			this.findSupplies.InitializeAddYourOwnFont(new TextFormat(EmbeddedFonts.FontName_TwCenMTTTF, 48, 0x0CB1A5, true));
			
			this.puzzleTextFormat = new TextFormat(EmbeddedFonts.FontName_TwCenMTTTF, 42, 0xffffff, true);
			
			this.puzzlePieces = new Vector.<GroupOfPuzzlePieces>();
			this.puzzlePieces.push(new GroupOfPuzzlePieces(this.jc_Skin.addend1_1, this.jc_Skin.middleInAddConnector0, this.jc_Skin.addend2_1, this.jc_Skin.addend3_1, this.jc_Skin.addend5_1, this.jc_Skin.addend6_1, this.jc_Skin.addconnector2_1, this.jc_Skin.addend4_1));
			this.puzzlePieces.push(new GroupOfPuzzlePieces(this.jc_Skin.addend1_2, this.jc_Skin.middleInAddConnector1, this.jc_Skin.addend2_2, this.jc_Skin.addend3_2, this.jc_Skin.addend5_2, this.jc_Skin.addend6_2, this.jc_Skin.addconnector2_2, this.jc_Skin.addend4_2));
			this.puzzlePieces.push(new GroupOfPuzzlePieces(this.jc_Skin.addend1_3, this.jc_Skin.middleInAddConnector2, this.jc_Skin.addend2_3, this.jc_Skin.addend3_3, this.jc_Skin.addend5_3, this.jc_Skin.addend6_3, this.jc_Skin.addconnector2_3, this.jc_Skin.addend4_3));
			this.puzzlePieces.push(new GroupOfPuzzlePieces(this.jc_Skin.addend1_4, this.jc_Skin.middleInAddConnector3, this.jc_Skin.addend2_4, this.jc_Skin.addend3_4, this.jc_Skin.addend5_4, this.jc_Skin.addend6_4, this.jc_Skin.addconnector2_4, this.jc_Skin.addend4_4));
			this.puzzlePieces.push(new GroupOfPuzzlePieces(this.jc_Skin.addend1_5, this.jc_Skin.middleInAddConnector4, this.jc_Skin.addend2_5, this.jc_Skin.addend3_5, this.jc_Skin.addend5_5, this.jc_Skin.addend6_5, this.jc_Skin.addconnector2_5, this.jc_Skin.addend4_5));
			this.puzzlePieces[0].Initialize(this.PUZZLE_TEXTS[0]);
			this.puzzlePieces[1].Initialize(this.PUZZLE_TEXTS[1]);
			this.puzzlePieces[2].Initialize(this.PUZZLE_TEXTS[2], this.CreateABudgetCallback);
			this.puzzlePieces[3].Initialize(this.PUZZLE_TEXTS[3], this.LookForVolunteersCallback);
			this.puzzlePieces[4].Initialize(this.PUZZLE_TEXTS[4], this.FindSuppliesCallback);
			
			this.emptyPuzzlePlaces = new Vector.<MovieClip>();
			this.emptyPuzzlePlaces.push(this.jc_Skin.blankend_1);
			this.emptyPuzzlePlaces.push(this.jc_Skin.blankconnector1_1);
			this.emptyPuzzlePlaces.push(this.jc_Skin.blankend_2);
			this.emptyPuzzlePlaces.push(this.jc_Skin.blankend_3);
			this.emptyPuzzlePlaces.push(this.jc_Skin.blankend_4);
			this.emptyPuzzlePlaces.push(this.jc_Skin.blankend_5);
			this.emptyPuzzlePlaces.push(this.jc_Skin.blankconnector2_1);
			this.emptyPuzzlePlaces.push(this.jc_Skin.blankend_6);
			
			this.popupPieces = new Vector.<PopupPuzzlePiece>();
			this.popupPieces.push(new PopupPuzzlePiece(this.jc_Skin.popupPiece0));
			this.popupPieces.push(new PopupPuzzlePiece(this.jc_Skin.popupPiece1));
			this.popupPieces.push(new PopupPuzzlePiece(this.jc_Skin.popupPiece2));
			this.popupPieces.push(new PopupPuzzlePiece(this.jc_Skin.popupPiece3));
			this.popupPieces.push(new PopupPuzzlePiece(this.jc_Skin.popupPiece4));
			this.popupPieces.push(new PopupPuzzlePiece(this.jc_Skin.popupPiece5));
			this.popupPieces.push(new PopupPuzzlePiece(this.jc_Skin.popupPiece6));
			
			for(var i:int = 0; i < this.popupPieces.length; i++)
			{
				this.popupPieces[i].puzzlePieceGraphic.visible = false;
			}
			
			this.currentEmptyPuzzleIndex = 0;
			this.activePuzzleGroupIndex = -1;
			this.placedPieces = new Vector.<PlacedPuzzlePiece>();
			
			this.notesDraggableObject = new DraggableObject(this.jc_Skin.notes, this.jc_Skin.notes.notesHitBox);
			this.notesWindow = new NotesWindow(this.jc_Skin.notes, new TextFormat(EmbeddedFonts.FontName_TwCenMTTTF, 48, 0x993092, true), SessionInformation_GirlsTablet.Age);
			this.notesVisibleWidth = this.jc_Skin.notes.edgeMarker.x;
			this.notesWindow.AddEventListeners();
			this.notesWindow.addEventListener(Event_NotesWindow.CLOSE, this.CloseNotesWindowHandler);
			this.notesWindow.addEventListener(Event_NotesWindow.SAVE, this.SaveNotesWindowHandler);
			
			this.SetUpChildIndexes();
		}
		
		public override function Init():void
		{
			super.Init();
			
			this.AddGlobalWindows();
			
			this.ToggleSubmitButton(false);
			
			this.InitPuzzlePieces();
			
			this.ToggleNoteButton(true);
			
			this.notesWindow.Init();
		}
		
		public override function DeInit():void
		{
			this.RemoveGlobalWindows();
		}
		
		private function InitPuzzlePieces():void
		{
			this.currentEmptyPuzzleIndex = 0;
			this.indexOfProblemPuzzlePiece = 1;
			this.activePuzzleGroupIndex = -1;
			
			for(var i:int = 0; i < this.puzzlePieces.length; i++)
			{
				this.puzzlePieces[i].SetVisible(true);
				this.puzzlePieces[i].AddDraggableEventListeners(this.PuzzleDragStartHandler, this.PuzzleDragEndHandler);
				this.puzzlePieces[i].SetPuzzlePiece(this.currentEmptyPuzzleIndex);
			}
			
			this.SetTextFormatToPuzzlePieceTextfield(this.jc_Skin.addend1_1.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.jc_Skin.addend1_2.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.jc_Skin.addend1_3.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.jc_Skin.addend1_4.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.jc_Skin.addend1_5.text, this.puzzleTextFormat);
			
			this.SetTextFormatToPuzzlePieceTextfield(this.jc_Skin.addend2_1.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.jc_Skin.addend2_2.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.jc_Skin.addend2_3.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.jc_Skin.addend2_4.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.jc_Skin.addend2_5.text, this.puzzleTextFormat);
			
			this.SetTextFormatToPuzzlePieceTextfield(this.jc_Skin.middleInAddConnector0.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.jc_Skin.middleInAddConnector1.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.jc_Skin.middleInAddConnector2.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.jc_Skin.middleInAddConnector3.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.jc_Skin.middleInAddConnector4.text, this.puzzleTextFormat);
			
			this.SetTextFormatToPuzzlePieceTextfield(this.jc_Skin.addconnector2_1.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.jc_Skin.addconnector2_2.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.jc_Skin.addconnector2_3.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.jc_Skin.addconnector2_4.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.jc_Skin.addconnector2_5.text, this.puzzleTextFormat);
			
			this.SetTextFormatToPuzzlePieceTextfield(this.jc_Skin.addend3_1.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.jc_Skin.addend3_2.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.jc_Skin.addend3_3.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.jc_Skin.addend3_4.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.jc_Skin.addend3_5.text, this.puzzleTextFormat);
			
			this.SetTextFormatToPuzzlePieceTextfield(this.jc_Skin.addend4_1.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.jc_Skin.addend4_2.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.jc_Skin.addend4_3.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.jc_Skin.addend4_4.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.jc_Skin.addend4_5.text, this.puzzleTextFormat);
			
			this.SetTextFormatToPuzzlePieceTextfield(this.jc_Skin.addend5_1.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.jc_Skin.addend5_2.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.jc_Skin.addend5_3.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.jc_Skin.addend5_4.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.jc_Skin.addend5_5.text, this.puzzleTextFormat);
			
			this.SetTextFormatToPuzzlePieceTextfield(this.jc_Skin.addend6_1.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.jc_Skin.addend6_2.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.jc_Skin.addend6_3.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.jc_Skin.addend6_4.text, this.puzzleTextFormat);
			this.SetTextFormatToPuzzlePieceTextfield(this.jc_Skin.addend6_5.text, this.puzzleTextFormat);
			
			this.SetUpChildIndexes();
		}
		
		private function SetTextFormatToPuzzlePieceTextfield(textField:TextField, textFormat:TextFormat, addDropShadow:Boolean = true):void
		{
			textField.defaultTextFormat = textFormat;
			textField.setTextFormat(textFormat);
			
			if(addDropShadow)
			{
				var dropShadow:DropShadowFilter = new DropShadowFilter(2, 45, 0, 0.6, 6, 6);
				textField.filters = [ dropShadow ];
			}
			else
			{
				textField.filters = [];
			}
		}
		
		
		private function PuzzleDragStartHandler(e:Event_DraggableObject, currentPuzzleGroup:GroupOfPuzzlePieces):void
		{
			this.ToggleSubmitButton(false);
			
			// if a piece is currently picked up in addition to this one
			if(this.activePuzzleGroupIndex != -1) 
			{
				// dont' drop the current piece if it's the "correct" active one (in the case of a second click on the same piece)
				if(! (this.puzzlePieces.indexOf(currentPuzzleGroup) == this.activePuzzleGroupIndex))
				{
					// but do drop it if it's the second+ one to be picked up
					currentPuzzleGroup.ForceDropActivePuzzlePieces();
				}
			}
			else
			{
				// otherwise we can just set the active index
				this.activePuzzleGroupIndex = this.puzzlePieces.indexOf(currentPuzzleGroup);
			}
			
		}
		
		private function PuzzleDragEndHandler(event:Event_DraggableObject, puzzlePieces:GroupOfPuzzlePieces):void
		{
			this.activePuzzleGroupIndex = -1;
			// check to see if the dragged puzzle piece is on top of the empty puzzle piece
			if(!puzzlePieces.placeholder)
			{
				if((event.target as DraggableObject).draggableObject.hitTestObject(this.emptyPuzzlePlaces[this.currentEmptyPuzzleIndex]))
				{
					puzzlePieces.SetFrame(2);
					puzzlePieces.placeholder = this.emptyPuzzlePlaces[this.currentEmptyPuzzleIndex];
					
					// need to tween from the current position to the below position
					(event.target as DraggableObject).draggableObject.x = this.emptyPuzzlePlaces[this.currentEmptyPuzzleIndex].x;
					(event.target as DraggableObject).draggableObject.y = this.emptyPuzzlePlaces[this.currentEmptyPuzzleIndex].y;
					
					// if the placed puzzle piece is a middle connector, put it on top so that the text doesn't get cut off or overlapped
					if(puzzlePieces.puzzlePieces[puzzlePieces.activePiece].name.indexOf("middleInAddConnector") != -1)
					{
						//this.jc_Skin.addChildAt(puzzlePieces.puzzlePieces[puzzlePieces.activePiece], this.jc_Skin.numChildren); // place it on top
					}
					
					this.placedPieces.push(new PlacedPuzzlePiece(puzzlePieces));
					
					if(puzzlePieces.optionCallback)
					{
						// remove drag listeners until option has been selected
						for(var i:int = 0; i < this.puzzlePieces.length; i++)
						{
							this.puzzlePieces[i].RemoveDraggableEventListeners();
						}
						puzzlePieces.optionCallback();
					}
					else
					{
						STEM_Popups.DisplayPopup(STEM_Popups.PROJECT_PLANNING);
						this.currentEmptyPuzzleIndex++;
						if(this.currentEmptyPuzzleIndex == puzzlePieces.puzzlePieces.length)
						{
							// show submit button
							this.ToggleSubmitButton(true);
						}
						else
						{
							for(i = 0; i < this.puzzlePieces.length; i++)
							{
								this.puzzlePieces[i].SetPuzzlePiece(this.currentEmptyPuzzleIndex);
							}
						}
						
						if(this.placedPieces.length > 1)
						{
							this.placedPieces[this.placedPieces.length-2].puzzlePieces.RemoveDraggableEventListeners();
						}
					}
				}
				else
				{
					// snap it back to its original location (add tween)
					(event.target as DraggableObject).draggableObject.x = (event.target as DraggableObject).originalPosition.x;
					(event.target as DraggableObject).draggableObject.y = (event.target as DraggableObject).originalPosition.y;
				}
			}
			else
			{
				if((event.target as DraggableObject).draggableObject.hitTestObject(puzzlePieces.placeholder))
				{
					if(this.currentEmptyPuzzleIndex == puzzlePieces.puzzlePieces.length)
					{
						this.ToggleSubmitButton(true);
					}
					
					// snap it back to its placed location
					(event.target as DraggableObject).draggableObject.x = puzzlePieces.placeholder.x;
					(event.target as DraggableObject).draggableObject.y = puzzlePieces.placeholder.y;
				}
				else
				{
					// the puzzle piece was in a placed position and was dragged out of it, send it back to it's original location and reset it so it can be placed again
					(event.target as DraggableObject).draggableObject.x = (event.target as DraggableObject).originalPosition.x;
					(event.target as DraggableObject).draggableObject.y = (event.target as DraggableObject).originalPosition.y;
					
					this.currentEmptyPuzzleIndex--;
					
					var pop:PlacedPuzzlePiece = this.placedPieces.pop();
					if(pop.linkedToNextPiece)
					{
						// need to remove the next puzzle piece from the child
						pop.puzzlePieces.puzzlePieces[pop.puzzlePieces.activePiece].removeChild(pop.puzzlePieces.puzzlePieces[pop.puzzlePieces.activePiece].getChildByName(pop.linkedPieceName));
						this.currentEmptyPuzzleIndex--;
					}
					
					puzzlePieces.placeholder = null;
					puzzlePieces.SetFrame(1);
					
					for(i = 0; i < this.puzzlePieces.length; i++)
					{
						this.puzzlePieces[i].SetPuzzlePiece(this.currentEmptyPuzzleIndex);
					}
					
					if(this.placedPieces.length > 0)
					{
						this.placedPieces[this.placedPieces.length-1].puzzlePieces.AddDraggableEventListeners(this.PuzzleDragStartHandler, this.PuzzleDragEndHandler);
					}
				}
			}
			
			var effectiveIndex:int = -1;
			for (i = 0; i < this.placedPieces.length; i ++)
			{
				effectiveIndex++;

				if(this.placedPieces[i].linkedToNextPiece)
				{
					effectiveIndex++;
				}
				
				if(effectiveIndex == this.indexOfProblemPuzzlePiece) 
				{
					this.jc_Skin.addChildAt(this.placedPieces[i].puzzlePieces.puzzlePieces[this.placedPieces[i].puzzlePieces.activePiece], this.jc_Skin.numChildren);
				}
			}
		}
		
		private function CreateABudgetCallback():void
		{
			this.createBudget.visible = true;
			this.createBudget.PlaceWindowOnTop();
			
			this.createBudget.AddEventListeners();
			this.createBudget.addEventListener(Event_CalloutWindow.OPTION_SELECTED, this.OptionSelectedHandler);
			this.createBudget.addEventListener(Event_CalloutWindow.CLOSE_WINDOW, this.OptionWindowClosedHandler);
		}
		
		private function LookForVolunteersCallback():void
		{
			this.lookForVolunteers.visible = true;
			this.lookForVolunteers.PlaceWindowOnTop();
			
			this.lookForVolunteers.AddEventListeners();
			this.lookForVolunteers.addEventListener(Event_CalloutWindow.OPTION_SELECTED, this.OptionSelectedHandler);
			this.lookForVolunteers.addEventListener(Event_CalloutWindow.CLOSE_WINDOW, this.OptionWindowClosedHandler);
		}
		
		private function FindSuppliesCallback():void
		{
			this.findSupplies.visible = true;
			this.findSupplies.PlaceWindowOnTop();
			
			this.findSupplies.AddEventListeners();
			this.findSupplies.addEventListener(Event_CalloutWindow.OPTION_SELECTED, this.OptionSelectedHandler);
			this.findSupplies.addEventListener(Event_CalloutWindow.CLOSE_WINDOW, this.OptionWindowClosedHandler);
		}
		
		private function OptionSelectedHandler(event:Event_CalloutWindow):void
		{
			event.target.removeEventListener(Event_CalloutWindow.OPTION_SELECTED, this.OptionSelectedHandler);
			
			var popupPiece:PopupPuzzlePiece = this.popupPieces[this.currentEmptyPuzzleIndex];
			var mc:MovieClip = popupPiece.puzzlePieceGraphic;
			mc.visible = true;
			mc.gotoAndStop(2);
			this.SetTextFormatToPuzzlePieceTextfield(mc.text, this.puzzleTextFormat);
			mc.text.text = event.optionText;
			mc.y = CitrusFramework.frameworkStageSize.y; // move it off the stage so the text doesn't flicker from the wrong position
			
			var parent:ProjectPlanning_App_JC = this; // because for some reason 'this' is not recognized inside the anonymous function
			TweenLite.delayedCall(1, 
				function():void 
				{
					parent.DelayedOptionSelectedHandler(popupPiece);
				},
				null, true);
			
			event.target.visible = false;
			event.target.RemoveEventListeners();
		}
		
		/**
		 * The dynamic text in the popup piece has to be centered vertically, but because of Flash, the text height
		 * isn't accurate until the piece is visible for one frame. So we set the text in OptionSelectedHandler, 
		 * delay one frame and then continue the option selected logic.
		 */
		private function DelayedOptionSelectedHandler(popupPiece:PopupPuzzlePiece):void
		{
			var mc:MovieClip = popupPiece.puzzlePieceGraphic;
			
			Utils_Text.FitTextToTextfield(mc.text, 4, true);
			mc.text.y = popupPiece.origTextPosition.y + mc.text.height / 2 - mc.text.textHeight / 2;
			
			this.currentEmptyPuzzleIndex++;
			
			mc.x = (this.emptyPuzzlePlaces[this.currentEmptyPuzzleIndex].x - this.emptyPuzzlePlaces[this.currentEmptyPuzzleIndex-1].x);
			mc.y = (this.emptyPuzzlePlaces[this.currentEmptyPuzzleIndex].y - this.emptyPuzzlePlaces[this.currentEmptyPuzzleIndex-1].y);
			// if the placed puzzle piece is a middle connector, put it on top so that the text doesn't get cut off or overlapped
			if(mc.name == "popupPiece0")
			{
				this.jc_Skin.addChildAt(this.placedPieces[this.placedPieces.length-1].puzzlePieces.puzzlePieces[this.placedPieces[this.placedPieces.length-1].puzzlePieces.activePiece], this.jc_Skin.numChildren-2);
				this.placedPieces[this.placedPieces.length-1].puzzlePieces.puzzlePieces[this.placedPieces[this.placedPieces.length-1].puzzlePieces.activePiece].addChild(mc);
			}
			else this.placedPieces[this.placedPieces.length-1].puzzlePieces.puzzlePieces[this.placedPieces[this.placedPieces.length-1].puzzlePieces.activePiece].addChildAt(mc, 0);
			this.placedPieces[this.placedPieces.length-1].linkedToNextPiece = true;
			this.placedPieces[this.placedPieces.length-1].linkedPieceName = mc.name;
			
			this.currentEmptyPuzzleIndex++;
			if(this.currentEmptyPuzzleIndex == this.puzzlePieces[0].puzzlePieces.length)
			{
				// show submit button
				this.ToggleSubmitButton(true);
				
				// make sure the puzzle piece has the drag listeners on it
				this.placedPieces[this.placedPieces.length-1].puzzlePieces.AddDraggableEventListeners(this.PuzzleDragStartHandler, this.PuzzleDragEndHandler);
			}
			else
			{
				this.ToggleSubmitButton(false);; // hide the submit button
				
				for(var i:int = 0; i < this.puzzlePieces.length; i++)
				{
					this.puzzlePieces[i].SetPuzzlePiece(this.currentEmptyPuzzleIndex);
					if(!this.puzzlePieces[i].placeholder)
					{
						this.puzzlePieces[i].AddDraggableEventListeners(this.PuzzleDragStartHandler, this.PuzzleDragEndHandler);
					}
				}
				if(this.placedPieces.length > 0)
				{
					this.placedPieces[this.placedPieces.length-1].puzzlePieces.AddDraggableEventListeners(this.PuzzleDragStartHandler, this.PuzzleDragEndHandler);
				}
			}
			
			
		}
		
		private function OptionWindowClosedHandler(event:Event_CalloutWindow):void
		{
			// the puzzle piece was in a placed position and was dragged out of it, send it back to it's original location and reset it so it can be placed again
			this.placedPieces[this.placedPieces.length-1].puzzlePieces.ResetPiecesToOriginalPositions();
			
			this.placedPieces[this.placedPieces.length-1].puzzlePieces.placeholder = null;
			this.placedPieces[this.placedPieces.length-1].puzzlePieces.SetFrame(1);
			
			for(var i:int = 0; i < this.puzzlePieces.length; i++)
			{
				this.puzzlePieces[i].SetPuzzlePiece(this.currentEmptyPuzzleIndex);
				if(!this.puzzlePieces[i].placeholder)
				{
					this.puzzlePieces[i].AddDraggableEventListeners(this.PuzzleDragStartHandler, this.PuzzleDragEndHandler);
				}
			}
			
			this.placedPieces.pop();
			
			if(this.placedPieces.length > 0)
			{
				this.placedPieces[this.placedPieces.length-1].puzzlePieces.AddDraggableEventListeners(this.PuzzleDragStartHandler, this.PuzzleDragEndHandler);
			}
			
			(event.target as AddYourOwnCalloutWindow).RemoveEventListeners();
			(event.target as AddYourOwnCalloutWindow).removeEventListener(Event_CalloutWindow.OPTION_SELECTED, this.OptionSelectedHandler);
			(event.target as AddYourOwnCalloutWindow).removeEventListener(Event_CalloutWindow.CLOSE_WINDOW, this.OptionWindowClosedHandler);
			(event.target as AddYourOwnCalloutWindow).visible = false;
		}
		
		private function ToggleSubmitButton(toggle:Boolean):void
		{
			if(toggle)
			{
				// (TODO add tween)
				this.jc_Skin.submit_Button.visible = true;
				this.jc_Skin.submit_Button.addEventListener(InputController.CLICK, this.SubmitButtonHandler);
			}
			else
			{
				// (TODO add tween)
				this.jc_Skin.submit_Button.visible = false;
				this.jc_Skin.submit_Button.removeEventListener(InputController.CLICK, this.SubmitButtonHandler);
			}
		}
		
		private function ToggleNoteButton(toggle:Boolean):void
		{
			if(toggle)
			{
				this.notesDraggableObject.AddEventListeners();
				this.notesDraggableObject.addEventListener(Event_DraggableObject.DRAG_UPDATE, this.NoteDragUpdateHandler);
				this.notesDraggableObject.addEventListener(Event_DraggableObject.DRAG_END, this.NoteDragEndHandler);
			}
			else
			{
				this.notesDraggableObject.RemoveEventListeners();
				this.notesDraggableObject.removeEventListener(Event_DraggableObject.DRAG_UPDATE, this.NoteDragUpdateHandler);
				this.notesDraggableObject.removeEventListener(Event_DraggableObject.DRAG_END, this.NoteDragEndHandler);
			}
		}
		
		private function NoteDragUpdateHandler(event:Event_DraggableObject):void
		{
			this.notesDraggableObject.draggableObject.y = this.notesDraggableObject.originalPosition.y;
			
			if(this.notesDraggableObject.draggableObject.x < (CitrusFramework.frameworkStageSize.x - this.notesVisibleWidth)) 
			{
				// don't allow the notes window to go out further than it's full size
				this.notesDraggableObject.draggableObject.x = (CitrusFramework.frameworkStageSize.x - this.notesVisibleWidth);
			}
			else if(this.notesDraggableObject.draggableObject.x > this.notesDraggableObject.originalPosition.x)
			{
				// don't let the window go past its original position outside (prevents it from going off screen)
				this.notesDraggableObject.draggableObject.x = this.notesDraggableObject.originalPosition.x;
			}
		}
		
		private function NoteDragEndHandler(event:Event_DraggableObject):void
		{
			// check to see how far the object was dragged
			if(Math.abs(this.notesDraggableObject.dragStartPosition.x - event.currentPosition.x) > 25)
			{
				// we moved it more than x pixels, consider this a drag, not a click
				if(this.notesDraggableObject.draggableObject.x < (CitrusFramework.frameworkStageSize.x - (this.notesVisibleWidth)/2))
				{
					// if the window is more than halfway out, snap it to its fully out position (TODO add tween)
					this.notesDraggableObject.draggableObject.x = (CitrusFramework.frameworkStageSize.x - this.notesVisibleWidth);
				}
				else
				{
					// the window is less than halfway out, snap it back into its closed position (TODO add tween)
					this.notesDraggableObject.draggableObject.x = this.notesDraggableObject.originalPosition.x;
				}
			}
			else
			{
				// we didn't move it very far, consider it a click
				if(this.notesDraggableObject.dragStartPosition.x == this.notesDraggableObject.originalPosition.x)
				{
					// the notes window is currently closed, so open it!
					this.notesDraggableObject.draggableObject.x = (CitrusFramework.frameworkStageSize.x - this.notesVisibleWidth);
				}
				else
				{
					// the notes window is open, so close it!
					this.notesDraggableObject.draggableObject.x = this.notesDraggableObject.originalPosition.x;
				}
			}
		}
		
	
		
		private function SetUpChildIndexes():void
		{
			// the 4 puzzle pieces should be at the top
			for(var piece:int = 0; piece < this.puzzlePieces.length; piece++)
			{
				for(var inner:int = 0; inner < this.puzzlePieces[piece].puzzlePieces.length; inner++)
				{
					this.jc_Skin.addChild(this.puzzlePieces[piece].puzzlePieces[inner]);
				}
			}
		}
		
		private function AddGlobalWindows():void
		{
			// put notes in the global layer
			CitrusFramework.globalLayer.addChild(this.notesDraggableObject.draggableObject);
			CitrusFramework.globalLayer.addChild(this.createBudget.calloutWindow);
			CitrusFramework.globalLayer.addChild(this.lookForVolunteers.calloutWindow);
			CitrusFramework.globalLayer.addChild(this.findSupplies.calloutWindow);
		}
		
		private function RemoveGlobalWindows():void
		{
			CitrusFramework.globalLayer.removeChild(this.notesDraggableObject.draggableObject);
			CitrusFramework.globalLayer.removeChild(this.createBudget.calloutWindow);
			CitrusFramework.globalLayer.removeChild(this.lookForVolunteers.calloutWindow);
			CitrusFramework.globalLayer.removeChild(this.findSupplies.calloutWindow);
		}
		
		private function CloseNotesWindowHandler(event:Event_NotesWindow):void
		{
			this.notesDraggableObject.draggableObject.x = this.notesDraggableObject.originalPosition.x;
		}
		
		private function SaveNotesWindowHandler(event:Event_NotesWindow):void
		{
			this.notesDraggableObject.draggableObject.x = this.notesDraggableObject.originalPosition.x;
		}
	}
}
package com.bpi.gsoc.girlstablet.networking
{
	import com.bpi.citrusas.networking.client.Client;
	import com.bpi.citrusas.networking.events.Event_ClientNetworkMessage;
	import com.bpi.citrusas.networking.networkmessage.NetworkMessageCall;
	import com.bpi.citrusas.networking.networkmessage.NetworkMessageManager_Client;
	import com.bpi.citrusas.states.StateControllerManager;
	import com.bpi.gsoc.framework.GSOC_RestartManager;
	import com.bpi.gsoc.framework.constants.AgeConstants;
	import com.bpi.gsoc.framework.constants.JourneyConstants;
	import com.bpi.gsoc.framework.constants.TrackConstants;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.networking.NetworkMessage_Types;
	import com.bpi.gsoc.framework.networking.ScoutManager;
	import com.bpi.gsoc.girlstablet.global_data.GirlsTablet_RestartManager;
	import com.bpi.gsoc.girlstablet.global_data.download.DownloadManager;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_IDs;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_BrainStorm;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_ChooseIssue;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_ChooseYourCareer;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_CommunityResources;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_ExploreYourPassion;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_KnowYourRole;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_Main;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_MindMap;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_PictureYourself;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_Poster;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_ProjectPlanning;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_RecordVideo;
	
	import flash.events.Event;
	import flash.utils.ByteArray;
	
	public class NetworkMessageManager_GirlsTablet extends NetworkMessageManager_Client
	{
		public function NetworkMessageManager_GirlsTablet()
		{
		}
		
		protected override function initializeNetworkMessageCalls():void
		{
			m_NetworkMessageCalls.push(new NetworkMessageCall(NetworkMessage_Types.TELL_CLIENT_LEAVE, leave));
			m_NetworkMessageCalls.push(new NetworkMessageCall(NetworkMessage_Types.TELL_CLIENT_RESTART, restart));
			m_NetworkMessageCalls.push(new NetworkMessageCall(NetworkMessage_Types.TELL_CLIENT_MOVETO_ATTRACT, moveToAttract));
			m_NetworkMessageCalls.push(new NetworkMessageCall(NetworkMessage_Types.TELL_CLIENT_MOVETO_WAITFORSTART, moveToWaitForStart));
			m_NetworkMessageCalls.push(new NetworkMessageCall(NetworkMessage_Types.TELL_CLIENT_MOVETO_SETUPATTRACT, moveToSetupAttract));
			m_NetworkMessageCalls.push(new NetworkMessageCall(NetworkMessage_Types.TELL_CLIENT_MOVETO_SESSIONATTRACT, moveToSessionAttract));
			m_NetworkMessageCalls.push(new NetworkMessageCall(NetworkMessage_Types.TELL_CLIENT_MOVETO_CHOOSEYOURCAREER_ACTIVITY, moveToChooseYourCareerActivity));
			m_NetworkMessageCalls.push(new NetworkMessageCall(NetworkMessage_Types.TELL_CLIENT_MOVETO_CHOOSEYOURCAREER_PRESENTATION, moveToSessionAttract));
			m_NetworkMessageCalls.push(new NetworkMessageCall(NetworkMessage_Types.TELL_CLIENT_MOVETO_EXPLOREYOURPASSION_ACTIVITY, moveToExploreYourPassionActivity));
			m_NetworkMessageCalls.push(new NetworkMessageCall(NetworkMessage_Types.TELL_CLIENT_MOVETO_EXPLOREYOURPASSION_DISCUSSION, moveToSessionAttract));
			m_NetworkMessageCalls.push(new NetworkMessageCall(NetworkMessage_Types.TELL_CLIENT_MOVETO_PICTUREYOURSELF_ACTIVITY, moveToPictureYourselfActivity));
			m_NetworkMessageCalls.push(new NetworkMessageCall(NetworkMessage_Types.TELL_CLIENT_MOVETO_ASSEMBLEYOURTEAM_ACTIVITY, moveToAssembleYourTeamActivity));
			m_NetworkMessageCalls.push(new NetworkMessageCall(NetworkMessage_Types.TELL_CLIENT_MOVETO_LEADERSCRIPT_01_DISCUSSION, moveToSessionAttract));
			m_NetworkMessageCalls.push(new NetworkMessageCall(NetworkMessage_Types.TELL_CLIENT_MOVETO_COMMUNITYRESOURCES_ACTIVITY, moveToCommunityResourcesActivity));
			m_NetworkMessageCalls.push(new NetworkMessageCall(NetworkMessage_Types.TELL_CLIENT_MOVETO_COMMUNITYRESOURCES_PRESENTATION, moveToSessionAttract));
			m_NetworkMessageCalls.push(new NetworkMessageCall(NetworkMessage_Types.TELL_CLIENT_MOVETO_LEADERSCRIPT_02_DISCUSSION, moveToSessionAttract));
			m_NetworkMessageCalls.push(new NetworkMessageCall(NetworkMessage_Types.TELL_CLIENT_MOVETO_BRAINSTORM_ACTIVITY, moveToBrainstormActivity));
			m_NetworkMessageCalls.push(new NetworkMessageCall(NetworkMessage_Types.TELL_CLIENT_MOVETO_BRAINSTORM_PRESENTATION, moveToSessionAttract));
			m_NetworkMessageCalls.push(new NetworkMessageCall(NetworkMessage_Types.TELL_CLIENT_MOVETO_PROJECTPLANNING_ACTIVITY, moveToProjectPlanningActivity));
			m_NetworkMessageCalls.push(new NetworkMessageCall(NetworkMessage_Types.TELL_CLIENT_MOVETO_PROJECTPLANNING_PRESENTATION, moveToSessionAttract));
			m_NetworkMessageCalls.push(new NetworkMessageCall(NetworkMessage_Types.TELL_CLIENT_MOVETO_CHOOSEISSUE_ACTIVITY, moveToChooseIssueActivity));
			m_NetworkMessageCalls.push(new NetworkMessageCall(NetworkMessage_Types.TELL_CLIENT_MOVETO_CHOOSEISSUE_DISCUSSION, moveToSessionAttract));
			m_NetworkMessageCalls.push(new NetworkMessageCall(NetworkMessage_Types.TELL_CLIENT_MOVETO_MINDMAP_ACTIVITY, moveToMindMapActivity));
			m_NetworkMessageCalls.push(new NetworkMessageCall(NetworkMessage_Types.TELL_CLIENT_MOVETO_MINDMAP_PRESENTATION, moveToSessionAttract));
			m_NetworkMessageCalls.push(new NetworkMessageCall(NetworkMessage_Types.TELL_CLIENT_MOVETO_LEADERSCRIPT_03_DISCUSSION, moveToSessionAttract));
			m_NetworkMessageCalls.push(new NetworkMessageCall(NetworkMessage_Types.TELL_CLIENT_MOVETO_KNOWYOURROLE_ACTIVITY, moveToKnowYourRoleActivity));
			m_NetworkMessageCalls.push(new NetworkMessageCall(NetworkMessage_Types.TELL_CLIENT_MOVETO_KNOWYOURROLE_PRESENTATION, moveToSessionAttract));
			m_NetworkMessageCalls.push(new NetworkMessageCall(NetworkMessage_Types.TELL_CLIENT_MOVETO_MAKEAVIDEO_ACTIVITY_ASSIGNROLES, moveToMakeAVideoAssignRoles));
			m_NetworkMessageCalls.push(new NetworkMessageCall(NetworkMessage_Types.TELL_CLIENT_MOVETO_MAKEAVIDEO_ACTIVITY_RECORD, moveToMakeAVideoRecord));
			m_NetworkMessageCalls.push(new NetworkMessageCall(NetworkMessage_Types.TELL_CLIENT_MOVETO_MAKEAPOSTER_ACTIVITY, moveToMakeAPosterActivity));
			m_NetworkMessageCalls.push(new NetworkMessageCall(NetworkMessage_Types.TELL_CLIENT_MOVETO_VIDEO, moveToSessionAttract));
			m_NetworkMessageCalls.push(new NetworkMessageCall(NetworkMessage_Types.TELL_TABLET_SYNC_TABLETINFORMATION, syncGirlsTablet));
			m_NetworkMessageCalls.push(new NetworkMessageCall(NetworkMessage_Types.TELL_CLIENT_MOVETO_WHOSHEISDISCUSSION, moveToSessionAttract));
			m_NetworkMessageCalls.push(new NetworkMessageCall(NetworkMessage_Types.TELL_CLIENT_MOVETO_WHATSHEDOESDISCUSSION, moveToSessionAttract));
			m_NetworkMessageCalls.push(new NetworkMessageCall(NetworkMessage_Types.TELL_CLIENT_MOVETO_WHATSHELOVESDISCUSSION, moveToSessionAttract));
			m_NetworkMessageCalls.push(new NetworkMessageCall(NetworkMessage_Types.TELL_CLIENT_MOVETO_GIRLVIDEOS_WATCH, moveToSessionAttract));
			m_NetworkMessageCalls.push(new NetworkMessageCall(NetworkMessage_Types.TELL_CLIENT_MOVETO_GIRLVIDEOS_LOAD, moveToSessionAttract));
			m_NetworkMessageCalls.push(new NetworkMessageCall(NetworkMessage_Types.TELL_CLIENT_MOVETO_WRAPUP, moveToWrapUp));
			m_NetworkMessageCalls.push(new NetworkMessageCall(NetworkMessage_Types.TELL_CLIENT_MOVETO_ENDSCREEN, moveToEndScreen));
			m_NetworkMessageCalls.push(new NetworkMessageCall(NetworkMessage_Types.TELL_TABLET_BEGIN_TABLETACTIVATION, beginTabletActivation));
		}
		
		private function moveToState(stateControllerID:int, stateID:int, shouldRestartState:Boolean = false, shouldForceStateChange:Boolean = false):void
		{
			StateControllerManager.LeaveAllActiveStates(stateControllerID);
			if(!StateControllerManager.IsStateControllerActive(stateControllerID) || shouldForceStateChange)
			{
				if(shouldRestartState || StateControllerManager.GetActiveAppStateID(stateControllerID) != stateID)
				{
					StateControllerManager.MoveToState(stateControllerID, stateID);
				}
			}
		}
		
		private function refreshSessionInformation(networkMessageByteArray:ByteArray):void
		{
			var trackKey:String = networkMessageByteArray.readUTF();
			var ageKey:String = networkMessageByteArray.readUTF();
			var themeKey:String = networkMessageByteArray.readUTF();
			var isVideoPlaying:Boolean = networkMessageByteArray.readBoolean();
			var sessionID:String = networkMessageByteArray.readUTF();
			var leaderUserName:String = networkMessageByteArray.readUTF();
			var leaderPassword:String = networkMessageByteArray.readUTF();
			var monitorTitle:String = networkMessageByteArray.readUTF();
			
			var tabletIDListLength:int = networkMessageByteArray.readInt();
			for(var index:int = 0; index < tabletIDListLength; index++)
			{
				var tabletID:String = networkMessageByteArray.readUTF();
				if(SessionInformation_GirlsTablet.TabletIndex == index)
				{
					SessionInformation_GirlsTablet.TeamInfo.Tablet_ID = tabletID;
				}
			}
			
			SessionInformation_GirlsTablet.Track = TrackConstants.GetTrackConstantFromTrackKey(trackKey);
			SessionInformation_GirlsTablet.Age = AgeConstants.GetAgeConstantFromAgeKey(ageKey);
			SessionInformation_GirlsTablet.Journey = JourneyConstants.GetJourneyConstantFromThemeKey(themeKey);
			SessionInformation_GirlsTablet.IsVideoPlaying = isVideoPlaying;
			SessionInformation_GirlsTablet.NotifySessionInformationUpdate();
			SessionInformation_GirlsTablet.Authentication_SessionID = sessionID;
			SessionInformation_GirlsTablet.Authentication_Leader_UserName = leaderUserName;
			SessionInformation_GirlsTablet.Authentication_Leader_Password = leaderPassword;
			
			SessionInformation_GirlsTablet.TeamInfo.ResetActiveIndex();
		}
		
		private function leave(networkMessageByteArray:ByteArray):void
		{
			moveToState(StateController_IDs.STATECONTROLLER_MAIN, StateController_StateIDs_Main.STATE_ATTRACT, true, true);
			
			// check to see if we should restart the interactive or reset the computer
			GSOC_RestartManager.CheckStatus();
		}
		
		private function restart(networkMessageByteArray:ByteArray):void
		{
			GirlsTablet_RestartManager.RestartTablet();
		}
		
		private function moveToAttract(networkMessageByteArray:ByteArray):void
		{
			refreshSessionInformation(networkMessageByteArray);
			moveToState(StateController_IDs.STATECONTROLLER_MAIN, StateController_StateIDs_Main.STATE_ATTRACT, true, true);
		}
		
		private function moveToWaitForStart(networkMessageByteArray:ByteArray):void
		{
			refreshSessionInformation(networkMessageByteArray);
			moveToState(StateController_IDs.STATECONTROLLER_MAIN, StateController_StateIDs_Main.STATE_WAITFORSTART, true, true);
		}
		
		private function moveToSetupAttract(networkMessageByteArray:ByteArray):void
		{
			refreshSessionInformation(networkMessageByteArray);
			moveToState(StateController_IDs.STATECONTROLLER_MAIN, StateController_StateIDs_Main.STATE_SETUPATTRACT, false, true);
		}
		
		private function moveToSessionAttract(networkMessageByteArray:ByteArray):void
		{
			refreshSessionInformation(networkMessageByteArray);
			moveToState(StateController_IDs.STATECONTROLLER_MAIN, StateController_StateIDs_Main.STATE_SESSIONATTRACT, false, true);
		}
		
		private function moveToChooseYourCareerActivity(networkMessageByteArray:ByteArray):void
		{
			refreshSessionInformation(networkMessageByteArray);
			moveToState(StateController_IDs.STATECONTROLLER_CHOOSEYOURCAREER, StateController_StateIDs_ChooseYourCareer.STATE_CAREERCHOICE, true, true);
		}
		
		private function moveToExploreYourPassionActivity(networkMessageByteArray:ByteArray):void
		{
			refreshSessionInformation(networkMessageByteArray);
			moveToState(StateController_IDs.STATECONTROLLER_EXPLOREYOURPASSION, StateController_StateIDs_ExploreYourPassion.STATE_MULTIPLEQUESTIONS, true, true);
		}
		
		private function moveToPictureYourselfActivity(networkMessageByteArray:ByteArray):void
		{
			refreshSessionInformation(networkMessageByteArray);
			moveToState(StateController_IDs.STATECONTROLLER_PICTUREYOURSELF, StateController_StateIDs_PictureYourself.STATE_NAMESELECTION, true, true);
		}
		
		private function moveToAssembleYourTeamActivity(networkMessageByteArray:ByteArray):void
		{
			refreshSessionInformation(networkMessageByteArray);
			moveToState(StateController_IDs.STATECONTROLLER_ASSEMBLEYOURTEAM, StateController_StateIDs_PictureYourself.STATE_NAMESELECTION, true, true);
		}
		
		private function moveToCommunityResourcesActivity(networkMessageByteArray:ByteArray):void
		{
			refreshSessionInformation(networkMessageByteArray);
			moveToState(StateController_IDs.STATECONTROLLER_COMMUNITYRESOURCES, StateController_StateIDs_CommunityResources.STATE_COMMUNITYRESOURCES, true, true);
		}
		
		private function moveToBrainstormActivity(networkMessageByteArray:ByteArray):void
		{
			refreshSessionInformation(networkMessageByteArray);
			moveToState(StateController_IDs.STATECONTROLLER_BRAINSTORM, StateController_StateIDs_BrainStorm.STATE_PROJECTGOALS, true, true);
		}
		
		private function moveToProjectPlanningActivity(networkMessageByteArray:ByteArray):void
		{
			refreshSessionInformation(networkMessageByteArray);
			moveToState(StateController_IDs.STATECONTROLLER_PROJECTPLANNING, StateController_StateIDs_ProjectPlanning.STATE_MAIN, true, true);
		}
		
		private function moveToChooseIssueActivity(networkMessageByteArray:ByteArray):void
		{
			refreshSessionInformation(networkMessageByteArray);
			moveToState(StateController_IDs.STATECONTROLLER_CHOOSEISSUE, StateController_StateIDs_ChooseIssue.STATE_CHOOSEISSUE, true, true);
		}
		
		private function moveToMindMapActivity(networkMessageByteArray:ByteArray):void
		{
			refreshSessionInformation(networkMessageByteArray);
			moveToState(StateController_IDs.STATECONTROLLER_MINDMAP, StateController_StateIDs_MindMap.STATE_MINDMAP, true, true);
		}
		
		private function moveToKnowYourRoleActivity(networkMessageByteArray:ByteArray):void
		{
			refreshSessionInformation(networkMessageByteArray);
			moveToState(StateController_IDs.STATECONTROLLER_KNOWYOURROLE, StateController_StateIDs_KnowYourRole.STATE_CHOICEWEB, true, true);
		}
		
		private function moveToMakeAVideoAssignRoles(networkMessageByteArray:ByteArray):void
		{
			refreshSessionInformation(networkMessageByteArray);
			moveToState(StateController_IDs.STATECONTROLLER_RECORDVIDEO, StateController_StateIDs_RecordVideo.STATE_ASSIGNROLES, true, true);
		}
		
		private function moveToMakeAVideoRecord(networkMessageByteArray:ByteArray):void
		{
			refreshSessionInformation(networkMessageByteArray);
			moveToState(StateController_IDs.STATECONTROLLER_RECORDVIDEO, StateController_StateIDs_RecordVideo.STATE_RECORDVIDEO, true, true);
		}
		
		private function moveToMakeAPosterActivity(networkMessageByteArray:ByteArray):void
		{
			refreshSessionInformation(networkMessageByteArray);
			moveToState(StateController_IDs.STATECONTROLLER_POSTER, StateController_StateIDs_Poster.STATE_POSTER, true, true);
		}
		
		private function moveToWrapUp(networkMessageByteArray:ByteArray):void
		{
			refreshSessionInformation(networkMessageByteArray);
			moveToState(StateController_IDs.STATECONTROLLER_MAIN, StateController_StateIDs_Main.STATE_SESSIONATTRACT, true, true);
		}
		
		private function moveToEndScreen(networkMessageByteArray:ByteArray):void
		{
			refreshSessionInformation(networkMessageByteArray);
			moveToState(StateController_IDs.STATECONTROLLER_MAIN, StateController_StateIDs_Main.STATE_END, true, true);
		}
		
		private function syncGirlsTablet(networkMessageByteArray:ByteArray):void
		{
			refreshSessionInformation(networkMessageByteArray);
			SessionInformation_GirlsTablet.SyncGirlsTablet(networkMessageByteArray);
		}
		
		private function beginTabletActivation(networkMessageByteArray:ByteArray):void
		{
			moveToState(StateController_IDs.STATECONTROLLER_MAIN, StateController_StateIDs_Main.STATE_WAITFORACTIVATION, true, true);
			
			if(SessionInformation_GirlsTablet.Authentication_SessionID.length == 0)
			{
				// we're not in a session... tell tablet we're good for now
				dispatchActivationCompleteEvent();
			}
			// Begin Tablet Activation
			else if(SessionInformation_GirlsTablet.CheckDefaultUserData())
			{
				// the team already exists... download the files that were uploaded and then join the session when it is done
				var dm:DownloadManager = new DownloadManager();
				dm.addEventListener(DownloadManager.EVENT_DOWNLOAD_COMPLETE, eh_DownloadComplete);
				dm.BeginDownload();
			}
			else
			{
				// need to create a default scout and team data
				ScoutManager.ScoutManagerEventDispatcher.addEventListener(ScoutManager.EVENT_SCOUTS_CREATE_COMPLETE, eh_ScoutsCreateComplete, false, -100);
				SessionInformation_GirlsTablet.CreateDefaultUserDataAsync();
			}
		}
		
		private function eh_ScoutsCreateComplete(evt:Event):void
		{
			SessionInformation_GirlsTablet.UploadCurrentInformation();
			dispatchActivationCompleteEvent();
		}
		
		private function eh_DownloadComplete(evt:Event):void
		{
			dispatchActivationCompleteEvent();
		}
		
		private function dispatchActivationCompleteEvent():void
		{
			Client.ClientEventDispatcher.dispatchEvent(new Event_ClientNetworkMessage(Event_ClientNetworkMessage.EVENT_SEND_NETWORK_MESSAGE, NetworkMessage_Types.TELL_SERVER_FINISH_TABLETACTIVATION));
		}
	}
}
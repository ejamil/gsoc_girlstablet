package com.bpi.gsoc.girlstablet.global_data.download
{
	import com.bpi.citrusas.citrus.api.files.BPINetFileInformation;
	import com.bpi.citrusas.citrus.controllers.user.UserDataMethods;
	import com.bpi.citrusas.citrus.controllers.user.events.Event_UserDataController;
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.citrusas.utils.Utils_Image;
	import com.bpi.gsoc.framework.constants.Keys_FileNames;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.data.userdata.GSOC_UserData_Girl_ImageKeys;
	import com.bpi.gsoc.framework.data.userdata.GSOC_UserData_Team_ImageKeys;
	
	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.net.URLRequest;
	import flash.utils.describeType;

	public class DownloadManager extends EventDispatcher
	{
		public static const EVENT_DOWNLOAD_COMPLETE:String = "com.bpi.gsoc.girlstablet.global_data.download.DownloadManager.EVENT_DOWNLOAD_COMPLETE";
		private static const SCOUT_DOWNLOAD_MANAGER_EVENT_KEY:String = "SCOUT_DOWNLOAD_MANAGER_EVENT_KEY";
		private static const ms_LifeLink:int = 60*60;
		private static var ms_TeamImageKeys:Vector.<String>;
		private static var ms_ScoutImageKeys:Vector.<String>;
		
		private var m_DownloadStarted:Boolean = false;
		private var m_AuthorizationToken:String = "";
		private var m_FileList:Array = new Array();
		private var m_Loader:Loader = new Loader();
		private var m_CurrentFile:Object = new Object();
		private var m_CurrentScoutIndex:int = -1;
		
		public function DownloadManager():void
		{
			ms_TeamImageKeys = new Vector.<String>();
			
			var desc:XML = describeType(GSOC_UserData_Team_ImageKeys);
			
			for each (var constName:XML in desc.constant)
			{
				ms_TeamImageKeys.push(GSOC_UserData_Team_ImageKeys[constName.@name]);
			}
			
			ms_ScoutImageKeys = new Vector.<String>();
			
			desc = describeType(GSOC_UserData_Girl_ImageKeys);
			
			for each (constName in desc.constant)
			{
				ms_ScoutImageKeys.push(GSOC_UserData_Girl_ImageKeys[constName.@name]);
			}
		}
		
		public function BeginDownload():void
		{
			if(m_DownloadStarted)
			{
				trace("already downloaded, can't be done again, create a new download manager to run again");
			}
			else
			{
				m_DownloadStarted = true;
				login();
			}
		}
		
		private function login():void
		{
			UserDataMethods.userDataMethodEventDispatcher.addEventListener(Event_UserDataController.USER_TOKEN_RETRIEVED_COMPLETE, eh_LoginComplete);
			UserDataMethods.userDataMethodEventDispatcher.addEventListener(Event_UserDataController.USER_TOKEN_RETRIEVED_ERROR, eh_LoginFailed);
			UserDataMethods.GetUserAuthorizationToken(CitrusFramework.serverURL, SessionInformation_GirlsTablet.Authentication_Leader_UserName, SessionInformation_GirlsTablet.Authentication_Leader_Password, SCOUT_DOWNLOAD_MANAGER_EVENT_KEY);
		}
		
		private function eh_LoginComplete(event:Event_UserDataController):void
		{
			if(event.eventKey == SCOUT_DOWNLOAD_MANAGER_EVENT_KEY)
			{
				UserDataMethods.userDataMethodEventDispatcher.removeEventListener(Event_UserDataController.USER_TOKEN_RETRIEVED_COMPLETE, eh_LoginComplete);
				UserDataMethods.userDataMethodEventDispatcher.removeEventListener(Event_UserDataController.USER_TOKEN_RETRIEVED_ERROR, eh_LoginFailed);
				
				m_AuthorizationToken = event.eventData["returnedData"]["token"];
				
				GetTeamFiles();
			}
		}
		
		private function eh_LoginFailed(event:Event_UserDataController):void
		{
			if(event.eventKey == SCOUT_DOWNLOAD_MANAGER_EVENT_KEY)
			{
				UserDataMethods.userDataMethodEventDispatcher.removeEventListener(Event_UserDataController.USER_TOKEN_RETRIEVED_COMPLETE, eh_LoginComplete);
				UserDataMethods.userDataMethodEventDispatcher.removeEventListener(Event_UserDataController.USER_TOKEN_RETRIEVED_ERROR, eh_LoginFailed);
				
				// couldn't log in... so dispatch the event that we're done
				this.dispatchEvent(new Event(EVENT_DOWNLOAD_COMPLETE));
			}
		}
		
		private function GetTeamFiles():void
		{
			UserDataMethods.userDataMethodEventDispatcher.addEventListener(Event_UserDataController.USER_FILE_RETRIEVE_COMPLETE, eh_FilesRetrieved);
			UserDataMethods.userDataMethodEventDispatcher.addEventListener(Event_UserDataController.USER_FILE_RETRIEVE_ERROR, eh_FilesFailed);
			UserDataMethods.ListFiles(CitrusFramework.serverURL, m_AuthorizationToken, SessionInformation_GirlsTablet.Authentication_SessionID, SessionInformation_GirlsTablet.TeamInfo.Tablet_ID + "/", false, ms_LifeLink, SCOUT_DOWNLOAD_MANAGER_EVENT_KEY);
		}
		
		private function GetScoutFiles():void
		{
			m_CurrentScoutIndex++;
			
			if(m_CurrentScoutIndex < SessionInformation_GirlsTablet.TeamInfo.GetTeamMemberCount())
			{
				UserDataMethods.userDataMethodEventDispatcher.addEventListener(Event_UserDataController.USER_FILE_RETRIEVE_COMPLETE, eh_FilesRetrieved);
				UserDataMethods.userDataMethodEventDispatcher.addEventListener(Event_UserDataController.USER_FILE_RETRIEVE_ERROR, eh_FilesFailed);
				UserDataMethods.ListFiles(CitrusFramework.serverURL, m_AuthorizationToken, SessionInformation_GirlsTablet.Authentication_SessionID, SessionInformation_GirlsTablet.TeamInfo.Tablet_ID + "/" + SessionInformation_GirlsTablet.TeamInfo.GetGirlInformationFromIndex(m_CurrentScoutIndex).UID + "/", false, ms_LifeLink, SCOUT_DOWNLOAD_MANAGER_EVENT_KEY);
			}
			else
			{
				// we've gotten all the scouts for the team, we're done
				this.dispatchEvent(new Event(EVENT_DOWNLOAD_COMPLETE));
			}
		}
		
		private function eh_FilesRetrieved(event:Event_UserDataController):void
		{
			if(event.eventKey == SCOUT_DOWNLOAD_MANAGER_EVENT_KEY)
			{
				UserDataMethods.userDataMethodEventDispatcher.removeEventListener(Event_UserDataController.USER_FILE_RETRIEVE_COMPLETE, eh_FilesRetrieved);
				UserDataMethods.userDataMethodEventDispatcher.removeEventListener(Event_UserDataController.USER_FILE_RETRIEVE_ERROR, eh_FilesFailed);
				
				m_FileList = event.eventData["returnedData"];
				ParseNextFile();
			}
		}
		
		private function eh_FilesFailed(event:Event_UserDataController):void
		{
			if(event.eventKey == SCOUT_DOWNLOAD_MANAGER_EVENT_KEY)
			{
				UserDataMethods.userDataMethodEventDispatcher.removeEventListener(Event_UserDataController.USER_FILE_RETRIEVE_COMPLETE, eh_FilesRetrieved);
				UserDataMethods.userDataMethodEventDispatcher.removeEventListener(Event_UserDataController.USER_FILE_RETRIEVE_ERROR, eh_FilesFailed);
				
				// couldn't get the files... there probably aren't any files, parse the scout files next
				GetScoutFiles();
			}
		}
		
		private function ParseNextFile():void
		{
			if(m_FileList.length > 0)
			{
				m_CurrentFile = m_FileList.shift();
				
				// download the file
				loadFile(BPINetFileInformation.GetFileURL(m_CurrentFile));
			}
			else
			{
				// we've gotten and saved all the team files... now get and save all scout files
				GetScoutFiles();
			}
		}
		
		private function loadFile(url:String):void
		{
			var urlRequest:URLRequest = new URLRequest(url);
			m_Loader = new Loader();
			addLoadFileEventListeners(m_Loader);
			m_Loader.load(urlRequest);
		}
		
		private function addLoadFileEventListeners(loader:Loader):void
		{
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, eh_FileLoadCompleteHandler);
			loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, eh_FileLoadErrorHandler);
		}
		
		private function removeLoadFileEventListeners(loader:Loader):void
		{
			loader.contentLoaderInfo.removeEventListener(Event.COMPLETE, eh_FileLoadCompleteHandler);
			loader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, eh_FileLoadErrorHandler);
		}
		
		private function eh_FileLoadCompleteHandler(event:Event):void
		{
			var loader:Loader = (event.target as LoaderInfo).loader;
			fileLoadComplete(true, loader);
		}
		
		private function eh_FileLoadErrorHandler(event:IOErrorEvent):void
		{
			var loader:Loader = (event.target as LoaderInfo).loader;
			fileLoadComplete(false, loader);
		}
		
		private function fileLoadComplete(isSuccessful:Boolean, loader:Loader):void
		{
			removeLoadFileEventListeners(loader);
			if(isSuccessful)
			{
				var bitmap:Bitmap = Utils_Image.ConvertDisplayObjectToBitmap(m_Loader);
				var s:Sprite = new Sprite();
				s.addChild(bitmap);
				
				SaveImageToKey(s);
				
				m_Loader.unload();
			}
			ParseNextFile();
		}
		
		private function SaveImageToKey(image:Sprite):void
		{
			// go through all the image keys for the team and see if the file name matches
			for(var index:int = 0; index < ms_TeamImageKeys.length; index++)
			{
				if(BPINetFileInformation.GetFileKey(m_CurrentFile).indexOf(Keys_FileNames[ms_TeamImageKeys[index]]) != -1)
				{
					SessionInformation_GirlsTablet.TeamInfo.Team_UserData.SetImage(ms_TeamImageKeys[index], image);
					return;
				}
			}
			
			// go through all the image keys for the scout and see if the file name matches
			for(index = 0; index < ms_ScoutImageKeys.length; index++)
			{
				if(BPINetFileInformation.GetFileKey(m_CurrentFile).indexOf(Keys_FileNames[ms_ScoutImageKeys[index]]) != -1)
				{
					SessionInformation_GirlsTablet.TeamInfo.GetGirlInformationFromIndex(m_CurrentScoutIndex).GirlUserData.SetImage(ms_ScoutImageKeys[index], image);
					return;
				}
			}
		}
	}
}
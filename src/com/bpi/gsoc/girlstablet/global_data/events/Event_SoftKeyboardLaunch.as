package com.bpi.gsoc.girlstablet.global_data.events
{
	import flash.events.Event;
	
	public class Event_SoftKeyboardLaunch extends Event
	{
		public static const KEYBOARD_LAUNCHED:String = "SOFT KEYBOARD LAUNCHED";
		
		public function Event_SoftKeyboardLaunch(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
	}
}
package com.bpi.gsoc.girlstablet.global_data.events
{
	import flash.events.Event;
	
	public class Event_UploadDefaultData extends Event
	{
		public static const EVENT_UPLOAD_DEFAULT_MINDMAP_ISSUE:String = "EVENT_UPLOAD_MINDMAP_ISSUE";
		public static const EVENT_UPLOAD_DEFAULT_MINDMAP_ROOTCAUSE:String = "EVENT_UPLOAD_MINDMAP_ROOTCAUSE";
		
		private var m_ShouldForceUpload:Boolean;
		
		public function Event_UploadDefaultData(type:String, shouldForceUpload:Boolean = false)
		{
			super(type, false, false);
			
			m_ShouldForceUpload = shouldForceUpload;
		}
		
		public function get E_ShouldForceUpload():Boolean
		{
			return m_ShouldForceUpload;
		}
	}
}
package com.bpi.gsoc.girlstablet.global_data
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.greensock.TweenLite;
	
	import flash.display.MovieClip;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	
	public class SoftKeyboardInput extends MovieClip
	{
		private static var m_Instance:SoftKeyboardInput;
		private static var m_TextBox:TextField;
		
		public function SoftKeyboardInput()
		{
			super();
			
			if(m_Instance)
			{
				throw new Error("SoftKeyboardInput is a singleton. Use getInstance().");
			}
			m_Instance = this;
			
			m_TextBox = new TextField();
			m_TextBox.border = true;
			m_TextBox.background = true;
			m_TextBox.needsSoftKeyboard = true;
			m_TextBox.type = TextFieldType.INPUT;
			m_TextBox.x = CitrusFramework.frameworkStage.width / 2;
			m_TextBox.y = CitrusFramework.frameworkStage.height / 2;
		}
		
		public function LaunchWindow():void
		{
			this.graphics.clear();
			this.graphics.beginFill(0x000000, 0.7);
			this.graphics.drawRect(0, 0, CitrusFramework.frameworkStageSize.x, CitrusFramework.frameworkStageSize.y);
			this.graphics.endFill();
			this.alpha = 0;
			
			this.addChild(m_TextBox);
			
			TweenLite.to(this, 0.5, {alpha:1, onComplete:this.activateTextField});
		}
		
		public function ExitWindow(onCompleteFunction:Function):void
		{
			this.deactivateTextField();
			TweenLite.to(this, 0.5, {alpha:0, onComplete:onCompleteFunction});
		}
		
		private function activateTextField():void
		{
			trace("Tween finished!");
		}
		
		private function deactivateTextField():void
		{
			trace('Text field deactivated');
		}
		public static function getInstance():SoftKeyboardInput
		{
			if(!m_Instance)
			{
				new SoftKeyboardInput();
			}
			return m_Instance;
		}
	}
}
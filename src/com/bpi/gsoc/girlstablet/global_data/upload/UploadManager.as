package com.bpi.gsoc.girlstablet.global_data.upload
{
	import com.bpi.citrusas.citrus.api.enums.BPINetContentTypes;
	import com.bpi.citrusas.citrus.controllers.user.UserDataMethods;
	import com.bpi.citrusas.citrus.controllers.user.events.Event_UserDataController;
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.citrusas.utils.Utils_Image;
	import com.bpi.gsoc.framework.constants.Keys_FileNames;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.utils.ByteArray;

	public class UploadManager
	{
		private static const EVENT_UPLOADMANAGER_LOGIN:String = "EVENT_UPLOADMANAGER_LOGIN";
		private static const EVENT_UPLOADMANAGER_UPLOAD:String = "EVENT_UPLOADMANAGER_UPLOAD";
		
		private static var m_FileInformationList:Vector.<UploadManager_FileInformation> = new Vector.<UploadManager_FileInformation>();
		
		private static var m_AuthorizationToken:String = "";
		
		private static var m_State:int = UploadManager_State.STATE_IDLE;
		
		public function UploadManager()
		{
		}
		
		private static function reset():void
		{
			m_FileInformationList = new Vector.<UploadManager_FileInformation>();
			m_AuthorizationToken = "";
			m_State = UploadManager_State.STATE_IDLE;
		}
		
		private static function login():void
		{
			if(m_State == UploadManager_State.STATE_IDLE)
			{
				m_State = UploadManager_State.STATE_LOGIN;
				
				UserDataMethods.GetUserAuthorizationToken(CitrusFramework.serverURL, SessionInformation_GirlsTablet.Authentication_Leader_UserName, SessionInformation_GirlsTablet.Authentication_Leader_Password, EVENT_UPLOADMANAGER_LOGIN);
				UserDataMethods.userDataMethodEventDispatcher.addEventListener(Event_UserDataController.USER_TOKEN_RETRIEVED_COMPLETE, eh_LoginComplete);
				UserDataMethods.userDataMethodEventDispatcher.addEventListener(Event_UserDataController.USER_TOKEN_RETRIEVED_ERROR, eh_LoginFailed);
			}
		}
		
		private static function eh_LoginComplete(evt:Event_UserDataController):void
		{
			if(evt.eventKey == EVENT_UPLOADMANAGER_LOGIN)
			{
				m_AuthorizationToken = evt.eventData["returnedData"]["token"];
				
				uploadNext();
			}
		}
		
		private static function eh_LoginFailed(evt:Event_UserDataController):void
		{
			if(evt.eventKey == EVENT_UPLOADMANAGER_LOGIN)
			{
				reset();
			}
		}
		
		private static function uploadNext():void
		{
			m_State = UploadManager_State.STATE_UPLOADNEXT;
			
			checkUpload();
		}
		
		private static function checkUpload():void
		{
			if(m_FileInformationList.length > 0)
			{
				if(m_State == UploadManager_State.STATE_UPLOADNEXT)
				{		
					var fileInformation:UploadManager_FileInformation = m_FileInformationList.shift();
					uploadFile(fileInformation);
				}
			}
			else
			{
				reset();
			}
		}
		
		private static function uploadFile(fileInformation:UploadManager_FileInformation):void
		{
			m_State = UploadManager_State.STATE_UPLOADING;
			
			addUploadEventListeners();
			var teamName:String = "unknown";
			switch(SessionInformation_GirlsTablet.TabletIndex)
			{
				case 0:
					teamName = "purple";
					break;
				case 1:
					teamName = "turquoise";
					break;
				case 2: 
					teamName = "blue";
					break;
				case 3:
					teamName = "yellow";
					break;
			}
			
			var metadata:Object = new Object();
			metadata["team_color"] = teamName;
			UserDataMethods.UploadFileData(CitrusFramework.serverURL, m_AuthorizationToken, SessionInformation_GirlsTablet.Authentication_SessionID, fileInformation.File_ByteArray, fileInformation.File_ServerPath, BPINetContentTypes.IMAGE_PNG, metadata, EVENT_UPLOADMANAGER_UPLOAD);
		}
		
		private static function eh_UploadComplete(evt:Event_UserDataController):void
		{
			if(evt.eventKey == EVENT_UPLOADMANAGER_UPLOAD)
			{
				uploadComplete(true);
			}
		}
		
		private static function eh_UploadFailed(evt:Event_UserDataController):void
		{
			if(evt.eventKey == EVENT_UPLOADMANAGER_UPLOAD)
			{
				uploadComplete(false);
			}
		}
		
		private static function uploadComplete(isSuccessful:Boolean):void
		{
			removeUploadEventListeners();
			uploadNext();
		}
		
		private static function addUploadEventListeners():void
		{
			UserDataMethods.userDataMethodEventDispatcher.addEventListener(Event_UserDataController.USER_FILE_UPLOAD_COMPLETE, eh_UploadComplete);
			UserDataMethods.userDataMethodEventDispatcher.addEventListener(Event_UserDataController.USER_FILE_UPLOAD_ERROR, eh_UploadFailed);
		}
		
		private static function removeUploadEventListeners():void
		{
			UserDataMethods.userDataMethodEventDispatcher.removeEventListener(Event_UserDataController.USER_FILE_UPLOAD_COMPLETE, eh_UploadComplete);
			UserDataMethods.userDataMethodEventDispatcher.removeEventListener(Event_UserDataController.USER_FILE_UPLOAD_ERROR, eh_UploadFailed);
		}
		
		private static function getServerPath(fileKey:String, scoutID:String = ""):String
		{
			var tabletID:String = SessionInformation_GirlsTablet.TeamInfo.Tablet_ID;
			return Keys_FileNames.GetServerPath(tabletID, fileKey, scoutID);
		}
		
		private static function removeDuplicateUploads(serverPath:String):void
		{
			for(var index:int = 0; index < m_FileInformationList.length; index++)
			{
				if(m_FileInformationList[index].File_ServerPath == serverPath)
				{
					m_FileInformationList.splice(index, 1);
					index--;
				}
			}
		}
		
		private static function uploadBitmap(bitmap:Bitmap, fileKey:String, scoutID:String = ""):void
		{			
			var serverPath:String = getServerPath(fileKey, scoutID);
			removeDuplicateUploads(serverPath);
			
			PNGEncoder2.level = CompressionLevel.FAST;
			var encoder:PNGEncoder2 = PNGEncoder2.encodeAsync(bitmap.bitmapData);
			encoder.targetFPS = 30;
			encoder.addEventListener(Event.COMPLETE, function(evt):void
			{
				var byteArray:ByteArray = encoder.png;
				m_FileInformationList.push(new UploadManager_FileInformation(byteArray, serverPath));
				
				login();
				
				evt.currentTarget.removeEventListener(evt.type, arguments.callee);
			});
		}
		
		public static function UploadBitmap(bitmap:Bitmap, fileKey:String, scoutID:String = ""):void
		{
			uploadBitmap(bitmap, fileKey, scoutID);
		}
		
		public static function UploadDisplayObject(displayObject:DisplayObject, fileKey:String, scoutID:String = ""):void
		{
			var bitmap:Bitmap = Utils_Image.ConvertDisplayObjectToBitmap(displayObject);
			uploadBitmap(bitmap, fileKey, scoutID);
		}
	}
}
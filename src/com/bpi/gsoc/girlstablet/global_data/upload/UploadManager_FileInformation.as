package com.bpi.gsoc.girlstablet.global_data.upload
{
	import flash.utils.ByteArray;

	public class UploadManager_FileInformation
	{
		private var m_File_ByteArray:ByteArray;
		private var m_File_ServerPath:String;
		
		public function UploadManager_FileInformation(byteArray:ByteArray, serverPath:String)
		{
			m_File_ByteArray = byteArray;
			m_File_ServerPath = serverPath;
		}
		
		public function get File_ByteArray():ByteArray
		{
			return m_File_ByteArray;
		}
		
		public function get File_ServerPath():String
		{
			return m_File_ServerPath;
		}
	}
}
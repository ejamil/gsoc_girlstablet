package com.bpi.gsoc.girlstablet.global_data.upload
{
	public class UploadManager_State
	{
		private static var ENUM_VALUE:int = 0;
		public static const STATE_IDLE:int = ENUM_VALUE++;
		public static const STATE_LOGIN:int = ENUM_VALUE++;
		public static const STATE_UPLOADNEXT:int = ENUM_VALUE++;
		public static const STATE_UPLOADING:int = ENUM_VALUE++;
	}
}
package com.bpi.gsoc.girlstablet.global_data
{
	import flash.display.MovieClip;
	import com.bpi.gsoc.framework.constants.AgeConstants;

	public class FrameControlUtils
	{
		public static function GoToAgeGraphics(target:MovieClip, age:String):void
		{
			if(!(target is MovieClip))
			{
				return;
			}

			switch(age)
			{
				case AgeConstants.KEY_DAISYBROWNIE:
					target.gotoAndStop('DB');
					break;
				case AgeConstants.KEY_JUNIORCADETTE:
					target.gotoAndStop('JC');
					break;
				case AgeConstants.KEY_SENIORAMBASSADOR:
					target.gotoAndStop('SA');
					break;
				default:
					target.gotoAndStop(0);
			}
			
			if(target.numChildren == 0)
			{
				return;
			}
			
			for(var i:int = 0; i < target.numChildren; i++)
			{
				FrameControlUtils.GoToAgeGraphics(target.getChildAt(i) as MovieClip, age);
			}
		}
		
		public static function StopAtFrameN(target:MovieClip, stopFrame:int = 0):void
		{
			if(!(target is MovieClip) || (stopFrame > target.totalFrames - 1) || (stopFrame < 0))
			{
				return;
			}
			
			target.gotoAndStop(stopFrame);
			
			if(target.numChildren == 0)
			{
				return;
			}

			for(var i:int = 0; i < target.totalFrames; i++)
			{
				for(var j:int = 0; j < target.numChildren; j++)
				{
					FrameControlUtils.StopAtFrameN(target.getChildAt(j) as MovieClip);
				}
			}
		}
	}
}
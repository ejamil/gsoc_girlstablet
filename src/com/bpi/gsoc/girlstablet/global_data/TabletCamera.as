package com.bpi.gsoc.girlstablet.global_data
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	
	import flash.geom.Point;
	import flash.media.Camera;
	import flash.media.Video;

	public class TabletCamera
	{
		private static var _ms_FrontCamera:Camera;
		private static var _ms_RearCamera:Camera;
		private static var ms_FrontVideo:Video;
		private static var ms_RearVideo:Video;
		private static var m_CameraAvailable:Boolean;
		private static var ms_FrontCamIsInitialized:Boolean;
		private static var ms_RearCamIsInitialized:Boolean;
		
		public static const FRONT_CAMERA_DIMS:Point = new Point(1920, 1080);
		public static const REAR_CAMERA_DIMS:Point = new Point(1920, 1080);
		
		public static const FRONT_CAM:int = 1;
		public static const REAR_CAM:int = 0;
		
		public static function Init():void
		{
			m_CameraAvailable = (Camera.names.length > 0);
			if(m_CameraAvailable)
			{	
				initFrontCamera();
				initRearCamera();
			}
			else
			{
				CitrusFramework.localLog.LogEvent("No system cameras were found.");
				ms_FrontCamIsInitialized = false;
				ms_RearCamIsInitialized = false;
			}
		}
		
		public static function DeInit():void
		{
			deInitFrontCamera();
			deInitRearCamera();
			m_CameraAvailable = false;
		}
		
		public static function GetFrontVideo():Video
		{
			if(ms_FrontVideo)
			{
				return ms_FrontVideo;
			}
			else
			{
				var msg:String = "The front camera does not exist. Returning rear camera instead.";
				CitrusFramework.localLog.LogEvent(msg);
				trace(msg);
				return ms_RearVideo;
			}
		}
		
		public static function GetRearVideo():Video
		{
			return ms_RearVideo;
		}
		
		public static function HasFrontVideo():Boolean
		{
			return ms_FrontVideo ? true : false;
		}
		
		private static function initFrontCamera():void
		{
			if(!ms_FrontCamIsInitialized)
			{
				// if the front camera exists, get it (assuming it's the second camera in the list)
				if(Camera.names.length > 1)
				{
					_ms_FrontCamera = Camera.getCamera(FRONT_CAM.toString());
				}
				
				if(_ms_FrontCamera)
				{
					_ms_FrontCamera.setMode(FRONT_CAMERA_DIMS.x, FRONT_CAMERA_DIMS.y, 30);
					ms_FrontVideo = new Video(FRONT_CAMERA_DIMS.x, FRONT_CAMERA_DIMS.y);
					ms_FrontVideo.attachCamera(_ms_FrontCamera);
					ms_FrontVideo.smoothing = true;
				}
				ms_FrontCamIsInitialized = true;
			}
		}
		
		private static function initRearCamera():void
		{
			if(!ms_RearCamIsInitialized)
			{
				// get the rear facing camera first - it's the zero index
				_ms_RearCamera = Camera.getCamera(REAR_CAM.toString());
				
				if(_ms_RearCamera)
				{
					_ms_RearCamera.setMode(REAR_CAMERA_DIMS.x, REAR_CAMERA_DIMS.y, 30);
					ms_RearVideo = new Video(REAR_CAMERA_DIMS.x, REAR_CAMERA_DIMS.y);
					ms_RearVideo.attachCamera(_ms_RearCamera);
					ms_RearVideo.smoothing = true;
				}
				ms_RearCamIsInitialized = true;
			}
		}
		
		private static function deInitFrontCamera():void
		{
			if(ms_FrontVideo) ms_FrontVideo.attachCamera(null);
			ms_FrontVideo = null;
			_ms_FrontCamera = null;
			ms_FrontCamIsInitialized = false;
			
		}
		
		private static function deInitRearCamera():void
		{
			if(ms_RearVideo) ms_RearVideo.attachCamera(null);
			ms_RearVideo = null;
			_ms_RearCamera = null;
			ms_RearCamIsInitialized = false;
		}
		
		/**
		 * Returns true if at least one system camera is visible, false otherwise.
		 */
		public static function IsCameraAvailable():Boolean
		{
			return m_CameraAvailable;
		}
	}
}
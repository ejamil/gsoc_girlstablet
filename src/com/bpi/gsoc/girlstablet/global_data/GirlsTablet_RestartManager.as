package com.bpi.gsoc.girlstablet.global_data
{
	import com.bpi.citrusas.batchfiles.BatchFileOperations;

	public final class GirlsTablet_RestartManager
	{
		public static function RestartTablet():void
		{
			BatchFileOperations.RunBatchFile("data/","restart.bat");
		}
	}
}
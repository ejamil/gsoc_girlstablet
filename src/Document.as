﻿package
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.citrusas.console.Console;
	import com.bpi.citrusas.networking.client.Client;
	import com.bpi.citrusas.networking.events.Event_ClientNetworkMessage;
	import com.bpi.citrusas.states.StateControllerManager;
	import com.bpi.gsoc.framework.GSOCFramework_Client;
	import com.bpi.gsoc.framework.camera.CameraNotifications;
	import com.bpi.gsoc.framework.constants.AgeConstants;
	import com.bpi.gsoc.framework.constants.JourneyConstants;
	import com.bpi.gsoc.framework.constants.TrackConstants;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.networking.NetworkMessage_Types;
	import com.bpi.gsoc.framework.sfx.GSOC_SFX;
	import com.bpi.gsoc.girlstablet.TempStateConfig;
	import com.bpi.gsoc.girlstablet.networking.NetworkMessageManager_GirlsTablet;
	import com.bpi.gsoc.girlstablet.states.attract.AppState_Attract;
	import com.bpi.gsoc.girlstablet.states.cms.AppState_CMS;
	import com.bpi.gsoc.girlstablet.states.end.AppState_End;
	import com.bpi.gsoc.girlstablet.states.gsoc_01pictureyourself.choose_team_name.ChooseTeamName_State;
	import com.bpi.gsoc.girlstablet.states.gsoc_01pictureyourself.customize_character.CustomizeCharacterTA_State;
	import com.bpi.gsoc.girlstablet.states.gsoc_01pictureyourself.customize_character.CustomizeCharacter_State;
	import com.bpi.gsoc.girlstablet.states.gsoc_01pictureyourself.loading.Loading_State;
	import com.bpi.gsoc.girlstablet.states.gsoc_01pictureyourself.name_selection.NameSelection_State;
	import com.bpi.gsoc.girlstablet.states.gsoc_01pictureyourself.take_picture.TakePicture_State;
	import com.bpi.gsoc.girlstablet.states.gsoc_02exploreyourpassion.multiple_questions.MultipleQuestions_State;
	import com.bpi.gsoc.girlstablet.states.gsoc_02exploreyourpassion.questions_done.QuestionsDone_State;
	import com.bpi.gsoc.girlstablet.states.gsoc_03chooseyourcareer.career_choice.CareerChoice_State;
	import com.bpi.gsoc.girlstablet.states.gsoc_03chooseyourcareer.career_done.CareerDone_State;
	import com.bpi.gsoc.girlstablet.states.gsoc_04knowyourrole.choice_web.ChoiceWeb_State;
	import com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.BrainStorm_State;
	import com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.project_goals.ProjectGoals_State;
	import com.bpi.gsoc.girlstablet.states.gsoc_06projectplanning.ProjectPlanning_State;
	import com.bpi.gsoc.girlstablet.states.gsoc_07communityresources.CommunityResources_State;
	import com.bpi.gsoc.girlstablet.states.gsoc_08chooseanissue.ChooseIssue_State;
	import com.bpi.gsoc.girlstablet.states.gsoc_09mindmap.MindMap_State;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.assignroles.AssignRoles_State;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.EditVideo_State;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.playbackvideo.PlayBackVideo_State;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.recordvideo.RecordVideo_State;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.reviewvideo.ReviewVideo_State;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.warmup.WarmUp_State;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.writescript.WriteScript_State;
	import com.bpi.gsoc.girlstablet.states.gsoc_11poster.Poster_State;
	import com.bpi.gsoc.girlstablet.states.networkconnection.AppState_NetworkConnection;
	import com.bpi.gsoc.girlstablet.states.networkconnection.tabletcolorselection.events.Event_TabletColor;
	import com.bpi.gsoc.girlstablet.states.sessionattract.AppState_SessionAttract;
	import com.bpi.gsoc.girlstablet.states.setupattract.AppState_SetupAttract;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_IDs;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_Main;
	import com.bpi.gsoc.girlstablet.states.waitforactivation.AppState_WaitForActivation;
	import com.bpi.gsoc.girlstablet.states.waitforstart.AppState_WaitForStart;
	import com.greensock.plugins.ThrowPropsPlugin;
	import com.greensock.plugins.TweenPlugin;
	
	import flash.geom.ColorTransform;
	import flash.geom.Point;
	
	[SWF(width="2736", height="1824", backgroundColor="#FFFFFF", frameRate="30")]
	public class Document extends GSOCFramework_Client
	{
		private var m_NetworkMessageManager:NetworkMessageManager_GirlsTablet;
		
		private var m_SharedObject_Data_TabletIndex_Key:String = "TabletIndex";
		
		public function Document()
		{
			m_NetworkMessageManager = new NetworkMessageManager_GirlsTablet();

			GSOC_SFX.InitializeSounds();
			super();
		}
		
		protected override function InitializeVariables():void
		{
			super.InitializeVariables();
			
			_frameworkVariables.CMSStorageFolder = "C:/Workspace/GSOC/GSOC_CMS_ScoutTablet/";
			_frameworkVariables.ConfigXMLPath = "data/GSOC_GirlsTablet_Config.xml";
			_frameworkVariables.CitrusFrameworkSharedObjectName = "GSOC_GirlsTablet";
			_frameworkVariables.StageSize = new Point(2736, 1824);
		}
		
		protected override function InitializeEventListeners():void
		{
			super.InitializeEventListeners();
			
			m_NetworkMessageManager.InitializeEventListeners();
			
			AppState_NetworkConnection.NetworkConnectionEventDispatcher.addEventListener(Event_TabletColor.EVENT_SAVE_ACTIVE_TABLETCOLOR, eh_SaveActiveTabletColor);
		}
		
		protected override function Initialize():void
		{
			super.Initialize();
			Console.Function(setAgeGroup, "DB", [0]);
			Console.Function(setAgeGroup, "JC", [1]);
			Console.Function(setAgeGroup, "SA", [2]);
			Console.Function(moveToState, "MoveTo", []);
			
			TweenPlugin.activate([ColorTransform]);
			TweenPlugin.activate([ThrowPropsPlugin]);
			
			SessionInformation_GirlsTablet.IsRunningLocally = CitrusFramework.frameworkConfigXML.shouldRunLocally == "true";
			SessionInformation_GirlsTablet.VideoPath = CitrusFramework.frameworkConfigXML.videoPath;
			
			m_NetworkMessageManager.Initialize();
			this.setExperienceConfigs();
			initializeStateControllers();
		}
		
		private function initializeStateControllers():void
		{
			initializeScreenStates(StateController_IDs.STATECONTROLLER_MAIN, new AppState_CMS(), new AppState_Attract(), new AppState_NetworkConnection(), new AppState_WaitForStart(), new AppState_SetupAttract(), new AppState_SessionAttract(), new AppState_End(), new AppState_WaitForActivation());
			initializeScreenStates(StateController_IDs.STATECONTROLLER_ASSEMBLEYOURTEAM, new NameSelection_State(), new TakePicture_State(), new CustomizeCharacterTA_State(), new ChooseTeamName_State(), new Loading_State());
			initializeScreenStates(StateController_IDs.STATECONTROLLER_COMMUNITYRESOURCES, new CommunityResources_State());
			initializeScreenStates(StateController_IDs.STATECONTROLLER_BRAINSTORM, new ProjectGoals_State(), new BrainStorm_State());
			initializeScreenStates(StateController_IDs.STATECONTROLLER_PROJECTPLANNING, new ProjectPlanning_State());
			initializeScreenStates(StateController_IDs.STATECONTROLLER_CHOOSEISSUE, new ChooseIssue_State());
			initializeScreenStates(StateController_IDs.STATECONTROLLER_MINDMAP, new MindMap_State());
			initializeScreenStates(StateController_IDs.STATECONTROLLER_PICTUREYOURSELF, new NameSelection_State(), new TakePicture_State(), new CustomizeCharacter_State(), new ChooseTeamName_State(), new Loading_State());
			initializeScreenStates(StateController_IDs.STATECONTROLLER_EXPLOREYOURPASSION, new MultipleQuestions_State(), new QuestionsDone_State());
			initializeScreenStates(StateController_IDs.STATECONTROLLER_CHOOSEYOURCAREER, new CareerChoice_State(), new CareerDone_State());
			initializeScreenStates(StateController_IDs.STATECONTROLLER_KNOWYOURROLE, new ChoiceWeb_State());
			initializeScreenStates(StateController_IDs.STATECONTROLLER_RECORDVIDEO, new AssignRoles_State(), new WriteScript_State(), new WarmUp_State(), new RecordVideo_State(), new ReviewVideo_State(), new EditVideo_State(), new PlayBackVideo_State());
			initializeScreenStates(StateController_IDs.STATECONTROLLER_POSTER, new Poster_State());
			
			moveToDefaultState();
		}
		
		private function eh_SaveActiveTabletColor(evt:Event_TabletColor):void
		{
			if(frameworkSharedObject)
			{
				frameworkSharedObject.data[m_SharedObject_Data_TabletIndex_Key] = evt.E_Index;
				frameworkSharedObject.flush();
			}
			SessionInformation_GirlsTablet.TabletIndex = evt.E_Index;
		}
		
		protected override function Start():void
		{
			checkTabletColor();
			CameraNotifications.CheckCameras();
			
			super.Start();
		}
		
		protected override function Update():void
		{
			super.Update();
			
			checkClientConnection();
		}
		
		private function checkTabletColor():void
		{
			var tabletIndex:int = 0;
			if(frameworkSharedObject && frameworkSharedObject.data.hasOwnProperty(m_SharedObject_Data_TabletIndex_Key))
			{
				tabletIndex = frameworkSharedObject.data[m_SharedObject_Data_TabletIndex_Key];
			}
			AppState_NetworkConnection.NetworkConnectionEventDispatcher.dispatchEvent(new Event_TabletColor(Event_TabletColor.EVENT_SET_ACTIVE_TABLETCOLOR, tabletIndex));
		}
		
		private function checkClientConnection():void
		{
			if(!SessionInformation_GirlsTablet.IsRunningLocally)
			{
				var isCMSActive:Boolean = StateControllerManager.CheckIfActiveAppState(StateController_IDs.STATECONTROLLER_MAIN, StateController_StateIDs_Main.STATE_CMS);
				var isOnAttract:Boolean = StateControllerManager.CheckIfActiveAppState(StateController_IDs.STATECONTROLLER_MAIN, StateController_StateIDs_Main.STATE_ATTRACT);
				if(!isCMSActive && !isOnAttract)
				{
					var isOnNetworkConnection:Boolean = StateControllerManager.GetActiveAppStateID(StateController_IDs.STATECONTROLLER_MAIN) == StateController_StateIDs_Main.STATE_NETWORKCONNECTION;
					if(!Client.IsConnected && !isOnNetworkConnection)
					{
						StateControllerManager.LeaveAllActiveStates();
						StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_MAIN, StateController_StateIDs_Main.STATE_ATTRACT);
					}
				}
			}	
		}
		
		private function initializeScreenStates(stateID:int, ... appStates):void
		{
			StateControllerManager.CreateStateController(stateID);
			for(var i:int = 0; i < appStates.length; i++)
			{
				StateControllerManager.AddState(stateID, appStates[i]);
			}
		}
		
		private function moveToDefaultState():void
		{
			var isRunningLocally:Boolean = SessionInformation_GirlsTablet.IsRunningLocally;
			var stateControllerID:int = isRunningLocally ? TempStateConfig.State : StateController_IDs.STATECONTROLLER_MAIN;
			var stateID:int = isRunningLocally ? TempStateConfig.Screen : StateController_StateIDs_Main.STATE_CMS;
			StateControllerManager.MoveToState(stateControllerID, stateID);
		}
		
		private function setAgeGroup(value:int):void
		{
			SessionInformation_GirlsTablet.Age = value;
			StateControllerManager.LeaveAllActiveStates();
			moveToDefaultState();
		}
		
		private function moveToState(sc:int, s:int):void
		{
			StateControllerManager.LeaveAllActiveStates();
			StateControllerManager.MoveToState(sc, s);
		}
		
		private function setExperienceConfigs():void
		{
			if([AgeConstants.KEY_DAISYBROWNIE, AgeConstants.KEY_JUNIORCADETTE, AgeConstants.KEY_SENIORAMBASSADOR].indexOf(int(CitrusFramework.frameworkConfigXML.experienceConfigs.age)) != -1)
			{
				SessionInformation_GirlsTablet.Age = int(CitrusFramework.frameworkConfigXML.experienceConfigs.age);
			}
			else
			{
				throw new Error("You have supplied an invalid value in the Age field in the config XML. Please supply one of: 0, 1, 2.");
			}
			
			if([JourneyConstants.JOURNEY_PLANET, JourneyConstants.JOURNEY_STORY, JourneyConstants.JOURNEY_WORLD].indexOf(int(CitrusFramework.frameworkConfigXML.experienceConfigs.journey)) != -1)
			{
				SessionInformation_GirlsTablet.Journey = int(CitrusFramework.frameworkConfigXML.experienceConfigs.journey);
			}
			else
			{
				throw new Error("You have supplied an invalid value in the Journey field in the config XML. Please supply one of: 0, 1, 2.");
			}
			
			if([TrackConstants.CAREER, TrackConstants.TAKE_ACTION].indexOf(int(CitrusFramework.frameworkConfigXML.experienceConfigs.track)) != -1)
			{
				SessionInformation_GirlsTablet.Track = int(CitrusFramework.frameworkConfigXML.experienceConfigs.track);
			}
			else
			{
				throw new Error("You have supplied an invalid value in the Age field in the config XML. Please supply one of: 0, 1.");
			}	
		}
		
		public static function TellServerActivityComplete():void
		{
			Client.ClientEventDispatcher.dispatchEvent(new Event_ClientNetworkMessage(Event_ClientNetworkMessage.EVENT_SEND_NETWORK_MESSAGE, NetworkMessage_Types.TELL_SERVER_ACTIVITY_COMPLETE));
		}
	}
}